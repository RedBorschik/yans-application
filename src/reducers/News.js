const merge = require('deepmerge');
import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    items: {},
    count_pages: 1,
    page: 1,
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.NEWS_SAVE:
            return {
                ...state,
                items: payload && Array.isArray(payload.items) ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : state.items,
                count_pages: payload.count_page,
                page: Number(state.page) + 1,
            };
            break;
        case ActionTypes.NEWS_SAVE_DETAIL:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            ...payload
                        } : payload,
                    }
                },
            };
            break;
        case ActionTypes.ORDERS_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.ORDERS_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_NEWS:
            return initialState;
            break;
        default:
            return state;
    }
}

export const getNewsById = (state, id) => state.news.items[id];
export const getNewsList = (state) => Object.keys(state.news.items).map((k) => state.news.items[k]);