import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: false,
    loading: false,
    loading_action: false,
    message: '',
    profile: {
        name: '',
        email: '',
        phone: '',
        another_phone: '',
        password: '',
        confirm_password: '',
    },
    language: 'ru',
    push: {
        order: false,
        items: false,
        news: false,
        chat: false,
    },
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_PROFILE:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    ...payload
                },
            };
            break;
        case ActionTypes.ACCOUNT_PUSH_GET:
            return {
                ...state,
                push: {
                    order: payload.info_orders,
                    items: payload.info_items,
                    news: payload.info_news_and_actions,
                    chat: payload.info_message_chat,
                },
            };
            break;
        case ActionTypes.ACCOUNT_PUSH_SAVE:
            return {
                ...state,
                push: {
                    order: payload.order,
                    items: payload.items,
                    news: payload.news,
                    chat: payload.chat,
                },
            };
            break;
        case ActionTypes.ACCOUNT_HAS_ERROR:
            console.log('action',action);
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.ACCOUNT_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.ACCOUNT_ACTION_IS_LOADING:
            return {
                ...state,
                loading_action: action.loading,
            };
            break;
        case ActionTypes.CLEAR_ACCOUNT:
            return initialState;
            break;
        default:
            return state;
    }
}
