const merge = require('deepmerge');
import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    loading_actions: false,
    message: '',
    items: {},
    delivery_list: [],
    pay_system_list: [],
    count_pages: 1,
    page: 1,
    new: {},
    last_order: null
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.NEW_ORDER:
            return {
                ...state,
                new: {
                    ...state.new,
                    ...payload
                },
                last_order: null,
            };
            break;
        case ActionTypes.SAVE_ORDERS_DELIVERY_LIST:
            return {
                ...state,
                delivery_list: payload
            };
            break;
        case ActionTypes.SAVE_ORDERS_PAY_SYSTEM_LIST:
            return {
                ...state,
                pay_system_list: payload
            };
            break;
        case ActionTypes.SAVE_ORDERS:
            return {
                ...state,
                items: payload && Array.isArray(payload.items) ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : state.items,
                count_pages: payload.count_page,
                page: Number(state.page) + 1,
                last_order: null,
                new: {}
            };
            break;
        case ActionTypes.SAVE_ORDER:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            detail: payload,
                        } : {detail: payload},
                    }
                },
                new: {},
            };
            break;
        case ActionTypes.SAVE_ORDER_PARAMS:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            params: payload.data,
                        } : {id: payload.id, params: payload.data},
                    }
                },
                last_order: null,
            };
            break;
        case ActionTypes.UPDATE_ORDER:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? merge(state.items[payload.id], payload) : payload,
                    }
                },
                last_order: null,
                new: {},
            };
            break;
        case ActionTypes.ADD_ORDER:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: payload,
                    }
                },
                last_order: payload,
                new: {}
            };
            break;
        case ActionTypes.ORDERS_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.ORDERS_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.ORDERS_ACTIONS_IS_LOADING:
            return {
                ...state,
                loading_actions: action.loading_actions,
            };
            break;
        case ActionTypes.RESET_ORDERS:
            return {
                ...state,
                items: {},
                count_pages: 1,
                page: 1,
                new: {},
                last_order: null,
            };
            break;
        case ActionTypes.CLEAR_ORDERS:
            return initialState;
            break;
        default:
            return state;
    }
}

export const getOrderById = (state, id) => state.orders.items[id];
export const getOrdersList = (state) => Object.keys(state.orders.items).map((k) => state.orders.items[k]);