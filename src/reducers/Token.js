import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    device_token: null,
    hash: null,
    error: null,
    message: '',
    loading: false
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_HASH:
            return {
                ...state,
                hash: action.hash,
            };
            break;
        case ActionTypes.REMOVE_HASH:
            return {
                ...state,
                hash: null,
            };
            break;
        case ActionTypes.SAVE_DEVICE_TOKEN:
            return {
                ...state,
                device_token: action.device_token,
            };
            break;
        case ActionTypes.REMOVE_DEVICE_TOKEN:
            return {
                ...state,
                device_token: null,
            };
            break;
        case ActionTypes.TOKEN_HAS_ERROR:
            return {
                ...state,
                error: action.error,
            };
            break;
        case ActionTypes.TOKEN_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_TOKEN:
            return initialState;
            break;
        default:
            return state;
    }
}

export const getHash = (state) => state.token.hash;
export const isLoading = (state) => state.token.loading;