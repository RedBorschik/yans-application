import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    types: []
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_LEGALTYPES:
            return {
                ...state,
                types: payload,
            };
            break;
        case ActionTypes.LEGALTYPES_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.LEGALTYPES_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        default:
            return state;
    }
}