import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    email: '',
    password: '',
    confirm_password: '',
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.REGISTER:
            return {
                ...state,
                email: payload.email,
                password: payload.password,
                confirm_password: payload.confirm_password,
            };
            break;
        case ActionTypes.REGISTER_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.REGISTER_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_REGISTER:
            return initialState;
            break;
        default:
            return state;
    }
}