import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    data: {},
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.PASSWORD_SAVE_DATA:
            return {
                ...state,
                data: {
                    ...state.data,
                    ...payload
                }
            };
            break;
        case ActionTypes.PASSWORD_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.PASSWORD_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_PASSWORD:
            return initialState;
            break;
        default:
            return state;
    }
}