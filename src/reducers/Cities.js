import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    items: [],
    city: '0000000363'
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_CITIES:
            return {
                ...state,
                items: payload,
            };
            break;
        case ActionTypes.CITIES_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.CITIES_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.SET_CITY:
            return {
                ...state,
                city: payload
            };
            break;
        default:
            return state;
    }
}

export const getCityByCode = (state, code) => {
    let filtered = state.cities.items.filter(item => {
        return item.code === code;
    });
    if (filtered && filtered[0]) {
        return filtered[0];
    } else {
        return null;
    }
};