import {combineReducers} from 'redux';

import Token from './Token';
import Login from './Login';
import Register from './Register';
import Password from './Password';
import Account from './Account';
import Navigator from './Navigation'
import Device from './Device';
import Catalog from './Catalog';
import Banners from './Banners';
import Address from './Address';
import Cities from './Cities';
import Contragents from './Contragents';
import LegalTypes from './LegalTypes';
import Basket from './Basket';
import Orders from './Orders';
import Search from './Search';
import Notifications from './Notifications';
import News from './News';

export default combineReducers({
    token: Token,
    login: Login,
    navigator: Navigator,
    device: Device,
    register: Register,
    account: Account,
    catalog: Catalog,
    banners: Banners,
    address: Address,
    cities: Cities,
    contragents: Contragents,
    legaltypes: LegalTypes,
    basket: Basket,
    orders: Orders,
    search: Search,
    password: Password,
    notify: Notifications,
    news: News
});
