import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    loading_detail: false,
    message: '',
    items: {},
    new_item: null,
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_ADDRESS:
            return {
                ...state,
                items: payload && Array.isArray(payload) ? payload.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : state.items,
            };
            break;
        case ActionTypes.SAVE_ITEM_ADDRESS:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            ...payload
                        } : payload,
                    }
                },
                new_item: payload.id
            };
            break;
        case ActionTypes.REMOVE_ITEM_ADDRESS:
            return {
                ...state,
                items: Object.keys(state.items).filter(key => Number(state.items[key].id) !== Number(payload)).reduce((obj, key) => {obj[key] = state.items[key]; return obj}, {}),
            };
            break;
        case ActionTypes.ADDRESS_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.ADDRESS_DETAIL_IS_LOADING:
            return {
                ...state,
                loading_detail: action.loading,
            };
            break;
        case ActionTypes.ADDRESS_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_ADDRESS:
            return initialState;
            break;
        default:
            return state;
    }
}

export const getAddressById = (state, id) => state.address.items[id];
export const getAddressList = (state) => Object.keys(state.address.items).map((k) => state.address.items[k]);