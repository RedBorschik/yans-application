import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    banners: [],
    error: null,
    message: '',
    loading: false
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_BANNERS:
            return {
                ...state,
                banners: payload.items,
            };
            break;
        case ActionTypes.BANNERS_HAS_ERROR:
            return {
                ...state,
                error: action.error,
            };
            break;
        case ActionTypes.BANNERS_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        default:
            return state;
    }
}

export const getBanners = (state) => state.banners.banners;
export const isLoading = (state) => state.banners.loading;