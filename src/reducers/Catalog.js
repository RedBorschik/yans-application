const merge = require('deepmerge');
import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    search_error: false,
    loading: {
        card: false,
        items: false,
        new_items: false,
        categories: false,
        favorites: false,
        sort: false,
        filter: false,
        search: false,
    },
    message: '',
    categories: [],
    items: {},
    sort: [],
    filter: [],
    pagination: {
        items: {
            ids: [],
            count_pages: 1,
            page: 1,
        },
        new_items: {
            ids: [],
            count_pages: 1,
            page: 1,
        },
        favorites: {
            ids: [],
            count_pages: 1,
            page: 1,
        },
        search: {
            ids: [],
            count_elements: 0,
            count_pages: 1,
            page: 1,
        },
    },
};

export default (state = initialState, action) => {
    const {type, payload} = action;
    //console.log(type);

    switch (type) {
        case ActionTypes.SAVE_CATALOG_SORT:
            return {
                ...state,
                sort: payload,
            };
            break;
        case ActionTypes.SAVE_CATALOG_FILTER:
            return {
                ...state,
                filter: payload,
            };
            break;
        case ActionTypes.SAVE_CATALOG_CATEGORIES:
            return {
                ...state,
                categories: payload,
            };
            break;
        case ActionTypes.ADD_CATALOG_FAVORITES_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload]: {
                            ...state.items[payload],
                            in_wish_list: true,
                        },
                    }
                },
                pagination: {
                    ...state.pagination,
                    favorites: {
                        ...state.pagination.favorites,
                        ids: [
                            ...state.pagination.favorites.ids,
                            payload,
                        ].filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                        }),
                    },
                },
            };
            break;
        case ActionTypes.REMOVE_CATALOG_FAVORITES_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload]: {
                            ...state.items[payload],
                            in_wish_list: false,
                        },
                    }
                },
                pagination: {
                    ...state.pagination,
                    favorites: {
                        ...state.pagination.favorites,
                        ids: state.pagination.favorites.ids.filter((item) => {if (item !== payload) return payload;}),
                    },
                },
            };
            break;
        case ActionTypes.SAVE_CATALOG_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            ...payload
                        } : payload,
                    }
                },
            };
        case ActionTypes.SAVE_CATALOG_ITEMS:
            return {
                ...state,
                items: merge(state.items, payload && payload.items ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : {}),
                pagination: {
                    ...state.pagination,
                    items: {
                        ...state.pagination.items,
                        count_pages: payload.count_pages,
                        page: Number(state.pagination.items.page) + 1,
                        ids: [
                            ...state.pagination.items.ids,
                            ...payload.items.map(item => item.id)
                        ].filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                        }),
                    },
                },
            };
        case ActionTypes.SAVE_CATALOG_NEW_ITEMS:
            return {
                ...state,
                items: merge(state.items, payload && payload.items ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : {}),
                pagination: {
                    ...state.pagination,
                    new_items: {
                        ...state.new_items,
                        count_pages: payload.count_pages,
                        page: Number(state.pagination.new_items.page) + 1,
                        ids: [
                            ...state.pagination.new_items.ids,
                            ...payload.items.map(item => item.id)
                        ].filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                        }),
                    },
                },
            };
            break;
        case ActionTypes.SAVE_CATALOG_FAVORITES_ITEMS:
            return {
                ...state,
                items: merge(state.items, payload && payload.items ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : {}),
                pagination: {
                    ...state.pagination,
                    favorites: {
                        ...state.favorites,
                        count_pages: payload.count_pages,
                        page: Number(state.pagination.favorites.page) + 1,
                        ids: [
                            ...state.pagination.favorites.ids,
                            ...payload.items.map(item => item.id)
                        ].filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                        }),
                    },
                },
            };
            break;
        case ActionTypes.SAVE_CATALOG_SEARCH_ITEMS:
            return {
                ...state,
                items: merge(state.items, payload && payload.items ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : {}),
                pagination: {
                    ...state.pagination,
                    search: {
                        ...state.search,
                        count_pages: payload.count_pages,
                        count_elements: payload.count_elements,
                        page: Number(state.pagination.search.page) + 1,
                        ids: [
                            ...state.pagination.search.ids,
                            ...payload.items.map(item => item.id)
                        ].filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                        }),
                    },
                },
            };
            break;
        case ActionTypes.RESET_CATALOG_ITEMS:
            return {
                ...state,
                //items: [],
                pagination: {
                    ...state.pagination,
                    items: {
                        ...state.items,
                        count_pages: 1,
                        page: 1,
                        ids: [],
                        count_elements: 0,
                    },
                },
            };
            break;
        case ActionTypes.RESET_SORT:
            return {
                ...state,
                sort: [],
            };
            break;
        case ActionTypes.RESET_FILTER:
            return {
                ...state,
                filter: [],
            };
            break;
        case ActionTypes.RESET_CATALOG_SEARCH_ITEMS:
            return {
                ...state,
                //items: [],
                pagination: {
                    ...state.pagination,
                    search: {
                        ...state.search,
                        count_pages: 1,
                        page: 1,
                        ids: [],
                    },
                },
            };
            break;
        case ActionTypes.RESET_CATALOG_NEW_ITEMS:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    new_items: {
                        ...state.new_items,
                        count_pages: 1,
                        page: 1,
                        ids: [],
                    },
                },
            };
            break;
        case ActionTypes.CATALOG_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.CATALOG_SEARCH_HAS_ERROR:
            return {
                ...state,
                search_error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.CATALOG_IS_LOADING:
            let bool = action.loading;
            return {
                ...state,
                loading: {
                    ...state.loading,
                    ...{
                        [action.data]: action.loading,
                    }
                },
            };
            break;
        case ActionTypes.CLEAR_CATALOG:
            return initialState;
            break;
        default:
            return state;
    }
}

export const isLoading = (state) => state.catalog.loading;
export const getPageCount = (state, type) => state.catalog.pagination[type] ? state.catalog.pagination[type].count_pages : null;
export const getPage = (state, type) => state.catalog.pagination[type] ? state.catalog.pagination[type].page : null;

export const getItemsList = (state, type) => {
    return state.catalog.pagination[type] ? (state.catalog.pagination[type].ids).map(id => state.catalog.items[id] || {}) : [];
};

export const getItemById = (state, id) => state.catalog.items[id];