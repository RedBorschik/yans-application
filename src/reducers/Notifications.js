import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    message: '',
    loading: false,
    token: null,
    first_enter: false,
    channel_name: ''
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_FCM_KEY:
            return {
                ...state,
                token: action.token,
            };
            break;
        case ActionTypes.SAVE_CHANNEL_NAME:
            return {
                ...state,
                channel_name: action.channel_name,
            };
            break;
        case ActionTypes.SAVE_FIRST_ENTER:
            return {
                ...state,
                first_enter: action.first_enter,
            };
            break;
        case ActionTypes.NOTIFY_HAS_ERROR:
            return {
                ...state,
                error: action.error,
            };
            break;
        case ActionTypes.NOTIFY_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_NOTIFICATIONS:
            return initialState;
            break;
        default:
            return state;
    }
}
