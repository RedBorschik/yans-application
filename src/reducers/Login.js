import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    loading: false,
    message: '',
    name: '',
    phone: '',
    email: '',
    password: '',
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.LOGIN:
            return {
                ...state,
                name: payload.name,
                email: payload.email,
                phone: payload.phone,
                password: payload.password,
            };
            break;
        case ActionTypes.LOGOUT:
            return {
                ...state,
                name: '',
                email: '',
                phone: '',
                password: '',
            };
            break;
        case ActionTypes.LOGIN_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.LOGIN_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.CLEAR_LOGIN:
            return initialState;
            break;
        default:
            return state;
    }
}