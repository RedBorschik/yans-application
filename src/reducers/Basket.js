import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    error: null,
    error_add: null,
    loading: false,
    loading_add: false,
    message: '',
    items: {},
    sum: 0,
    show_modal: false,
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_BASKET:
            return {
                ...state,
                items: payload && Array.isArray(payload.items) ? payload.items.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {}) : {},
                sum: payload.sum,
            };
            break;
        case ActionTypes.ADD_BASKET_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            current: payload.current
                        } : payload,
                    }
                },
                sum: payload.sum,
            };
            break;
        case ActionTypes.UPDATE_BASKET_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...{
                        [payload.id]: state.items[payload.id] ? {
                            ...state.items[payload.id],
                            in_wish_list: payload.in_wish_list
                        } : payload,
                    }
                },
            };
            break;
        case ActionTypes.REMOVE_BASKET_ITEM:
            return {
                ...state,
                items: Object.keys(state.items).filter(key => payload.ids.indexOf(state.items[key].id) < 0).reduce((obj, key) => {
                    obj[key] = state.items[key];
                    return obj;
                }, {}),
                sum: payload.sum,
            };
            break;
        case ActionTypes.BASKET_HAS_ERROR:
            return {
                ...state,
                error: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.BASKET_ADD_HAS_ERROR:
            return {
                ...state,
                error_add: action.error,
                message: action.message,
            };
            break;
        case ActionTypes.BASKET_IS_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
            break;
        case ActionTypes.BASKET_IS_ADD_LOADING:
            return {
                ...state,
                loading_add: action.loading,
            };
            break;
        case ActionTypes.VIEW_ADD_BASKET_MODAL:
            return {
                ...state,
                show_modal: payload
            };
            break;
        case ActionTypes.RESET_BASKET:
            return {
                ...state,
                items: {},
                sum: 0,
            };
            break;
        case ActionTypes.CLEAR_BASKET:
            return initialState;
            break;
        default:
            return state;
    }
}

export const getBasketList = (state) => Object.keys(state.basket.items).map((k) => state.basket.items[k]);