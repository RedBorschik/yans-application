import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    device: {
        id: '',
        type: '',
        model: '',
        mark: '',
        version: '',
    },
    connected: null
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SET_DEVICE:
            return {
                ...state,
                device: {
                    id: payload.id,
                    type: payload.type,
                    model: payload.model,
                    mark: payload.mark,
                    version: payload.version,
                }
            };
            break;
        case ActionTypes.SET_CONNECTION:
            return {
                ...state,
                connected: payload
            };
            break;
        default:
            return state;
    }
}