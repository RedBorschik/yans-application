import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
    items: []
};

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ActionTypes.SAVE_SEARCH_DATA:
            let found = state.items.some(function (el) {
                return el === payload;
            });
            if (found) {
                return state;
            } else {
                return {
                    ...state,
                    items: [
                        payload,
                        ...state.items.slice(0, 16)
                    ],
                };
            }
            break;
        case ActionTypes.CLEAR_SEARCH:
            return initialState;
            break;
        default:
            return state;
    }
}
