import * as React from 'react';
import {connect} from 'react-redux';
import {Platform} from 'react-native';
import Tab from './Tab';
import styled from 'styled-components/native';
import {colors} from '../../utils/theme';
import {messages} from '../../Messages';
import {getBasketList} from '../../reducers/Basket';

const Footer = styled.View`
    height: ${Platform.OS === 'android' ? 56 : 49};
    backgroundColor: ${colors.WHITE};
    flexDirection: row;
    justifyContent: space-around;
    alignItems: stretch;
    elevation: 6;
`;

const MainTabBar = (props) => {
    const {navigation, basketCount} = props;
    const {routes, index: activeRouteIndex} = navigation.state;

    return (
        <Footer>
            {routes.map((route, index) => {
                if (route.key !== 'Home' && route.key !== 'Order') return (
                    <Tab
                        key={index}
                        title={messages.bottomMenu[route.routeName]}
                        active={activeRouteIndex === index}
                        route_name={route.routeName}
                        badge={(route.key === 'Cart' && basketCount > 0) ? basketCount : null}
                        onPress={() => navigation.navigate((route.routes && route.routes[0] ? route.routes[0].routeName : route.routeName))}
                    />
                );
            })}
        </Footer>
    );
};

const mapStateToProps = (state) => ({
    basketCount: Object.keys(getBasketList(state)).length,
});

export default connect(mapStateToProps, {})(MainTabBar);
