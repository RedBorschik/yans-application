import * as React from 'react';
import {Badge} from 'react-native-elements';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import {colors} from '../../utils/theme';
import {Account, Favorites, Catalog, Chat, Cart} from '../icons/menu/';

const TabWrap = styled.TouchableOpacity`
    width: 20%;
`;

const TabButton = styled.View`
    height: ${Platform.OS === 'android' ? 56 : 49};
    flex: 1;
    alignItems: center;
    justifyContent: center;
    flexDirection: column;
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 6 : 2};
    paddingBottom: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 7 : 3};
`;

const TabText = styled.Text`
    color: ${colors.GREY_3};
    fontSize: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360  ? 12 : 10};
    lineHeight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 15 : 13};
    fontFamily: CoreRhino45Regular;
`;

const TabActiveText = styled.Text`
    color: ${colors.GREEN_2};
    fontSize: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 12 : 10};
    lineHeight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 15 : 13};
    fontFamily: CoreRhino45Regular;
`;

const IconWrap = styled.View`
    height: 28;
    width: 28;
    alignItems: center;
    justifyContent: center;
`;

const Tab = ({title, active, onPress, route_name, badge}) => {
    let Icon = '';
    switch(route_name){
        case 'Account':
            Icon = <Account active={active}/>;
            break;
        case 'Favorites':
            Icon = <Favorites active={active}/>;
            break;
        case 'Catalog':
            Icon = <Catalog active={active}/>;
            break;
        case 'Chat':
            Icon = <Chat active={active}/>;
            break;
        case 'Cart':
            Icon = <Cart active={active}/>;
            break;
    }
    return (
        <TabWrap onPress={onPress}>
            <TabButton>
                <IconWrap>
                    {Icon}
                    {badge ? <Badge
                        status="success"
                        value={badge}
                        containerStyle={{ position: 'absolute', top: -1, right: -1 }}
                    /> : null}
                </IconWrap>
                {active ? <TabActiveText>{title}</TabActiveText> : <TabText>{title}</TabText>}
            </TabButton>
        </TabWrap>
    );
};

export default Tab;