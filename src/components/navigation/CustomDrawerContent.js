import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Linking, TouchableOpacity, Platform} from 'react-native';
import NavigationService from '../../NavigationService';
import LoginActions from './../../actions/Login';

import Logo from '../icons/Logo';
import DrawerHeader from '../decor/DrawerHeader';
import CityView from '../city/CityView';

import styled from 'styled-components';
import {colors} from '../../utils/theme';

const ContainerView = styled.ScrollView`
    flex: 1;
    elevation: 4;
`;

const Wrapper = styled.View`
    flex: 1;
    justifyContent: space-between;
`;

const DrawerContainer = styled.View``;

const ItemContainer = styled.View`
`;

const Item = styled.TouchableOpacity`
    paddingTop: 13px;
    paddingBottom: 13px;
    paddingLeft: ${Platform.OS === 'android' ? 16 : 8};
    borderBottomColor: rgba(0,0,0,0.04);
    borderStyle: solid; 
    borderBottomWidth: 1px 
`;

const ItemText = styled.Text`
    fontSize: 15px; 
    color: ${colors.TEXT};
    fontFamily: CoreRhino45Regular;
`;

const PhonesContainer = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: ${Platform.OS === 'android' ? 'flex-start' : 'center'};
    alignItems: ${Platform.OS === 'android' ? 'flex-start' : 'center'};
    paddingTop: ${Platform.OS === 'android' ? 24 : 11};
    paddingBottom: ${Platform.OS === 'android' ? 24 : 12};
    paddingHorizontal: 16;
`;

const Phone = styled.Text`
    fontSize: 14px; 
    color: ${colors.BLUE_8};
    fontFamily: CoreRhino45Regular;
`;

class DrawerScreen extends Component {
    render() {
        const {navigation} = this.props;
        const {routes, index: activeRouteIndex} = navigation.state;
        let index = 0;
        let mainStack = navigation.state.routes.find(obj => obj.routeName === 'MainStack');
        if (mainStack) {
            let Tabs = mainStack.routes.find(obj => obj.routeName === 'Tabs');
            if (Tabs) {
                let Home = Tabs.routes.find(obj => obj.routeName === 'Home');
                if (Home) {
                    index = Number(Home.index);
                }
            }
        }
        //console.log(this.props.navigator)
        return (
            <ContainerView>
                <Wrapper>
                    <DrawerHeader/>
                    <Logo style={{marginTop: Platform.OS === 'android' ? 45 : 16, marginBottom: Platform.OS === 'android' ? 0 : 16}}/>
                    <CityView/>
                    <DrawerContainer>
                        <ItemContainer>
                            <Item onPress={() => {NavigationService.navigate('Main');}}><ItemText>Главная</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('About');}}><ItemText>О компании</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('News');}}><ItemText>Новости</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('Faq');}}><ItemText>Вопросы и ответы</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('Payment');}}><ItemText>Оплата и доставка</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('Return');}}><ItemText>Политика возврата</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('Suppliers');}}><ItemText>Поставщикам</ItemText></Item>
                            <Item onPress={() => {NavigationService.navigate('Contacts');}}><ItemText>Контакты</ItemText></Item>
                        </ItemContainer>
                    </DrawerContainer>
                    {/*<ButtonContainer>
                    <Button title="Logout" onPress={() => this.props.logoutUser()}/>
                </ButtonContainer>*/}
                    <PhonesContainer>
                        <TouchableOpacity onPress={() => {Linking.openURL(`tel:${'+7 (701) 073 37 33'}`);}}><Phone>+7 (701) 073 37 33</Phone></TouchableOpacity>
                        <TouchableOpacity onPress={() => {Linking.openURL(`tel:${'+7 (717) 278 37 33'}`);}}><Phone>+7 (717) 278 37 33</Phone></TouchableOpacity>
                        <TouchableOpacity onPress={() => {Linking.openURL(`tel:${'+7 (727) 317 77 33'}`);}}><Phone>+7 (727) 317 77 33</Phone></TouchableOpacity>
                    </PhonesContainer>
                </Wrapper>
            </ContainerView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    logoutUser: () => dispatch(LoginActions.logoutUser()),
});

const mapStateToProps = (state) => ({
    navigator: state.navigator,
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerScreen);
