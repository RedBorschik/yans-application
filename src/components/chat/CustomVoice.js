import React, {Component} from 'react';
import {
    PermissionsAndroid,
    Platform,
    Dimensions,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    ActivityIndicator
} from 'react-native';
import AudioRecorderPlayer, {
    AVEncoderAudioQualityIOSType,
    AVEncodingOption,
    AudioEncoderAndroidType,
    AudioSet,
    AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';

import Modal from 'react-native-modal';
import Pulse from 'react-native-pulse';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome';
import ActionButton from './ActionButton';

const getColor = () => {

};

export default class CustomVoice extends Component {
    audioRecorderPlayer: AudioRecorderPlayer;

    constructor(props) {
        super(props);

        this.state = {
            recordSecs: 0,
            recordTime: '00:00:00',
            currentPositionSec: 0,
            currentDurationSec: 0,
            playTime: '00:00:00',
            duration: '00:00:00',

            isRecording: false,
            isRecorded: false,
            isPlaying: false,
            isStopped: false
        };

        this.audioRecorderPlayer = new AudioRecorderPlayer();
        this.onAudioPress = this.props.onAudioPress;
    }

    componentDidMount() {
        const msg = this.onStartRecord();
        console.log(msg);
    }

    componentWillUnmount() {
        if (!this.state.isStopped) {
            const msg = this.onStopRecord();
            console.log(msg);
        }
    }

    onStartRecord = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the storage');
                } else {
                    console.log('permission denied');
                    return;
                }
            } catch (err) {
                console.warn(err);
                return;
            }
        }
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the camera');
                } else {
                    console.log('permission denied');
                    return;
                }
            } catch (err) {
                console.warn(err);
                return;
            }
        }

        console.log('НАЖАТА КНОПКА ЗАПИСИ');

        const path = Platform.select({
            ios: 'testFile.m4a',
            android: 'sdcard/testFile.m4a',
        });
        const audioSet: AudioSet = {
            AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
            AudioSourceAndroid: AudioSourceAndroidType.MIC,
            AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
            AVNumberOfChannelsKeyIOS: 2,
            AVFormatIDKeyIOS: AVEncodingOption.aac,
        };

        const result = await this.audioRecorderPlayer.startRecorder(path, audioSet);

        this.audioRecorderPlayer.addRecordBackListener((e: any) => {
            this.setState({
                recordSecs: e.current_position,
                recordTime: this.audioRecorderPlayer.mmssss(
                    Math.floor(e.current_position),
                ),
                isRecording: true,
                isRecorded: false
            });
        });

        console.log('RESULT');
        console.log(result);
    };

    onStopRecord = async () => {
        const result = await this.audioRecorderPlayer.stopRecorder();
        this.audioRecorderPlayer.removeRecordBackListener();
        this.setState({
            recordSecs: 0,
            isRecording: false,
            isRecorded: true,
            isStopped: true
        });
        console.log(result);
        console.log('ЗАПИСТЬ ОСТАНОВЛЕНА');
        this.onAudioPress();
    };

    // onStartPlay = async () => {
    //     console.log('onStartPlay');
    //
    //     const path = Platform.select({
    //         ios: 'chat_message.m4a',
    //         android: 'sdcard/chat_message.mp4',
    //     });
    //
    //     const msg = await this.audioRecorderPlayer.startPlayer(path);
    //     this.audioRecorderPlayer.setVolume(1.0);
    //     console.log(msg);
    //     this.audioRecorderPlayer.addPlayBackListener((e: any) => {
    //         if (e.current_position === e.duration) {
    //             console.log('finished');
    //             this.audioRecorderPlayer.stopPlayer();
    //         }
    //         this.setState({
    //             currentPositionSec: e.current_position,
    //             currentDurationSec: e.duration,
    //             playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
    //             duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
    //             isPlaying: e.current_position === e.duration ? false : true
    //         });
    //     });
    // };

    // onStopPlay = async () => {
    //     console.log('onStopPlay');
    //     this.audioRecorderPlayer.stopPlayer();
    //     this.audioRecorderPlayer.removePlayBackListener();
    //     this.setState({
    //         isPlaying: false,
    //     });
    // };

    render() {
        const {onClosePress, isFileLoading} = this.props;
        const {recordTime} = this.state;

        return (
            <Modal isVisible={true}
                   style={styles.modal}
                   backdropOpacity={0.4}
                   swipeDirection="down"
                   onSwipe={onClosePress}
                   onBackdropPress={onClosePress}
                   onBackButtonPress={onClosePress}>

                {isFileLoading &&
                <View style={styles.container}>
                    <Text style={{fontSize: 24, marginBottom: 10, marginTop: -10}}>Загрузка</Text>
                    <ActivityIndicator size="large" color="#79C780"/>
                </View>
                }

                {!isFileLoading &&
                <View style={styles.container}>
                    <TouchableOpacity onPress={this.onStopRecord}>
                        <Pulse color='#52A0ED'
                               numPulses={3}
                               diameter={100}
                               speed={20}
                               duration={1000}
                               style={{marginLeft: 0, marginTop: -30}}/>
                        <View style={[styles.button, {backgroundColor: '#52A0ED'}]}>
                            <Icon name={"microphone"}
                                  size={28}
                                  color="#fff"
                            />
                        </View>
                    </TouchableOpacity>

                    < Text style={styles.timer}>{recordTime}</Text>

                    <TouchableOpacity onPress={onClosePress}>
                        <View style={[styles.button, {backgroundColor: 'transparent'}]}>
                            <Icon name={"times"}
                                  size={28}
                                  color="#aaa"
                            />
                        </View>
                    </TouchableOpacity>
                </View>
                }
            </Modal>
        );
    }
}

const {width} = Dimensions.get("window");

const styles = StyleSheet.create({
    modal: {
        justifyContent: 'flex-end',
        padding: 0,
        margin: 0,
    },
    container: {
        backgroundColor: '#fafafa',
        width: width,
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15,

        elevation: 1,
        shadowOffset: {width: 2, height: 2},
        shadowColor: '#000',
        shadowOpacity: 0.2
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: 15,
        marginLeft: 15
    },
    timer: {
        fontSize: 18
    },
});