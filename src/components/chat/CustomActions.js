import React, {Component} from 'react';
import {
    Dimensions,
    StyleSheet,
    View,
    Text,
    ActivityIndicator
} from 'react-native';
import Modal from 'react-native-modal';
import ActionButton from './ActionButton';

export default class CustomActions extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {onPhotoPress, onGalleryPress, onDocPress, onClosePress, isFileLoading} = this.props;

        return (
            <Modal isVisible={true}
                   style={styles.modal}
                // animationIn={'slideOutUp'}
                // animationOut={'slideInDown'}
                   backdropOpacity={0.4}
                   swipeDirection="down"
                   onSwipe={onClosePress}
                   onBackdropPress={onClosePress}
                   onBackButtonPress={onClosePress}>

                {isFileLoading &&
                <View style={[styles.container, {flexDirection: 'column'}]}>
                    <Text style={{fontSize: 24, marginBottom: 10, marginTop: -10}}>Загрузка</Text>
                    <ActivityIndicator size="large" color="#79C780"/>
                </View>
                }
                {!isFileLoading &&
                <View style={styles.container}>
                    <ActionButton name='camera' text='Камера' color='#52A0ED' size={28}
                                  onPress={onPhotoPress}/>
                    <ActionButton name='image' text='Галлерея' color='#34C865' size={28}
                                  onPress={onGalleryPress}/>
                    <ActionButton name='file' text='Файл' color='#F2C04B' size={28}
                                  onPress={onDocPress}/>
                </View>
                }
            </Modal>
        );
    }
}

const {width} = Dimensions.get("window");

const styles = StyleSheet.create({
    modal: {
        justifyContent: 'flex-end',
        padding: 0,
        margin: 0,
    },
    container: {
        backgroundColor: '#fafafa',
        width: width - 16,
        height: 140,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        margin: 8,
        elevation: 1,
        shadowOffset: {width: 2, height: 2},
        shadowColor: '#000',
        shadowOpacity: 0.2
    },
});