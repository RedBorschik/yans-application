import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';

export default class ActionButton extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {name, color, text, size, onPress} = this.props;

        return (
            <TouchableOpacity onPress={onPress} style={{padding: 7, margin: 2}}>
                <Animatable.View style={[styles.actionButton, {backgroundColor: color}]}
                                 animation={'zoomIn'}
                                 delay={200}
                                 duration={100}
                >
                    <Icon name={name} size={size} color="#fff"/>
                </Animatable.View>
                <Text style={styles.actionButtonText}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    actionButton: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 15,
        marginBottom: 5
    },
    actionButtonText: {
        color: '#636363',
        alignSelf: 'center',
        fontSize: 14
    },
});