import React from "react";
import {SystemMessage} from 'react-native-gifted-chat';

export default renderSystemMessage = (props) => (
    <SystemMessage {...props}
                   textStyle={{
                       textAlign: 'center',
                       paddingHorizontal: 15
                   }}
    />
)