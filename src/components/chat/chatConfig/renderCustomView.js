import React from "react";
import CustomView from '../CustomView';

export default renderCustomView = (props) => (
    <CustomView
        {...props}
    />
);