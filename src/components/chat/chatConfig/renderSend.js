import React from "react";
import {View} from 'react-native';
import {Send} from 'react-native-gifted-chat';
import Icon from 'react-native-vector-icons/FontAwesome';

export default renderSend = (props) => (
    <Send {...props} >
        <View style={{margin: 0, marginRight: 10, marginTop: -37, padding: 0}}>
           <Icon style={{color: '#636363', fontSize: 28}} name={'arrow-circle-up'}/>
        </View>
    </Send>

);