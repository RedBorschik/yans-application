import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default renderCustomActions = (props, onPaperclipPress, onVoicePress) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={[props.containerStyle, styles.actionIcon]} onPress={onPaperclipPress}>
                <Icon name="paperclip" size={22} color="#636363"/>
            </TouchableOpacity>
            <TouchableOpacity style={[props.containerStyle, styles.actionIcon]} onPress={onVoicePress}>
                <Icon name="microphone" size={22} color="#636363"/>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginLeft: 5,
        marginRight: -9
    },
    actionIcon: {
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: -6
    },
});