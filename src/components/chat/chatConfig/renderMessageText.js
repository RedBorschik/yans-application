import React from "react";
import {MessageText} from 'react-native-gifted-chat';

export default renderMessageText = (props) => (
    <MessageText {...props}
                 textStyle={{
                     left: {
                         color: '#212521'
                     },
                     right: {
                         color: '#212521'
                     }
                 }}
    />
);