import React from "react";
import {Bubble} from 'react-native-gifted-chat';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    Button
} from 'react-native';

export default renderBubble = (props) => {
    // console.log(props);

    const {
        currentMessage,
        nextMessage,
        previousMessage,
        imageProps,
        videoProps,
        lightboxProps,
        textInputProps,
    } = props;

    // console.log(imageProps);

    return (
        <Bubble {...props}
                wrapperStyle={{
                    left: {
                        backgroundColor: '#ddeff9',
                    },
                    right: {
                        backgroundColor: '#c4fbc6'
                    }
                }}
        />
    );
}