import React from "react";
import {LoadEarlier} from 'react-native-gifted-chat';

export default renderLoadEarlier = (props) => (
    <LoadEarlier
        {...props}
        label={'Предыдущие сообщения'}
        textStyle={{marginHorizontal: 20}}
    />
);