import React from "react";
import {Time} from 'react-native-gifted-chat';

export default renderTime = (props) => (
    <Time {...props}
          textStyle={{
              left: {
                  color: '#aaa'
              },
              right: {
                  color: '#aaa'
              }
          }}
    />
);