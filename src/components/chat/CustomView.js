import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    Dimensions
} from 'react-native';

import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';

import Icon from 'react-native-vector-icons/FontAwesome';

import AudioRecorderPlayer, {
    AVEncoderAudioQualityIOSType,
    AVEncodingOption,
    AudioEncoderAndroidType,
    AudioSet,
    AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';

export default class CustomView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPositionSec: 0,
            currentDurationSec: 0,
            playTime: '00:00',
            duration: '00:00',
            isPlaying: false,

            fileSize: ' '
        };

        this.audioRecorderPlayer = new AudioRecorderPlayer();
    }

    componentWillUnmount() {
        console.log('onStopPlay');
        this.audioRecorderPlayer.stopPlayer();
        this.audioRecorderPlayer.removePlayBackListener();
        this.setState({
            isPlaying: false,
        });
    }

    renderAudio(audio) {
        const onPress = () => {
            if (this.state.isPlaying) {
                onStopPlay();
            } else {
                onStartPlay();
            }
        };

        const onStartPlay = async () => {
            console.log('onStartPlay');

            const msg = await this.audioRecorderPlayer.startPlayer(audio);
            this.audioRecorderPlayer.setVolume(1.0);
            console.log(msg);
            this.audioRecorderPlayer.addPlayBackListener((e) => {

                // console.log(`${Math.floor(e.current_position/1000)} : ${Math.floor(e.duration/1000)}`);

                if (e.current_position === e.duration) {
                    console.log('finished');
                    this.audioRecorderPlayer.stopPlayer();
                }
                this.setState({
                    currentPositionSec: Math.floor(e.current_position / 1000),
                    currentDurationSec: Math.floor(e.duration / 1000),
                    // playTime: Math.floor(e.current_position/1000),
                    // duration: Math.floor(e.duration/1000),
                    playTime: this.audioRecorderPlayer.mmss(Math.floor(e.current_position / 1000)),
                    duration: this.audioRecorderPlayer.mmss(Math.floor(e.duration / 1000)),
                    isPlaying: e.current_position === e.duration ? false : true
                });
            });
        };

        const onStopPlay = async () => {
            console.log('onStopPlay');
            const msg = await this.audioRecorderPlayer.stopPlayer();
            console.log(msg);
            this.audioRecorderPlayer.removePlayBackListener();
            this.setState({
                isPlaying: false,
            });
        };

        const {
            isPlaying,
            playTime,
            duration,
            currentPositionSec,
            currentDurationSec
        } = this.state;

        let loaderWidth = isPlaying ? (currentPositionSec / currentDurationSec) * 150 : 6;

        return (
            <TouchableOpacity style={[styles.container, this.props.containerStyle]}
                              onPress={onPress}>
                <View style={styles.messageContainer}>
                    <View style={styles.iconContainer}>
                        <Icon size={20}
                              color={'#fff'}
                              name={isPlaying ? 'pause' : 'play'}/>
                    </View>
                    <View style={styles.infoContainer}>
                        <View style={styles.loaderContainer}>
                            <View style={styles.loaderBack}/>
                            <View style={[styles.loaderFront, {width: loaderWidth}]}/>
                        </View>
                        <Text style={styles.infoText}>
                            {isPlaying ? playTime : duration}
                        </Text>
                    </View>
                </View>

            </TouchableOpacity>
        );
    }

    renderVideo() {
        return (
            <View>
                <Text>VIDEO</Text>
            </View>
        )
    }

    renderDoc(doc, local = false) {
        let fileName = 'Документ';
        let localFile = '';

        if (!local) {
            fileName = doc.slice((doc.lastIndexOf("/") - 1 >>> 0) + 2);
            localFile = `${RNFS.DocumentDirectoryPath}/${fileName}`;

            const options = {
                fromUrl: doc,
                toFile: localFile
            };
            RNFS.downloadFile(options).promise.then(
                data => this.setState({fileSize: formatBytes(data.bytesWritten)})
            );
        }

        const formatBytes = (bytes, decimals = 2) => {
            if (bytes === 0) return '0 Bytes';

            const k = 1024;
            const dm = decimals < 0 ? 0 : decimals;
            const sizes = ['Байтов', 'Кб', 'Мб', 'Гб', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(bytes) / Math.log(k));

            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        };

        const onPress = () => {
            FileViewer.open(local ? doc : localFile);
        };

        return (
            <TouchableOpacity style={[styles.container, this.props.containerStyle]}
                              onPress={onPress}>
                <View style={styles.messageContainer}>
                    <View style={styles.iconContainer}>
                        <Icon size={20} name={'file'} color={'#fff'}/>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text ellipsizeMode={'tail'} numberOfLines={2} style={styles.infoText}>{fileName}</Text>
                        <Text style={styles.infoText}>{this.state.fileSize}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        if (this.props.currentMessage.audio !== undefined) {
            return this.renderAudio(this.props.currentMessage.audio);
        }

        if (this.props.currentMessage.doc !== undefined) {
            return this.renderDoc(this.props.currentMessage.doc);
        }

        if (this.props.currentMessage.doc1 !== undefined) {
            return this.renderDoc(this.props.currentMessage.doc1, true);
        }

        return null;
    }
}

const styles = StyleSheet.create({
    container: {},
    mapView: {
        width: 80,
        height: 80,
        borderRadius: 13,
        margin: 3,
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 13,
        margin: 3,
        resizeMode: 'cover'
    },
    webview: {
        flex: 1,
    },
    imageActive: {
        flex: 1,
        resizeMode: 'contain',
    },

    messageContainer: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'flex-end',
    },
    iconContainer: {
        height: 50,
        width: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#79C780',

    },
    infoContainer: {
        marginLeft: 10,
    },
    infoText: {
        color: '#79C780',
        maxWidth: Dimensions.get('window').width - 150,
        marginRight: 5
    },

    loaderContainer: {
        width: 150,
        height: 6,
        marginBottom: 5
    },
    loaderBack: {
        width: 150,
        position: 'absolute',
        top: 0,
        left: 0,
        height: 6,
        borderRadius: 3,
        backgroundColor: '#aaa'
    },
    loaderFront: {
        width: 6,
        position: 'absolute',
        top: 0,
        left: 0,
        height: 6,
        borderRadius: 3,
        backgroundColor: '#79C780'
    }
});