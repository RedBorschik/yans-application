import React from 'react';
import {Platform, Dimensions} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import styled from 'styled-components/native';

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
`;

const Favorite = ({onPress, active, style}) => (
    <IconLeftContainer onPress={onPress} style={style}>
        <Svg width={21} height={21}>
            <G fill="none" fillRule="evenodd">
                <Path d="M-3-2h28v28H-3z"/>
                <Path
                    fill={active ? '#5BBB5E' : 'rgba(255, 255, 255, 0.7)'}
                    stroke={active ? '#5BBB5E' : '#5BBB5E'}
                    strokeWidth={1.3}
                    d="M10.5 16.25l-5.584 2.936 1.066-6.218-4.517-4.404 6.243-.907L10.5 2l2.792 5.657 6.243.907-4.517 4.404 1.066 6.218z"
                />
            </G>
        </Svg>
    </IconLeftContainer>
);

export default Favorite;
