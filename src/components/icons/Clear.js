import React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
    <Svg viewBox="0 0 24 24" {...props} width="16" height="16">
        <Path
            d="M12 0C5.4 0 0 5.4 0 12s5.4 12 12 12 12-5.4 12-12S18.6 0 12 0zm4.7 15.3c.4.4.4 1 0 1.4-.2.2-.4.3-.7.3-.3 0-.5-.1-.7-.3L12 13.4l-3.3 3.3c-.2.2-.4.3-.7.3-.3 0-.5-.1-.7-.3-.4-.4-.4-1 0-1.4l3.3-3.3-3.3-3.3c-.4-.4-.4-1 0-1.4s1-.4 1.4 0l3.3 3.3 3.3-3.3c.4-.4 1-.4 1.4 0 .4.4.4 1 0 1.4L13.4 12l3.3 3.3z"
            fill="#929292"
        />
    </Svg>
);

export default SvgComponent;
