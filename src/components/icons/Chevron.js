import React from 'react'
import Svg, { Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg width={14} height={9} {...props}>
        <Path
            d="M12.314 1.657L6.657 7.314 1 1.657"
            stroke="#929292"
            strokeWidth={2}
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)

export default SvgComponent
