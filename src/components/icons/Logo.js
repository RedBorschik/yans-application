import React from 'react';
import {Platform, Dimensions} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import styled from 'styled-components/native';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const View = styled.View`
    width: 100%;
    alignItems:  ${Platform.OS === 'android' ? 'flex-start' : 'center'};
    justifyContent: ${Platform.OS === 'android' ? 'flex-start' : 'center'};
    paddingLeft: ${Platform.OS === 'android' ? 16 : 0};
    paddingBottom: 3
`;

const SvgComponent = props => (
    <View>
        <Svg {...props} viewBox="0 0 44 23.7" width={44} height={24}>
            <Path
                d="M3.9 23.4V15L0 5h3.1l2.3 6.5L7.7 5h3.1L6.9 15.1v8.4h-3zm14.1 0l-.8-3.7h-3.7l-.8 3.7H9.6L13.8 5H17l4.2 18.4H18zm-2.6-12.8l-1.3 6.3h2.5l-1.2-6.3zm14.3 12.8l-4.2-11.3v11.3h-3V5h2.7l4.2 11.3V5h3v18.4h-2.7zm4.8-4.8v-.3l3.1-.2v.5c0 1.6.6 2.4 1.7 2.4s1.7-.8 1.7-2.4v-.5c0-.7-.1-1.3-.4-1.7-.3-.4-.7-.7-1.4-.9l-1-.3c-1.3-.4-2.3-1-2.8-1.8s-.8-1.9-.8-3.4v-.4c0-1.6.4-2.8 1.1-3.7.7-.9 1.9-1.3 3.4-1.3 2.9 0 4.5 1.7 4.6 5.1v.5l-3.1.1v-.7c0-1.5-.5-2.3-1.5-2.3s-1.5.8-1.5 2.3v.3c0 .7.1 1.3.4 1.7.3.4.8.7 1.4.9l.9.3c1.4.4 2.3 1 2.8 1.8s.8 1.9.8 3.4v.5c0 1.6-.4 2.9-1.2 3.8-.8.9-2 1.4-3.5 1.4s-2.7-.5-3.5-1.4c-.8-.8-1.2-2.1-1.2-3.7zM39 0h2.3l-1.6 2.9h-2.3L39 0z"/>
        </Svg>
    </View>
);

export default SvgComponent;
