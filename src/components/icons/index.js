import IconBack from './Back';
import IconChevron from './Chevron';
import IconFavorite from './Favorite';
import IconCart from './Cart';
import IconMap from './Map';
import IconClose from './Close';
import IconLogo from './Logo';
import IconHamburger from './Hamburger';
import IconNewLabel from './NewLabel';
import IconSearch from './Search';
import IconSettings from './Settings';
import IconEmpty from './Empty';
import IconCloseBtn from './CloseBtn';

export {
    IconBack,
    IconChevron,
    IconFavorite,
    IconCart,
    IconMap,
    IconClose,
    IconLogo,
    IconHamburger,
    IconNewLabel,
    IconSearch,
    IconSettings,
    IconEmpty,
    IconCloseBtn
};
