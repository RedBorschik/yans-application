import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg
        {...props}
        id="prefix__\u0421\u043B\u043E\u0439_1"
        x={0}
        y={0}
        viewBox="0 0 21 19"
        width={51} height={50}
        xmlSpace="preserve"
    >
        <G id="prefix__Page-1">
            <G id="prefix__Ui_state" transform="translate(-174 -1271)">
                <G transform="translate(175 1271)" id="prefix__basket">
                    <G id="prefix__shopping-basket" transform="translate(0 4.272)">
                        <Path
                            id="prefix__Shape"
                            d="M19 2.7c0-1.1-.9-2-2-2H2c-1.1 0-2 .9-2 2 0 1 .7 1.8 1.6 2l.9 7c.1.5.3 1 .8 1.4.4.4.9.6 1.5.6h9.5c.5 0 1.1-.2 1.5-.6.4-.4.7-.9.8-1.4l.9-7c.8-.2 1.5-1 1.5-2zm-3.6 8.9c-.1.5-.6 1-1.1 1H4.7c-.5 0-1.1-.5-1.1-1l-.9-6.8h13.6l-.9 6.8zM17 3.7H2c-.5 0-.9-.4-.9-.9s.4-.9.9-.9h1s0 0 0 0 0 0 0 0h12.8s0 0 0 0 0 0 0 0h1c.5 0 .9.4.9.9.2.4-.2.9-.7.9z"
                            fill="#ffffff"
                            stroke="#ffffff"
                            strokeWidth={0.1}
                        />
                        <Path
                            id="prefix__Path"
                            className="prefix__st1"
                            stroke='none'
                            fill="#ffffff"
                            d="M9.5 6.6c-.3 0-.6.2-.6.6v3c0 .3.2.6.6.6.3 0 .6-.2.6-.6v-3c0-.3-.3-.6-.6-.6z"
                        />
                        <Path
                            id="prefix__Path_1_"
                            className="prefix__st1"
                            stroke='none'
                            fill="#ffffff"
                            d="M12.5 6.6c-.3 0-.6.2-.6.6v3c0 .3.2.6.6.6s.6-.2.6-.6v-3c-.1-.3-.3-.6-.6-.6z"
                        />
                        <Path
                            id="prefix__Path_2_"
                            className="prefix__st1"
                            stroke='none'
                            fill="#ffffff"
                            d="M6.5 6.6c-.3 0-.5.3-.5.6v3c0 .3.2.6.6.6s.6-.2.6-.6v-3c-.1-.3-.4-.6-.7-.6z"
                        />
                    </G>
                    <Path
                        id="prefix__Path_3_"
                        className="prefix__st1"
                        stroke='none'
                        fill="#ffffff"
                        d="M7.1.5c-.4-.3-1-.1-1.2.3l-2.3 4c-.2.4-.1.9.3 1.2.4.2.9.1 1.2-.3l2.3-4c.2-.5.1-1-.3-1.2z"
                    />
                    <Path
                        id="prefix__Path-Copy"
                        className="prefix__st1"
                        stroke='none'
                        fill="#ffffff"
                        d="M12 .5c.4-.2.9-.1 1.2.3l2.3 4c.2.4.1.9-.3 1.2-.4.2-.9.1-1.2-.3l-2.3-4c-.2-.5-.1-1 .3-1.2z"
                    />
                </G>
            </G>
        </G>
    </Svg>
);

export default SvgComponent;
