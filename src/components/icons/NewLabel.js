import React from 'react'
import Svg, { G, Path, Text, TSpan } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg width={42} height={20} {...props}>
        <G fill="none" fillRule="evenodd">
            <Path fill="#5BBB5E" d="M0 0h42l-7.374 10L42 20H0z" />
            <Text
                fontFamily="SFProText-Bold, SF Pro Text"
                fontSize={10}
                fontWeight="bold"
                fill="#FFF"
            >
                <TSpan x={6} y={14}>
                    {'NEW'}
                </TSpan>
            </Text>
        </G>
    </Svg>
)

export default SvgComponent
