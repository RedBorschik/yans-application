import React from 'react'
import Svg, { G, Path } from 'react-native-svg'
import styled from 'styled-components/native';
/* SVGR has dropped some elements not supported by react-native-svg: style, title */

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  justifyContent: center;
  marginLeft: 4;
  alignItems: center;
`;

const Delete = (props) => (
    <IconLeftContainer onPress={props.onPress}>
        <Svg viewBox="0 0 17 22" width="17" height="22" {...props}>
            <G fill={props.color ? props.color : '#929292'} stroke={props.color ? props.color : '#929292'} strokeWidth={0.3}>
                <Path d="M10.9 8.5c-.2 0-.4.2-.4.5v9c0 .3.2.5.4.5s.4-.2.4-.5V9c0-.3-.2-.5-.4-.5zM6.1 8.5c-.2 0-.4.2-.4.5v9c0 .3.2.5.4.5s.4-.2.4-.5V9c0-.3-.2-.5-.4-.5z"/>
                <Path
                    d="M2.2 7v11.5c0 .7.2 1.3.6 1.8.4.5 1 .7 1.5.7h8.2c.6 0 1.1-.3 1.5-.7.4-.5.6-1.1.6-1.8V7c.8-.2 1.3-1.1 1.2-2-.1-.9-.8-1.6-1.6-1.6H12v-.5c0-.5-.2-1-.5-1.3-.2-.4-.6-.6-1.1-.6H6.6c-.5 0-.9.2-1.2.5-.4.4-.5.9-.5 1.4v.6H2.7C1.8 3.4 1.1 4.1 1 5c-.1.9.4 1.7 1.2 2zm10.4 13.1H4.4c-.7 0-1.3-.7-1.3-1.6V7h10.8v11.5c0 .9-.6 1.6-1.3 1.6zM5.7 2.9c0-.2.1-.5.2-.7.2-.2.5-.3.7-.3h3.8c.2 0 .5.1.6.3.2.2.2.4.2.7v.6H5.7v-.6zm-3 1.5h11.7c.4 0 .8.4.8.8s-.3.8-.8.8H2.7c-.4 0-.8-.4-.8-.8s.3-.8.8-.8z"/>
                <Path d="M8.5 8.5c-.2 0-.4.2-.4.5v9c0 .3.2.5.4.5s.4-.2.4-.5V9c0-.3-.2-.5-.4-.5z"/>
            </G>
        </Svg>
    </IconLeftContainer>
);

export default Delete;
