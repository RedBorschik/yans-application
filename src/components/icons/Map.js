import React from 'react';
import Svg, {Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg width={props.width ? props.width : 13} height={props.height ? props.height : 18} viewBox="0 0 13 18" {...props}>
        <Path
            d="M6.5 18C2.167 12.791 0 8.979 0 6.562 0 2.938 2.91 0 6.5 0S13 2.938 13 6.563C13 8.979 10.833 12.79 6.5 18zm0-8c1.88 0 3.405-1.539 3.405-3.438 0-1.898-1.525-3.437-3.405-3.437S3.095 4.664 3.095 6.563C3.095 8.46 4.62 10 6.5 10z"
            fill={props.color ? props.color : '#5BBB5E'}
            fillRule="evenodd"
        />
    </Svg>
);

export default SvgComponent;
