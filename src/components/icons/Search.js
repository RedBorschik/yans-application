import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import Svg, { Path } from 'react-native-svg'

const IconRightContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginRight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
  alignItems: center;
`;

const Search = ({onPress}) => (
    <IconRightContainer onPress={onPress}>
        <Svg viewBox="0 0 16.8 16.8" width={18} height={18}>
            <Path
                d="M7.1 13.6C3.5 13.6.6 10.7.6 7.1S3.5.6 7.1.6s6.5 2.9 6.5 6.5-2.9 6.5-6.5 6.5zm4.7-1.8l4.3 4.3-4.3-4.3z"
                fill="none"
                stroke="#929292"
                strokeWidth={1.333}
                strokeLinejoin="round"
            />
        </Svg>
    </IconRightContainer>
);

export default Search;
