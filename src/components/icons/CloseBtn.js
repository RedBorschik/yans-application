import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import Svg, { G, Path } from 'react-native-svg'
import {colors} from './../../utils/theme';

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
  alignItems: center;
`;

const Back = ({ onPress }) => (
  <IconLeftContainer onPress={onPress}>
      <Svg width={18} height={17}>
          <G fill="none" fillRule="evenodd">
              <Path d="M-5-6h28v28H-5z"/>
              <Path
                  d="M16.443 0L9.01 7.454 1.552.01.5 1.03c-.005.005 0 .014 0 .014l7.464 7.459-7.412 7.436.984 1.06h.016l7.441-7.47 7.438 7.433 1.07-1.02v-.012l-7.462-7.45 7.422-7.452L16.46 0h-.017z"
                  fill="#929292"
              />
          </G>
      </Svg>
  </IconLeftContainer>
);

export default Back;
