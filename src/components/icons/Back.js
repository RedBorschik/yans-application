import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import Svg, { G, Path } from 'react-native-svg'
import {colors} from './../../utils/theme';

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
`;

const Back = ({ onPress }) => (
  <IconLeftContainer onPress={onPress}>
      <Svg width={22} height={14}>
          <G
              strokeWidth={1.5}
              stroke={colors.GREY_3}
              fill="none"
              fillRule="evenodd"
              strokeLinecap="round"
              strokeLinejoin="round"
          >
              <Path d="M6.943 12.657L1.286 7l5.657-5.657h0M2.25 7h18.333" />
          </G>
      </Svg>
  </IconLeftContainer>
);

export default Back;
