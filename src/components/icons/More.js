import React from 'react'
import styled from 'styled-components/native';
import Svg, { Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title */

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  justifyContent: center;
`;

const More = (props) => (
    <IconLeftContainer onPress={props.onPress}>
        <Svg viewBox="0 0 7 24" width="7" height="24" {...props}>
            <Path
                d="M3.5 6C2.1 6 1 4.9 1 3.5S2.1 1 3.5 1 6 2.1 6 3.5 4.9 6 3.5 6zm0 8.3c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5S6 10.4 6 11.8s-1.1 2.5-2.5 2.5zm0 8.4c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5S6 18.8 6 20.2c0 1.3-1.1 2.5-2.5 2.5z"
                fill="none"
                stroke="#929292"
                strokeWidth={1.4}
            />
        </Svg>
    </IconLeftContainer>
);

export default More;
