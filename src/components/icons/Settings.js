import React from 'react';
import styled from 'styled-components/native';

const IconRightContainer = styled.TouchableOpacity`
  height: 100%;
  paddingRight: 15;
  justifyContent: center;
`;

const Settings = ({ onPress }) => (
  <IconRightContainer onPress={onPress}>
  </IconRightContainer>
);

export default Settings;
