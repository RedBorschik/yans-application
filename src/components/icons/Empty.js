import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';

const IconLeftContainer = styled.View`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
`;

const Empty = ({ onPress }) => (
  <IconLeftContainer/>
);

export default Empty;
