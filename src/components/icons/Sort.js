import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg viewBox="0 0 13 16" width="13" height="16" {...props}>
        <G
            fill="none"
            stroke="#929292"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <Path d="M13 11.9l-3 3-3-3h0m3 2.3V1.3M0 4.1l3-3 3 3h0M3 1.8v12.9"/>
        </G>
    </Svg>
);

export default SvgComponent;
