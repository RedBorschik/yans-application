import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg
        {...props}
        id="prefix__\u0421\u043B\u043E\u0439_1"
        x={0}
        y={0}
        viewBox="0 0 22 16"
        xmlSpace="preserve"
        width={22} height={16}
    >
        <G id="prefix__Page-1">
            <G id="prefix__Ui_state" transform="translate(-294 -1272)">
                <G id="prefix__catalog" transform="translate(290 1266)">
                    <Path id="prefix__Line-4" className="prefix__st0" d="M5.5 8h3"
                          stroke={props.active ? '#5bbb5e' : '#929292'}
                          fill={props.active ? '#5bbb5e' : '#929292'}/>
                    <Path
                        stroke={props.active ? '#5bbb5e' : '#929292'}
                        id="prefix__Line-4-Copy"
                        className="prefix__st0"
                        d="M5.5 13h1"
                    />
                    <Path
                        stroke={props.active ? '#5bbb5e' : '#929292'}
                        fill={props.active ? '#5bbb5e' : '#929292'}
                        id="prefix__Line-4-Copy-2"
                        className="prefix__st0"
                        d="M5.5 18H9"
                    />
                    <Path
                        id="prefix__Combined-Shape"
                        d="M19.5 17l3.6 3.6-3.6-3.6c-1.1 1.2-2.7 2-4.5 2-3.3 0-6-2.7-6-6s2.7-6 6-6 6 2.7 6 6c0 1.5-.6 3-1.5 4z"
                        fill="none"
                        stroke={props.active ? '#5bbb5e' : '#929292'}
                        strokeWidth={1.5}
                        strokeLinejoin="round"
                    />
                </G>
            </G>
        </G>
    </Svg>

);

export default SvgComponent;
