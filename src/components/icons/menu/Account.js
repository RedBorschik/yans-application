import React from 'react';
import Svg, {G, Path, Ellipse} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg {...props} viewBox="0 0 17 18" width={20} height={18}>
        <G
            transform="translate(1 1)"
            fill="none"
            stroke={props.active ? '#5bbb5e' : '#929292'}
            strokeWidth={1.3}
        >
            <Path
                d="M15 16c0-4.1-3.4-7.3-7.5-7.3S0 11.9 0 16"
                strokeLinecap="round"
            />
            <Ellipse cx={7.5} cy={4.3} rx={4.4} ry={4.3} />
        </G>
    </Svg>
);

export default SvgComponent;
