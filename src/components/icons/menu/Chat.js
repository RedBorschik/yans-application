import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg {...props} viewBox="0 0 21 18" width={21} height={18}>
        <Path
            d="M10.5 15.7c-.9 0-1.8-.1-2.7-.3-.2 0-1.2.7-2.2 1.1-1.1.4-2.3.5-2.5.5-.3-.1.4-.8.9-1.6.4-.6.5-1.4.3-1.4-2-1.4-3.3-3.4-3.3-5.7C1 4.3 5.3 1 10.5 1S20 4.3 20 8.3s-4.3 7.4-9.5 7.4zM6.5 9c.3 0 .5-.2.5-.5S6.8 8 6.5 8s-.5.2-.5.5.2.5.5.5zm4 0c.3 0 .5-.2.5-.5s-.2-.5-.5-.5-.5.2-.5.5.2.5.5.5zm4 0c.3 0 .5-.2.5-.5s-.2-.5-.5-.5-.5.2-.5.5.2.5.5.5z"
            fill="none"
            stroke={props.active ? '#5bbb5e' : '#929292'}
            strokeWidth={1.2}
        />
    </Svg>
);

export default SvgComponent;
