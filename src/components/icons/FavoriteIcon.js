import React from 'react';
import Svg, {G, Path} from 'react-native-svg';

const Favorite = (props) => (
    <Svg width={21} height={21}>
        <G fill="none" fillRule="evenodd">
            <Path d="M-3-2h28v28H-3z"/>
            <Path
                fill={props.fill ? props.fill : 'none'}
                stroke={props.color ? props.color : '#929292'}
                strokeWidth={1.3}
                d="M10.5 16.25l-5.584 2.936 1.066-6.218-4.517-4.404 6.243-.907L10.5 2l2.792 5.657 6.243.907-4.517 4.404 1.066 6.218z"
            />
        </G>
    </Svg>
);

export default Favorite;
