import React from 'react'
import Svg, { G, Path } from 'react-native-svg'
import styled from 'styled-components/native';
/* SVGR has dropped some elements not supported by react-native-svg: style, title */

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  justifyContent: center;
  alignItems: center;
`;

const Copy = ({ onPress }) => (
    <IconLeftContainer onPress={onPress}>
        <Svg
            id="prefix__\u0421\u043B\u043E\u0439_1"
            x={0}
            y={0}
            viewBox="0 0 17 20"
            width="17"
            height="20"
            xmlSpace="preserve"
        >
            <G id="prefix__Page-1">
                <G id="prefix__Ui_state" transform="translate(-526 -1215)">
                    <G id="prefix__copy" transform="translate(519 1211)">
                        <Path
                            id="prefix__Shape"
                            className="prefix__st0"
                            d="M18.3 8.1h-9c-.8 0-1.5.6-1.5 1.4v12c0 .8.7 1.4 1.5 1.4h9c.8 0 1.5-.6 1.5-1.4v-12c0-.7-.7-1.4-1.5-1.4zm.4 13.4c0 .2-.2.4-.5.4h-9c-.3 0-.5-.2-.5-.4v-12c0-.2.2-.4.5-.4h9c.3 0 .5.2.5.4v12z"
                        />
                        <Path
                            id="prefix__Path"
                            className="prefix__st0"
                            d="M21.7 5h-9c-.8 0-1.5.6-1.5 1.4 0 .3.2.5.5.5s.5-.2.5-.5c0-.2.2-.4.5-.4h9c.3 0 .5.2.5.4v12c0 .2-.2.4-.5.4s-.5.2-.5.5.2.5.5.5c.8 0 1.5-.6 1.5-1.4v-12c0-.8-.6-1.4-1.5-1.4z"
                        />
                    </G>
                </G>
            </G>
        </Svg>
    </IconLeftContainer>
);

export default Copy;