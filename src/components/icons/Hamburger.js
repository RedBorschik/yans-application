import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import Svg, { Path } from 'react-native-svg'
import {colors} from './../../utils/theme';

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
  alignItems: center;
`;

const Hamburger = ({onPress}) => (
    <IconLeftContainer onPress={onPress}>
        <Svg width={22} height={15}>
            <Path
                d="M1 13.5h12H1zm0-6h16H1zm0-6h20H1z"
                stroke={colors.GREY_3}
                strokeWidth={1.5}
                fill="none"
                fillRule="evenodd"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </Svg>
    </IconLeftContainer>
);

export default Hamburger;
