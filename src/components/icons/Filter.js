import React from 'react';
import Svg, {Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg viewBox="0 0 14 14" width="14" height="14" {...props}>
        <Path
            fill="#929292"
            d="M13.4 0H.6C.3 0 0 .2 0 .5c0 1.5.7 3 1.8 4l2.4 2.1c.4.4.7.9.7 1.5v5.3c0 .4.5.7.8.5l3.2-2.2c.2-.1.2-.3.2-.5V8c0-.6.2-1.1.7-1.5l2.4-2.1c1.1-1 1.8-2.5 1.8-4 0-.2-.3-.4-.6-.4zm-1.9 3.7L9.1 5.8c-.6.6-1 1.4-1 2.3V11L6 12.4V8.1c0-.9-.4-1.7-1-2.3L2.6 3.7C1.8 3 1.3 2.1 1.1 1.1h11.7c-.1 1-.6 1.9-1.3 2.6z"
        />
    </Svg>
);

export default SvgComponent;
