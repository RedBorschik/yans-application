import React from 'react';
import Svg, {Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg viewBox="0 0 12 19" width="12" height="19" {...props}>
        <Path
            d="M10.6.3C10 0 9.4-.1 8.8.1c-.6.2-1.1.5-1.4 1.1L.6 13.1c0 .1-.1.1-.1.2L0 17.6c0 .2.1.4.3.5.1 0 .2.1.3.1.1 0 .2 0 .3-.1l3.5-2.6c.1 0 .1-.1.1-.2l6.9-11.9c.7-1.1.3-2.5-.8-3.1zM1.2 16.5l.3-2.3 1.6.9-1.9 1.4zm2.7-2.2l-2.1-1.2 5.5-9.6 2.1 1.2-5.5 9.6zm6.6-11.4l-.5.9-2.1-1.2.5-.9c.2-.3.4-.5.7-.6.3-.1.6 0 .9.1.3.2.5.4.6.7s0 .7-.1 1z"
            fill="#929292"
        />
    </Svg>
);

export default SvgComponent;
