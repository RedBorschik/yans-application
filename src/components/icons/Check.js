import React from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import Svg, {Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const IconLeftContainer = styled.View`
  height: 28;
  width: 28;
  marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
  justifyContent: center;
`;

const SvgComponent = props => (
    <IconLeftContainer>
        <Svg viewBox="0 0 14 10" width="14" height="10" {...props}>
            <Path
                fill="none"
                stroke={props.style && props.style.color ? props.style.color : "#5bbb5e"}
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M13 1.2L5.2 9 1 4.8"
            />
        </Svg>
    </IconLeftContainer>
);

export default SvgComponent;
