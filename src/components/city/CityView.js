import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dimensions, StatusBar, Platform} from 'react-native';
import Svg, { Path } from 'react-native-svg'
import Modal from 'react-native-modal';
import citiesActions from '../../actions/Cities';
import {getCityByCode} from '../../reducers/Cities';
import Loader from '../helpers/Loader';
import Input from '../helpers/Input';
import IconCloseBtn from '../icons/CloseBtn';
import IconEmpty from '../icons/Empty';
import IconCheck from '../icons/Check';
import {ListItem} from 'react-native-elements';

import Map from '../icons/Map';
import styled from 'styled-components/native';

const ContentWrap = styled.View`
    marginTop: 10;
    paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
    paddingBottom: 12;
    borderBottomColor: rgba(0,0,0,0.04);
    borderStyle: solid; 
    borderBottomWidth: 1px 
`;

const InlineRow = styled.TouchableOpacity`
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
`;

const Label = styled.Text`
    color: rgb(35, 31, 32);
    fontSize: 15;
    fontFamily: CoreRhino45Regular;
    marginBottom: 16;
`;

const City = styled.Text`
    color: rgb(36, 80, 104);
    fontSize: 16;
    fontFamily: CoreRhino65Bold;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${(Dimensions.get('window').height)};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: 44;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: 80%;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    marginLeft: ${Platform.OS === 'android' ? 16 : 8};
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
`;

const ModalScroll = styled.ScrollView`
    flex: 1;
    marginTop: 44;
`;

const ModalList = styled.View`
    flex: 1;
    paddingTop: 8;
    paddingLeft: 8;
    paddingTop: 8;
    paddingBottom: 16;
`;

const IconChevronWrap = styled.View`
    transform: rotate(-90deg);
    height: 44;
    width: 28;
    marginLeft: ${Platform.OS === 'android' ? 16 : 8};
    justifyContent: center; 
    alignItems: center;   
`;

const InputWrap = styled.View`
    paddingLeft: ${Platform.OS === 'android' ? 8 : 0};
    paddingRight: ${Platform.OS === 'android' ? 16 : 8}; 
    paddingBottom: 16;
`;

const Small = styled.Text`
    paddingTop: 6;
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
`;

class CityView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            search: '',
        };
    }

    componentDidMount() {
        if (this.props.cities.length <= 0) {
            this.props.fetchCities();
        }
    }

    showEventPopup = () => {
        this.setState({visible: true});
    };

    hideEventPopup = () => {
        this.setState({visible: false});
    };

    selectCity = (value) => {
        this.props.selectCitiesFetch(value).then(() => {
            this.hideEventPopup();
        });
    };

    searchCity = (value) => {
        this.setState(prevState => ({...prevState, search: value}));
    };

    searchedList = (cities, search) => {
        return cities.filter(item => {
            return search.trim().length <= 0 || item.name.toLowerCase().indexOf(search.toLowerCase()) >= 0;
        });
    };

    render() {
        const {loading, city, cities, account_city} = this.props;
        const {visible, search} = this.state;
        return (
            <ContentWrap>
                {loading === true && <Loader fullscreen={true}/>}
                <Label>Ваш город:</Label>
                <InlineRow onPress={() => this.showEventPopup()}>
                    <City>{city ? city.name : 'Нур-Султан'}</City>
                    <Map style={{marginLeft: 11}}/>
                </InlineRow>
                {cities && cities.length > 0 && <Modal
                    isVisible={visible}
                    onBackdropPress={() => this.hideEventPopup()}
                    onSwipeComplete={() => this.hideEventPopup()}
                    swipeDirection={'left'}
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                    }}>
                    <ModalContent>
                        <ModalHeader>
                            <IconCloseBtn onPress={() => this.hideEventPopup()}/>
                            <ModalHeaderInner><ModalHeaderText>Выбор города</ModalHeaderText></ModalHeaderInner>
                            <IconEmpty/>
                        </ModalHeader>
                        <ModalScroll>
                            <ModalList>
                                <InputWrap>
                                    <Input
                                        label="Ваш город"
                                        underlineColorAndroid='transparent'
                                        onChangeText={(value) => this.searchCity(value)}
                                        value={search}/>
                                    <Svg viewBox="0 0 16.8 16.8" width={17} height={17} style={{
                                        position: 'absolute',
                                        right: 24,
                                        top: 16
                                    }}>
                                        <Path
                                            d="M7.1 13.6C3.5 13.6.6 10.7.6 7.1S3.5.6 7.1.6s6.5 2.9 6.5 6.5-2.9 6.5-6.5 6.5zm4.7-1.8l4.3 4.3-4.3-4.3z"
                                            fill="none"
                                            stroke="#929292"
                                            strokeWidth={1.333}
                                            strokeLinejoin="round"
                                        />
                                    </Svg>
                                    {/*<Small>Подсказка появится после ввода трех симовлов</Small>*/}
                                </InputWrap>
                                {this.searchedList(cities, search).map((item, i) => (
                                    <ListItem
                                        key={item.code}
                                        title={item.name}
                                        onPress={() => this.selectCity(item.code)}
                                        bottomDivider
                                        chevron={(city && city.code && item.code.toString() === city.code.toString()) ? <IconCheck/> : null}
                                    />
                                ))}
                            </ModalList>
                        </ModalScroll>
                    </ModalContent>
                </Modal>}
            </ContentWrap>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    selectCitiesFetch: (code) => dispatch(citiesActions.selectCitiesFetch(code)),
    fetchCities: () => dispatch(citiesActions.fetchCities()),
});

const mapStateToProps = (state, props) => ({
    loading: state.cities.loading,
    cities: state.cities.items,
    city: getCityByCode(state, state.cities.city),
    account_city: state.cities.city,
});

export default connect(mapStateToProps, mapDispatchToProps)(CityView);
