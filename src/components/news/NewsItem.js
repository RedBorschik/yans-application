import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';

const Entities = require('html-entities').XmlEntities;

import ImageCard from '../catalog/elements/ImageCard';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

const Item = styled.TouchableOpacity` 
    width: ${(Dimensions.get('window').width / 2) - (Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 24 : 12)};
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: ${colors.WHITE};
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    overflow: visible;
`;

const ContentWrap = styled.View`
    flex: 1;
    marginTop: 8;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 16;
    flexDirection: column;
`;

const Title = styled.Text`
    fontSize: 15;
    lineHeight: 22;
    fontFamily: CoreRhino45Regular;
    color: #231f20;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    paddingTop: 16;
`;

const Date = styled.View`
    height: 20;
    paddingHorizontal: 4;
    justifyContent: center;
    position: absolute;
    top: 0;
    left: -1;
    zIndex: 5;
    backgroundColor: #5BBB5E;
    borderTopLeftRadius: 6;
`;

const DateText = styled.Text`
    color: #ffffff;
    fontSize: 12;
    fontFamily: CoreRhino45Regular;
`;

export default class SmallCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {item} = this.props;
        if (item) {
            let name = new Entities();
            name = name.decode(item.name);
            return (
                item && <Item onPress={this.props.onPress}>
                    <ImageCard image={item.image} resize={'cover'}/>
                    <Date><DateText>{item.date}</DateText></Date>
                    <ContentWrap>
                        <Title>{name}</Title>
                        <MainText numberOfLines={6}>{new Entities().decode(item.preview.replace('&nbsp;', " "))}</MainText>
                    </ContentWrap>
                </Item>
            );
        } else {
            return null;
        }
    }
}
