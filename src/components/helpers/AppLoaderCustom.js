import React, {Component} from 'react';
import {View, Image, Dimensions, StatusBar} from 'react-native';

export default class AppLoaderCustom extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Image source={require('../../../assets/images/loader2.png')}
                                 style={{position: 'absolute', top: 0, left: 0, right: 0, width: '100%', height: '100%'}}/>
                <Image
                    style={{
                        position: 'absolute',
                        top: '67%',
                        left: Dimensions.get('window').width / 2 - 17,
                        width: 34,
                        height: 34,
                    }}
                    source={require('../../../assets/images/preloader.gif')}
                />
            </View>
        );
    }
}