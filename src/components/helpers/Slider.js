import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dimensions, Platform} from 'react-native';
import Preloader from '../helpers/Preloader';
import NavigationService from '../../NavigationService';
import styled from 'styled-components/native';
import {Image} from 'react-native-elements';
import Carousel, {Pagination} from 'react-native-snap-carousel';

const SliderContainer = styled.View`
    overflow: visible;
`;

const Slide = styled.View`
    flex: 1;
    opacity: 1;
`;

const SlideTouch = styled.TouchableOpacity`
    flex: 1;
    opacity: 1;
`;

const SlideInner = styled.View`
    borderRadius: ${Platform.OS === 'android' ? 8 : 6};
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: #3787b9;
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 0.18;
    overflow: hidden;
`;

class Slider extends Component {
    constructor(props) {
        super(props);

        let height = props.slideHeight;

        if (props.slideStyles && props.slideStyles.paddingTop) {
            height += props.slideStyles.paddingTop;
        }
        if (props.slideStyles && props.slideStyles.paddingBottom) {
            height += props.slideStyles.paddingBottom;
        }
        if (props.slideStyles && props.slideStyles.paddingVertical) {
            height += props.slideStyles.paddingVertical;
        }

        let width = props.slideWidth;

        if (props.slideStyles && props.slideStyles.paddingLeft) {
            width += props.slideStyles.paddingLeft;
        }
        if (props.slideStyles && props.slideStyles.paddingRight) {
            width += props.slideStyles.paddingRight;
        }
        if (props.slideStyles && props.slideStyles.paddingHorizontal) {
            width += props.slideStyles.paddingHorizontal;
        }

        this.state = {
            activeSlide: 0,
            height: height,
            width: width,
        };
    }

    _renderItem = ({item, index}) => {
        const {slideHeight, slideWidth, slideStyles, slideInnerStyles, resizeMode} = this.props;
        const {width, height} = this.state;
        return (item.filter ? <SlideTouch
            onPress={() => NavigationService.navigate('Section', {filter: item.filter, go_back_name: 'Home'})}
            style={{
            ...slideStyles,
            width: width,
            height: height,
        }}>
            <SlideInner style={{
                ...slideInnerStyles,
                height: slideHeight,
                width: slideWidth,
            }}>
                <Image
                    source={{uri: item.img}}
                    style={{width: '100%', height: slideHeight}}
                    resizeMode={resizeMode ? resizeMode : 'cover'}
                    PlaceholderContent={<Preloader/>}
                />
            </SlideInner>
        </SlideTouch> : <Slide style={{
            ...slideStyles,
            width: width,
            height: height,
        }}>
            <SlideInner style={{
                ...slideInnerStyles,
                height: slideHeight,
                width: slideWidth,
            }}>
                <Image
                    source={{uri: item.img}}
                    style={{width: '100%', height: slideHeight}}
                    resizeMode={resizeMode ? resizeMode : 'cover'}
                    PlaceholderContent={<Preloader/>}
                />
            </SlideInner>
        </Slide>);
    };

    render() {
        const {activeSlide} = this.state;
        const {items, slideStyles} = this.props;
        const {width, height} = this.state;
        return (
            <SliderContainer>
                <Carousel
                    containerCustomStyle={{overflow: 'visible', height: height}}
                    contentContainerCustomStyle={{overflow: 'visible'}}
                    inactiveSlideOpacity={0.6}
                    inactiveSlideScale={0.65}
                    firstItem={0}
                    ref={c => this._slider1Ref = c}
                    data={items}
                    renderItem={(item, index) => this._renderItem(item, index)}
                    sliderWidth={width}
                    itemWidth={width}
                    loop={true}
                    onSnapToItem={(index) => this.setState({activeSlide: index})}
                />
                <Pagination
                    carouselRef={this._slider1Ref}
                    tappableDots={!!this._slider1Ref}
                    dotsLength={items.length}
                    activeDotIndex={activeSlide}
                    containerStyle={{position: 'absolute', left: 0, right: 0, bottom: slideStyles && slideStyles.paddingBottom ? 8 + slideStyles.paddingBottom : 8, paddingVertical: 0}}
                    dotContainerStyle={{marginHorizontal: 3}}
                    dotStyle={{
                        width: 8,
                        height: 8,
                        borderRadius: 10,
                        backgroundColor: '#FFFFFF',
                    }}
                    inactiveDotStyle={{
                        width: 6,
                        height: 6,
                    }}
                    inactiveDotOpacity={.8}
                    inactiveDotScale={1}
                />
            </SliderContainer>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Slider);
;
