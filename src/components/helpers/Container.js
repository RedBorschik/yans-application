import React, {Component} from 'react';

import {ScrollView, Platform} from 'react-native';
import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

const ContainerView = styled.View`
  flex: 1; 
  backgroundColor: ${colors.WHITE};  
`;

const InnerView = styled.View`
  paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
  paddingTop: ${Platform.OS === 'android' ? 16 : 8};
  paddingBottom: 16;
`;

const InnerViewPadding = styled.View`
  paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
  paddingTop: ${Platform.OS === 'android' ? 16 : 8};
  paddingBottom: 108;
`;

export default class Container extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ContainerView>
                <ScrollView onScroll={this.props.onScroll} nestedScrollEnabled={false}>
                    {this.props.footer ?
                        <InnerViewPadding style={this.props.style}>
                            {this.props.children}
                        </InnerViewPadding>
                        : <InnerView style={this.props.style}>
                            {this.props.children}
                        </InnerView>
                    }

                </ScrollView>
                {this.props.footer}
            </ContainerView>
        );
    }
}
