import Container from './Container';
import Loader from './Loader';
import LoaderPage from './LoaderPage';
import Error from './Error';
import Slider from './Slider';


export {
    Container,
    Loader,
    LoaderPage,
    Error,
    Slider,
};
