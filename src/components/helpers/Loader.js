import React, {Component} from 'react';
import {Modal, View, StyleSheet, Image, Dimensions} from 'react-native';
import Preloader from './Preloader';

export default class Loader extends Component {
    render() {
        const {
            loading,
            fullscreen,
            ...attributes
        } = this.props;
        return (
            <Modal
                style={{
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                }}
                transparent={true}
                animationType={'none'}
                visible={loading}>
                <View style={{
                    alignItems: 'center',
                    flexDirection: 'column',
                    justifyContent: 'space-around',
                    backgroundColor: '#ffffff40',
                    height: fullscreen ? Dimensions.get('window').height : '100%'
                }}>
                    <View style={styles.activityIndicatorWrapper}>
                        {this.props.children}
                        <Preloader/>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040',
    },
    activityIndicatorWrapper: {
        backgroundColor: 'transparent',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
});