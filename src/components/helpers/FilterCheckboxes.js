import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {IconBack, IconEmpty, IconCheck} from '../../components';
import {ListItem} from 'react-native-elements';
import styled from 'styled-components/native';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height - StatusBar.currentHeight};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: ${Platform.OS === 'android' ? 56 : 44};
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 159) : (Dimensions.get('window').width - 120)};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 28 : 0};
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    width: 100%;
    overflow: hidden;
`;

const ModalList = styled.ScrollView`
    flex: 1;
    marginTop: ${Platform.OS === 'android' ? 60 : 48};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 16;
`;

const SelectInner = styled.View``;

const Reset = styled.TouchableOpacity`
    marginHorizontal: 16;
`;

const ResetText = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 17;
    color: #5bbb5e;
`;

export default class Select extends Component {
    constructor(props) {
        super(props);
    }

    hideEventPopup = () => {
        if (this.props.hideModal) {
            this.props.hideModal();
        }
    };

    resetEventPopup = (index) => {
        if (this.props.onReset) {
            this.props.onReset(index);
        }
    };

    selectItem = (index, name, value) => {
        if (this.props.onSelect) {
            this.props.onSelect(index, name, value);
        }
    };

    render() {
        const {item, index, visible} = this.props;

        let list = item ? item.value : [];
        let title = item ? item.name : '';

        return (
            item !== null ? <Modal
                isVisible={visible}
                onBackdropPress={() => this.hideEventPopup()}
                animationIn="slideInRight"
                animationOut="slideOutRight"
                style={{
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    margin: 0,
                }}>
                <ModalContent>
                    <ModalHeader>
                        <IconBack onPress={() => this.hideEventPopup()}/>
                        <ModalHeaderInner><ModalHeaderText>{title}</ModalHeaderText></ModalHeaderInner>
                        <Reset onPress={() => this.resetEventPopup(index)}><ResetText>Сбросить</ResetText></Reset>
                    </ModalHeader>
                    <ModalList>
                        {list.length > 0 && list.map((item, i) => (
                            <ListItem
                                key={item.value}
                                title={item.label}
                                onPress={() => this.selectItem(index, item.name, item.value)}
                                bottomDivider
                                chevron={item.checked ? <IconCheck/> : null}
                            />
                        ))}
                        <SelectInner style={{marginBottom: 16}}/>
                    </ModalList>
                </ModalContent>
            </Modal> : null
        );
    }
}
