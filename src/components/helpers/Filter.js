import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {Button} from 'react-native-elements';
import {IconCloseBtn, IconEmpty, IconChevron} from '../icons';
import FilterCheckboxes from './FilterCheckboxes';
import FilterRange from './FilterRange';
import styled from 'styled-components/native';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height - StatusBar.currentHeight};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: ${Platform.OS === 'android' ? 56 : 44};
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View``;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 28 : 0}
`;

const ModalList = styled.ScrollView`
    flex: 1;
    marginTop: 44;
`;

const ModalInner = styled.View`
    paddingLeft: 8;
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 86;
`;

const IconChevronWrap = styled.View`
    transform: rotate(-90deg);
    width: 28;
    marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    justifyContent: center; 
    alignItems: center;   
`;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginVertical: 0;
`;

const ListItemContainer = styled.TouchableOpacity`
    width: 100%;
    height: 44;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingVertical: 2.5; 
    paddingRight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 8 : 0};
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
`;

const ListInner = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 76) : (Dimensions.get('window').width - 52)};
`;

const Divider = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef;
`;

const Checked = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
    fontSize: 13;
`;

const ValuesRow = styled.View`
    flexDirection: row;
    alignItems: center;
`;

const ValueCol = styled.View`
    flexDirection: row;
    alignItems: center;
    marginLeft: 16;
`;

const ValueLabel = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 13;
    color: #231f20;
    paddingRight: 8;
    lineHeight: 15;
`;

const ValueItem = styled(ValueLabel)`
    color: rgba(35, 31, 32, 0.5);
    paddingRight: 4;
`;

const ValueCurrency = styled.Text`
    color: rgba(0, 0, 0, 0.5);
    paddingRight: 0;
    fontSize: 13;
    lineHeight: 15;
    fontFamily: CoreRhino65Bold;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

const Reset = styled.TouchableOpacity`
    marginHorizontal: 16;
`;

const ResetText = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 17;
    color: #5bbb5e;
`;

export default class Select extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filter: props.filter.map(item => {
                item.visible = false;
                switch (item.type) {
                    case 'A':
                        item.selected = false;
                        if ((Number(item.value.min.value) !== Number(item.value_min) || Number(item.value.max.value) !== Number(item.value_max))) {
                            item.selected = true;
                            checkGlobal = true;
                        }
                        break;
                    case 'F':
                        item.checked = 0;
                        item.value.map((obj) => {
                            if (obj.checked) {
                                item.checked++;
                            }
                        });
                        break;
                }
                return item;
            }),
            filtered: props.isFiltered,
            send: props.isFiltered
        };
    }

    hideEventPopup = () => {
        if (this.props.hideModal) {
            this.props.hideModal();
        }
    };

    sendFilter = () => {
        this.hideEventPopup();

        let filter = {};

        this.state.filter.map(item => {
            switch (item.type) {
                case 'A':
                    if (item.selected) {
                        filter[item.value.min.name] = item.value.min.value;
                        filter[item.value.max.name] = item.value.max.value;
                    }
                    break;
                case 'F':
                    if (item.checked && item.checked > 0) {
                        item.value.map(obj => {
                            if (!filter[obj.name]) {
                                filter[obj.name] = [];
                            }
                            if (obj.checked) {
                                filter[obj.name].push(obj.value);
                            }
                        })
                    }
                    break;
            }
        });

        if (this.props.onSend) {
            this.props.onSend(Object.keys(filter).length > 0 ? filter : null);
        }
    };

    showFilter = (index) => {
        this.setState(prevState => ({
            ...prevState,
            filter: prevState.filter.map((list, idx) => {
                if (idx === index) {
                    return {
                        ...list,
                        visible: true,
                    };
                }
                return list;
            }),
        }));
    };

    hideFilter = (index) => {
        this.setState(prevState => ({
            ...prevState,
            filter: prevState.filter.map((list, idx) => {
                if (idx === index) {
                    return {
                        ...list,
                        visible: false,
                    };
                }
                return list;
            }),
        }));
    };

    setFilterItemRange = (index, min, max) => {
        const {filter} = this.state;
        let prop = filter[index];

        if (prop && prop.value) {
            this.setState(prevState => ({
                ...prevState,
                filter: prevState.filter.map((item, idx) => {
                    if (idx === index) {
                        return {
                            ...item,
                            value: {
                                ...item.value,
                                min: {
                                    ...item.value.min,
                                    value: min,
                                },
                                max: {
                                    ...item.value.max,
                                    value: max,
                                },
                            },
                            selected: true,
                        };
                    }
                    return item;
                }),
            }));
            this.setState({
                filtered: true,
            });
        }
    };

    resetFilterItemRange = (index) => {
        const {filter} = this.state;
        let prop = filter[index];
        let checkGlobal = false;
        if (prop) {
            this.setState(prevState => ({
                ...prevState,
                filter: prevState.filter.map((item, idx) => {
                    if (idx === index) {
                        return {
                            ...item,
                            value: {
                                ...item.value,
                                min: {
                                    ...item.value.min,
                                    value: item.value_min,
                                },
                                max: {
                                    ...item.value.max,
                                    value: item.value_max,
                                },
                            },
                        };
                    }
                    switch (item.type) {
                        case 'A':
                            item.selected = false;
                            if ((Number(item.value.min.value) !== Number(item.value_min) || Number(item.value.max.value) !== Number(item.value_max))) {
                                item.selected = true;
                                checkGlobal = true;
                            }
                            break;
                        case 'F':
                            item.checked = 0;
                            item.value.map((obj, j) => {
                                if (obj.checked) {
                                    item.checked++;
                                }
                            });
                            break;
                    }

                    return item;
                }),
            }));
            this.setState({
                filtered: checkGlobal,
            });
        }
    };

    selectFilterItem = (index, name, value) => {
        const {filter} = this.state;
        let prop = filter[index];
        let checkGlobal = false;
        if (prop) {
            let itemIndex;
            prop.value.map((item, i) => {
                if (item.value === value) {
                    itemIndex = i;
                }
            });
            this.setState(prevState => ({
                ...prevState,
                filter: prevState.filter.map((item, idx) => {
                    item.checked = 0;
                    if (idx === index) {
                        return {
                            ...item,
                            value: item.value.map((obj, j) => {
                                if (item.multiple === false && j !== itemIndex) {
                                    obj.checked = false;
                                }
                                if (j === itemIndex) {
                                    obj.checked = !obj.checked;
                                }
                                if (obj.checked) {
                                    item.checked++;
                                }
                                return obj;
                            }),
                        };
                    }

                    return item;
                }),
            }));
            this.setState({
                filtered: true,
            });
        }
    };

    resetFilterItems = (index) => {
        const {filter} = this.state;
        let prop = filter[index];
        let checkGlobal = false;
        if (prop) {
            this.setState(prevState => ({
                ...prevState,
                filter: prevState.filter.map((item, idx) => {
                    if (idx === index) {
                        return {
                            ...item,
                            value: item.value.map((obj, j) => {
                                obj.checked = false;
                                return obj;
                            }),
                        };
                    }
                    switch (item.type) {
                        case 'A':
                            item.selected = false;
                            if ((Number(item.value.min.value) !== Number(item.value_min) || Number(item.value.max.value) !== Number(item.value_max))) {
                                item.selected = true;
                                checkGlobal = true;
                            }
                            break;
                        case 'F':
                            item.checked = 0;
                            item.value.map((obj, j) => {
                                if (obj.checked) {
                                    item.checked++;
                                }
                            });
                            break;
                    }
                    return item;
                }),
            }));
            this.setState({
                filtered: checkGlobal,
            });
        }
    };

    resetFilter = () => {
        const {filter} = this.state;
        this.setState({
            filtered: false,
            send: this.props.isFiltered
        });
        filter.map((item, index) => {
            switch (item.type) {
                case 'A':
                    this.resetFilterItemRange(index);
                    break;
                case 'F':
                    this.resetFilterItems(index);
                    break;
            }
        });
    };

    render() {
        const {visible} = this.props;
        const {filter, filtered, send} = this.state;

        return (
            <Wrap>
                {filter && filter.length > 0 ? <Modal
                    isVisible={visible}
                    onBackdropPress={() => this.hideEventPopup()}
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                    }}>
                    <ModalContent>
                        <ModalHeader>
                            <IconCloseBtn onPress={() => this.hideEventPopup()}/>
                            <ModalHeaderInner style={{
                                width: filtered ? (Platform.OS === 'android' ? (Dimensions.get('window').width - 159) : (Dimensions.get('window').width - 120)) : '90%',
                            }}><ModalHeaderText>Фильтр</ModalHeaderText></ModalHeaderInner>
                            {filtered ? <Reset onPress={() => this.resetFilter()}><ResetText>Сбросить</ResetText></Reset> : <IconEmpty/>}
                        </ModalHeader>
                        <ModalList>
                            <ModalInner>
                                {filter.map((item, index) => (
                                    <ListItemWrap key={index}>
                                        <ListItemContainer
                                            onPress={() => this.showFilter(index)}>
                                            <ListInner>
                                                <ListItem numberOfLines={2}
                                                          style={{
                                                              width: (!!item.checked && item.checked > 0) ? '70%' : 'auto',
                                                          }}>{item.name}</ListItem>
                                                {(!!item.checked && item.checked > 0) && <Checked>Выбрано {item.checked}</Checked>}
                                                {(!!item.selected && item.selected === true && (item.value.min.value !== item.value_min || item.value.max.value !== item.value_max)) &&
                                                <ValuesRow>
                                                    <ValueCol>
                                                        <ValueLabel>от</ValueLabel>
                                                        <ValueItem>{item.value.min.value}</ValueItem>
                                                        {/*<ValueCurrency>₸</ValueCurrency>*/}
                                                    </ValueCol>
                                                    <ValueCol>
                                                        <ValueLabel>до</ValueLabel>
                                                        <ValueItem>{item.value.max.value}</ValueItem>
                                                        {/*<ValueCurrency>₸</ValueCurrency>*/}
                                                    </ValueCol>
                                                </ValuesRow>}
                                            </ListInner>
                                            <IconChevronWrap><IconChevron/></IconChevronWrap>
                                        </ListItemContainer>
                                        <Divider/>
                                        {item.type === 'A' && item.visible && <FilterRange
                                            onChange={(index, min, max) => this.setFilterItemRange(index, min, max)}
                                            onReset={(index) => this.resetFilterItemRange(index)}
                                            visible={this.state.filter[index].visible}
                                            hideModal={() => this.hideFilter(index)}
                                            index={index}
                                            item={item}/>}
                                        {item.type === 'F' && item.visible && <FilterCheckboxes
                                            onSelect={(index, name, value) => this.selectFilterItem(index, name, value)}
                                            onReset={(index) => this.resetFilterItems(index)}
                                            visible={this.state.filter[index].visible}
                                            hideModal={() => this.hideFilter(index)}
                                            index={index}
                                            item={item}/>}
                                    </ListItemWrap>
                                ))}
                            </ModalInner>
                        </ModalList>
                        {(filtered || send) && <ButtonView>
                            <Button title="Применить" onPress={() => this.sendFilter()}/>
                        </ButtonView>}
                    </ModalContent>
                </Modal> : null}
            </Wrap>
        );
    }
}
