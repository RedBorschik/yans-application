import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {Button} from 'react-native-elements';
import {IconCloseBtn, IconEmpty} from '../icons';
import Select from '../../components/helpers/Select';
import Input from '../../components/helpers/Input';
import styled from 'styled-components/native';
import {connect} from 'react-redux';
import validator from 'validator';
import {Alert} from 'react-native';

import citiesActions from '../../actions/Cities';
import addressActions from '../../actions/Address';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height - StatusBar.currentHeight};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: ${Platform.OS === 'android' ? 56 : 44};
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: 90%
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 28 : 0}
`;

const ModalList = styled.ScrollView`
    flex: 1;
    marginTop: 44;
`;

const ModalInner = styled.View`
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 86;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

class Create extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: {
                city: '',
                city_name: '',
                address: '',
                contact_person: '',
                name_shop: '',
                phone: '',
            },
            errors: {
                city: false,
                address: false,
                contact_person: false,
                name_shop: false,
                phone: false,
            },
        };
    }

    hideEventPopup = () => {
        if (this.props.hideModal) {
            this.props.hideModal();
        }
    };

    componentDidMount() {
        if (this.props.cities.length <= 0) {
            this.props.fetchCities();
        }
    }

    selectCity = (label, value) => {
        this.setState(prevState => ({
            item: {
                ...prevState.item,
                city: value,
                city_name: label,
            },
        }), () => {
            const {item} = this.state;
            let checkCity = item.city === null;
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    city: checkCity,
                },
            }));
        });
    };

    validateForm = () => {
        const {item} = this.state;
        let phone = item.phone.replace(/[^0-9.]/g, '');
        let checkNameShop = validator.isEmpty(item.name_shop);
        let checkCity = item.city === null;
        let checkPhone = !validator.isMobilePhone(phone) || validator.isEmpty(item.phone);
        let checkPerson = validator.isEmpty(item.contact_person);
        let checkAddress = validator.isEmpty(item.address);

        this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                city: checkCity,
                address: checkAddress,
                contact_person: checkPerson,
                name_shop: checkNameShop,
                phone: checkPhone,
            },
        }));

        return !checkAddress && !checkCity && !checkPhone && !checkAddress && !checkPerson;
    };

    getUpdatedProps = () => {
        return this.props;
    };


    submitForm = () => {
        if (this.validateForm() === false) return false;

        const {item} = this.state;
        let data = {
            city: item.city,
            city_name: item.city_name,
            address: item.address,
            contact_person: item.contact_person,
            name_shop: item.name_shop,
            phone: item.phone,
        };

        this.props.addFetchAddress(data).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                this.hideEventPopup();
            }
        });
    };

    render() {
        const {visible, cities} = this.props;
        const {item, errors} = this.state;

        return (
            <Wrap>
                <Modal
                    isVisible={visible}
                    onBackdropPress={() => this.hideEventPopup()}
                    onSwipeComplete={() => this.hideEventPopup()}
                    swipeDirection={'left'}
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                    }}>
                    <ModalContent>
                        <ModalHeader>
                            <IconCloseBtn onPress={() => this.hideEventPopup()}/>
                            <ModalHeaderInner><ModalHeaderText>Новый адрес доставки</ModalHeaderText></ModalHeaderInner><IconEmpty/>
                        </ModalHeader>
                        <ModalList>
                            <ModalInner>
                                <Input
                                    label="Название организации"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, name_shop: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, name_shop: validator.isEmpty(item.name_shop)}}))}}
                                    error={errors.name_shop}
                                    value={item.name_shop}/>
                                <Input
                                    label="Контакное лицо"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, contact_person: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, contact_person: validator.isEmpty(item.contact_person)}}))}}
                                    error={errors.contact_person}
                                    value={item.contact_person}/>
                                <Input
                                    label="Телефон"
                                    type="tel"
                                    keyboardType={'phone-pad'}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, phone: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, phone: !validator.isMobilePhone(item.phone.replace(/[^0-9.]/g, '')) || validator.isEmpty(item.phone)}}))}}
                                    error={errors.phone}
                                    value={item.phone}/>
                                <Select
                                    title={'Выбор города'}
                                    label={'Город:'}
                                    text={item.city_name}
                                    list={cities}
                                    selected={item.city}
                                    error={errors.city}
                                    onSelect={(label, value) => this.selectCity(label, value)}/>
                                <Input
                                    label="Адрес"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, address: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, address: validator.isEmpty(item.address)}}))}}
                                    error={errors.address}
                                    value={item.address}/>
                            </ModalInner>
                        </ModalList>
                        <ButtonView>
                            <Button title="Сохранить" onPress={() => this.submitForm()}/>
                        </ButtonView>
                    </ModalContent>
                </Modal>
            </Wrap>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    addFetchAddress: (params, is_popup) => dispatch(addressActions.addFetchAddress(params, is_popup)),
    fetchCities: () => dispatch(citiesActions.fetchCities()),
});

const mapStateToProps = (state, props) => ({
    cities: state.cities.items,
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);
