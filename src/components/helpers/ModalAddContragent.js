import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {Button} from 'react-native-elements';
import {IconCloseBtn, IconEmpty} from '../icons';
import Select from '../../components/helpers/Select';
import Input from '../../components/helpers/Input';
import styled from 'styled-components/native';
import {connect} from 'react-redux';
import validator from 'validator';
import {Alert} from 'react-native';

import legaltypesActions from '../../actions/LegalTypes';
import contragentsActions from '../../actions/Contragents';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height - StatusBar.currentHeight};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: ${Platform.OS === 'android' ? 56 : 44};
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: 90%
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 28 : 0}
`;

const ModalList = styled.ScrollView`
    flex: 1;
    marginTop: 44;
`;

const ModalInner = styled.View`
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 86;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

class Create extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: {
                name: '',
                inn: '',
                bik: '',
                address: '',
                bank: '',
                rs: '',
                form: '',
                form_name: '',
            },
            errors: {
                name: false,
                inn: false,
                bik: false,
                address: false,
                bank: false,
                rs: false,
                form: false,
            },
        };
    }

    hideEventPopup = () => {
        if (this.props.hideModal) {
            this.props.hideModal();
        }
    };

    componentDidMount() {
        if (this.props.types.length <= 0) {
            this.props.listFetchLegalTypes();
        }
    }

    selectForm = (label, value) => {
        this.setState(prevState => ({
            item: {
                ...prevState.item,
                form_name: label,
                form: value,
            },
        }), () => {
            const {item} = this.state;
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    form: validator.isEmpty(item.form),
                },
            }));
        });
    };

    validateForm = () => {
        const {item} = this.state;
        let checkName = validator.isEmpty(item.name);
        let checkInn = !validator.isNumeric(item.inn) || !validator.isLength(item.inn, {min: 12, max: 12});
        let checkBik = !validator.isAlphanumeric(item.bik) || !validator.isLength(item.bik, {min: 8, max: 8});
        let checkRs = !validator.isLength(item.rs, {min: 20, max: 20}) || !validator.isAlphanumeric(item.rs);
        let checkAddress = validator.isEmpty(item.address);
        let checkBank = validator.isEmpty(item.bank);
        let checkForm = validator.isEmpty(item.form);

        this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                name: checkName,
                inn: checkInn,
                bik: checkBik,
                address: checkAddress,
                bank: checkBank,
                rs: checkRs,
                form: checkForm,
            },
        }));

        return !checkName && !checkInn && !checkBik && !checkAddress && !checkBank && !checkRs && !checkForm;
    };

    getUpdatedProps = () => {
        return this.props;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        this.props.addFetchContragents(this.state.item, true).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                this.hideEventPopup();
            }
        });
    };

    render() {
        const {visible, types} = this.props;
        const {item, errors} = this.state;

        return (
            <Wrap>
                <Modal
                    isVisible={visible}
                    onBackdropPress={() => this.hideEventPopup()}
                    onSwipeComplete={() => this.hideEventPopup()}
                    swipeDirection={'left'}
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                    }}>
                    <ModalContent>
                        <ModalHeader>
                            <IconCloseBtn onPress={() => this.hideEventPopup()}/>
                            <ModalHeaderInner><ModalHeaderText>Новый контрагент</ModalHeaderText></ModalHeaderInner><IconEmpty/>
                        </ModalHeader>
                        <ModalList>
                            <ModalInner>
                                <Select
                                    title={'Выбор формы юр. лица'}
                                    label={'Форма'}
                                    text={item.form_name}
                                    list={types}
                                    selected={item.form}
                                    error={errors.form}
                                    onSelect={(label, value) => this.selectForm(label, value)}/>
                                <Input
                                    label="Название организации"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, name: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, name: validator.isEmpty(item.name)}}))}}
                                    error={errors.name}
                                    value={item.name}/>
                                <Input
                                    label="ИНН/БИН"
                                    keyboardType="numeric"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, inn: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, inn: !validator.isNumeric(item.inn) || !validator.isLength(item.inn, {min: 12, max: 12})}}))}}
                                    error={errors.inn}
                                    value={item.inn}/>
                                <Input
                                    label="Банк"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, bank: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, bank: validator.isEmpty(item.bank)}}))}}
                                    error={errors.bank}
                                    value={item.bank}/>
                                <Input
                                    label="Расчетный счет"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, rs: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, rs: !validator.isLength(item.rs, {min: 20, max: 20}) || !validator.isAlphanumeric(item.rs)}}))}}
                                    error={errors.rs}
                                    value={item.rs}/>
                                <Input
                                    label="БИК"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, bik: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, bik: !validator.isAlphanumeric(item.bik) || !validator.isLength(item.bik, {min: 8, max: 8})}}))}}
                                    error={errors.bik}
                                    value={item.bik}/>
                                <Input
                                    label="Юридический адрес"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, address: value}}))}
                                    onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, address: validator.isEmpty(item.address)}}))}}
                                    error={errors.address}
                                    value={item.address}/>
                            </ModalInner>
                        </ModalList>
                        <ButtonView>
                            <Button title="Сохранить" onPress={() => this.submitForm()}/>
                        </ButtonView>
                    </ModalContent>
                </Modal>
            </Wrap>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    addFetchContragents: (data, is_popup) => dispatch(contragentsActions.addFetchContragents(data, is_popup)),
    listFetchLegalTypes: () => dispatch(legaltypesActions.listFetchLegalTypes()),
});

const mapStateToProps = (state, props) => ({
    types: state.legaltypes.types,
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);
