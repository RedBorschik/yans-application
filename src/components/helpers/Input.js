import React, {Component} from 'react';
import {View, Text, Animated} from 'react-native';
import {Input} from 'react-native-elements';
import TextInputMask from 'react-native-text-input-mask';
import Error from './Error';
import styled from 'styled-components/native';

const Info = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
    fontSize: 13;
    lineHeight: 15;
`;

export default class FloatingLabelInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFocused: false,
        };
        this._animatedIsFocused = new Animated.Value(props.value === '' || props.value.indexOf('+7') >= 0 ? 0 : 1);
    }

    handleFocus = () => this.setState({isFocused: true});
    handleBlur = () => {
        this.setState({isFocused: false});
        if (this.props.onBlur) {
            this.props.onBlur();
        }
    };

    componentDidUpdate() {
        Animated.timing(this._animatedIsFocused, {
            toValue: (this.state.isFocused || this.props.value !== '' || (this.input && this.input.value && this.input.value.indexOf('+7') >= 0)) ? 1 : 0,
            duration: 200,
        }).start();
    }

    render() {
        const {label, ...props} = this.props;
        const labelStyle = {
            position: 'absolute',
            left: 0,
            top: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [12.5, 3.5],
            }),
            fontSize: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [15, 13],
            }),
            lineHeight: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [18, 15],
            }),
            color: 'rgba(35, 31, 32, 0.6)',
            fontFamily: 'CoreRhino45Regular',
        };
        return (
            <View style={{paddingTop: 16}}>
                <Animated.Text style={labelStyle}>
                    {label} {/*{props.infoText && props.infoText !== '' ? <Info>{props.infoText}</Info>: null}*/}
                </Animated.Text>
                {props.type && props.type === 'tel' ? <TextInputMask
                    {...props}
                    keyboardType={'phone-pad'}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    refInput={ref => { this.input = ref; }}
                    blurOnSubmit
                    mask={'+7 ([000]) [000] [00] [00]'}
                    placeholder={this.state.isFocused ? '+7 (___) ___ __ __' : ''}
                    style={{
                        borderBottomWidth: 1,
                        fontSize: 15,
                        lineHeight: 18,
                        fontFamily: 'CoreRhino45Regular',
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        height: 28,
                        borderBottomColor: '#d3d3d3',
                    }}
                /> : <Input
                    {...props}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    blurOnSubmit
                />}
                <Error visible={props.error} text={props.errorText || 'Пожалуйста, заполните поле'}/>
            </View>
        );
    }
}