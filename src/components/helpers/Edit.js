import React from 'react';
import styled from 'styled-components/native';
import Icon from '../icons/Edit';

const IconLeftContainer = styled.TouchableOpacity`
  height: 28;
  width: 28;
  marginLeft: 6;
  justifyContent: center;
`;

const Edit = ({onPress, active, style}) => (
    <IconLeftContainer onPress={onPress} style={style}>
       <Icon/>
    </IconLeftContainer>
);

export default Edit;
