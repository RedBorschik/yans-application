import React, {Component} from 'react';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

const FormValidationMessage = styled.Text`
  width: 100%;
  fontSize: 12;
  lineHeight: 16;
  color: ${colors.RED};
`;

export default class Error extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            (this.props.visible) ? <FormValidationMessage style={this.props.style}>{this.props.text}</FormValidationMessage> : null
        );
    }
}
