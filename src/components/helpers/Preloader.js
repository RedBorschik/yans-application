import React, {Component} from 'react';
import {Image} from 'react-native';

export default class Preloader extends Component {
    render() {
        return (
            <Image
                style={{
                    width: 34,
                    height: 34,
                }}
                source={require('../../../assets/images/preloader.gif')}
            />
        );
    }
}