import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {IconBack} from '../../components';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import styled from 'styled-components/native';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height - StatusBar.currentHeight};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: ${Platform.OS === 'android' ? 56 : 44};
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 159) : (Dimensions.get('window').width - 120)};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 28 : 0};
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: ${Platform.OS === 'android' ? 'left' : 'center'};
    width: 100%;
    overflow: hidden;
`;

const ModalList = styled.View`
    flex: 1;
    marginTop: ${Platform.OS === 'android' ? 60 : 48};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingRight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 16;
`;

const Reset = styled.TouchableOpacity`
    marginHorizontal: 16;
`;

const ResetText = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 17;
    color: #5bbb5e;
`;

const ValuesRow = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    marginBottom: 20;
`;

const ValueCol = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const ValueLabel = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 17;
    color: #231f20;
    paddingRight: 16;
    lineHeight: 20;
`;

const ValueItem = styled(ValueLabel)`
    color: rgba(35, 31, 32, 0.5);
    paddingRight: 4;
`;

const ValueCurrency = styled.Text`
    color: rgba(0, 0, 0, 0.5);
    paddingRight: 0;
    fontSize: 20;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
`;

export default class FilterRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            multiSliderValue: [Number(this.props.item.value.min.value), Number(this.props.item.value.max.value)],
        }
    }

    hideEventPopup = () => {
        if (this.props.hideModal) {
            this.props.hideModal();
        }
    };

    resetEventPopup = (index) => {
        this.setState({
            multiSliderValue: [Number(this.props.item.value_min), Number(this.props.item.value_max)],
        });
        if (this.props.onReset) {
            this.props.onReset(index);
        }
    };

    multiSliderValuesChange = values => {
        this.setState({
            multiSliderValue: values,
        })
    };

    changeValue = (values) => {
        const {index} = this.props;
        if (this.props.onChange && Array.isArray(values) && values.length === 2) {
            this.props.onChange(index, values[0], values[1]);
        }
    };

    render() {
        const {item, index, visible} = this.props;
        const {multiSliderValue} = this.state;

        return (
            item !== null ? <Modal
                isVisible={visible}
                onBackdropPress={() => this.hideEventPopup()}
                animationIn="slideInRight"
                animationOut="slideOutRight"
                style={{
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    margin: 0,
                }}>
                <ModalContent>
                    <ModalHeader>
                        <IconBack onPress={() => this.hideEventPopup()}/>
                        <ModalHeaderInner><ModalHeaderText>{item.name}</ModalHeaderText></ModalHeaderInner>
                        <Reset onPress={() => this.resetEventPopup(index)}><ResetText>Сбросить</ResetText></Reset>
                    </ModalHeader>
                    <ModalList>
                        <ValuesRow>
                            <ValueCol>
                                <ValueLabel>от</ValueLabel>
                                <ValueItem>{multiSliderValue[0]}</ValueItem>
                                {/*<ValueCurrency>₸</ValueCurrency>*/}
                            </ValueCol>
                            <ValueCol>
                                <ValueLabel>до</ValueLabel>
                                <ValueItem>{multiSliderValue[1]}</ValueItem>
                                {/*<ValueCurrency>₸</ValueCurrency>*/}
                            </ValueCol>
                        </ValuesRow>
                        <MultiSlider
                            values={multiSliderValue}
                            sliderLength={Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? (Dimensions.get('window').width - 36) : (Dimensions.get('window').width - 16)}
                            min={Number(item.value_min)}
                            max={Number(item.value_max)}
                            onValuesChangeFinish={(values) => this.changeValue(values)}
                            onValuesChange={(values) => this.multiSliderValuesChange(values)}
                            step={1}
                            allowOverlap
                            snapped
                        />
                    </ModalList>
                </ModalContent>
            </Modal> : null
        );
    }
}
