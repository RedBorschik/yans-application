import React, {Component} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Preloader from './Preloader';

export default class LoaderPage extends Component {
    render() {
        const {
            loading,
            ...attributes
        } = this.props;
        return (
            <View style={styles.activityIndicatorWrapper}>
                <Preloader/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    activityIndicatorWrapper: {
        width: '100%',
        height: 40,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
});