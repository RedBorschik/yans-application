import React, {Component} from 'react';
import Modal from 'react-native-modal';
import {Dimensions, StatusBar, Platform} from 'react-native';
import {IconCloseBtn, IconEmpty, IconCheck, IconChevron} from '../../components';
import {ListItem} from 'react-native-elements';
import styled from 'styled-components/native';
import Error from './Error';

const Wrap = styled.View`
    flex: 1;
`;

const ModalContent = styled.View`
    width: ${(Dimensions.get('window').width)};
    height: ${Dimensions.get('window').height};
    backgroundColor: #FFFFFF;
`;

const ModalHeader = styled.View`
    width: 100%;
    height: 44;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    position: absolute;
    left: 0;
    top: 0;
    zIndex: 1;
`;

const ModalHeaderInner = styled.View`
    width: 80%;
    textAlign: center;
`;

const ModalHeaderText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
    textAlign: center;
`;

const ModalList = styled.ScrollView`
    flex: 1;
    marginTop: 48;
    paddingLeft: 8;
    paddingTop: 8;
    paddingBottom: 16;
`;

const SelectView = styled.TouchableOpacity`
    width: 100%;
    height: 44;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    borderBottomWidth: 1;
    borderBottomColor: #d3d3d3;
    paddingVertical: 2.5; 
`;

const SelectInner = styled.View``;

const SelectTopLabel = styled.Text`
    paddingTop: 1;
    paddingBottom: 2;
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 13;
    lineHeight: 15;
`;

const SelectLabel = styled.Text`
    paddingTop: 1;
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
`;

const SelectText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 20; 
    fontFamily: CoreRhino45Regular;
`;

const IconChevronWrap = styled.View`
    transform: rotate(-90deg);
    height: 44;
    width: 28;
    marginLeft: ${Platform.OS === 'android' ? 16 : 8};
    justifyContent: center; 
    alignItems: center;   
`;

export default class Select extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        };
    }

    showEventPopup = () => {
        this.setState({visible: true});
    };

    hideEventPopup = () => {
        this.setState({visible: false});
    };

    selectItem = (label, value) => {
        this.hideEventPopup();
        if (this.props.onSelect) {
            this.props.onSelect(label, value);
        }
    };

    render() {
        const {list, title, label, selected, text, error, errorText} = this.props;
        const {visible} = this.state;
        return (
            <Wrap>
                <SelectView onPress={() => this.showEventPopup()}>
                    <SelectInner>
                        {text !== 0 && text !== '' ? <SelectTopLabel>{label}</SelectTopLabel> : <SelectLabel>{label}</SelectLabel>}
                        {text !== 0 && text !== '' && <SelectText>{text}</SelectText>}
                    </SelectInner>
                    <IconChevronWrap><IconChevron/></IconChevronWrap>
                </SelectView>
                <Error visible={error} text={errorText || 'Пожалуйста, выберите значение'}/>
                {list && list.length > 0 && <Modal
                    isVisible={visible}
                    onBackdropPress={() => this.hideEventPopup()}
                    onSwipeComplete={() => this.hideEventPopup()}
                    swipeDirection={'left'}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 0,
                    }}>
                    <ModalContent>
                        <ModalHeader>
                            <IconCloseBtn onPress={() => this.hideEventPopup()}/>
                            <ModalHeaderInner><ModalHeaderText>{title}</ModalHeaderText></ModalHeaderInner>
                            <IconEmpty/>
                        </ModalHeader>
                        <ModalList>
                            {list.map((item, i) => (
                                <ListItem
                                    key={item.id || item.code}
                                    title={item.name}
                                    onPress={() => this.selectItem(item.name, item.id || item.code)}
                                    bottomDivider
                                    chevron={(selected && (item.code ? item.code.toString() : item.id.toString()) === selected.toString()) ? <IconCheck/> : null}
                                />
                            ))}
                            <SelectInner style={{marginBottom: 16}}/>
                        </ModalList>
                    </ModalContent>
                </Modal>}
            </Wrap>
        );
    }
}
