import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import ImageCard from './elements/ImageCard';
import ComplectItem from './elements/ComplectItem';
import ComplectItemSm from './elements/ComplectItemSm';

const Entities = require('html-entities').XmlEntities;

import {Slider} from '../helpers';
import {Title, Label, Quantity} from './elements';
import {IconFavorite, IconChevron} from '../icons';
import Modal from 'react-native-modal';
import {Button} from 'react-native-elements';
import {Alert} from 'react-native';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';

import styled from 'styled-components/native';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
`;

const MutedText = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
`;

const MutedText2 = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
`;

const Wrap = styled.View`
    flex: 1; 
`;

const ScrollWrap = styled.ScrollView``;

const Item = styled.View` 
    flex: 1;
    paddingTop: ${Platform.OS === 'android' ? 0 : 8};
    paddingBottom: 76;
`;

const SliderWrap = styled.View`
    height: ${Platform.OS === 'android' ? 224 : 189};
    paddingHorizontal: 1;
`;

const Content = styled.View`
    flex: 1;
    marginTop: 8;
    paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
    paddingBottom: 16;
    flexDirection: column;
    justifyContent: space-between;
`;

const InfoWrap = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    alignItems: flex-start;
    paddingTop: 10;
`;

const InfoValue = styled(MutedText)`
    fontSize: 12;
`;

const SmallPrice = styled(MutedText)`
    fontSize: 13;
    lineHeight: 13;
    paddingTop: 3;
`;

const Currency = styled(MutedText)`
    fontSize: 15;
`;

const InfoSmallList = styled.View``;

const InfoSmallItem = styled(MutedText)`
    fontSize: 12;
    lineHeight: 15;
    marginBottom: 4;
`;

const ButtonView = styled.View`
    position: absolute;
    bottom: 24;
    left: 28px;
    right: 28px;
`;

const CollapseTitle = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingVertical: 13;
`;

const CollapseName = styled(MainText)`
    fontFamily: CoreRhino65Bold;
`;

const ComplectsName = styled(CollapseName)`
    marginVertical: 24;
`;

const PropItem = styled.View`
    flexDirection: row;
    justifyContent: flex-end;
    alignItems: baseline;
    flexWrap: wrap;
    marginVertical: 13;
`;

const PropText = styled(MutedText2)``;

const PropDelimer = styled.View`
    flexGrow: 1;
    flexShrink: 1; 
    flexBasis: 0;
    overflow: hidden;
`;

const PropDelimerText = styled(MutedText2)`
    marginHorizontal: 4;
`;

const IconChevronWrap = styled.View`
    width: 28;
    height: 28;
    justifyContent: center;
    alignItems: center;
`;

const Description = styled(MainText)`
    lineHeight: 22;
    marginTop: 5;
`;

const ModalContent = styled.View`
    backgroundColor: #FFFFFF;
    width: 100%;
    borderTopLeftRadius: 10;
    borderTopRightRadius: 10;
    paddingVertical: 16;
    paddingHorizontal: 16;
`;

const ModalName = styled(MainText)`
    paddingTop: 8;
`;

const TotalPrice = styled.View`
    paddingVertical: 24;
    marginVertical: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const TotalPricePage = styled(TotalPrice)`
    paddingVertical: 0;
`;

const TotalPriceLabel = styled.Text`
    color: #1f4c65;
    fontSize: 17;
    lineHeight: 28;
    fontFamily: CoreRhino65Bold;
`;

const TotalPriceValue = styled(TotalPriceLabel)`
    fontSize: 23;
`;

const TotalPriceCurrency = styled(TotalPriceLabel)`
    fontSize: 20;
`;

const ModalDecorLine = styled.View`
    width: 36;
    height: 5;
    borderRadius: 3;
    backgroundColor: #ffffff;
    marginBottom: 8;
`;

const ComplectsWrap = styled.View``;

const Mtop = styled.View`
    marginTop: 24;
`;

export default class SmallCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            packing: [],
            packingNames: [],
            selectedIndex: 0,
            name: '',
            curPack: {
                piecePrice: 0,
                price: 0,
                count: 1,
                quantity: 0,
            },
            showModal: false,
            collapsed: {
                props: false,
                descr: false,
            },
            complects: [],
            totalPrice: 0,
        };
    }

    getCurPacking = () => {
        let packing = this.props.item.packing;
        let codes = ['piece', 'pack', 'box'];
        let names = ['шт', 'упаковк', 'коробка'];
        let idx = [];
        if (packing) {
            let curCodes = Object.keys(packing);
            codes = codes.filter((item, index) => {
                if (curCodes.includes(item)) {
                    idx.push(index);
                    return item;
                }
            });
            names = names.filter((item, index) => {
                if (idx.includes(index)) return item;
            });
        }
        return {
            codes: codes,
            names: names,
        };
    };

    setCurPackData = () => {
        const {item} = this.props;
        let {packing, complects, curPack} = this.state;
        let selectedIndex = this.state.selectedIndex;
        let elemPack = item.packing[packing[selectedIndex]];
        let price = Number(elemPack.price) * Number(curPack.count);
        let quantity = Number(elemPack.quantity_in_pack) * Number(curPack.count);
        let totalPrice = price;

        if (item.complects && item.complects.length > 0 && complects.length > 0) {
            item.complects.map(el => {
                if (complects.indexOf(el.id) >= 0) {
                    let pack = null;
                    let found = Object.keys(el.packing).some(key => {
                        pack = el.packing[key];
                        return quantity % el.packing[key].quantity_in_pack === 0;
                    });
                    if (found && pack) {
                        totalPrice += Number(pack.price) * Number(quantity / pack.quantity_in_pack);
                    }
                }
            });
        }

        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                piecePrice: (Number(elemPack.price) / Number(elemPack.quantity_in_pack)),
                price: (price).toFixed(0),
                quantity: quantity,
            },
            totalPrice: (totalPrice).toFixed(0),
        }));
    };

    addPackCount = () => {
        const {item} = this.props;
        let packing = this.state.packing;
        let selectedIndex = this.state.selectedIndex;
        let curPack = item.packing[packing[selectedIndex]];
        if (this.state.curPack.count >= curPack.quantity) {
            Alert.alert(
                '',
                'К сожалению, на складе осталось только ' + item.quantity + ' единиц товара',
                [
                    {text: 'OK'},
                ],
            );
            return;
        }
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) + 1,
            },
        }), () => this.setCurPackData());
    };

    removePackCount = () => {
        if (this.state.curPack.count <= 1) return;
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) - 1,
            },
        }), () => this.setCurPackData());
    };

    updatePack = (selectedIndex) => {
        this.setState(prevState => ({
            selectedIndex: selectedIndex,
            curPack: {
                ...prevState.curPack,
                count: 1,
            },
        }), () => this.setCurPackData());
    };

    toggleComplectItem = (item) => {
        if (this.state.complects.indexOf(item.id) >= 0) {
            this.setState(prevState => ({
                ...prevState,
                complects: prevState.complects.filter(val => val !== item.id),
            }), () => this.setCurPackData());
        } else {
            this.setState(prevState => ({
                ...prevState,
                complects: [
                    ...prevState.complects,
                    item.id,
                ],
            }), () => this.setCurPackData());
        }
    };

    addBasketEvent = () => {
        const {item, addBasket} = this.props;
        const {curPack, selectedIndex, packing, complects} = this.state;
        let curcode = packing[selectedIndex];
        let products = [{
            id: item.id,
            quantity: curPack.count,
            packing: curcode,
        }];

        if (item.complects && item.complects.length > 0 && complects.length > 0) {
            item.complects.map(el => {
                if (complects.indexOf(el.id) >= 0) {
                    let pack = null;
                    let found = Object.keys(el.packing).some(key => {
                        pack = el.packing[key];
                        return curPack.quantity % el.packing[key].quantity_in_pack === 0;
                    });
                    if (found && pack) {
                        products.push({
                            id: el.id,
                            quantity: curPack.quantity / pack.quantity_in_pack,
                            packing: pack.code,
                        });
                    }
                }
            });
        }

        console.log(products);

        if (addBasket) {
            addBasket(products);
            this.setState({showModal: false});
        }
    };

    componentWillMount() {
        const {item} = this.props;
        /*console.log('item', item);*/
        if (item) {
            let name = new Entities();
            name = name.decode(item.name);

            if (item.packing) {
                let packing = this.getCurPacking().codes;
                let packingNames = this.getCurPacking().names;

                this.setState({
                    packing: packing,
                    packingNames: packingNames,
                    name: name,
                }, () => this.setCurPackData());
            } else {
                this.setState({
                    name: name,
                });
            }
        }
    }

    render() {
        const {item, removeFavorite, addFavorite, toggleComplectItem} = this.props;
        const {name, curPack, selectedIndex, packingNames, showModal, totalPrice, complects} = this.state;
        let images = item.images && item.images.length ? item.images.map(src => {return {img: src};}) : [];

        return (
            item ? <Wrap>
                <ScrollWrap>
                    <Item>
                        <IconFavorite onPress={() => item.in_wish_list ? removeFavorite(item.id) : addFavorite(item.id)} active={item.in_wish_list} style={{position: 'absolute', top: 16, right: 15, zIndex: 9}}/>
                        {(item.label && item.label.code === 'NEWPRODUCT') && <Label style={{top: 20, left: -1}}/>}
                        <SliderWrap>
                            {images.length > 0 ? <Slider
                                items={images}
                                slideWidth={Dimensions.get('window').width - 2}
                                slideHeight={224}
                                resizeMode={'contain'}
                                bottomRadius={0}
                                slideInnerStyles={Platform.OS === 'android' ? {
                                    borderRadius: 0,
                                    elevation: 0,
                                } : {
                                    borderBottomLeftRadius: 0,
                                    borderBottomRightRadius: 0,
                                }}
                            /> : <ImageCard style={Platform.OS === 'android' ? {borderTopLeftRadius: 0, borderTopRightRadius: 0} : {}} image={item.image} resize={'contain'} is_large={true}/>}
                        </SliderWrap>
                        <Content>
                            <InfoWrap>
                                <InfoValue>{!!item.article && 'Арт. ' + item.article}</InfoValue>
                            </InfoWrap>
                            <Title
                                name={name}
                                price={curPack.price}
                                lines={10}
                            />
                            <InfoWrap style={{paddingTop: 16}}>
                                <InfoSmallList>
                                    {(item.packing && item.packing.box) && <InfoSmallItem>кол-во в коробке {item.packing.box.quantity_in_pack}</InfoSmallItem>}
                                    {(item.packing && item.packing.pack) && <InfoSmallItem>кол-во в упаковке {item.packing.pack.quantity_in_pack}</InfoSmallItem>}
                                </InfoSmallList>
                                {curPack.piecePrice !== 0 ? <SmallPrice>{curPack.piecePrice} <Currency>₸/шт</Currency></SmallPrice> : null}
                            </InfoWrap>
                            {item.quantity > 0 ? <Quantity
                                style={{paddingTop: 6, marginBottom: 16}}
                                buttons={packingNames}
                                selectedIndex={selectedIndex}
                                quantity={curPack.quantity}
                                groupPress={(selectedIndex) => this.updatePack(selectedIndex)}
                                removeQuantity={() => this.removePackCount()}
                                addQuantity={() => this.addPackCount()}
                            /> : <MutedText2 style={{marginTop: 12}}>Нет в наличии</MutedText2>}
                            {item.properties && item.properties.length && <Collapse
                                isCollapsed={this.state.collapsed.props}
                                onToggle={(isCollapsed) => this.setState(prevState => ({...prevState.collapsed, collapsed: {props: isCollapsed}}))}>
                                <CollapseHeader>
                                    <CollapseTitle>
                                        <CollapseName>Характеристики</CollapseName>
                                        <IconChevronWrap
                                            style={{
                                                transform: [{rotate: this.state.collapsed.props ? '180deg' : '0deg'}],
                                            }}
                                        ><IconChevron/></IconChevronWrap>
                                    </CollapseTitle>
                                </CollapseHeader>
                                <CollapseBody>
                                    {item.properties.map((item, key) => (
                                        <PropItem key={key}>
                                            <PropText>{item.name}</PropText>
                                            <PropDelimer><PropDelimerText numberOfLines={1}>....................................................................................................</PropDelimerText></PropDelimer>
                                            <PropText style={{textAlign: 'right'}}>{item.value}</PropText>
                                        </PropItem>
                                    ))}
                                </CollapseBody>
                            </Collapse>}
                            {item.description && <Collapse
                                isCollapsed={this.state.collapsed.descr}
                                onToggle={(isCollapsed) => this.setState(prevState => ({...prevState.collapsed, collapsed: {descr: isCollapsed}}))}>
                                <CollapseHeader>
                                    <CollapseTitle>
                                        <CollapseName>Описание</CollapseName>
                                        <IconChevronWrap
                                            style={{
                                                transform: [{rotate: this.state.collapsed.descr ? '180deg' : '0deg'}],
                                            }}
                                        ><IconChevron/></IconChevronWrap>
                                    </CollapseTitle>
                                </CollapseHeader>
                                <CollapseBody>
                                    <Description>{new Entities().decode(item.description)}</Description>
                                </CollapseBody>
                            </Collapse>}

                            {item.complects && item.complects.length > 0 && item.quantity > 0 && <ComplectsWrap>
                                <ComplectsName>Купить комплект:</ComplectsName>
                                {item.complects.map(complect => (<ComplectItem
                                    key={complect.id}
                                    item={complect}
                                    selected={complects.indexOf(complect.id) >= 0}
                                    quantity={curPack.quantity}
                                    toggleComplectItem={(item) => this.toggleComplectItem(item)}
                                    addFavorite={(id) => addFavorite(id)}
                                    removeFavorite={(id) => removeFavorite(id)}
                                />))}
                            </ComplectsWrap>}

                            {totalPrice && totalPrice !== 0 && item.complects && item.complects.length > 0 && item.quantity > 0 ? <TotalPricePage>
                                <TotalPriceLabel>ИТОГО:</TotalPriceLabel>
                                <TotalPriceValue>{totalPrice} <TotalPriceCurrency>₸</TotalPriceCurrency></TotalPriceValue>
                            </TotalPricePage> : null}
                        </Content>
                    </Item>
                </ScrollWrap>

                {item.quantity > 0 && <ButtonView>
                    <Button
                        title="Добавить в корзину"
                        onPress={() => this.setState({showModal: true})}
                        buttonStyle={{
                            marginTop: 22,
                            marginHorizontal: 16,
                        }}
                    />
                </ButtonView>}

                {item.quantity > 0 && <Modal
                    isVisible={showModal}
                    onBackdropPress={() => this.setState({showModal: false})}
                    onSwipeComplete={() => this.setState({showModal: false})}
                    swipeDirection={'down'}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        margin: 0,
                    }}>
                    <ModalDecorLine/>
                    <ModalContent>
                        <InfoValue>{!!item.article && 'Арт. ' + item.article}</InfoValue>
                        <ModalName numberOfLines={1}>{name}</ModalName>
                        <Quantity
                            style={{paddingTop: 24}}
                            buttons={packingNames}
                            selectedIndex={selectedIndex}
                            quantity={curPack.quantity}
                            groupPress={(selectedIndex) => this.updatePack(selectedIndex)}
                            removeQuantity={() => this.removePackCount()}
                            addQuantity={() => this.addPackCount()}
                        />
                        {item.complects && item.complects.length > 0 && <ComplectsWrap>
                            {item.complects.map(complect => (<ComplectItemSm
                                key={complect.id}
                                item={complect}
                                selected={complects.indexOf(complect.id) >= 0}
                                quantity={curPack.quantity}
                                toggleComplectItem={(item) => this.toggleComplectItem(item)}
                            />))}
                        </ComplectsWrap>}
                        {(totalPrice && totalPrice !== 0 && item.complects && item.complects.length > 0) ? <TotalPrice>
                            <TotalPriceLabel>ИТОГО:</TotalPriceLabel>
                            <TotalPriceValue>{totalPrice} <TotalPriceCurrency>₸</TotalPriceCurrency></TotalPriceValue>
                        </TotalPrice> : <Mtop/>}
                        <Button
                            title="Добавить в корзину"
                            onPress={() => this.addBasketEvent()}
                            buttonStyle={{
                                marginTop: 0,
                                marginHorizontal: 16,
                            }}
                        />
                    </ModalContent>
                </Modal>}
            </Wrap> : null
        );
    }
}
