import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import {IconChevron} from '../icons/';
import {ListItem, Divider} from 'react-native-elements';
import {colors} from '../../utils/theme';

const Container = styled.View`
    paddingVertical: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
`;

const Inner = styled.View`
    paddingLeft: 8;
`;

const IconChevronWrap = styled.View`
    transform: rotate(-90deg);
    width: 28;
    marginRight: ${Platform.OS === 'android' ? 8 : 0};
    justifyContent: center; 
    alignItems: center;   
`;

class MenuSectionsList extends Component {
    constructor(props) {
        super(props);
    }

    pressNavigation = (item) => {
       this.props.checkNavigation(item);
    };

    render() {
        const {sections} = this.props;
        return (
            <Container>
                {Platform.OS === 'ios' && <Divider/>}
                <Inner>
                    {
                        sections.map((item, key) => (
                            <ListItem
                                key={item.value_param}
                                title={item.name}
                                onPress={() => this.pressNavigation(item)}
                                bottomDivider
                                chevron={<IconChevronWrap><IconChevron/></IconChevronWrap>}
                                containerStyle={{
                                    paddingLeft: Platform.OS === 'android' ? 8 : 0
                                }}
                            />
                        ))
                    }
                </Inner>
            </Container>
        );
    }
}

export default MenuSectionsList;
