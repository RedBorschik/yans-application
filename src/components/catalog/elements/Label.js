import React, {Component} from 'react';
import styled from 'styled-components/native';
import Icon from '../../icons/NewLabel';

const IconLeftContainer = styled.View`
    height: 20;
    width: 42;
    justifyContent: center;
    position: absolute;
    top: 12;
    left: -1;
    zIndex: 5;
`;

export default class Label extends Component {
    render() {
        return (
            <IconLeftContainer style={this.props.style}>
                <Icon/>
            </IconLeftContainer>
        );
    }
}
