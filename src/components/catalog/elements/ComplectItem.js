import React, {Component} from 'react';
import {Switch, Alert} from 'react-native';
import SmallCard from '../SmallCard';

import styled from 'styled-components/native';

const ItemWrap = styled.View`
    flexDirection: row;
    alignItems: center;
`;

const SwitchWrap = styled.View`
    flex: 1;
    paddingLeft: 64;
`;

export default class ComplectItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: {
                ...props.item,
                selected: props.selected,
            },
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.selected !== this.state.item.selected) {
            this.setState(prevState => ({
                ...prevState,
                item: {
                    ...prevState.item,
                    selected: newProps.selected,
                },
            }));
        }
    }

    toggleComplectItem = (value) => {
        const {item} = this.state;
        if (item.quantity < this.props.quantity) {
            Alert.alert(
                '',
                'К сожалению, на складе осталось только ' + item.quantity + ' единиц товара ' + item.name,
                [
                    {text: 'OK'},
                ],
            );
            return;
        }
        this.setState(prevState => ({
            ...prevState,
            item: {
                ...prevState.item,
                selected: value,
            },
        }));
        this.props.toggleComplectItem(this.state.item);
    };

    render() {
        const {addFavorite, removeFavorite} = this.props;
        const {item} = this.state;
        return (
            <ItemWrap>
                <SmallCard
                    key={item.id}
                    item={item}
                    onPress={() => NavigationService.navigate('Card', {id: item.id, go_back_name: state.routeName}, 'Card' + item.id)}
                    addFavorite={() => addFavorite(item.id)}
                    removeFavorite={() => removeFavorite(item.id)}
                />
                <SwitchWrap>
                    <Switch
                        trackColor={item.selected ? '#5bbb5e61' : '#9e9e9e'}
                        thumbColor={item.selected ? '#5bbb5e' : '#ffffff'}
                        onValueChange={(value) => this.toggleComplectItem(value)}
                        value={item.selected}/>
                </SwitchWrap>
            </ItemWrap>
        );
    }
}
