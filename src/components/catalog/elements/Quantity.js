import React, {Component} from 'react';

import {Icon,  ButtonGroup} from 'react-native-elements';

import styled from 'styled-components/native';

const QuantityWrap = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    paddingTop: 16;
`;

const Counter = styled.View`
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    marginLeft: 4;
`;

const CounterBtn = styled.TouchableOpacity`
    width: 24;
    height: 24;
    backgroundColor: rgb(170, 218, 247);
    borderRadius: 50;
    justifyContent: center;
    alignItems: center;
    paddingRight: 1
`;

const CounterText = styled.Text`
    color: rgb(35, 31, 32);
    fontSize: 15;
    lineHeight: 15;
    fontFamily: CoreRhino45Regular;
    paddingHorizontal: 8;
    maxWidth: 76;
`;

export default class Quantity extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {groupPress, selectedIndex, buttons, quantity, removeQuantity, addQuantity, style} = this.props;
        return (
            <QuantityWrap style={style}>
                <ButtonGroup
                    onPress={groupPress}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    containerStyle={{
                        borderRadius: 6,
                        height: 29,
                        width: 151,
                        marginBottom: 0,
                    }}
                    buttonStyle={{
                        paddingBottom: 2,
                        paddingHorizontal: 4,
                    }}
                    textStyle={{
                        lineHeight: 18,
                    }}
                />
                <Counter>
                    <CounterBtn onPress={removeQuantity}><Icon name='remove' size={17} color='#231f20'/></CounterBtn>
                    <CounterText numberOfLines={1}>{quantity}</CounterText>
                    <CounterBtn onPress={addQuantity}><Icon name='add' size={17} color='#231f20'/></CounterBtn>
                </Counter>
            </QuantityWrap>
        );
    }
}
