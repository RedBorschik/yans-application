import Title from './Title';
import Label from './Label';
import Quantity from './Quantity';

export {
    Title,
    Label,
    Quantity,
};
