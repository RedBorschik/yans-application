import React, {Component} from 'react';
import Modal from 'react-native-modal';
import styled from 'styled-components/native';
import Check from '../../icons/Check';

const SortItem = styled.TouchableOpacity`
    paddingVertical: 13;
    paddingHorizontal: 16;
    height: 56;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const SortItemText = styled.Text`
    fontSize: 16;
    lineHeight: 20;
    fontFamily: CoreRhino45Regular;
    color: #231f20;
`;

const SortTitle = styled.Text`
    paddingTop: 13;
    paddingBottom: 12;
    paddingHorizontal: 16;
    fontSize: 16;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
    color: #231f20;
    borderBottomColor: #efefef;
    borderBottomWidth: 1;
`;

const ModalContent = styled.View`
    backgroundColor: #fafafa;
    width: 100%;
    borderTopLeftRadius: 10;
    borderTopRightRadius: 10;
    paddingBottom: 8;
`;

const ModalDecorLine = styled.View`
    width: 36;
    height: 5;
    borderRadius: 3;
    backgroundColor: #ffffff;
    marginBottom: 8;
`;

const IconWrap = styled.View`
    width: 60;
    justifyContent: center; 
    alignItems: center;   
`;

export default class ModalAddBasket extends Component {
    constructor(props) {
        super(props);
    }

    componentWillUnmount() {

    }

    selectItem = (name, value) => {
        this.props.hidePopup();
        if (this.props.onSelect) {
            this.props.onSelect(name, value);
        }
    };

    reset = () => {
        this.props.hidePopup();
        if (this.props.onReset) {
            this.props.onReset();
        }
    };

    hideEventPopup = () => {
        if (this.props.hidePopup) {
            this.props.hidePopup();
        }
    };

    render() {
        const {visible, list, selected} = this.props;
        //console.log(selected)
        return (
            <Modal
                isVisible={visible}
                onBackdropPress={() => this.hideEventPopup()}
                onSwipeComplete={() => this.hideEventPopup()}
                swipeDirection={'down'}
                style={{
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    margin: 0,
                }}>
                <ModalDecorLine/>
                <ModalContent>
                    <SortTitle>Сортировка</SortTitle>
                    {list.length && list.map((item, key) => (<SortItem style={{backgroundColor: selected && selected === item.value ? '#5bbb5e' : 'transparent'}} key={'sort' + key} onPress={() => this.selectItem(item.sortField, item.value)}>
                        <SortItemText style={{color: selected && selected === item.value ? '#ffffff' : '#231f20'}}>{(item.name || item.NAME).toLowerCase()}</SortItemText>
                        {selected && selected === item.value && <IconWrap><Check style={{color: '#ffffff'}}/></IconWrap>}
                    </SortItem>))}
                </ModalContent>
            </Modal>
        );
    }
}
