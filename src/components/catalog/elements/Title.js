import React, {Component} from 'react';

import styled from 'styled-components/native';
import {colors} from '../../../utils/theme';

const TitleWrap = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    paddingTop: 7;
`;

const NameWrap = styled.View`
    flex: 1;
`;

const Currency = styled.Text`
    color: ${colors.BLUE_5};
    fontFamily: CoreRhino65Bold;
    fontSize: 20;
`;

const Name = styled.Text`
    fontSize: 17;
    lineHeight: 22;
    fontFamily: CoreRhino45Regular;
    color: #231f20;
`;

const Price = styled.Text`
    color: ${colors.BLUE_5};
    fontFamily: CoreRhino65Bold;
    fontSize: 23;
    lineHeight: 28;
    marginLeft: 14;
    marginTop: 1;
    flex: none; 
`;


export default class Title extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {name, price, lines} = this.props;
        return (
            <TitleWrap>
                <NameWrap>
                    <Name numberOfLines={lines || 2}>{name}</Name>
                </NameWrap>
                {(price && price !== 0) ? <Price>{price} <Currency>₸</Currency></Price> : null}
            </TitleWrap>
        );
    }
}
