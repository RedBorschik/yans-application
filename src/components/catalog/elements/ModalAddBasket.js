import React, {Component} from 'react';
import Modal from 'react-native-modal';
import styled from 'styled-components/native';

import IconClose from '../../icons/Close';
import IconCart from '../../icons/Cart';

const View = styled.View`
    backgroundColor: #FFFFFF;
    width: 256;
    height: 236;
    borderRadius: 10;
    paddingVertical: 32;
    paddingHorizontal: 32;
    flexDirection: column;
    alignItems: center;
`;

const IconWrap = styled.View`
    width: 94;
    height: 94;
    borderRadius: 50;
    justifyContent: center;
    alignItems: center;
    backgroundColor: rgb(91, 187, 94);
`;

const Text = styled.Text`
    paddingTop: 30;
    color: rgb(35, 31, 32);
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 24;
    textAlign: center;
`;
const CloseButton = styled.TouchableHighlight`
    height: 28;
    width: 28;
    justifyContent: center;
    position: absolute;
    top: 8;
    right: 8;
`;

export default class ModalCard extends Component {
    constructor(props) {
        super(props);
    }

    hideModalEvent = () => {
        this.props.hidePopup();
    };

    render() {
        const {visible} = this.props;
        return (
            <Modal
                isVisible={visible}
                onBackdropPress={() => this.hideModalEvent()}
                style={{
                    alignItems: 'center'
                }}>
                <View>
                    <IconWrap>
                        <IconCart/>
                    </IconWrap>
                    <Text>{'Товар добавлен\nв корзину'.toUpperCase()}</Text>

                    <CloseButton onPress={() => this.hideModalEvent()}>
                        <IconClose/>
                    </CloseButton>
                </View>
            </Modal>
        );
    }
}
