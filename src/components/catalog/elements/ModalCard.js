import React, {Component} from 'react';
import Modal from 'react-native-modal';
import styled from 'styled-components/native';

const View = styled.View`
    backgroundColor: #FFFFFF;
    width: 100%;
    borderTopLeftRadius: 10;
    borderTopRightRadius: 10;
    paddingVertical: 32;
    paddingHorizontal: 32;
`;


export default class ModalAddBasket extends Component {
    constructor(props) {
        super(props);
    }

    componentWillUnmount(){

    }

    render() {
        const {visible, hidePopup} = this.props;
        return (
            <Modal
                isVisible={visible}
                onBackdropPress={hidePopup}
                swipeDirection={'bottom'}
                style={{
                    alignItems: 'flex-end',
                    justifyContent: 'flex-end',
                    margin: 0
                }}>
                <View>

                </View>
            </Modal>
        );
    }
}
