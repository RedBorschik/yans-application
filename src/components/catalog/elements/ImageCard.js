import React, {Component} from 'react';

import {Platform} from 'react-native';
import styled from 'styled-components/native';
import {Image} from 'react-native-elements';

const ImageWrap = styled.View`
    width: 100%;
    borderTopLeftRadius: 6;
    borderTopRightRadius: 6;
    overflow: hidden;
`;

export default class SmallCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {is_large, style, resize} = this.props;
        let height = is_large ? (Platform.OS === 'android' ? 224 : 189) : 104;
        return (
            <ImageWrap style={{...style, height: height}}>
            <Image
                source={{uri: this.props.image}}
                style={{height: height}}
                resizeMode={resize ? resize : 'cover'}
                /*PlaceholderContent={<Preloader/>}*/
            />
            </ImageWrap>
        );
    }
}
