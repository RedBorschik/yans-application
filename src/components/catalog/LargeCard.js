import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';

const Entities = require('html-entities').XmlEntities;

import Title from './elements/Title';
import Label from './elements/Label';
import ImageCard from './elements/ImageCard';
import Quantity from './elements/Quantity';
import IconFavorite from '../icons/Favorite';
import {Button} from 'react-native-elements';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

const Item = styled.TouchableOpacity`  
    width: 100%;
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: ${colors.WHITE};
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
`;

const ContentWrap = styled.View`
    marginTop: 8;
    paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
    paddingBottom: 16;
    flexDirection: column;
    justifyContent: space-between;
`;

const InfoWrap = styled.View`
    flexDirection: row;
    justifyContent: space-between;
    paddingTop: 10;
`;

const InfoValue = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontSize: 12;
    fontFamily: CoreRhino45Regular;
`;

const SmallPrice = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontSize: 13;
    lineHeight: 13;
    fontFamily: CoreRhino45Regular;
`;

const Currency = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontSize: 15;
    fontFamily: CoreRhino45Regular;
`;

const MutedText = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
    marginTop: 16;
`;

export default class LargeCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            packing: [],
            packingNames: [],
            selectedIndex: 0,
            name: '',
            curPack: {
                piecePrice: 0,
                price: 0,
                count: 1,
                quantity: 0,
            },
            showModal: false
        };
    }

    getCurPacking = () => {
        let packing = this.props.item.packing;
        let codes = ['piece', 'pack', 'box'];
        let names = ['шт', 'упаковк', 'коробка'];
        let idx = [];
        let curCodes = Object.keys(packing);
        codes = codes.filter((item, index) => {
            if (curCodes.includes(item)) {
                idx.push(index);
                return item;
            }
        });
        names = names.filter((item, index) => {
            if (idx.includes(index)) return item;
        });

        return {
            codes: codes,
            names: names,
        };
    };

    setCurPackData = () => {
        const {item} = this.props;
        let packing = this.state.packing;
        let selectedIndex = this.state.selectedIndex;
        let curPack = item.packing[packing[selectedIndex]];
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                piecePrice: (Number(curPack.price) / Number(curPack.quantity_in_pack)),
                price: (Number(curPack.price) * prevState.curPack.count).toFixed(0),
                quantity: Number(curPack.quantity_in_pack) * prevState.curPack.count,
            },
        }));
    };

    addPackCount = () => {
        const {item} = this.props;
        let packing = this.state.packing;
        let selectedIndex = this.state.selectedIndex;
        let curPack = item.packing[packing[selectedIndex]];
        if (this.state.curPack.count >= curPack.quantity) return;
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) + 1,
            },
        }), () => this.setCurPackData());
    };

    removePackCount = () => {
        if (this.state.curPack.count <= 1) return;
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) - 1,
            },
        }), () => this.setCurPackData());
    };

    updatePack = (selectedIndex) => {
        this.setState(prevState => ({
            selectedIndex: selectedIndex,
            curPack: {
                ...prevState.curPack,
                count: 1,
            },
        }), () => this.setCurPackData());
    };

    componentWillMount() {
        const {item} = this.props;
        if (item) {
            let name = new Entities();
            name = name.decode(item.name);

            if (item.packing) {
                let packing = this.getCurPacking().codes;
                let packingNames = this.getCurPacking().names;

                this.setState({
                    packing: packing,
                    packingNames: packingNames,
                    name: name,
                }, () => this.setCurPackData());
            } else {
                this.setState({
                    name: name,
                });
            }
        }
    }

    addBasketEvent = () => {
        const {item, addBasket} = this.props;
        const {curPack, selectedIndex, packing} = this.state;
        let curcode = packing[selectedIndex];

        if (addBasket) {
            addBasket([{
                id: item.id,
                quantity: curPack.count,
                packing: curcode
            }]);
        }
    };

    render() {
        const {item, removeFavorite, addFavorite} = this.props;
        const {name, curPack, selectedIndex, packingNames} = this.state;
        return (
            item && <Item onPress={this.props.onPress}>
                <IconFavorite onPress={item.in_wish_list ? removeFavorite : addFavorite} active={item.in_wish_list} style={{position: 'absolute', top: 16, right: 15.6, zIndex: 9}}/>
                <ImageCard image={item.image} is_large={true} resize={'cover'}/>
                {(item.label && item.label.code === 'NEWPRODUCT') && <Label style={{top: 20, left: -2}}/>}
                <ContentWrap>
                    <InfoWrap>
                        <InfoValue>{!!item.article && 'Арт. ' + item.article}</InfoValue>
                        {curPack.piecePrice !== 0 ? <SmallPrice>{curPack.piecePrice} <Currency>₸/шт</Currency></SmallPrice> : null}
                    </InfoWrap>
                    <Title
                        name={name}
                        price={curPack.price}
                    />
                    {item.quantity > 0 ? <Quantity
                        buttons={packingNames}
                        selectedIndex={selectedIndex}
                        quantity={curPack.quantity}
                        groupPress={(selectedIndex) => this.updatePack(selectedIndex)}
                        removeQuantity={() => this.removePackCount()}
                        addQuantity={() => this.addPackCount()}
                    /> : <MutedText>Нет в наличии</MutedText>}
                    {item.quantity > 0 && <Button
                        title="Добавить в корзину"
                        onPress={() => this.addBasketEvent()}
                        buttonStyle={{
                            marginTop: 22,
                            marginHorizontal: Platform.OS === 'android' ? 0 : 16,
                            elevation: 0,
                        }}
                    />}
                </ContentWrap>
            </Item>
        );
    }
}
