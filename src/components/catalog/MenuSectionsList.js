import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import {IconChevron, IconMenuCatalog} from '../../components/';
import NavigationService from '../../NavigationService';
import {colors} from '../../utils/theme';

const Container = styled.View`
    flex: 1;
`;

const ScrollView = styled.ScrollView`
    flex: 1;
    marginTop: ${Platform.OS === 'android' ? 50 : 0}
`;

const Inner = styled.View`
    paddingLeft: 8;
    paddingVertical: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 22 : 8};
`;

const Title = styled.Text`
    color: #231f20;
    fontSize: ${Platform.OS === 'android' ? 26 : 34};
    lineHeight: ${Platform.OS === 'android' ? 32 : 42}; 
    fontFamily: CoreRhino65Bold;
    paddingBottom: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 14};
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 0 : 8};
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    height: 50;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
`;

const IconWrap = styled.View`
    width: 28;
    height: 28;
    justifyContent: center;
    alignItems: center;
    marginRight: ${Platform.OS === 'android' ? 16 : 8};
    paddingRight: 0; 
    marginLeft: ${Platform.OS === 'android' ? 8 : 0}
`;

const IconChevronWrap = styled.View`
    transform: rotate(-90deg);
    width: ${Platform.OS === 'android' ? 60 : 44};
    justifyContent: center; 
    alignItems: center;   
`;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginVertical: 0;
`;

const ListItemContainer = styled.TouchableOpacity`
    width: 100%;
    minHeight: 44;
    paddingVertical: 7;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const ListInner = styled.View`
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 120) : (Dimensions.get('window').width - 88)};
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15; 
    lineHeight: 19;
`;

const Divider = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef;
`;

const Icon = styled.Image``;

class MenuSectionsList extends Component {
    constructor(props) {
        super(props);
    }

    checkNavigation = (item) => {
        if (item.items) {
            NavigationService.navigate('MenuSubsections', {sections: item.items, title: item.name}, 'Subsections' + item.value_param);
        } else {
            NavigationService.navigate('Section', {section: item.value_param, title: item.name}, 'Section' + item.value_param);
        }
    };

    render() {
        const {sections} = this.props;
        /* const icons = {
            392: 'https://www.yans.kz/upload/medialibrary/fad/fad290a8eecc4b65dd94f6f27c095f5a.png',
            438: 'https://www.yans.kz/upload/medialibrary/354/3546e2ebeb483e4b1357b782a34b68de.png',
            434: 'https://www.yans.kz/upload/medialibrary/986/98699a08117e6a7596d36f8d258ea42a.png',
            466: 'https://www.yans.kz/upload/medialibrary/454/454d46d5f8f0a1fc079b204f0451ee85.png',
            397: 'https://www.yans.kz/upload/medialibrary/722/7226652694a22221a21841bb59326e69.png',
            405: 'https://www.yans.kz/upload/medialibrary/722/7226652694a22221a21841bb59326e69.png',
            406: 'https://www.yans.kz/upload/medialibrary/722/7226652694a22221a21841bb59326e69.png',
            409: 'https://www.yans.kz/upload/medialibrary/6ae/6aea80f789bd5aa3b5def99ee20376eb.png',
            411: 'https://www.yans.kz/upload/medialibrary/6cd/6cd1c57caf1def88ffd46729a764d637.png',
            419: 'https://www.yans.kz/upload/medialibrary/b8e/b8e4227e44004346b50251eb1f09f5ac.png',
            415: 'https://www.yans.kz/upload/medialibrary/55e/55e5799551c29fdb5d1c519314e9816e.png',
            412: 'https://www.yans.kz/upload/medialibrary/81a/81a7fbb798058e0b821cccc265f70018.png',
            421: 'https://www.yans.kz/upload/medialibrary/1c1/1c153ddfe4f49e37a50f65b11bd131a7.png',
            422: 'https://www.yans.kz/upload/medialibrary/77d/77da1e4f10251c3ae0090cd457ba55d9.png',
        };*/
        const icons = {
            392: require('../../../assets/images/1.png'),
            438: require('../../../assets/images/2.png'),
            434: require('../../../assets/images/3.png'),
            466: require('../../../assets/images/4.png'),
            397: require('../../../assets/images/5.png'),
            405: require('../../../assets/images/5.png'),
            406: require('../../../assets/images/5.png'),
            409: require('../../../assets/images/6.png'),
            411: require('../../../assets/images/7.png'),
            419: require('../../../assets/images/8.png'),
            415: require('../../../assets/images/9.png'),
            412: require('../../../assets/images/10.png'),
            421: require('../../../assets/images/11.png'),
            422: require('../../../assets/images/12.png'),
        };
        return (
            <Container>
                {Platform.OS === 'android' && <FixedTitle><Title>Каталог</Title></FixedTitle>}
                <ScrollView>
                    {Platform.OS === 'ios' && <Title>Каталог</Title>}
                    {Platform.OS === 'ios' && <Divider/>}
                    <Inner>
                        {
                            sections.map((item, key) => (
                                <ListItemWrap key={item.value_param}>
                                    <ListItemContainer
                                        onPress={() => this.checkNavigation(item)}>
                                        <IconWrap>{icons[item.value_param] ? <Icon
                                            style={{
                                                width: 28,
                                                height: 28,
                                            }}
                                            resizeMode="contain"
                                            source={icons[item.value_param]}
                                        /> : null}</IconWrap>
                                        <ListInner>
                                            <ListItem numberOfLines={2}>{item.name}</ListItem>
                                        </ListInner>
                                        <IconChevronWrap><IconChevron/></IconChevronWrap>
                                    </ListItemContainer>
                                    <Divider/>
                                </ListItemWrap>
                            ))
                        }
                    </Inner>
                </ScrollView>
            </Container>
        );
    }
}

export default MenuSectionsList;
