import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';

const Entities = require('html-entities').XmlEntities;

import Label from './elements/Label';
import ImageCard from './elements/ImageCard';
import IconFavorite from '../icons/Favorite';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

const Item = styled.TouchableOpacity` 
    width: ${(Dimensions.get('window').width / 2) - (Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 24 : 12)};
    height: 238;
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: ${colors.WHITE};
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    overflow: visible;
`;

const ContentWrap = styled.View`
    flex: 1;
    marginTop: 8;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 16;
    flexDirection: column;
    justifyContent: space-between;
`;

const Title = styled.Text`
    fontSize: 15;
    lineHeight: 22;
    fontFamily: CoreRhino45Regular;
    color: #231f20;
`;

const Price = styled.Text`
    color: ${colors.BLUE_5};
    fontFamily: CoreRhino65Bold;
    fontSize: 23;
    lineHeight: 28;
    marginTop: 16;
`;

const Currency = styled.Text`
    color: ${colors.BLUE_5};
    fontFamily: CoreRhino65Bold;
    fontSize: 20;
`;

const Quanity = styled.Text`
    color: ${colors.RED};
    fontFamily: CoreRhino65Bold;
    fontSize: 14;
    lineHeight: 16;
    paddingTop: 6;
`;

export default class SmallCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {item} = this.props;
        if (item) {
            let price = '';
            if (item.packing && !item.price) {
                let codes = Object.keys(item.packing);
                if (codes.indexOf('piece') >= 0) {
                    price = Number(item.packing.piece.price).toFixed(0);
                } else if (codes.indexOf('pack') >= 0) {
                    price = Number(item.packing.pack.price).toFixed(0);
                } else if (codes.indexOf('box') >= 0) {
                    price = Number(item.packing.box.price).toFixed(0);
                }
            }

            if (item.price) {
                price = item.price;
            }

            let name = new Entities();
            name = name.decode(item.name);
            return (
                item && <Item onPress={this.props.onPress}>
                    <IconFavorite onPress={item.in_wish_list ? this.props.removeFavorite : this.props.addFavorite} active={item.in_wish_list} style={{position: 'absolute', top: 8, right: 8, zIndex: 9}}/>
                    <ImageCard image={item.image} resize={'cover'}/>
                    {(item.label && item.label.code === 'NEWPRODUCT') && <Label/>}
                    <ContentWrap>
                        <Title numberOfLines={item.quantity > 0 ? 3 : 2}>{name}</Title>
                        {item.quantity <= 0 ? <Quanity>нет в наличии</Quanity> : null}
                        {price !== '' && <Price>{price} <Currency>₸</Currency></Price>}
                    </ContentWrap>
                </Item>
            );
        } else {
            return null;
        }
    }
}
