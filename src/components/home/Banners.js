import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dimensions, Platform} from 'react-native';
import styled from 'styled-components/native';
import Slider from '../helpers/Slider';

const BannersContainer = styled.View`
    overflow: visible;
`;

class BannersList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {banners} = this.props;
        let SlideWidth = Dimensions.get('window').width - (Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 32 : 16);
        return (
            <BannersContainer>
                {banners.length && <Slider
                    items={banners}
                    slideWidth={SlideWidth}
                    slideHeight={118}
                    slideStyles={{
                        paddingTop: Platform.OS === 'android' ? 8 : 0,
                        paddingBottom: 16,
                        paddingHorizontal: Platform.OS === 'android' ? 16 : 8
                    }}
                    slideInnerStyles={{
                        borderRadius: 8
                    }}
                />}
            </BannersContainer>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(BannersList);
;
