import React, {Component} from 'react';
import {Image,StatusBar} from 'react-native';

export default class StartBackground extends Component {
    render() {
        return (
            <Image source={require('../../../assets/images/bg/bg.png')} style={{position: 'absolute', top: 0, left: 0, right: 0}} imageStyle={{resizeMode: 'contain', alignSelf: 'flex-start'}}/>
        );
    }
}
