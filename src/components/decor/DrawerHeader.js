import React, {Component} from 'react';
import {Dimensions, Platform} from 'react-native';
import styled from 'styled-components';

const View = styled.View`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: ${Platform.OS === 'android' ? 79 : 58};
`;

const Circle1 = styled.View`
    width: ${Platform.OS === 'android' ? 47 : 32};
    height: ${Platform.OS === 'android' ? 47 : 32};
    backgroundColor: rgb(31, 76, 101);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 13 : -14};
    top: ${Platform.OS === 'android' ? -28 : 5};    
`;

const Circle2 = styled.View`
    width: 22;
    height: 22;
    backgroundColor: rgb(55, 135, 185);
    borderRadius: 50;
    position: absolute;
    left: 34;
    top: -9;    
`;

const Circle3 = styled.View`
    width: ${Platform.OS === 'android' ? 18 : 12.8};
    height: ${Platform.OS === 'android' ? 18 : 12.8};
    backgroundColor: rgb(62, 156, 212);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 73 : 28};
    top: ${Platform.OS === 'android' ? 14 : 35};    
`;

const Circle4 = styled.View`
    width: ${Platform.OS === 'android' ? 25 : 18.2};
    height: ${Platform.OS === 'android' ? 25 : 18.2};
    backgroundColor: rgb(21, 194, 236);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 105 : 69};
    top: ${Platform.OS === 'android' ? 32 : 16};    
`;

const Circle5 = styled.View`
    width: ${Platform.OS === 'android' ? 35 : 21};
    height: ${Platform.OS === 'android' ? 35 : 21};
    backgroundColor: rgb(47, 188, 175);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 180 : 157};
    top: ${Platform.OS === 'android' ? -23 : -9};    
`;

const Circle6 = styled.View`
    width: ${Platform.OS === 'android' ? 19 : 13.9};
    height: ${Platform.OS === 'android' ? 19 : 13.9};
    backgroundColor: rgb(71, 186, 131);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 159 : 161};
    top: ${Platform.OS === 'android' ? 23 : 34};    
`;

const Circle7 = styled.View`
    width: 47;
    height: 47;
    backgroundColor: rgb(137, 197, 76);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 207 : 196};
    top: ${Platform.OS === 'android' ? 23 : 28};    
`;

const Circle8 = styled.View`
    width: ${Platform.OS === 'android' ? 18 : 12.8};
    height: ${Platform.OS === 'android' ? 18 : 12.8};
    backgroundColor: rgb(91, 187, 94);
    borderRadius: 50;
    position: absolute;
    left: ${Platform.OS === 'android' ? 245 : 218};
    top: ${Platform.OS === 'android' ? -3 : 13};    
`;

export default class StartBackground extends Component {
    render() {
        return (
            <View>
                <Circle1/>
                {Platform.OS !== 'android' && <Circle2/>}
                <Circle3/>
                <Circle4/>
                <Circle5/>
                <Circle6/>
                <Circle7/>
                <Circle8/>
            </View>
        );
    }
}
