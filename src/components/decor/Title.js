import React, {Component} from 'react';
import {Platform} from 'react-native';

import styled from 'styled-components/native';
import {colors, theme} from '../../utils/theme';

const TitleText = styled.Text`
  width: 100%;
  fontSize: 25;
  lineHeight: 30;
  color: ${colors.BLUE_8};
  fontFamily: CoreRhino65Bold;
  marginBottom: 17;
  marginTop: ${Platform.OS === 'android' ? 180 : 120};
`;

export default class Title extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TitleText>{this.props.title}</TitleText>
        );
    }
}
