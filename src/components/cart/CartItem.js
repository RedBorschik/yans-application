import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';

import {Alert} from 'react-native';
import Modal from 'react-native-modal';
import IconMore from '../icons/More';
import IconFavorite from '../icons/FavoriteIcon';
import IconDelete from '../icons/Delete';
import Quantity from '../catalog/elements/Quantity';

const Entities = require('html-entities').XmlEntities;

import {SwipeRow} from 'react-native-swipe-list-view';

import styled from 'styled-components/native';
import {Button} from 'react-native-elements/src/buttons/Button';

const ItemWrap = styled.View`    
    marginBottom: 16;
    backgroundColor: transparent;
    overflow: visible;
`;

const Item = styled.View`
    marginLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8}; 
    width: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? Dimensions.get('window').width - 32 : Dimensions.get('window').width - 16};
    height: 114;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingVertical: 16;
    paddingHorizontal: 8;
    flexDirection: row;
    alignItems: stretch;
`;

const ContentWrap = styled.View`
    flex: 1;
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    flexDirection: column;
    justifyContent: space-between;
`;

const TitleWrap = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const Title = styled.Text`
    fontSize: 13;
    lineHeight: 17;
    fontFamily: CoreRhino45Regular;
    width: 85%;
`;

const Price = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 20;
`;

const Currency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 13;
`;

const Image = styled.Image`
    width: 82;
    height: 82;    
    borderRadius: 6;
    overflow: hidden;
`;

const InfoRow = styled.View`
    flexDirection: row;
    alignItems: flex-end;
    justifyContent: space-between;
    width: 100%;
    paddingRight: 8;
`;

const InfoCol = styled.View`
    alignItems: flex-start;
`;

const InfoItem = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
    fontSize: 12;
    marginTop: 3;
`;

const BtnMore = styled(IconMore)`
    position: absolute;
    top: -8;
    right: 8;
`;

const ButtonFavorite = styled.TouchableOpacity`
    height: 114;
    width: 76;
    backgroundColor: #5bbb5e;
    alignItems: center;
    justifyContent: center;
    borderRadius: 6;
`;

const ButtonRemove = styled.TouchableOpacity`
    height: 114;
    width: 76;
    backgroundColor: #fb0a06;
    alignItems: center;
    justifyContent: center;
    borderRadius: 6;
`;

const ModalItem = styled.TouchableOpacity`
    paddingVertical: 13;
    paddingHorizontal: 16;
    height: 56;
    flexDirection: row;
    alignItems: center;
    borderBottomColor: #efefef;
    borderBottomWidth: 1;
`;

const ModalItemText = styled.Text`
    fontSize: 16;
    lineHeight: 20;
    fontFamily: CoreRhino45Regular;
    color: #231f20;
`;

const ModalIcon = styled.View`
    width: 28;
    height: 28;
    alignItems: center;
    justifyContent: center;
    marginRight: 16;
`;

const ModalHeader = styled.View`
    paddingTop: 13;
    paddingBottom: 16;
    paddingHorizontal: 16;
    borderBottomColor: #efefef;
    borderBottomWidth: 1;
`;

const ModalTitle = styled.Text`
    fontSize: 16;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
    color: #231f20;
`;

const ModalContent = styled.View`
    backgroundColor: #fafafa;
    width: 100%;
    borderTopLeftRadius: 10;
    borderTopRightRadius: 10;
    paddingBottom: 8;
`;

const ModalDecorLine = styled.View`
    width: 36;
    height: 5;
    borderRadius: 3;
    backgroundColor: #ffffff;
    marginBottom: 8;
`;

const Buttons = styled.View`
    flexDirection: row;
    alignItems: center;
    position: absolute;
    top: 0;
    right: 16;
`;

export default class CartItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            left: 0,
            packing: [],
            packingNames: [],
            selectedIndex: 0,
            curPack: {
                price: 0,
                count: 1,
                quantity: 0,
            },
            showModal: false,
        };
    }

    getCurPacking = () => {
        let packing = this.props.item.packing;
        let codes = ['piece', 'pack', 'box'];
        let names = ['шт', 'упаковк', 'коробка'];
        let idx = [];
        let curCodes = Object.keys(packing);
        codes = codes.filter((item, index) => {
            if (curCodes.includes(item)) {
                idx.push(index);
                return item;
            }
        });
        names = names.filter((item, index) => {
            if (idx.includes(index)) return item;
        });

        return {
            codes: codes,
            names: names,
        };
    };

    setCurPackData = () => {
        const {item} = this.props;
        let packing = this.state.packing;
        let selectedIndex = this.state.selectedIndex;
        let curPack = item.packing[packing[selectedIndex]];
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                price: (Number(curPack.price) * prevState.curPack.count).toFixed(0),
                quantity: Number(curPack.quantity_in_pack) * prevState.curPack.count,
            },
        }));
    };

    addPackCount = () => {
        const {item} = this.props;
        let packing = this.state.packing;
        let selectedIndex = this.state.selectedIndex;
        let curPack = item.packing[packing[selectedIndex]];
        if (this.state.curPack.count >= curPack.quantity) return;
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) + 1,
            },
        }), () => this.setCurPackData());
    };

    removePackCount = () => {
        if (this.state.curPack.count <= 1) return;
        this.setState(prevState => ({
            curPack: {
                ...prevState.curPack,
                count: Number(prevState.curPack.count) - 1,
            },
        }), () => this.setCurPackData());
    };

    updatePack = (selectedIndex) => {
        this.setState(prevState => ({
            selectedIndex: selectedIndex,
            curPack: {
                ...prevState.curPack,
                count: 1,
            },
        }), () => this.setCurPackData());
    };

    removeItemEvent = () => {
        Alert.alert(
            '',
            'Вы действительно хотите удалить этот элемент?',
            [
                {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Да', onPress: () => {
                    this.setState({showModal: false});
                    this.props.removeItem(this.props.item.id);
                },
                },
            ],
            {cancelable: true},
        );
    };

    updateCountItem = function() {
        const {updateItem, item} = this.props;
        const {curPack, selectedIndex, packing} = this.state;
        let curcode = packing[selectedIndex];

        if (updateItem && (item.current.code !== curcode || item.current.quantity !== curPack.count)) {
            updateItem({
                id: item.id,
                quantity: curPack.count,
                packing: curcode,
            });
        }
    };

    componentWillMount() {
        const {item} = this.props;
        if (item) {
            if (item.packing) {
                let packing = this.getCurPacking().codes;
                let packingNames = this.getCurPacking().names;

                let curIdx = 0;
                packing.map((val, idx) => {
                    if (item.current.code === val) {
                        curIdx = idx;
                    }
                });

                this.setState(prevState => ({
                    ...prevState,
                    packing: packing,
                    packingNames: packingNames,
                    selectedIndex: curIdx,
                    curPack: {
                        ...prevState.curPack,
                        price: item.current.price,
                        count: item.current.quantity,
                    },
                }), () => this.setCurPackData());
            }
        }
    }

    render() {
        const {item, addFavorite, removeFavorite} = this.props;
        const {left, curPack, selectedIndex, packingNames} = this.state;

        if (item) {
            let name = new Entities();
            name = name.decode(item.name);

            let count = 1;
            if (item.current && item.packing && item.packing[item.current.code]) {
                count = Number(item.packing[item.current.code].quantity_in_pack) * Number(item.current.quantity);
            }

            return (
                item && <ItemWrap>
                    <SwipeRow
                        style={{
                            overflow: 'visible'
                        }}
                        leftOpenValue={0} rightOpenValue={-152}>
                        <Buttons>
                            <ButtonFavorite onPress={item.in_wish_list ? removeFavorite : addFavorite}><IconFavorite color="#ffffff" fill={item.in_wish_list ? '#ffffff' : 'none'}/></ButtonFavorite>
                            <ButtonRemove onPress={this.removeItemEvent}><IconDelete onPress={this.removeItemEvent} color="#ffffff"/></ButtonRemove>
                        </Buttons>
                        <Item>
                            <Image
                                source={{uri: item.image}}
                                imageStyle={{resizeMode: 'cover'}}
                            />
                            <ContentWrap>
                                <TitleWrap>
                                    <Title numberOfLines={2}>{name}</Title>
                                    <BtnMore onPress={() => this.setState({showModal: true})}/>
                                </TitleWrap>
                                <InfoRow>
                                    <InfoCol>
                                        {item.current && item.current.name && <InfoItem>{item.current.name.toLowerCase()}</InfoItem>}
                                        {item.current && item.current.quantity && <InfoItem>{count} шт</InfoItem>}
                                    </InfoCol>
                                    {item.current && item.current.price && <Price>{Number(item.current.price).toFixed(0)} <Currency>₸</Currency></Price>}
                                </InfoRow>
                            </ContentWrap>
                        </Item>
                    </SwipeRow>
                    <Modal
                        isVisible={this.state.showModal}
                        onBackdropPress={() => {
                            this.setState({showModal: false});
                            this.updateCountItem();
                        }}
                        onSwipeComplete={() => {
                            this.setState({showModal: false});
                            this.updateCountItem();
                        }}
                        swipeDirection={'down'}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            margin: 0,
                        }}>
                        <ModalDecorLine/>
                        <ModalContent>
                            <ModalHeader>
                                <ModalTitle>Сортировка</ModalTitle>
                                <Quantity
                                    buttons={packingNames}
                                    selectedIndex={selectedIndex}
                                    quantity={curPack.quantity}
                                    groupPress={(selectedIndex) => this.updatePack(selectedIndex)}
                                    removeQuantity={() => this.removePackCount()}
                                    addQuantity={() => this.addPackCount()}
                                />
                            </ModalHeader>
                            <ModalItem onPress={addFavorite}>
                                <ModalIcon><IconFavorite color="#929292"/></ModalIcon>
                                <ModalItemText>Перенeсти в избранное</ModalItemText>
                            </ModalItem>
                            <ModalItem onPress={this.removeItemEvent} style={{
                                borderBottomColor: 'transparent',
                            }}>
                                <ModalIcon><IconDelete color="#929292"/></ModalIcon>
                                <ModalItemText>Удалить</ModalItemText>
                            </ModalItem>
                        </ModalContent>
                    </Modal>
                </ItemWrap>
            );
        } else {
            return null;
        }
    }
}
