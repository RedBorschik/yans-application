import firebase from 'react-native-firebase';

import type {RemoteMessage} from 'react-native-firebase';
import {Platform} from 'react-native';

const channel_name = 'yansChannel';

export default async (message: RemoteMessage) => {
    console.log('message', message);
    if (message && message.messageId) {
        const notification = new firebase.notifications.Notification().setNotificationId(message.messageId).
            setTitle(message.data.title).
            setBody(message.data.body).
            setData({click: message._data.click_action_custom});

        if (Platform.OS === 'android') {
            notification.android
            .setPriority(firebase.notifications.Android.Priority.High)
            .android.setChannelId(channel_name)
            .android.setVibrate(1000);
        }

        await firebase.notifications().displayNotification(notification);
    }
}