import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';
import ActionsToken from './Token';
import NavigationService from '../NavigationService';

const registerHasError = (bool, text) => {
    return {
        type: ActionTypes.REGISTER_HAS_ERROR,
        error: bool,
        message: text,
    };
};

const registerIsLoading = (bool) => {
    return {
        type: ActionTypes.REGISTER_IS_LOADING,
        loading: bool,
    };
};

const registerUser = (email, password, confirm_password) => {
    return (dispatch, getState) => {
        dispatch(registerHasError(false, ''));
        dispatch(registerIsLoading(true));

        if (!email || !password || !confirm_password) {
            dispatch(registerHasError(true, errors.auth.data));
            dispatch(registerIsLoading(false));

            return;
        }

        let data = {
            ...api.params,
            params: {
                email: email,
                password: password,
                confirm_password: confirm_password,
                language: 'ru',
                device: getState().device.device,
                device_token: getState().token.device_token,
            },
            method: api.methods.auth.register,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            console.log('register', res);
            dispatch(registerIsLoading(false));
            if (res.result) {
                dispatch(registerHasError(false));
                dispatch(ActionsToken.saveUserHash(res.result));
            } else if (res.error) {
                dispatch(registerHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(registerHasError(true, error));
        });
    };
};

export default {
    registerHasError,
    registerIsLoading,
    registerUser,
};