import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';

const passwordHasError = (bool, text) => {
    return {
        type: ActionTypes.PASSWORD_HAS_ERROR,
        error: bool,
        message: text,
    };
};

const passwordIsLoading = (bool) => {
    return {
        type: ActionTypes.PASSWORD_IS_LOADING,
        loading: bool,
    };
};

const saveDataPass = (data) => {
    return {
        type: ActionTypes.PASSWORD_SAVE_DATA,
        payload: data,
    };
};

const fetchPasswordForgotSend = (email, phone) => {
    return (dispatch, getState) => {
        dispatch(passwordHasError(false, ''));
        dispatch(passwordIsLoading(true));

        let data = {
            ...api.params,
            params: {
                language: getState().account.language,
                device_token: getState().token.device_token,
            },
            method: !!email ? api.methods.password.forgot.email : api.methods.password.forgot.phone,
        };

        if (!!email) data.params.email = email;
        if (!!phone) data.params.phone = phone;

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(passwordIsLoading(false));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(passwordHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(passwordHasError(false, res.result));
                    dispatch(saveDataPass({
                        email: email,
                        phone: phone,
                    }));
                } else if (res.error) {
                    dispatch(passwordHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(passwordHasError(true, error));
        });
    };
};

const fetchPasswordCheck = (pass) => {
    return (dispatch, getState) => {
        dispatch(passwordHasError(false, ''));
        dispatch(passwordIsLoading(true));

        let data = {
            ...api.params,
            params: {
                language: getState().account.language,
                device_token: getState().token.device_token,
                one_time_password: pass,
            },
            method: api.methods.password.check,
        };

        if (getState().password.data.email !== '') data.params.email = getState().password.data.email;
        if (getState().password.data.phone !== '') data.params.phone = getState().password.data.phone;

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(passwordIsLoading(false));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(passwordHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(passwordHasError(false, res.result));
                    dispatch(saveDataPass({
                        pass: pass,
                    }));
                } else if (res.error) {
                    dispatch(passwordHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(passwordHasError(true, error));
        });
    };
};

const fetchPasswordChange = (pass, confirm_pass) => {
    return (dispatch, getState) => {
        dispatch(passwordHasError(false, ''));
        dispatch(passwordIsLoading(true));

        let data = {
            ...api.params,
            params: {
                language: getState().account.language,
                device_token: getState().token.device_token,
                password: pass,
                confirm_password: confirm_pass
            },
            method: api.methods.password.change,
        };

        if (getState().password.data.pass !== '') data.params.one_time_password = getState().password.data.pass;
        if (getState().password.data.email !== '') data.params.email = getState().password.data.email;
        if (getState().password.data.phone !== '') data.params.phone = getState().password.data.phone;

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(passwordIsLoading(false));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(passwordHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(passwordHasError(false, res.result));
                } else if (res.error) {
                    dispatch(passwordHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(passwordHasError(true, error));
        });
    };
};

export default {
    passwordHasError,
    passwordIsLoading,
    fetchPasswordForgotSend,
    fetchPasswordCheck,
    fetchPasswordChange,
};