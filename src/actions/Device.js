import * as ActionTypes from '../constants/ActionTypes';
import DeviceInfo from 'react-native-device-info';

const setDevice = (data) => {
    return {
        type: ActionTypes.SET_DEVICE,
        payload: data,
    };
};

const setConnection = (bool) => ({
    type: ActionTypes.SET_CONNECTION,
    payload: bool,
});

const getDeviceFromDeviceInfo = () => {
    return {
        id: DeviceInfo.getUniqueId(),
        type: DeviceInfo.getBrand().toLowerCase() === 'apple' ? 'apple' : 'google',
        model: DeviceInfo.getModel(),
        mark: DeviceInfo.getBrand(),
        version: DeviceInfo.getSystemVersion(),
    };
};

const getDevice = () => {
    return (dispatch) => {
        return dispatch(setDevice(getDeviceFromDeviceInfo()));
    };
};

export default {
    getDevice,
    setDevice,
    setConnection,
};