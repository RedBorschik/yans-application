import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';

const accountHasError = (bool, text) => ({
    type: ActionTypes.ACCOUNT_HAS_ERROR,
    error: bool,
    message: text,
});

const accountIsLoading = (bool) => ({
    type: ActionTypes.ACCOUNT_IS_LOADING,
    loading: bool,
});

const accountActionIsLoading = (bool) => ({
    type: ActionTypes.ACCOUNT_ACTION_IS_LOADING,
    loading: bool,
});

const saveProfile = (data) => ({
    type: ActionTypes.SAVE_PROFILE,
    payload: data,
});

const savePush = (data) => ({
    type: ActionTypes.ACCOUNT_PUSH_SAVE,
    payload: data,
});

const getPush = (data) => ({
    type: ActionTypes.ACCOUNT_PUSH_GET,
    payload: data,
});

const updateFetchProfile = (profile) => {
    return (dispatch, getState) => {
        dispatch(accountHasError(false, ''));
        dispatch(accountActionIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.update,
        };

        data.params = {
            ...data.params,
            ...profile
        };

        console.log(data)

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(accountActionIsLoading(false));
            console.log(res);
            if (res.result) {
                dispatch(accountHasError(false, 'Изменения сохранены'));
                dispatch(saveProfile(profile));
            } else if (res.error) {
                dispatch(accountHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(accountHasError(true, error));
        });
    };
};

const getFetchProfile = () => {
    return (dispatch, getState) => {
        dispatch(accountHasError(false, ''));
        dispatch(accountIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.get,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(accountIsLoading(false));
            if (res.result) {
                dispatch(accountHasError(false));
                dispatch(saveProfile(res.result));
            } else if (res.error) {
                dispatch(accountHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(accountHasError(true, error));
        });
    };
};

const fetchUpdatePush = (push) => {
    return (dispatch, getState) => {
        dispatch(accountHasError(false, ''));
        dispatch(accountActionIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...push
            },
            method: api.methods.user.push.update,
        };

        console.log('update props', data);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(accountActionIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(accountHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    console.log('update props', res);
                    dispatch(accountHasError(false));
                    dispatch(savePush(push));
                } else if (res.error) {
                    dispatch(accountHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(accountHasError(true, error));
        });
    };
};

const fetchGetPush = () => {
    return (dispatch, getState) => {
        dispatch(accountHasError(false, ''));
        dispatch(accountIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.push.get,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(accountIsLoading(false));
            if (res.result) {
                dispatch(accountHasError(false));
                dispatch(getPush(res.result));
            } else if (res.error) {
                dispatch(accountHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(accountHasError(true, error));
        });
    };
};

export default {
    accountHasError,
    accountIsLoading,
    saveProfile,
    getFetchProfile,
    updateFetchProfile,
    fetchGetPush,
    fetchUpdatePush,
};