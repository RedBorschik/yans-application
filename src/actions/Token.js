import * as ActionTypes from '../constants/ActionTypes';
import DeviceActions from './Device';
import {api} from '../utils/api';
import {errors} from '../utils/errors';


const tokenHasError = (bool, text) => ({
    type: ActionTypes.TOKEN_HAS_ERROR,
    error: bool,
    message: text,
});

const tokenIsLoading = (bool) => ({
    type: ActionTypes.TOKEN_IS_LOADING,
    loading: bool,
});

/*hash*/
const saveHash = (token) => ({
    type: ActionTypes.SAVE_HASH,
    hash: token
});

const removeHash = () => ({
    type: ActionTypes.REMOVE_HASH,
});

const saveUserHash = (data) => {
    return (dispatch, getState) => {
        dispatch(saveHash(data));
    }
};

const removeUserHash = () => {
    return (dispatch, getState) => {
        dispatch(removeHash());
    }
};

/*device token*/
const saveDeviceToken = (token) => ({
    type: ActionTypes.SAVE_DEVICE_TOKEN,
    device_token: token
});

const removeDeviceToken = () => ({
    type: ActionTypes.REMOVE_DEVICE_TOKEN,
});

const fetchUserDeviceToken = () => {
    return (dispatch, getState) => {
        dispatch(tokenIsLoading(true));
        dispatch(tokenHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                language: getState().account.language,
                ...getState().device.device
            },
            method: api.methods.token.device.get,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(tokenIsLoading(false));
            if (res.result) {
                dispatch(tokenHasError(false));
                dispatch(saveDeviceToken(res.result));
            } else if (res.error) {
                dispatch(tokenHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(tokenHasError(true, error));
        });
    }
};

const fetchUserCheckHash = () => {
    return (dispatch, getState) => {
        dispatch(tokenIsLoading(true));
        dispatch(tokenHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.token.check,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(tokenIsLoading(false));
            if (res.result) {
                dispatch(tokenHasError(false));
            } else if (res.error) {
                dispatch(tokenHasError(true, res.error.message));
                dispatch(removeUserHash());
            }
        }).catch((error) => {
            dispatch(tokenHasError(true, error));
        });
    }
};

const checkUserEnterData = () => {
    return dispatch => Promise.all([
        dispatch(DeviceActions.getDevice()),
        dispatch(fetchUserDeviceToken()),
        dispatch(fetchUserCheckHash()),
    ]);
};

export default {
    saveHash,
    removeHash,
    saveUserHash,
    removeUserHash,
    fetchUserDeviceToken,
    fetchUserCheckHash,
    checkUserEnterData
};