import * as ActionTypes from '../constants/ActionTypes';

const setSearch = (str) => ({
    type: ActionTypes.SAVE_SEARCH_DATA,
    payload: str,
});

export default {
    setSearch
};