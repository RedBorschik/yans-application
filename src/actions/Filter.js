import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';
import catalogActions from './Catalog';

const saveCatalogFilter = (data) => ({
    type: ActionTypes.SAVE_CATALOG_FILTER,
    payload: data,
});

const fetchCatalogFilter = (section, filter) => {
    return (dispatch, getState) => {
        dispatch(catalogActions.catalogHasError(false, ''));
        dispatch(catalogActions.catalogIsLoading(true, 'filter'));

        let data = {
            ...api.params,
            params: {
                device_token: getState().token.device_token,
                language: getState().account.language,
                section: section,
                filter: filter
            },
            method: api.methods.catalog.filter,
        };

        console.log(data);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogActions.catalogIsLoading(false, 'filter'));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(catalogActions.catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogActions.catalogHasError(false));
                    dispatch(saveCatalogFilter(res.result));
                } else if (res.error) {
                    dispatch(catalogActions.catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogActions.catalogIsLoading(false, 'filter'));
            dispatch(catalogActions.catalogHasError(true, error));
        });
    };
};

export default {
    fetchCatalogFilter,
};