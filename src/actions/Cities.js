import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';

const citiesHasError = (bool, text) => ({
    type: ActionTypes.CITIES_HAS_ERROR,
    error: bool,
    message: text,
});

const citiesIsLoading = (bool) => ({
    type: ActionTypes.CITIES_IS_LOADING,
    loading: bool,
});

const saveCities = (data) => ({
    type: ActionTypes.SAVE_CITIES,
    payload: data,
});

const setCity = (code) => ({
    type: ActionTypes.SET_CITY,
    payload: code,
});

const fetchCities = () => {
    return (dispatch, getState) => {
        dispatch(citiesIsLoading(true));
        dispatch(citiesHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                language: getState().account.language,
                device_token: getState().token.device_token,
            },
            method: api.methods.cities.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(citiesIsLoading(false));
            if (res.result) {
                dispatch(citiesHasError(false));
                dispatch(saveCities(res.result));
            } else if (res.error) {
                dispatch(citiesHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(citiesHasError(true, error));
        });
    };
};

const selectCitiesFetch = (city) => {
    return (dispatch, getState) => {
        dispatch(citiesIsLoading(true));
        dispatch(citiesHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                language: getState().account.language,
                device_token: getState().token.device_token,
                city: city,
            },
            method: api.methods.cities.select,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(citiesIsLoading(false));
            if (res.result) {
                dispatch(citiesHasError(false));
                dispatch(setCity(city));
            } else if (res.error) {
                dispatch(citiesHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(citiesHasError(true, error));
        });
    };
};

export default {
    citiesHasError,
    citiesIsLoading,
    saveCities,
    fetchCities,
    selectCitiesFetch,
    setCity,
};