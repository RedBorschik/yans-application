import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';
import actionsCatalog from './Catalog';

const showAddBasketModal = () => ({
    type: ActionTypes.VIEW_ADD_BASKET_MODAL,
    payload: true,
});

const hideAddBasketModal = () => ({
    type: ActionTypes.VIEW_ADD_BASKET_MODAL,
    payload: false,
});

const basketHasError = (bool, text) => ({
    type: ActionTypes.BASKET_HAS_ERROR,
    error: bool,
    message: text,
});

const basketAddHasError = (bool, text) => ({
    type: ActionTypes.BASKET_ADD_HAS_ERROR,
    error: bool,
    message: text,
});

const basketIsLoading = (bool) => ({
    type: ActionTypes.BASKET_IS_LOADING,
    loading: bool,
});

const basketIsAddLoading = (bool) => ({
    type: ActionTypes.BASKET_IS_ADD_LOADING,
    loading: bool,
});

const saveBasket = (data) => ({
    type: ActionTypes.SAVE_BASKET,
    payload: data,
});

const addItemBasket = (data) => ({
    type: ActionTypes.ADD_BASKET_ITEM,
    payload: data,
});

const updateItemBasket = (data) => ({
    type: ActionTypes.UPDATE_BASKET_ITEM,
    payload: data,
});

const removeItemBasket = (data) => ({
    type: ActionTypes.REMOVE_BASKET_ITEM,
    payload: data,
});

const resetBasket = () => ({
    type: ActionTypes.RESET_BASKET
});

const showAddBasketModalAction = () => {
    return (dispatch) => {
        dispatch(showAddBasketModal());
    }
};

const hideAddBasketModalAction = () => {
    return (dispatch) => {
        dispatch(hideAddBasketModal());
    }
};

const removeFetchBasket = (ids) => {
    return (dispatch, getState) => {
        dispatch(basketHasError(false, ''));
        dispatch(basketIsAddLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                items: ids,
            },
            method: api.methods.basket.delete,
        };

        return  fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(basketIsAddLoading(false));
            if (typeof res !== 'object') {
                dispatch(basketHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(basketHasError(false));
                    dispatch(removeItemBasket({ids: ids, sum: res.result.sum}));
                } else if (res.error) {
                    dispatch(basketHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(basketHasError(true, error));
        });
    };
};

const addFetchBasket = (products) => {
    return (dispatch, getState) => {
        dispatch(basketAddHasError(false, ''));
        dispatch(basketIsAddLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                products: products,
            },
            method: api.methods.basket.add,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(basketIsAddLoading(false));
            if (typeof res !== 'object') {
                dispatch(basketAddHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(showAddBasketModal());
                    dispatch(basketAddHasError(false));
                    res.result.map(item => {
                        dispatch(addItemBasket({id: item}));
                    });
                } else if (res.error) {
                    dispatch(basketAddHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(basketAddHasError(true, error));
        });
    };
};

const updateFetchBasket = (item) => {
    return (dispatch, getState) => {
        dispatch(basketHasError(false, ''));
        dispatch(basketIsAddLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...item
            },
            method: api.methods.basket.update,
        };

        return  fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(basketIsAddLoading(false));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(basketHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(basketHasError(false));
                    dispatch(addItemBasket({id: item.id, ...res.result}));
                } else if (res.error) {
                    dispatch(basketHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(basketHasError(true, error));
        });
    };
};

const listFetchBasket = () => {
    return (dispatch, getState) => {
        dispatch(basketHasError(false, ''));
        dispatch(basketIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.basket.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(basketIsLoading(false));
            if (res.result) {
                dispatch(basketHasError(false));
                dispatch(saveBasket(res.result));
            } else if (res.error) {
                dispatch(basketHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(basketHasError(true, error));
        });
    };
};

export default {
    basketHasError,
    basketIsLoading,
    saveBasket,
    addFetchBasket,
    listFetchBasket,
    updateFetchBasket,
    removeFetchBasket,
    showAddBasketModal,
    hideAddBasketModal,
    hideAddBasketModalAction,
    showAddBasketModalAction,
    addItemBasket,
    resetBasket,
    basketAddHasError,
    updateItemBasket,
};