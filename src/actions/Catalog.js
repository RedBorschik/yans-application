import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import searchActions from './Search';
import basketActions from './Basket';
import {errors} from '../utils/errors';

const catalogHasError = (bool, text) => ({
    type: ActionTypes.CATALOG_HAS_ERROR,
    error: bool,
    message: text,
});

const catalogSearchHasError = (bool, text) => ({
    type: ActionTypes.CATALOG_SEARCH_HAS_ERROR,
    error: bool,
    message: text,
});

const catalogIsLoading = (bool, data) => ({
    type: ActionTypes.CATALOG_IS_LOADING,
    loading: bool,
    data: data,
});

const saveCatalogItems = (data) => ({
    type: ActionTypes.SAVE_CATALOG_ITEMS,
    payload: data,
});

const resetSort = () => ({
    type: ActionTypes.RESET_SORT,
});

const resetFilter = () => ({
    type: ActionTypes.RESET_FILTER,
});

const saveCatalogItem = (data) => ({
    type: ActionTypes.SAVE_CATALOG_ITEM,
    payload: data,
});

const saveCatalogCategories = (data) => ({
    type: ActionTypes.SAVE_CATALOG_CATEGORIES,
    payload: data,
});

const saveCatalogNewItems = (data) => ({
    type: ActionTypes.SAVE_CATALOG_NEW_ITEMS,
    payload: data,
});

const saveCatalogSearchItems = (data) => ({
    type: ActionTypes.SAVE_CATALOG_SEARCH_ITEMS,
    payload: data,
});

const saveCatalogFavoritesItems = (data) => ({
    type: ActionTypes.SAVE_CATALOG_FAVORITES_ITEMS,
    payload: data,
});

const resetItems = () => ({
    type: ActionTypes.RESET_CATALOG_ITEMS,
});

const resetSearchItems = () => ({
    type: ActionTypes.RESET_CATALOG_SEARCH_ITEMS,
});

const resetNewItems = () => ({
    type: ActionTypes.RESET_CATALOG_NEW_ITEMS,
});

const addFavoritesItem = (id) => ({
    type: ActionTypes.ADD_CATALOG_FAVORITES_ITEM,
    payload: id,
});

const removeFavoritesItem = (id) => ({
    type: ActionTypes.REMOVE_CATALOG_FAVORITES_ITEM,
    payload: id,
});

const fetchSearchItems = (search) => {
    return (dispatch, getState) => {
        //if (getState().catalog.loading) return false;
        if (Number(getState().catalog.pagination.search.page) > getState().catalog.pagination.search.count_pages) return false;

        dispatch(catalogSearchHasError(false, ''));
        dispatch(catalogIsLoading(true, 'search'));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().catalog.pagination.search.page,
                search: search,
            },
            method: api.methods.catalog.search,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'search'));
            console.log('res', res);
            if (typeof res !== 'object') {
                // console.log(res);
                dispatch(catalogSearchHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    //console.log(res.result.items.map(item => item.id));
                    dispatch(catalogSearchHasError(false));
                    dispatch(saveCatalogSearchItems(res.result));
                    dispatch(searchActions.setSearch(search));
                } else if (res.error) {
                    dispatch(catalogSearchHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogIsLoading(false, 'search'));
            dispatch(catalogSearchHasError(true, error));
        });
    };
};

const fetchRemoveFavoritesItem = (id, basket_id) => {
    return (dispatch, getState) => {
        dispatch(catalogHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.catalog.favorites.remove,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            if (res.result) {
                dispatch(catalogHasError(false));
                dispatch(removeFavoritesItem(id));
                if (basket_id) {
                    dispatch(basketActions.updateItemBasket({
                        id: basket_id,
                        in_wish_list: false,
                    }));
                }
            } else if (res.error) {
                dispatch(catalogHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchAddFavoritesItem = (id, basket_id) => {
    return (dispatch, getState) => {
        dispatch(catalogHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.catalog.favorites.add,
        };

        console.log(data);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            console.log(res);
            if (res.result) {
                dispatch(catalogHasError(false));
                dispatch(addFavoritesItem(id));
                if (basket_id) {
                    dispatch(basketActions.updateItemBasket({
                        id: basket_id,
                        in_wish_list: true,
                    }));
                }
            } else if (res.error) {
                dispatch(catalogHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchFavoritesItems = () => {
    return (dispatch, getState) => {
        //if (getState().catalog.loading) return false;
        if (Number(getState().catalog.pagination.favorites.page) > getState().catalog.pagination.favorites.count_pages) return false;

        dispatch(catalogHasError(false, ''));
        dispatch(catalogIsLoading(true, 'favorites'));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().catalog.pagination.favorites.page,
            },
            method: api.methods.catalog.favorites.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'favorites'));
            if (typeof res !== 'object') {
                // console.log(res);
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogHasError(false));
                    dispatch(saveCatalogFavoritesItems(res.result));
                } else if (res.error) {
                    dispatch(catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogIsLoading(false, 'favorites'));
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchCatalogNewItems = () => {
    return (dispatch, getState) => {
        //if (getState().catalog.loading) return false;
        if (Number(getState().catalog.pagination.new_items.page) > getState().catalog.pagination.new_items.count_pages) return false;

        dispatch(catalogHasError(false, ''));
        dispatch(catalogIsLoading(true, 'new_items'));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().catalog.pagination.new_items.page,
            },
            method: api.methods.catalog.new_items,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'new_items'));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogHasError(false));
                    dispatch(saveCatalogNewItems(res.result));
                } else if (res.error) {
                    dispatch(catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogIsLoading(false, 'new_items'));
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchCatalogItems = (section, sort, filter) => {
    return (dispatch, getState) => {
        //if (getState().catalog.loading) return false;
        if (Number(getState().catalog.pagination.items.page) > getState().catalog.pagination.items.count_pages) return false;

        dispatch(catalogHasError(false, ''));
        dispatch(catalogIsLoading(true, 'items'));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().catalog.pagination.items.page,
            },
            method: api.methods.catalog.items,
        };

        if (section) {
            data.params.section = section;
        }

        if (sort) {
            data.params.sort = sort;
        }

        if (filter) {
            data.params.filter = filter;
        }

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'items'));
            if (typeof res !== 'object') {
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                //console.log(res);
                if (res.result) {
                    dispatch(catalogHasError(false));
                    dispatch(saveCatalogItems(res.result));
                } else if (res.error) {
                    dispatch(catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogIsLoading(false, 'items'));
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchCatalogDetailItem = (id) => {
    return (dispatch, getState) => {
        //if (getState().catalog.loading) return false;
        if (Number(getState().catalog.pagination.items.page) > getState().catalog.pagination.items.count_pages) return false;

        dispatch(catalogHasError(false, ''));
        dispatch(catalogIsLoading(true, 'card'));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.catalog.detail,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'card'));
            // console.log('detail', res);
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogHasError(false));
                    dispatch(saveCatalogItem(res.result));
                } else if (res.error) {
                    dispatch(catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(catalogIsLoading(false, 'card'));
            dispatch(catalogHasError(true, error));
        });
    };
};

const fetchCatalogCategories = () => {
    return (dispatch, getState) => {
        dispatch(catalogHasError(false, ''));
        dispatch(catalogIsLoading(true, 'categories'));

        let data = {
            ...api.params,
            params: {
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.catalog.categories,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(catalogIsLoading(false, 'categories'));
            if (typeof res !== 'object') {
                console.log(res);
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogHasError(false));
                    dispatch(saveCatalogCategories(res.result));
                } else if (res.error) {
                    dispatch(catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogIsLoading(false, 'categories'));
            dispatch(catalogHasError(true, error));
        });
    };
};

export default {
    catalogHasError,
    catalogIsLoading,
    saveCatalogItems,
    saveCatalogNewItems,
    fetchCatalogItems,
    fetchCatalogNewItems,
    fetchAddFavoritesItem,
    fetchRemoveFavoritesItem,
    fetchFavoritesItems,
    fetchCatalogDetailItem,
    fetchCatalogCategories,
    resetItems,
    saveCatalogSearchItems,
    resetSearchItems,
    fetchSearchItems,
    resetSort,
    resetFilter,
};