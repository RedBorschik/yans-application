import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';
import ActionsToken from './Token';
import NavigationService from '../NavigationService';

const loginHasError = (bool, text) => {
    return {
        type: ActionTypes.LOGIN_HAS_ERROR,
        error: bool,
        message: text,
    };
};

const loginIsLoading = (bool) => {
    return {
        type: ActionTypes.LOGIN_IS_LOADING,
        loading: bool,
    };
};

const loginUser = (email, phone, password) => {
    return (dispatch, getState) => {
        dispatch(loginHasError(false, ''));
        dispatch(loginIsLoading(true));

        if ((!phone && !email) || !password) {
            dispatch(loginHasError(true, errors.auth.data));
            dispatch(loginIsLoading(false));

            return;
        }

        let data = {
            ...api.params,
            params: {
                password: password,
                language: 'ru',
                device: getState().device.device,
                device_token: getState().token.device_token,
            },
            method: !!email ? api.methods.auth.email : api.methods.auth.phone,
        };

        if (!!email) data.params.email = email;
        if (!!phone) data.params.phone = phone;

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(loginIsLoading(false));
            if (res.result) {
                console.log('login res',res.result)
                dispatch(loginHasError(false));
                dispatch(ActionsToken.saveUserHash(res.result));
            } else if (res.error) {
                dispatch(loginHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(loginHasError(true, error));
        });
    };
};


const clearAccount = () => {
    return {
        type: ActionTypes.CLEAR_ACCOUNT,
    };
};

const clearAddress = () => {
    return {
        type: ActionTypes.CLEAR_ADDRESS,
    };
};

const clearBasket = () => {
    return {
        type: ActionTypes.CLEAR_BASKET,
    };
};

const clearCatalog = () => {
    return {
        type: ActionTypes.CLEAR_CATALOG,
    };
};

const clearContagents = () => {
    return {
        type: ActionTypes.CLEAR_CONTRAGENTS,
    };
};

const clearOrders = () => {
    return {
        type: ActionTypes.CLEAR_ORDERS,
    };
};

const clearLogin = () => {
    return {
        type: ActionTypes.CLEAR_LOGIN,
    };
};

const clearNotifications = () => {
    return {
        type: ActionTypes.CLEAR_NOTIFICATIONS,
    };
};

const clearPassword = () => {
    return {
        type: ActionTypes.CLEAR_PASSWORD,
    };
};

const clearRegister = () => {
    return {
        type: ActionTypes.CLEAR_REGISTER,
    };
};

const clearSearch = () => {
    return {
        type: ActionTypes.CLEAR_SEARCH,
    };
};

const logoutUser = () => {
    return (dispatch, getState) => {
        dispatch(ActionsToken.removeUserHash());
        NavigationService.navigate('Auth');
        dispatch(clearSearch());
        dispatch(clearRegister());
        dispatch(clearPassword());
        dispatch(clearNotifications());
        dispatch(clearLogin());
        dispatch(clearOrders());
        dispatch(clearContagents());
        dispatch(clearCatalog());
        dispatch(clearBasket());
        dispatch(clearAddress());
        dispatch(clearAccount());
    };
};

export default {
    loginHasError,
    loginIsLoading,
    loginUser,
    logoutUser,
};