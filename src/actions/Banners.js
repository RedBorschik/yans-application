import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';


const bannersHasError = (bool, text) => ({
    type: ActionTypes.BANNERS_HAS_ERROR,
    error: bool,
    message: text,
});

const bannersIsLoading = (bool) => ({
    type: ActionTypes.BANNERS_IS_LOADING,
    loading: bool,
});

/*hash*/
const saveBanners = (data) => ({
    type: ActionTypes.SAVE_BANNERS,
    payload: data
});

const fetchBanners = () => {
    return (dispatch, getState) => {
        dispatch(bannersIsLoading(true));
        dispatch(bannersHasError(false, ''));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.device_token,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.banners.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(bannersIsLoading(false));
            if (res.result) {
                dispatch(bannersHasError(false));
                dispatch(saveBanners(res.result));
            } else if (res.error) {
                dispatch(bannersHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(bannersHasError(true, error));
        });
    }
};

export default {
    bannersHasError,
    bannersIsLoading,
    saveBanners,
    fetchBanners
};