import * as ActionTypes from '../constants/ActionTypes';
import basketActions from './Basket';
import {api} from '../utils/api';

const ordersHasError = (bool, text) => ({
    type: ActionTypes.ORDERS_HAS_ERROR,
    error: bool,
    message: text,
});

const ordersIsLoading = (bool) => ({
    type: ActionTypes.ORDERS_IS_LOADING,
    loading: bool,
});

const ordersActionsIsLoading = (bool) => ({
    type: ActionTypes.ORDERS_ACTIONS_IS_LOADING,
    loading_actions: bool,
});

const saveOrder = (data) => ({
    type: ActionTypes.SAVE_ORDER,
    payload: data,
});

const saveOrders = (data) => ({
    type: ActionTypes.SAVE_ORDERS,
    payload: data,
});

const saveDeliveryList = (data) => ({
    type: ActionTypes.SAVE_ORDERS_DELIVERY_LIST,
    payload: data,
});

const savePaySystemList = (data) => ({
    type: ActionTypes.SAVE_ORDERS_PAY_SYSTEM_LIST,
    payload: data,
});


const newOrder = (data) => ({
    type: ActionTypes.NEW_ORDER,
    payload: data,
});

const resetOrders = () => ({
    type: ActionTypes.RESET_ORDERS,
});

const addOrder = (data) => ({
    type: ActionTypes.ADD_ORDER,
    payload: data,
});

const saveOrderParams = (data, id) => ({
    type: ActionTypes.SAVE_ORDER_PARAMS,
    payload: {
        data: data,
        id: id
    },
});

const updateOrder = (data) => ({
    type: ActionTypes.UPDATE_ORDER,
    payload: data,
});

const addFetchOrder = (order) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersActionsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                city: getState().cities.city,
                ...order
            },
            method: api.methods.orders.add,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(ordersActionsIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(ordersHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                   // console.log(res.result);
                    dispatch(ordersHasError(false));
                    dispatch(addOrder(res.result));
                    dispatch(basketActions.resetBasket());
                } else if (res.error) {
                    dispatch(ordersHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const cancelFetchOrder = (id) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersActionsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id
            },
            method: api.methods.orders.cancel,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(ordersActionsIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(ordersHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(ordersHasError(false));
                    dispatch(updateOrder({
                        id: id,
                        status: 'Отменен',
                        detail: {
                            status: 'Отменен'
                        }
                    }));
                } else if (res.error) {
                    dispatch(ordersHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const listFetchOrders = () => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().orders.page,
            },
            method: api.methods.orders.list,
        };

       // console.log(api.methods.orders.list);
        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            if (res.result) {
                dispatch(ordersHasError(false));
                dispatch(saveOrders(res.result));
            } else if (res.error) {
                dispatch(ordersHasError(true, res.error.message));
            }
            dispatch(ordersIsLoading(false));
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const detailFetchOrders = (id) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.orders.detail,
        };
       // console.log(api.methods.orders.detail);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
           // console.log(res);
            dispatch(ordersIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(ordersHasError(false));
                    dispatch(saveOrder(res.result));
                } else if (res.error) {
                    dispatch(ordersHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const repeatFetchOrders = (id) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersActionsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.orders.repeat,
        };
        //console.log(api.methods.orders.repeat);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(ordersActionsIsLoading(false));
            if (res.result) {
                dispatch(ordersHasError(false, 'Товары добавлены в корзину'));
                dispatch(newOrder({id: id}));
                res.result.map(item => {
                    dispatch(basketActions.addItemBasket({id: item}));
                });
            } else if (res.error) {
                dispatch(ordersHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const listFetchDeliveryList = (params) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                city: getState().cities.city,
                ...params
            },
            method: api.methods.orders.delivery_list,
        };

       // console.log(api.methods.orders.delivery_list);
        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            if (res.result) {
                dispatch(ordersHasError(false));
                dispatch(saveDeliveryList(res.result));
            } else if (res.error) {
                dispatch(ordersHasError(true, res.error.message));
            }
            dispatch(ordersIsLoading(false));
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const listFetchPaySystemList = (params) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...params
            },
            method: api.methods.orders.pay_system_list,
        };
       // console.log(api.methods.orders.pay_system_list);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            if (res.result) {
                dispatch(ordersHasError(false));
                dispatch(savePaySystemList(res.result));
            } else if (res.error) {
                dispatch(ordersHasError(true, res.error.message));
            }
            dispatch(ordersIsLoading(false));
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

const paramsOrderFetch = (id) => {
    return (dispatch, getState) => {
        dispatch(ordersHasError(false, ''));
        dispatch(ordersIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.orders.params,
        };

       // console.log(api.methods.orders.params);

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(ordersIsLoading(false));
            if (res.result) {
                dispatch(ordersHasError(false));
                dispatch(saveOrderParams(res.result, id));
            } else if (res.error) {
                dispatch(ordersHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(ordersHasError(true, error));
        });
    };
};

export default {
    ordersHasError,
    ordersIsLoading,
    saveOrders,
    ordersActionsIsLoading,
    addFetchOrder,
    cancelFetchOrder,
    listFetchOrders,
    resetOrders,
    detailFetchOrders,
    repeatFetchOrders,
    newOrder,
    listFetchDeliveryList,
    listFetchPaySystemList,
    paramsOrderFetch
};