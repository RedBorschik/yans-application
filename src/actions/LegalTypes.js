import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';

const legaltypesHasError = (bool, text) => ({
    type: ActionTypes.LEGALTYPES_HAS_ERROR,
    error: bool,
    message: text,
});

const legaltypesIsLoading = (bool) => ({
    type: ActionTypes.LEGALTYPES_IS_LOADING,
    loading: bool,
});

const saveLegalTypes = (data) => ({
    type: ActionTypes.SAVE_LEGALTYPES,
    payload: data,
});


const listFetchLegalTypes = () => {
    return (dispatch, getState) => {
        dispatch(legaltypesHasError(false, ''));
        dispatch(legaltypesIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.contragents.types,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(legaltypesIsLoading(false));
            if (res.result) {
                dispatch(legaltypesHasError(false));
                dispatch(saveLegalTypes(res.result));
            } else if (res.error) {
                dispatch(legaltypesHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(legaltypesHasError(true, error));
        });
    };
};

export default {
    legaltypesIsLoading,
    legaltypesHasError,
    listFetchLegalTypes,
};