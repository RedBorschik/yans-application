import * as ActionTypes from '../constants/ActionTypes';
import basketActions from './Basket';
import {api} from '../utils/api';

const newsHasError = (bool, text) => ({
    type: ActionTypes.NEWS_HAS_ERROR,
    error: bool,
    message: text,
});

const newsIsLoading = (bool) => ({
    type: ActionTypes.NEWS_IS_LOADING,
    loading: bool,
});

const saveNews = (data) => ({
    type: ActionTypes.NEWS_SAVE,
    payload: data,
});

const saveNewsDetail = (data) => ({
    type: ActionTypes.NEWS_SAVE_DETAIL,
    payload: data,
});

const listFetchNews = () => {
    return (dispatch, getState) => {
        dispatch(newsHasError(false, ''));
        dispatch(newsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                device_token: getState().token.device_token,
                language: getState().account.language,
                page: getState().news.page,
            },
            method: api.methods.news.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            if (res.result) {
                dispatch(newsHasError(false));
                dispatch(saveNews(res.result));
            } else if (res.error) {
                dispatch(newsHasError(true, res.error.message));
            }
            dispatch(newsIsLoading(false));
        }).catch((error) => {
            dispatch(newsHasError(true, error));
        });
    };
};

const detailFetchNews = (id) => {
    return (dispatch, getState) => {
        dispatch(newsHasError(false, ''));
        dispatch(newsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.news.detail,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(newsIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(newsHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(newsHasError(false));
                    dispatch(saveNewsDetail(res.result));
                } else if (res.error) {
                    dispatch(newsHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(newsHasError(true, error));
        });
    };
};


export default {
    newsHasError,
    newsIsLoading,
    saveNews,
    listFetchNews,
    detailFetchNews
};