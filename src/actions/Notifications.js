import * as ActionTypes from '../constants/ActionTypes';
import firebase, {Notification} from 'react-native-firebase';
import {api} from '../utils/api';

const notifyHasError = (bool, text) => ({
    type: ActionTypes.NOTIFY_HAS_ERROR,
    error: bool,
    message: text,
});

const notifyIsLoading = (bool) => ({
    type: ActionTypes.NOTIFY_IS_LOADING,
    loading: bool,
});

const saveFcmKey = (token) => ({
    type: ActionTypes.SAVE_FCM_KEY,
    token: token,
});

const saveChannelName = (channel_name) => ({
    type: ActionTypes.SAVE_CHANNEL_NAME,
    channel_name: channel_name,
});

const saveFirstEnter = (bool) => ({
    type: ActionTypes.SAVE_FIRST_ENTER,
    first_enter: bool,
});

const fetchSaveFmcToken = (token_fcm) => {
    return (dispatch, getState) => {
        dispatch(notifyHasError(false, ''));
        dispatch(notifyIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                token_fcm: token_fcm,
            },
            method: api.methods.user.push.fmc,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(notifyIsLoading(false));
            //console.log(data.method, res);
            if (typeof res !== 'object') {
                dispatch(notifyHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    console.log('Save token');
                    dispatch(notifyHasError(false));
                    dispatch(saveFcmKey(token_fcm));
                } else if (res.error) {
                    dispatch(notifyHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(notifyHasError(true, error));
        });
    };
};

const getToken = () => {
    return (dispatch, getState) => {
        //console.log('before fcmToken: ', getState().notify.token);
        if (!getState().notify.token) {
            firebase.messaging().getToken().then((res) => {
                //console.log('after fcmToken: ', res);
                dispatch(fetchSaveFmcToken(res));
            });
        }
    };
};

const requestPermission = () => {
    return (dispatch, getState) => {
        firebase.messaging().requestPermission().then(() => {
            //console.log('permission checked');
            dispatch(getToken());
        }).catch(error => {
            //console.log('permission rejected');
        });
    };
};

const checkPermission = () => {
    return (dispatch, getState) => {
        firebase.messaging().hasPermission().then(enabled => {
            if (enabled) {
                //console.log('Permission granted');
                dispatch(getToken());
            } else {
                //console.log('Request Permission');
                dispatch(requestPermission());
            }
        });
    };
};

const createChannel = () => {
    return (dispatch, getState) => {
        const channel_name = 'yansChannel';
        const channel = new firebase.notifications.Android.Channel(channel_name, 'Yans Mess Channel', firebase.notifications.Android.Importance.Max);

        return firebase.notifications().android.createChannel(channel).then(() => {
            dispatch(saveChannelName(channel_name));
        });
    };
};

export default {
    saveFirstEnter,
    saveFcmKey,
    notifyHasError,
    notifyIsLoading,
    checkPermission,
    requestPermission,
    createChannel,
    fetchSaveFmcToken,
};