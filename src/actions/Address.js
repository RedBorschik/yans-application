import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';

const addressHasError = (bool, text) => ({
    type: ActionTypes.ADDRESS_HAS_ERROR,
    error: bool,
    message: text,
});

const addressIsLoading = (bool) => ({
    type: ActionTypes.ADDRESS_IS_LOADING,
    loading: bool,
});

const addressDetailIsLoading = (bool) => ({
    type: ActionTypes.ADDRESS_DETAIL_IS_LOADING,
    loading: bool,
});

const saveAddress = (data) => ({
    type: ActionTypes.SAVE_ADDRESS,
    payload: data,
});

const saveItemAddress = (data) => ({
    type: ActionTypes.SAVE_ITEM_ADDRESS,
    payload: data,
});

const removeItemAddress = (id) => ({
    type: ActionTypes.REMOVE_ITEM_ADDRESS,
    payload: id,
});

const removeFetchAddress = (id) => {
    return (dispatch, getState) => {
        dispatch(addressHasError(false, ''));
        dispatch(addressDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id
            },
            method: api.methods.user.addresses.delete,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(addressDetailIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(addressHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(addressHasError(false));
                    dispatch(removeItemAddress(id))
                } else if (res.error) {
                    dispatch(addressHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(addressHasError(true, error));
        });
    };
};

const addFetchAddress = (address) => {
    return (dispatch, getState) => {
        dispatch(addressHasError(false, ''));
        dispatch(addressDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...address
            },
            method: api.methods.user.addresses.add,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(addressDetailIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(addressHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(addressHasError(false));
                    dispatch(saveItemAddress({
                        address: address.address,
                        city: {
                            code: address.city,
                            name: address.city_name,
                        },
                        contact_person: address.contact_person,
                        id: res.result,
                        name_shop: address.name_shop,
                        phone: address.phone,
                    }));
                } else if (res.error) {
                    dispatch(addressHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(addressHasError(true, error));
        });
    };
};

const detailFetchAddress = (id) => {
    return (dispatch, getState) => {
        dispatch(addressHasError(false, ''));
        dispatch(addressIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.user.addresses.detail,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(addressIsLoading(false));
            if (res.result) {
                dispatch(addressHasError(false));
                dispatch(saveItemAddress(res.result));
            } else if (res.error) {
                dispatch(addressHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(addressHasError(true, error));
        });
    };
};

const updateFetchAddress = (address) => {
    return (dispatch, getState) => {
        dispatch(addressHasError(false, ''));
        dispatch(addressDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...address
            },
            method: api.methods.user.addresses.update,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(addressDetailIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(addressHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(addressHasError(false));
                    dispatch(saveItemAddress({
                        address: address.address,
                        city: {
                            code: address.city,
                            name: address.city_name,
                        },
                        contact_person: address.contact_person,
                        id: address.id,
                        name_shop: address.name_shop,
                        phone: address.phone,
                    }));
                } else if (res.error) {
                    dispatch(addressHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(addressHasError(true, error));
        });
    };
};

const listFetchAddress = () => {
    return (dispatch, getState) => {
        dispatch(addressHasError(false, ''));
        dispatch(addressIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.addresses.list,
        };

        return  fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(addressIsLoading(false));
            if (res.result) {
                dispatch(addressHasError(false));
                dispatch(saveAddress(res.result));
            } else if (res.error) {
                dispatch(addressHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(addressHasError(true, error));
        });
    };
};

export default {
    addressHasError,
    addressIsLoading,
    saveAddress,
    addFetchAddress,
    listFetchAddress,
    updateFetchAddress,
    detailFetchAddress,
    removeFetchAddress,
    addressDetailIsLoading,
};