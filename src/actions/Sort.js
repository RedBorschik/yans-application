import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';
import catalogActions from './Catalog';

const saveCatalogSort = (data) => ({
    type: ActionTypes.SAVE_CATALOG_SORT,
    payload: data,
});

const fetchCatalogSort = () => {
    return (dispatch, getState) => {
        dispatch(catalogActions.catalogHasError(false, ''));
        dispatch(catalogActions.catalogIsLoading(true, 'sort'));

        let data = {
            ...api.params,
            params: {
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.catalog.sort,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text()
            }
        }).then(res => {
            dispatch(catalogActions.catalogIsLoading(false, 'sort'));
            if (typeof res !== 'object') {
                dispatch(catalogActions.catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(catalogActions.catalogHasError(false));
                    dispatch(saveCatalogSort(res.result));
                } else if (res.error) {
                    dispatch(catalogActions.catalogHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            console.log(error);
            dispatch(catalogActions.catalogIsLoading(false, 'sort'));
            dispatch(catalogActions.catalogHasError(true, error));
        });
    };
};

export default {
    fetchCatalogSort,
};