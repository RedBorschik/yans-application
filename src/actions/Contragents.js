import * as ActionTypes from '../constants/ActionTypes';
import {api} from '../utils/api';
import {errors} from '../utils/errors';

const contragentsHasError = (bool, text) => ({
    type: ActionTypes.CONTRAGENTS_HAS_ERROR,
    error: bool,
    message: text,
});

const contragentsIsLoading = (bool) => ({
    type: ActionTypes.CONTRAGENTS_IS_LOADING,
    loading: bool,
});

const contragentsDetailIsLoading = (bool) => ({
    type: ActionTypes.CONTRAGENTS_DETAIL_IS_LOADING,
    loading: bool,
});

const saveContragents = (data) => ({
    type: ActionTypes.SAVE_CONTRAGENTS,
    payload: data,
});

const saveItemContragents = (data) => ({
    type: ActionTypes.SAVE_ITEM_CONTRAGENTS,
    payload: data,
});

const removeItemContragents = (id) => ({
    type: ActionTypes.REMOVE_ITEM_CONTRAGENTS,
    payload: id,
});

const removeFetchContragents = (id) => {
    return (dispatch, getState) => {
        dispatch(contragentsHasError(false, ''));
        dispatch(contragentsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.user.contragents.delete,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(contragentsIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(contragentsHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(contragentsHasError(false));
                    dispatch(removeItemContragents(id));
                } else if (res.error) {
                    dispatch(contragentsHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(contragentsHasError(true, error));
        });
    };
};

const addFetchContragents = (contragent) => {
    return (dispatch, getState) => {
        dispatch(contragentsHasError(false, ''));
        dispatch(contragentsDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...contragent
            },
            method: api.methods.user.contragents.add,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(contragentsDetailIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(contragentsHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(contragentsHasError(false));
                    dispatch(saveItemContragents({
                        name: contragent.name,
                        inn: contragent.inn,
                        bik: contragent.bik,
                        address: contragent.address,
                        bank: contragent.bank,
                        rs: contragent.rs,
                        form: {
                            id: contragent.form,
                            name: contragent.form_name,
                        },
                        id: res.result,
                    }));
                } else if (res.error) {
                    dispatch(contragentsHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(contragentsHasError(true, error));
        });
    };
};

const detailFetchContragents = (id) => {
    return (dispatch, getState) => {
        dispatch(contragentsHasError(false, ''));
        dispatch(contragentsDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                id: id,
            },
            method: api.methods.user.contragents.detail,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => res.json()).then(res => {
            dispatch(contragentsDetailIsLoading(false));
            if (res.result) {
                dispatch(contragentsHasError(false));
                dispatch(saveItemContragents(res.result));
            } else if (res.error) {
                dispatch(contragentsHasError(true, res.error.message));
            }
        }).catch((error) => {
            dispatch(contragentsHasError(true, error));
        });
    };
};

const updateFetchContragents = (contragent) => {
    return (dispatch, getState) => {
        dispatch(contragentsHasError(false, ''));
        dispatch(contragentsDetailIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
                ...contragent
            },
            method: api.methods.user.contragents.update,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text();
            }
        }).then(res => {
            dispatch(contragentsDetailIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(contragentsHasError(true, 'ERROR'));
            } else {
                if (res.result) {
                    dispatch(contragentsHasError(false));
                    dispatch(saveItemContragents({
                        name: contragent.name,
                        inn: contragent.inn,
                        bik: contragent.bik,
                        address: contragent.address,
                        bank: contragent.bank,
                        rs: contragent.rs,
                        form: {
                            id: contragent.form,
                            name: contragent.form_name,
                        },
                        id: contragent.id,
                    }));
                } else if (res.error) {
                    dispatch(contragentsHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(contragentsHasError(true, error));
        });
    };
};

const listFetchContragents = () => {
    return (dispatch, getState) => {
        dispatch(contragentsHasError(false, ''));
        dispatch(contragentsIsLoading(true));

        let data = {
            ...api.params,
            params: {
                hash: getState().token.hash,
                device_token: getState().token.device_token,
                language: getState().account.language,
            },
            method: api.methods.user.contragents.list,
        };

        return fetch(api.url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((res) => {
            const contentType = res.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                return res.json();
            } else {
                return res.text()
            }
        }).then(res => {
            dispatch(contragentsIsLoading(false));
            if (typeof res !== 'object') {
                dispatch(catalogHasError(true, 'Произошла ошибка на сервере. Извините за неудобство'));
            } else {
                if (res.result) {
                    dispatch(contragentsHasError(false));
                    dispatch(saveContragents(res.result));
                } else if (res.error) {
                    dispatch(contragentsHasError(true, res.error.message));
                }
            }
        }).catch((error) => {
            dispatch(contragentsHasError(true, error));
        });
    };
};

export default {
    contragentsHasError,
    contragentsIsLoading,
    saveContragents,
    addFetchContragents,
    listFetchContragents,
    updateFetchContragents,
    detailFetchContragents,
    removeFetchContragents,
    contragentsDetailIsLoading,
};