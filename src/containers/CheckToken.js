import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StatusBar, AsyncStorage} from 'react-native';
import accountActions from '../actions/Account';
import basketActions from '../actions/Basket';
import ordersActions from '../actions/Orders';
import notifyActions from '../actions/Notifications';
import AppLoaderCustom from '../components/helpers/AppLoaderCustom';

import SplashScreen from 'react-native-splash-screen';

import styled from 'styled-components/native';

const ContainerView = styled.View`
  flex: 1;
  flexDirection: column;
  justifyContent: flex-start;
  alignItems: stretch; 
`;

class CheckToken extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
    }

    getUserOrderBasketData = () => {
        return Promise.all([
            this.props.listFetchBasket(),
            this.props.listFetchOrders(),
            this.props.getFetchProfile(),
        ]);
    };

    navigateByNotificationData = (data) => {
        if (typeof data !== 'object') return false;
        if (!data.type) return false;

        switch (data.type) {
            case 'order':
                if (data.id) this.props.navigation.navigate('OrdersDetail', {id: data.id});
                break;
            case 'product':
                if (data.id) this.props.navigation.navigate('Cart', {id: data.id});
                break;
            case 'news':
                 if (data.id) NavigationService.navigate('NewsDetail', {id: data.id});
                 break;
            case 'chat':
                this.props.navigation.navigate('Chat');
                break;
            default:
                this.props.navigation.navigate('Main');
                break;
        }
    };

    isJson = (text) => {
        if (typeof text !== 'string') {
            return false;
        }
        try {
            JSON.parse(text);
            return true;
        } catch (error) {
            return false;
        }
    };

    parseNotificationData = (action) => {
        let text = action.replace(/&quot;/g, '"');
        if (this.isJson(text)) {
            return JSON.parse(text);
        }
        return null;
    };

    async checkNotify() {
        let context = this;
        try {
            let value = await AsyncStorage.getItem('click_action');
            if (value !== null) {
                this.navigateByNotificationData(this.parseNotificationData(value));
                AsyncStorage.removeItem('click_action');
            }
            else {
                this.props.navigation.navigate('Main');
            }
        } catch (error) {
            this.props.navigation.navigate('Main');
        }
    }

    componentDidMount() {
        if (this.props.hash && this.props.hash !== null) {
            this.getUserOrderBasketData().then(() => {
                this.checkNotify();
                //this.props.navigation.navigate('Card', {id: 11920});
            });
            //this.props.navigation.navigate('Section', {section: 393, title: 'Пластиковые стаканы'});
            //this.props.navigation.navigate('Favorites');
        } else {
            this.props.navigation.navigate('Auth');
        }

        SplashScreen.hide();

        if (!this.props.first_enter) {
            this.props.checkPermission();
            this.props.saveFirstEnter(true);
        }

    }

    render() {
        return (
            <AppLoaderCustom/>
        );
    }
}

const mapStateToProps = state => ({
    hash: state.token.hash,
    navigator: state.navigator,
    first_enter: state.notify.first_enter,
});

const mapDispatchToProps = (dispatch) => ({
    getFetchProfile: () => dispatch(accountActions.getFetchProfile()),
    listFetchBasket: () => dispatch(basketActions.listFetchBasket()),
    listFetchOrders: () => dispatch(ordersActions.listFetchOrders()),
    checkPermission: () => dispatch(notifyActions.checkPermission()),
    saveFirstEnter: (bool) => dispatch(notifyActions.saveFirstEnter(bool)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckToken);