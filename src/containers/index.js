import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ThemeProvider} from 'react-native-elements';
import {StatusBar, Platform, View, Alert} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

import Navigator from '../Navigator';
import notifyActions from '../actions/Notifications';
import NavigationService from '../NavigationService';
import Notifications from '../Notifications';

import actionsDevice from './../actions/Device';
import actionsToken from './../actions/Token';

import AppLoaderCustom from '../components/helpers/AppLoaderCustom';
import Loader from '../components/helpers/Loader';

import styled from 'styled-components';
import {theme, global} from '../utils/theme';

const Container = styled.View`
    flex: 1;
`;

const StatusBarAndroid = styled.View`
    background-color: transparent;
`;

const ConnectionWrap = styled.View`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    zIndex: 100;
    backgroundColor: rgba(255,255,255,0.5);
`;

const ConnectionBar = styled.View`
    position: absolute;
    left: 0;
    bottom: 0;
    zIndex: 10;
    width: 100%;
    paddingVertical: 16;
    paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8}
    backgroundColor: #a4121c;
`;

const ConnectionText = styled.Text`
    color: #ffffff;
    fontSize: 13;
    lineHeight: 15; 
    fontFamily: CoreRhino45Regular;
`;

const ButtonWrap = styled.View`
    position: absolute;
    top: 10;
    right: 10;
    zIndex: 10000;
`;

const Button = styled.Button`
    backgroundColor: #E91E63;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    color: #ffffff;
    height: 30;
    elevation: 8;
`;

class Root extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            connected: null,
        };
    }

    componentDidMount() {
        const {props} = this;
        const {loaded} = this.state;

        this.unsubscribe = NetInfo.addEventListener(state => {
            let isConnected = state.isConnected;
            console.log('Connect:', isConnected);
            if (isConnected === true && loaded === false) {
                props.checkUserEnterData().then(() => {
                    props.setConnection(true);
                    this.setState({loaded: true});
                });
            }
            props.setConnection(isConnected);
        });
    }

    componentWillUnmount() {
        if (!!this.unsubscribe && typeof this.unsubscribe === 'function') {
            this.unsubscribe();
        }
    }

    render() {
        const {loaded} = this.state;
        const {connected, hash} = this.props;
        if (loaded === false) {
            return <Container>
                <AppLoaderCustom/>
                {connected === false && <ConnectionBar><ConnectionText>Подключение к интернету не доступно</ConnectionText></ConnectionBar>}
            </Container>;
        } else return (
            <ThemeProvider theme={theme}>
                <View style={global}>
                    <StatusBar/>
                    <Navigator ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }}/>
                    {connected === false && <ConnectionWrap>
                        <Loader>
                           {/* <ButtonWrap>
                                <Button title={connected ? 'Отсоединить' : 'Соединить'}
                                        onPress={() => this.props.setConnection(!connected)}/>
                            </ButtonWrap>*/}
                        </Loader>
                        <ConnectionBar><ConnectionText>Подключение к интернету не доступно</ConnectionText></ConnectionBar>
                    </ConnectionWrap>}
                    {hash ? <Notifications/> : null}
                    {/*<ButtonWrap>
                        <Button title={connected ? 'Отсоединить' : 'Соединить'}
                                onPress={() => this.props.setConnection(!connected)}/>
                    </ButtonWrap>*/}
                </View>
            </ThemeProvider>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchUserCheckHash: () => dispatch(actionsToken.fetchUserCheckHash()),
    fetchUserDeviceToken: () => dispatch(actionsToken.fetchUserDeviceToken()),
    getDevice: () => dispatch(actionsDevice.getDevice()),
    saveUserHash: (data) => dispatch(actionsToken.saveUserHash(data)),
    checkUserEnterData: () => dispatch(actionsToken.checkUserEnterData()),
    setConnection: (bool) => dispatch(actionsDevice.setConnection(bool)),
    checkPermission: () => dispatch(notifyActions.checkPermission()),
    createChannel: () => dispatch(notifyActions.createChannel()),
    fetchSaveFmcToken: () => dispatch(notifyActions.fetchSaveFmcToken()),
});

const mapStateToProps = (state) => ({
    curState: state,
    loaded: state.token.loaded,
    hash: state.token.hash,
    connected: state.device.connected,
    channel_name: state.notify.channel_name,
});

export default connect(mapStateToProps, mapDispatchToProps)(Root);

