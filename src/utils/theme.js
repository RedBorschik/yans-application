import {Platform} from 'react-native';

export const colors = {
    WHITE: '#FFFFFF',
    PINK_50: '#FCE4EC',
    PINK_100: '#F8BBD0',
    PINK_200: '#F48FB1',
    PINK_300: '#F06292',
    PINK_400: '#EC407A',
    PINK_500: '#E91E63',
    BLACK: '#000000',
    TEXT: '#231f20',
    GREEN_1: '#89c54c',
    GREEN_2: '#5bbb5e',
    GREEN_3: '#47ba83',
    GREEN_4: '#2fbcaf',
    BLUE_1: '#15c2ec',
    BLUE_2: '#3e9cd4',
    BLUE_3: '#3787b9',
    BLUE_4: '#367192',
    BLUE_5: '#1f4c65',
    BLUE_6: '#aadaf7',
    BLUE_8: '#245068',
    GREY: '#d3d3d3',
    GREY_2: 'rgba(35, 31, 32, 0.6)',
    GREY_3: '#929292',
    GREY_4: '#f3f3f3',
    RED: '#ff0000',
};
export const theme = {
    colors: {
        grey0: colors.TEXT
    },
    /*Divider: {
        backgroundColor: '#efefef'
    },*/
    Button: {
        buttonStyle: {
            backgroundColor: colors.GREEN_1,
            borderRadius: 25,
            shadowOffset: {
                width: 1,
                height: 17,
            },
            shadowRadius: 25,
            shadowColor: colors.GREEN_1,
            shadowOpacity: 0.64,
            marginTop: 31.5,
            elevation: 5,
            height: 44,
        },
        titleStyle: {
            fontFamily: 'CoreRhino65Bold',
            fontSize: 17,
        },
    },
    ButtonGroup: {
        innerBorderStyle: {
            color: colors.BLUE_6,
        },
        ButtonStyle: {
            backgroundColor: colors.WHITE,
        },
        selectedButtonStyle: {
            backgroundColor: colors.BLUE_6,
        },
        textStyle: {
            color: colors.BLUE_8,
            fontSize: 13,
            fontFamily: 'CoreRhino45Regular',
        },
        selectedTextStyle: {
            color: colors.BLUE_8,
            fontFamily: 'CoreRhino45Regular',
        },
        containerStyle: {
            height: 40,
            borderColor: colors.BLUE_6,
            alignSelf: 'stretch',
            marginLeft: 0,
            borderRadius: 6,
            marginBottom: 15.5,
        },
    },
    Input: {
        inputStyle: {
            fontSize: 15,
            lineHeight: 18,
            fontFamily: 'CoreRhino45Regular',
            paddingHorizontal: 0,
            paddingVertical: 0,
        },
        containerStyle: {
            paddingHorizontal: 0,
            paddingVertical: 0,
            marginVertical: 0,
        },
        inputContainerStyle: {
            borderBottomColor: colors.GREY,
            height: 28,
        },
        placeholderTextColor: colors.GREY_2,
    },
    ListItem: {
        containerStyle: {
            minHeight: 44,
            paddingVertical: 7,
            paddingRight: 8,
            paddingLeft: Platform.OS === 'android' ? 8 : 0,
            alignItems: 'center'
        },
        contentContainerStyle: {
            padding: 0,
        },
        titleStyle: {
            fontSize: 15,
            fontWeight: '100',
            fontFamily: 'CoreRhino45Regular',
        },
        dividerStyle: {
            backgroundColor: '#efefef'
        },
        pad: 8
    },
    Badge: {
        badgeStyle: {
            minWidth: 16,
            height: 16
        },
        textStyle: {
            fontFamily: 'CoreRhino45Regular',
            fontSize: 10,
        }
    }

};
export const global = {
    flex: 1,
    backgroundColor: colors.WHITE,
};