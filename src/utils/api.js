export const api = {
    url: 'https://www.yans.kz/mobile/api/',
    params: {
        jsonrpc: '2.0',
        id: 1,
    },
    methods: {
        banners: {
            list: 'banner.list',
        },
        token: {
            device: {
                get: 'device.token.get',
                check: 'device.token.check',
            },
            check: 'user.check_hash',
        },
        auth: {
            email: 'user.auth.email',
            phone: 'user.auth.phone',
            register: 'user.auth.register',
        },
        user: {
            get: 'user.profile.get',
            update: 'user.profile.update',
            addresses: {
                list: 'user.address.list',
                add: 'user.address.add',
                delete: 'user.address.delete',
                update: 'user.address.update',
                detail: 'user.address.detail',
            },
            contragents: {
                types: 'user.contragent.types',
                list: 'user.contragent.list',
                add: 'user.contragent.add',
                delete: 'user.contragent.delete',
                update: 'user.contragent.update',
                detail: 'user.contragent.detail',
            },
            push: {
                get: 'user.pushSettings.get',
                update: 'user.pushSettings.update',
                fmc: 'user.pushToken.set',
            },
        },
        catalog: {
            filter: 'catalog.filter.param.list',
            sort: 'catalog.sort.list',
            categories: 'catalog.section.list',
            detail: 'catalog.item.detail',
            new_items: 'catalog.item.new.list',
            items: 'catalog.item.list',
            search: 'catalog.search',
            favorites: {
                list: 'catalog.favorites.list',
                add: 'catalog.favorites.add',
                remove: 'catalog.favorites.delete',
            },
        },
        cities: {
            list: 'location.city.list',
            select: 'user.set_city',
        },
        basket: {
            list: 'basket.item.list',
            add: 'basket.item.add',
            delete: 'basket.item.delete',
            update: 'basket.item.update',
        },
        orders: {
            list: 'user.order.list',
            detail: 'user.order.detail',
            add: 'order.add',
            repeat: 'order.repeat',
            cancel: 'order.cancel',
            delivery_list: 'order.delivery.list',
            pay_system_list: 'order.pay_system.list',
            params: 'order.params',
        },
        password: {
            forgot: {
                phone: 'user.password.forgot.phone',
                email: 'user.password.forgot.email',
            },
            check: 'user.password.otp.check',
            change: 'user.password.change',
        },
        news: {
            list: 'news.item.list',
            detail: 'news.item.detail',
        },
    },
    file_upload_url: 'https://www.yans.kz/mobile/files.php',
    ws_url: 'ws://88.99.93.90:19893/',
};