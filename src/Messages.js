export const messages = {
    routes: {
        Account: 'Личный кабинет',
        Profile: 'Профиль',
        Settings: 'Настройки',
        AddressList: 'Адреса доставки',
        AddressDetail: 'Адреса доставки',
        AddressNew: 'Адреса доставки',
        ContragentsList: 'Контрагенты',
        ContragentsDetail: 'Контрагенты',
        ContragentsNew: 'Контрагенты',
        News: 'Новости',
        OrdersList: 'Мои заказы',
        OrdersDetail: 'Мои заказы',
        OrdersNew: 'Оформление заказа',
        OrdersCreated: 'Оформление заказа',
        OrdersStep1: 'Оформление заказа. Шаг 1',
        OrdersStep2: 'Оформление заказа. Шаг 2',
        OrdersStep3: 'Оформление заказа. Шаг 3',
        OrdersStep4: 'Оформление заказа. Шаг 4',
        OrdersStep5: 'Оформление заказа. Шаг 5',
    },
    bottomMenu: {
        Account: 'Профиль',
        Favorites: 'Избранное',
        Catalog: 'Каталог',
        Cart: 'Корзина',
        Chat: 'Чат',
    }
};
