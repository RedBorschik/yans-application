import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import catalogActions from '../../actions/Catalog';
import {getPage, getPageCount, getItemsList} from '../../reducers/Catalog';
import IconClear from '../../components/icons/Clear';
import {LoaderPage, IconSearch, SmallCard} from '../../components/';
import {ListItem} from 'react-native-elements';
import styled from 'styled-components/native';
import {Dimensions, StatusBar, Keyboard, Platform} from 'react-native';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Inner = styled.View`
    paddingVertical: 16;
`;

const ListSearch = styled.View`
    paddingLeft: 8;
`;

const ItemsList = styled.View`
    flex: 1;
    overflow: visible;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    justifyContent: space-between;
    flexDirection: row;
    flexWrap: wrap;    
`;

const HeaderWrap = styled.View`
    height: 50;
    paddingLeft: 8;
    paddingRight: 16;
    elevation: 8;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    backgroundColor: #ffffff;
`;

const Reset = styled.TouchableOpacity`
    marginLeft: 8;
`;

const ResetText = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 17;
    color: #5bbb5e;
`;

const Clear = styled.TouchableOpacity`
    position: absolute;
    right: 9;
    top: 9;
`;

const Field = styled.View`
    width: ${Dimensions.get('window').width - 117};
`;

const Input = styled.TextInput`
    width: 100%;
    height: 34;
    paddingLeft: 8;
    paddingRight: 34;
    borderRadius: 4;
    paddingVertical: 0;
    backgroundColor: rgba(142, 142, 147, 0.12);
    borderWidth: 0;
     fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
    color: #231f20;
`;

const SearchBtn = styled.View`
    position: absolute;
    left: 16;
    top: 3;
    zIndex: 2;
`;

const TitleWrap = styled.View`
    width: 100%;
    marginBottom: 16;
`;

const Title = styled.Text`
    color: #231f20;
    fontSize: 20;
    lineHeight: 24; 
    fontFamily: CoreRhino65Bold;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 17;
    lineHeight: 22; 
    fontFamily: CoreRhino45Regular;
`;

const WrapEmpty = styled.View``;

const Bold = styled(MainText)`
    fontFamily: CoreRhino65Bold;
`;

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            showSearchBtn: false,
            isSearched: false
        };
    }

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.page_count) && this.state.isSearched) return false;
        if (this.isCloseToBottom(nativeEvent)) {
            this.props.fetchSearchItems(this.state.search);
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    searchSubmit = (value) => {
        if (value !== '' || this.state.search !== '') {
            Keyboard.dismiss();
            this.setState({
                isSearched: true
            });
            this.props.fetchSearchItems(value || this.state.search);
        }
    };

    setSearchFromList = (value) => {
        this.setState({
            search: value.trim(),
            showSearchBtn: value.length > 0,
        });
        this.searchSubmit(value);
    };

    setSearchValue = (value) => {
        this.setState({
            search: value.trim(),
            showSearchBtn: value.length > 0,
        });
    };

    clearSearchValue = () => {
        this.setState({
            search: '',
            showSearchBtn: false,
        });
    };

    toBack = () => {
        this.props.resetSearchItems();
        this.clearSearchValue();
        this.setState({
            isSearched: false
        });
        const {state} = this.props.navigation;
        if (state.params && state.params.go_back_name) {
            NavigationService.navigate(state.params.go_back_name === 'Catalog' ? 'Sections' : state.params.go_back_name);
        } else {
            NavigationService.setTopLevelNavigator();
        }
    };

    getFilteredList = () => {
        const {search} = this.state;
        const {last_search} = this.props;
      return last_search.filter(item => search === '' || item.indexOf(search) >= 0)
    };

    render() {
        const {search, showSearchBtn, isSearched} = this.state;
        const {last_search, items, loading, fetchAddFavoritesItem, fetchRemoveFavoritesItem, error, count_elements} = this.props;
        const {state} = this.props.navigation;

        return (
            <Container>
                <HeaderWrap>
                    <Field>
                        {showSearchBtn && <SearchBtn><IconSearch onPress={() => this.searchSubmit()}/></SearchBtn>}
                        <Input underlineColorAndroid='transparent'
                               onChangeText={(value) => this.setSearchValue(value)}
                               value={search}
                               returnKeyType='search'
                               autoFocus={true}
                               onSubmitEditing={() => this.searchSubmit()}
                               clearButtonMode="while-editing"
                               style={{
                                   paddingLeft: showSearchBtn ? 52 : 8,
                               }}/>
                        <Clear onPress={() => this.clearSearchValue()}><IconClear/></Clear>
                    </Field>
                    <Reset onPress={() => this.toBack()}><ResetText>Отменить</ResetText></Reset>
                </HeaderWrap>
                <ScrollWrap onScroll={this.onScrollPage} nestedScrollEnabled={false}>
                    <Inner>
                        <ItemsList>
                            <MainText>{(state.params && state.params.go_back_name) ? state.params.go_back_name : ''}</MainText>
                            {loading === false && error && <WrapEmpty>
                                <MainText>Сожалеем, но по запросу <Bold>"{search}"</Bold> ничего не найдено.</MainText>
                            </WrapEmpty>}
                            {items.length > 0 && <TitleWrap><Title>Найдено {count_elements} товаров</Title></TitleWrap>}
                            {items.length > 0 ? items.map((item, key) => (<SmallCard
                                key={item.id}
                                item={item}
                                onPress={() => NavigationService.navigate('Card', {id: item.id, go_back_name: state.routeName}, 'Card' + item.id)}
                                addFavorite={() => fetchAddFavoritesItem(item.id)}
                                removeFavorite={() => fetchRemoveFavoritesItem(item.id)}
                            />)) : null}
                        </ItemsList>
                        {loading && <LoaderPage/>}
                        {last_search.length > 0 && items.length === 0 && loading === false && !isSearched && <ListSearch>
                            {this.getFilteredList().map((item, key) => <ListItem
                                key={key}
                                title={item}
                                onPress={() => this.setSearchFromList(item)}
                                bottomDivider
                                chevron={null}
                            />)}
                        </ListSearch>}
                    </Inner>
                </ScrollWrap>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchAddFavoritesItem: (id) => dispatch(catalogActions.fetchAddFavoritesItem(id)),
    fetchRemoveFavoritesItem: (id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id)),
    fetchSearchItems: (search) => dispatch(catalogActions.fetchSearchItems(search)),
    resetSearchItems: () => dispatch(catalogActions.resetSearchItems()),
});

const mapStateToProps = (state, props) => ({
    last_search: state.search.items,
    error: state.catalog.search_error,
    items: getItemsList(state, 'search'),
    loading: state.catalog.loading.search,
    page: getPage(state, 'search'),
    page_count: getPageCount(state, 'search'),
    count_elements: state.catalog.pagination.search.count_elements,
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
