import React, {Component} from 'react';
import {ScrollView} from 'react-native';
import {MenuSubsectionsList} from '../../components/';
import NavigationService from '../../NavigationService';

class MenuSubsections extends Component {
    constructor(props) {
        super(props);
    }

    checkNavigation = (item) => {
        if (item.items) {
            NavigationService.navigate('MenuSubsections', {sections: item.items, title: item.name}, 'Subsections' + item.value_param);
        } else {
            NavigationService.navigate('Section', {section: item.value_param, title: item.name}, 'Section' + item.value_param);
        }
    };

    render() {
        const {sections} = this.props.navigation.state.params;

        return (
            <ScrollView>
                {sections && sections.length && <MenuSubsectionsList checkNavigation={this.checkNavigation} sections={sections}/>}
            </ScrollView>
        );
    }
}

export default MenuSubsections;
