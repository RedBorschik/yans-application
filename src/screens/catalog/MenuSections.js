import React, {Component} from 'react';
import {connect} from 'react-redux';
import catalogActions from '../../actions/Catalog';
import {LoaderPage, MenuSectionsList} from '../../components/';
import {View} from 'react-native';

class MenuSections extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchCatalogCategories();
    }

    render() {
        const {loading, categories} = this.props;
        return (
            <View style={{flex: 1}}>
                {(!loading && categories && categories.length > 0) && <MenuSectionsList sections={categories}/>}
                {loading === true && <LoaderPage/>}
            </View>
        );
    }
}
const mapDispatchToProps = (dispatch) => ({
    fetchCatalogCategories: () => dispatch(catalogActions.fetchCatalogCategories()),
});

const mapStateToProps = (state, props) => ({
    categories: state.catalog.categories,
    loading: state.catalog.loading.categories
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuSections);
