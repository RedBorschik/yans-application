import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import NavigationService from '../../NavigationService';
import catalogActions from '../../actions/Catalog';
import sortActions from '../../actions/Sort';
import filterActions from '../../actions/Filter';
import styled from 'styled-components/native';
import {LoaderPage, SmallCard, IconFilter, IconSort, Filter, ModalSort} from '../../components/';
import {getPage, getPageCount, getItemsList} from '../../reducers/Catalog';

const Container = styled.ScrollView`
`;

const ItemsList = styled.View`
    flex: 1;
    justifyContent: space-between;
    flexDirection: row;
    flexWrap: wrap;    
    overflow: visible;
    paddingVertical: 16;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
`;

const BtnList = styled.View`
    flexDirection: row;
    alignItems: center;
    marginTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 18};
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8}; 
`;

const BtnListItem = styled.TouchableOpacity`
    width: 50%;
    height: 28;
    borderColor: #aadaf7;
    borderWidth: 1;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
`;

const BtnSort = styled(BtnListItem)`
    borderTopRightRadius: 4;
    borderBottomRightRadius: 4;
`;

const BtnFilter = styled(BtnListItem)`
    borderTopLeftRadius: 4;
    borderBottomLeftRadius: 4;
    borderRightWidth: 0;
`;

const BtnNotAwaliable = styled.View`
     width: 50%;
    height: 28;
    borderColor: #aadaf7;
    borderWidth: 1;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    opacity: 0.5;
`;

const BtnSortNotAwaliable = styled(BtnNotAwaliable)`
    borderTopRightRadius: 4;
    borderBottomRightRadius: 4;
`;

const BtnFilterNotAwaliable = styled(BtnNotAwaliable)`
    borderTopLeftRadius: 4;
    borderBottomLeftRadius: 4;
    borderRightWidth: 0;
`;

const BtnText = styled.Text`
    color: #231f20;
    fontSize: 13;
    lineHeight: 15; 
    fontFamily: CoreRhino45Regular;
`;

const BtnIconWrap = styled.View`
    width: 28;
    height: 28;
    alignItems: center;
    justifyContent: center;
    marginRight: 4;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visibleFilter: false,
            visibleSort: false,
            sorted: {
                name: null,
                value: null,
            },
            filtered: props.filtered,
        };
    }

    componentDidMount() {
        this.props.resetItems();
        this.props.resetFilter();
        this.props.resetSort();
        const {section} = this.props.navigation.state.params;
        if (section) {
            this.props.fetchCatalogItems(section);
            this.props.fetchCatalogSort();
            this.props.fetchCatalogFilter(section);
        }

        if (this.props.filtered) {
            this.props.fetchCatalogItems(null, null, this.props.filtered);
            this.props.fetchCatalogSort();
            this.props.fetchCatalogFilter(null, this.props.filtered);
        }
    }

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.page_count) || !this.props.items.length) return false;
        if (this.isCloseToBottom(nativeEvent)) {
            const {section} = this.props.navigation.state.params;
            if (this.state.sorted.value) {
                this.props.fetchCatalogItems(section, this.state.sorted);
            } else {
                this.props.fetchCatalogItems(section);
            }
        }
    };

    sendSort = (name, value) => {
        const {section} = this.props.navigation.state.params;

        if (name && value) {
            let sort = {};
            sort[name] = value;
            this.setState(prevState => ({
                ...prevState,
                sorted: {
                    ...prevState.sorted,
                    name: name,
                    value: value,
                },
            }));

            this.props.resetItems();
            this.props.fetchCatalogItems(section, sort, this.state.filtered);
        } else {
            if (this.state.sorted.value) {
                this.props.resetItems();
                this.props.fetchCatalogItems(section, null, this.state.filtered);
                this.setState(prevState => ({
                    ...prevState,
                    sorted: {
                        ...prevState.sorted,
                        name: null,
                        value: null,
                    },
                }));
            }
        }
    };

    resetFilter = (filter) => {
        const {section} = this.props.navigation.state.params;
        this.props.fetchCatalogFilter(section, filter);
    };

    sendFilter = (filter) => {
        const {section} = this.props.navigation.state.params;
        let sort = null;
        if (this.state.sort) {
            let sort = {};
            sort[this.state.sort.name] = this.state.sort.value;
        }

        this.resetFilter(filter);
        this.props.fetchCatalogSort();

        if (filter) {
            this.props.resetItems();
            this.setState(prevState => ({
                ...prevState,
                filtered: filter,
            }), () => this.props.fetchCatalogItems(section, sort, filter));
        } else {
            this.props.resetItems();
            this.setState(prevState => ({
                ...prevState,
                filtered: null,
            }), () => this.props.fetchCatalogItems(section, sort));
        }
    };

    selectSort = (index) => {
        const {sort} = this.props;
        let item = sort[index];

        if (item) {
            this.sendSort(item.sortField, item.value);
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    render() {
        const {loading, fetchAddFavoritesItem, fetchRemoveFavoritesItem, items, sort, filter} = this.props;
        const {filtered, visibleFilter, sorted, visibleSort} = this.state;
        //console.log(sort);
        //console.log(filter);

        let options = sort.map((item, key) => item.name);
        options.push('Отмена');

        return (
            <Container onScroll={this.onScrollPage} nestedScrollEnabled={false}>
                <BtnList>
                    {filter.length > 0 ?
                    <BtnFilter onPress={() => this.setState({visibleFilter: true})}>
                        <BtnIconWrap><IconFilter/></BtnIconWrap>
                        <BtnText>Фильтры</BtnText>
                    </BtnFilter> : <BtnFilterNotAwaliable>
                            <BtnIconWrap><IconFilter/></BtnIconWrap>
                            <BtnText>Фильтры</BtnText>
                        </BtnFilterNotAwaliable>}
                    {sort.length > 0 ? <BtnSort onPress={() => Platform.OS === 'android' ? this.setState({visibleSort: true}) : this.SortSheet.show()}>
                        <BtnIconWrap><IconSort/></BtnIconWrap>
                        <BtnText>Сортировка</BtnText>
                    </BtnSort> : <BtnSortNotAwaliable>
                        <BtnIconWrap><IconSort/></BtnIconWrap>
                        <BtnText>Сортировка</BtnText>
                        </BtnSortNotAwaliable>}
                </BtnList>
                {filter.length > 0 && <Filter
                    onSend={(filter) => this.sendFilter(filter)}
                    visible={visibleFilter}
                    hideModal={() => this.setState({visibleFilter: false})}
                    filter={filter}
                    isFiltered={filtered !== null}
                />}
                {sort && sort.length > 0 && Platform.OS === 'android' && <ModalSort
                    visible={visibleSort}
                    selected={sorted.value}
                    hidePopup={() => this.setState({visibleSort: false})}
                    list={sort}
                    onSelect={(name, value) => this.sendSort(name, value)}
                    onReset={() => this.sendSort()}
                />}
                {sort.length > 0 && Platform.OS === 'ios' && <ActionSheet
                    ref={o => this.SortSheet = o}
                    title={'Сортировать каталог'}
                    options={options}
                    cancelButtonIndex={options.length - 1}
                    destructiveButtonIndex={1}
                    onPress={(index) => this.selectSort(index)}
                />}
                <ItemsList>
                    {items.length > 0 ? items.map((item, key) => (<SmallCard
                        key={item.id}
                        item={item}
                        onPress={() => NavigationService.navigate('Card', {id: item.id}, 'Card' + item.id)}
                        addFavorite={() => fetchAddFavoritesItem(item.id)}
                        removeFavorite={() => fetchRemoveFavoritesItem(item.id)}
                    />)) : null}
                </ItemsList>
                {loading === true && <LoaderPage/>}
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchAddFavoritesItem: (id) => dispatch(catalogActions.fetchAddFavoritesItem(id)),
    fetchRemoveFavoritesItem: (id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id)),
    fetchCatalogSort: () => dispatch(sortActions.fetchCatalogSort()),
    fetchCatalogFilter: (section, filter) => dispatch(filterActions.fetchCatalogFilter(section, filter)),
    resetItems: () => dispatch(catalogActions.resetItems()),
    resetSort: () => dispatch(catalogActions.resetSort()),
    resetFilter: () => dispatch(catalogActions.resetFilter()),
    fetchCatalogItems: (section, sort, filter) => dispatch(catalogActions.fetchCatalogItems(section, sort, filter)),
    hideAddBasketModal: () => dispatch(basketActions.hideAddBasketModal()),
    addFetchBasket: (products) => dispatch(basketActions.addFetchBasket(products)),
});

const mapStateToProps = (state, props) => ({
    items: getItemsList(state, 'items'),
    loading: state.catalog.loading.items,
    page: getPage(state, 'items'),
    page_count: getPageCount(state, 'items'),
    sort: state.catalog.sort,
    filter: state.catalog.filter,
    filtered: props.navigation.state.params && props.navigation.state.params.filter ? props.navigation.state.params.filter : null
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
