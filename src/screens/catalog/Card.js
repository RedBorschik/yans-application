import React, {Component} from 'react';
import {connect} from 'react-redux';
import {LoaderPage, Loader, DetailCard, ModalAddBasket} from '../../components/';
import basketActions from '../../actions/Basket';
import catalogActions from '../../actions/Catalog';
import {getItemById} from '../../reducers/Catalog';
import styled from 'styled-components/native';
import {Alert} from 'react-native';

const Container = styled.View`
    flex: 1;
`;

class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
        };
    }

    componentWillMount() {
        if (this.props.navigation.state.params && !!this.props.navigation.state.params.id) {
            this.props.fetchCatalogDetailItem(this.props.navigation.state.params.id);
        } else {
            this.props.catalogIsLoading(false);
        }
    }

    getUpdatedProps = () => {
        return this.props;
    };

    addToBasket = (products) => {
        this.props.addFetchBasket(products).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string')  Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                this.setState({showModal: true});
                setTimeout(() => {
                    this.setState({showModal: false});
                }, 500);
            }
        });
    };

    render() {
        const {loading, loading_add, item, fetchAddFavoritesItem, fetchRemoveFavoritesItem} = this.props;
        const {showModal} = this.state;/*
        console.log(item ? item : null);*/
        return (
            <Container>
                {(item && !loading) && <DetailCard
                    item={this.props.item}
                    addBasket={(products) => this.addToBasket(products)}
                    addFavorite={(id) => fetchAddFavoritesItem(id)}
                    removeFavorite={(id) => fetchRemoveFavoritesItem(id)}
                />}
                {loading !== false ? <LoaderPage/> : null}
                {loading_add === true && <Loader/>}
                <ModalAddBasket
                    visible={showModal}
                    hidePopup={() => this.setState({showModal: false})}/>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    catalogIsLoading: (bool) => dispatch(catalogActions.catalogIsLoading(bool)),
    fetchCatalogDetailItem: (id) => dispatch(catalogActions.fetchCatalogDetailItem(id)),
    fetchAddFavoritesItem: (id) => dispatch(catalogActions.fetchAddFavoritesItem(id)),
    fetchRemoveFavoritesItem: (id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id)),
    addFetchBasket: (products) => dispatch(basketActions.addFetchBasket(products)),
});

const mapStateToProps = (state, props) => ({
    items: state.catalog.items,
    loading: state.catalog.loading.card,
    loading_add: state.basket.loading_add,
    item: props.navigation.state.params && props.navigation.state.params.id ? getItemById(state, props.navigation.state.params.id) : null,
});

export default connect(mapStateToProps, mapDispatchToProps)(Card);

