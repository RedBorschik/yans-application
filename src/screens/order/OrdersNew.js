import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StatusBar} from 'react-native';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import {getOrdersList, getOrderById} from '../../reducers/Orders';
import styled from 'styled-components/native';
import Loader from '../../components/helpers/Loader';
import LoaderPage from '../../components/helpers/LoaderPage';
import {NavigationEvents} from 'react-navigation';
import {Button} from 'react-native-elements';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Inner = styled.View`
    flex: 1;
    paddingTop: 21;
    paddingBottom: 126;
    paddingHorizontal: 16;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
`;

const SubTitle = styled(MainText)`
    paddingBottom: 16;
    fontFamily: CoreRhino65Bold;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

const ButtonsWrap = styled.View`
    position: absolute;
    top: 16;
    right: 8;
    zIndex: 1;
    flexDirection: row;
    alignItems: center;
`;

const PropWrap = styled.View`
    marginBottom: 16;
`;

const PropInner = styled.Text``;

const PropName = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    marginRight: 4;
`;

const PropValue = styled(MainText)`
    lineHeight: 18; 
`;

const Delimer = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef; 
    marginVertical: 16;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
        };
    }

    checkOrder = () => {
        const {props} = this;
        let id = props.id ? props.id : props.items.length > 0 ? props.items[props.items.length - 1].id : null;
        if (id) {
            props.newOrder({id: id});
            props.paramsOrderFetch(id).then(() => {
                let item = this.getProps().item;
                console.log(item);
                if (!item || item && !item.params) {
                    NavigationService.navigate('OrdersStep1');
                }
                this.setState({
                    isLoading: false,
                });
            });
        } else {
            NavigationService.navigate('OrdersStep1');
        }
    };

    checkProps = () => {
        const {props} = this;
        if (props.items.length <= 0) {
           this.props.listFetchOrders().then(() => {
                this.checkOrder();
            });
        } else {
            this.checkOrder();
        }
    };

    componentDidMount() {
        this.checkProps();
    }

    submitForm = () => {
        const {params} = this.props.item;
        let data = {
            pay_system: params.pay.id,
            delivery: params.delivery.id,
            comment: params.recipient.comment ? params.recipient.comment.value : '',
            address: params.address,
            recipient: {
                type: params.recipient.user_type ? params.recipient.user_type.value.id : 1,
            },
        };
        if (params.recipient.user_type && Number(params.recipient.user_type.value.id) === 2) {
            data.recipient = {
                ...data.recipient,
                contragent: params.contragent,

            };
        } else {
            data.recipient = {
                ...data.recipient,
                name: params.recipient.fio ? params.recipient.fio.value : '',
                phone: params.recipient.phone ? params.recipient.phone.value : '',
                email: params.recipient.email ? params.recipient.email.value : '',
            };
        }
        this.props.addFetchOrder(data).then(() => {
            if (this.getProps().last_order !== null) {
                NavigationService.navigate('OrdersCreated');
            }
        });
    };

    getProps = () => {
        return this.props;
    };

    render() {
        const {isLoading} = this.state;

        if (isLoading) {
            return (<Container style={{flex: 1, alignItems: 'center'}}>
                <NavigationEvents onWillFocus={() => {this.checkProps();}}/>
                <StatusBar barStyle="default"/>
                <LoaderPage/>
            </Container>);
        } else {
            const {loading, loading_actions, item} = this.props;
            let params = null;

            if (item) {
                params = this.props.item.params;
            }

            return (
                <Container>
                    <NavigationEvents onWillFocus={() => {this.checkProps();}}/>
                    <ScrollWrap>
                        {loading === true && <LoaderPage/>}
                        {loading_actions === true && <Loader/>}
                        {item && params && <Inner>
                            <MainText>Чтобы оформить заказ по следующим данным, нажмите "Оформить заказ".</MainText>
                            <MainText style={{marginTop: 8}}>Для корректировки данных нажмите "Изменить".</MainText>
                            <Delimer/>

                            {params.recipient && Object.keys(params.recipient).length > 0 && Object.keys(params.recipient).map((key) => (<PropWrap key={key}><PropInner>
                                <PropName>{params.recipient[key].name}:&nbsp;</PropName>
                                <PropValue>{typeof params.recipient[key].value === 'object' ? params.recipient[key].value.name : params.recipient[key].value}</PropValue>
                            </PropInner>
                            </PropWrap>))}

                            {params.pay && <PropWrap>
                                <PropInner>
                                    <PropName>Параметры оплаты:&nbsp;</PropName>
                                    <PropValue>{params.pay.name}</PropValue>
                                </PropInner>
                            </PropWrap>}

                            {params.delivery && <PropWrap>
                                <PropInner>
                                    <PropName>Способ доставки:&nbsp;</PropName>
                                    <PropValue>{params.delivery.name}</PropValue>
                                </PropInner>
                            </PropWrap>}

                        </Inner>}
                    </ScrollWrap>
                    {(item && params) ? <ButtonView>
                        <Button buttonStyle={{
                            backgroundColor: '#ffffff',
                            borderColor: '#89c54c',
                            borderWidth: 1,
                        }} titleStyle={{
                            color: '#89c54c',
                        }} title="Изменить" onPress={() => NavigationService.navigate('OrdersStep1')}/>
                        <Button buttonStyle={{
                            marginTop: 16,
                        }} title="Оформить заказ" onPress={() => this.submitForm()}/>
                    </ButtonView> : null}
                </Container>
            );
        }
    }
}

const mapDispatchToProps = (dispatch) => ({
    ordersHasError: (bool, text) => dispatch(ordersActions.ordersHasError(bool, text)),
    paramsOrderFetch: (id) => dispatch(ordersActions.paramsOrderFetch(id)),
    listFetchOrders: (id) => dispatch(ordersActions.listFetchOrders(id)),
    addFetchOrder: (data) => dispatch(ordersActions.addFetchOrder(data)),
    cancelFetchOrder: (id) => dispatch(ordersActions.cancelFetchOrder(id)),
    newOrder: (data) => dispatch(ordersActions.newOrder(data)),
});

const mapStateToProps = (state, props) => ({
    items: getOrdersList(state),
    itemsObj: state.orders.items,
    loading: state.orders.loading,
    loading_actions: state.orders.loading_action,
    error: state.orders.error,
    message: state.orders.message,
    last_order: state.orders.last_order,
    id: state.orders.new.id ? state.orders.new.id : null,
    item: state.orders.new.id ? getOrderById(state, state.orders.new.id) : null,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
