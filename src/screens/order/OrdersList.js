import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import {getOrdersList} from '../../reducers/Orders';
import styled from 'styled-components/native';
import LoaderPage from '../../components/helpers/LoaderPage';
import Loader from '../../components/helpers/Loader';
import IconCopy from '../../components/icons/Copy';
import DeleteButton from '../../components/icons/Delete';
import {Alert} from 'react-native';

const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const ItemsList = styled.View`
    flex: 1;
    overflow: visible;
    paddingTop: 16;
    paddingBottom: 16;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
`;

const Item = styled.View`
    width: 100%;
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
`;

const ItemInner = styled.TouchableOpacity`
    paddingTop: 16;
    paddingHorizontal: 16;    
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
    fontFamily: CoreRhino65Bold;
`;

const ButtonsWrap = styled.View`
    position: absolute;
    top: 8;
    right: 8;
    zIndex: 1;
    flexDirection: row;
    alignItems: center;
`;

const PropWrap = styled.View`
    marginBottom: 16;
`;

const PropInner = styled.Text``;

const PropName = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    marginRight: 4;
`;

const PropValue = styled(MainText)`
    lineHeight: 18; 
`;

const Text = styled.Text`
    fontSize: 15px;   
    color: #245068;
    fontFamily: CoreRhino45Regular;
    paddingHorizontal: 8;
`;

const Date = styled(PropName)`
    lineHeight: 20;
`;

const Status = styled.Text`
    fontSize: 15px;   
    color: #5bbb5e;
    fontFamily: CoreRhino65Bold;
`;

const Currency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 13;
`;

const PropPrice = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 20;
    paddingHorizontal: 4;
`;

const BasketList = styled.ScrollView`
    paddingHorizontal: 16;   
    paddingBottom: 16;
`;

const BasketItem = styled.View`
    width: 82;
    marginRight: 8;
`;

const BasketImage = styled.Image`
    width: 82;
    height: 82;    
    borderRadius: 6;
    overflow: hidden;
`;

const BasketCount = styled.Text`
    paddingTop: 8;
    color: #231f20;
    fontSize: 13;
    lineHeight: 15; 
    fontFamily: CoreRhino45Regular;
    textAlign: center;
`;

const BasketStub = styled.View`
    paddingRight: 16;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view: true,
        };
    }

    getUpdatedProps = () => {
        return this.props;
    };

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.count_pages)) return false;
        if (this.isCloseToBottom(nativeEvent) && this.state.view) {
            this.props.listFetchOrders();
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    cancelOrder = (id) => {
        Alert.alert(
            '',
            'Вы действительно хотите отменить этот заказ?',
            [
                {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Да', onPress: () => {this.props.cancelFetchOrder(id).then(() => {
                    if (this.getUpdatedProps().error) {
                        if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                            'Ошибка',
                            this.getUpdatedProps().message,
                            [
                                {text: 'OK'},
                            ],
                        );
                    }
                });}},
            ],
            {cancelable: true},
        );
    };

    repeatOrder = (id) => {
        this.props.repeatFetchOrders(id).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string')  Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    '',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        })
    };

    render() {
        const {items, loading, loading_actions} = this.props;
        const {view} = this.state;
        return (
            <Container>
                <NavigationEvents onWillFocus={() => {
                    this.props.resetItems();
                    this.props.listFetchOrders();
                    this.setState({view: true});
                }} onWillBlur={() => { this.setState({view: false});}}/>
                <ScrollWrap onScroll={this.onScrollPage}  nestedScrollEnabled={false}>
                    <ItemsList>
                        {items.length <= 0 && loading === false && <Text>Список заказов пуст</Text>}
                        {items.length > 0 && view ? items.reverse().map((item, key) => (<Item key={item.id}>
                            <ButtonsWrap>
                                <IconCopy onPress={() => this.repeatOrder(item.id)}/>
                                <DeleteButton onPress={() => this.cancelOrder(item.id)}/>
                            </ButtonsWrap>
                            <ItemInner onPress={() => NavigationService.navigate('OrdersDetail', {id: item.id})}>
                                <Title>Заказ № {item.id} <Date>от {item.date_insert}</Date></Title>
                                {item.status && <PropWrap>
                                    {(item.status === 'Отменен' || item.status === 'Отменён') ? <Status style={{color: '#ff0000'}}>{item.status}</Status> : <Status>{item.status}</Status>}
                                </PropWrap>}
                                {item.price && <PropWrap><PropInner>
                                    <PropName>Сумма к оплате:&nbsp;</PropName>
                                    <PropPrice>{item.price} <Currency>₸</Currency></PropPrice>
                                </PropInner>
                                </PropWrap>}
                                {item.pay && <PropWrap><PropInner>
                                    <PropName>Способ оплаты:&nbsp;</PropName>
                                    <PropValue>{item.pay}</PropValue>
                                </PropInner>
                                </PropWrap>}
                                {item.delivery && <PropWrap><PropInner>
                                    <PropName>Доставка:&nbsp;</PropName>
                                    <PropValue>{item.delivery}</PropValue>
                                </PropInner>
                                </PropWrap>}
                            </ItemInner>
                            <PropWrap style={{marginBottom: 8, paddingHorizontal: 16}}><PropInner>
                                <PropName>Товар:&nbsp;</PropName>
                            </PropInner>
                            </PropWrap>
                            <BasketList horizontal={true}>
                                {item.basket && item.basket.length > 0 && item.basket.map(basket => (<BasketItem key={basket.id}>
                                    <BasketImage
                                        source={{uri: basket.image}}
                                        resizeMode={this.props.resizeMode ? this.props.resizeMode : 'cover'}/>
                                    <BasketCount>{basket.quantity} шт</BasketCount>
                                </BasketItem>))}
                                <BasketStub/>
                            </BasketList>
                        </Item>)) : null}
                    </ItemsList>
                    {loading === true && <LoaderPage/>}
                    {loading_actions === true && <Loader/>}
                </ScrollWrap>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchOrders: () => dispatch(ordersActions.listFetchOrders()),
    resetItems: () => dispatch(ordersActions.resetOrders()),
    repeatFetchOrders: (id) => dispatch(ordersActions.repeatFetchOrders(id)),
    ordersHasError: (bool) => dispatch(ordersActions.ordersHasError(bool)),
    cancelFetchOrder: (id) => dispatch(ordersActions.cancelFetchOrder(id)),
});

const mapStateToProps = (state, props) => ({
    items: getOrdersList(state),
    loading: state.orders.loading,
    page: state.orders.page,
    count_pages: state.orders.count_pages,
    loading_actions: state.orders.loading_actions,
    error: state.orders.error,
    message: state.orders.message,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
