import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dimensions, Platform} from 'react-native';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import styled from 'styled-components/native';
import {Loader, LoaderPage, IconCheck} from '../../components/';
import {Button} from 'react-native-elements';

const Container = styled.View`
    flex: 10;
`;

const ScrollWrap = styled.ScrollView`
    flex: 10;
    marginTop: 42;
`;

const Inner = styled.View`
    flex: 10;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 16;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    fontSize: 20;
    fontFamily: CoreRhino65Bold;
`;

const IconChevronWrap = styled.View`
    width: 28;
    marginHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    justifyContent: center; 
    alignItems: center;   
`;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginVertical: 0;
`;

const ListItemContainer = styled.TouchableOpacity`
    width: 100%;
    height: 44;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingVertical: 2.5; 
    paddingRight: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 8 : 0};
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
`;

const ListInner = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 76) : (Dimensions.get('window').width - 52)};
`;

const Divider = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef;
`;

const Price = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 20;
    marginLeft: 16;
    flex: none;
`;

const Currency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 13;
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    paddingTop: 8;
    paddingBottom: 16;
    height: 42;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const Attention = styled.Text`
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 18;
    paddingVertical: 8;
    color: #ff0000;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedPaySystem: null,
        };
    }

    getUpdatedProps = () => {
        return this.props;
    };

    componentDidMount() {
        if (this.props.new.recipient) {
            this.props.listFetchPaySystemList({
                type: this.props.new.recipient.type
            }).then(() => {
                if (this.getUpdatedProps().pay_system_list.length === 1) {
                    this.setState({
                        selectedPaySystem: this.getUpdatedProps().pay_system_list[0].id
                    })
                }
            })
        }
    };

    selectDelivery = (id) => {
        this.setState({
            selectedPaySystem: id,
        });
    };

    validateForm = () => {
        const {selectedPaySystem} = this.state;

        return selectedPaySystem !== null;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        const {selectedPaySystem} = this.state;

        this.props.newOrder({pay_system: selectedPaySystem});
        NavigationService.navigate('OrdersStep5');
    };

    render() {
        const {loading_actions, loading, pay_system_list} = this.props;
        const {selectedPaySystem} = this.state;

        return (
            <Container>
                <FixedTitle><Title>Варианты оплаты</Title></FixedTitle>
                <ScrollWrap>
                    {(loading_actions === true) && <Loader/>}
                    <Inner>
                        {loading === true && <LoaderPage/>}
                        {pay_system_list.length > 0 && pay_system_list.map((item, key) => (
                            <ListItemWrap key={item.id}>
                                <ListItemContainer onPress={() => this.selectDelivery(item.id)}>
                                    <ListInner>
                                        <ListItem numberOfLines={2}>{item.name}</ListItem>
                                        {/*<Price>{item.price} <Currency>₸</Currency></Price>*/}
                                    </ListInner>
                                    {selectedPaySystem === item.id ? <IconChevronWrap><IconCheck/></IconChevronWrap> : null}
                                </ListItemContainer>
                                <Divider/>
                            </ListItemWrap>
                        ))}
                    </Inner>
                </ScrollWrap>
                <ButtonView>
                    <Button title="Продолжить оформление" onPress={() => this.submitForm()}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchPaySystemList: (data) => dispatch(ordersActions.listFetchPaySystemList(data)),
    newOrder: (data) => dispatch(ordersActions.newOrder(data)),
});

const mapStateToProps = (state, props) => ({
    pay_system_list: state.orders.pay_system_list,
    loading: state.orders.loading,
    loading_actions: state.orders.loading_actions,
    new: state.orders.new,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
