import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import styled from 'styled-components/native';
import {Loader, LoaderPage, IconCheck} from '../../components/';
import {Button, ListItem} from 'react-native-elements';
import ModalAddAddress from '../../components/helpers/ModalAddAddress';
import addressActions from '../../actions/Address';
import {getAddressList} from '../../reducers/Address';

const Container = styled.View`
    flex: 10;
`;

const ScrollWrap = styled.ScrollView`
    flex: 10;
    marginTop: 42;
`;

const Inner = styled.View`
    flex: 10;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 16;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    fontSize: 20;
    fontFamily: CoreRhino65Bold;
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    paddingTop: 8;
    paddingBottom: 16;
    height: 42;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            selectedAddress: null,
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.new_item !== null && this.props.new_item === null) {
            this.setState({
                selectedAddress: newProps.new_item,
            });
        }
    }

    getUpdatedProps = () => {
        return this.props;
    };

    componentDidMount() {
        this.props.listFetchAddress().then(() => {
            if (this.getUpdatedProps().addresses.length === 1) {
                this.setState({
                    selectedAddress: this.getUpdatedProps().addresses[0].id
                })
            }
        })
    };

    selectAddress = (id) => {
        this.setState({
            selectedAddress: id,
        });
    };

    validateForm = () => {
        const {selectedAddress} = this.state;

        return selectedAddress !== null;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        const {selectedAddress} = this.state;

        this.props.newOrder({address: selectedAddress});
        NavigationService.navigate('OrdersStep3');
    };

    render() {
        const {loading_actions, loading_address_detail, loading_address, addresses} = this.props;
        const {selectedAddress, showModal} = this.state;
        return (
            <Container>
                <FixedTitle><Title>Адреса доставки</Title></FixedTitle>
                <ScrollWrap>
                    {(loading_actions === true || loading_address_detail === true) && <Loader/>}
                    <Inner>
                        {loading_address === true && <LoaderPage/>}
                        {addresses.length > 0 && addresses.map(address => (<ListItem
                            key={address.id}
                            title={address.name_shop}
                            onPress={() => this.selectAddress(address.id)}
                            bottomDivider
                            chevron={selectedAddress === address.id ? <IconCheck/> : null}
                        />))}
                        <ListItem
                            title="Новый адрес"
                            onPress={() => this.setState({showModal: true})}
                            bottomDivider
                            chevron={null}
                        />
                    </Inner>
                    <ModalAddAddress
                        visible={showModal}
                        hideModal={() => this.setState({showModal: false})}
                    />
                </ScrollWrap>
                <ButtonView>
                    <Button title="Продолжить оформление" onPress={() => this.submitForm()}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchAddress: () => dispatch(addressActions.listFetchAddress()),
    newOrder: (data) => dispatch(ordersActions.newOrder(data)),
});

const mapStateToProps = (state, props) => ({
    addresses: getAddressList(state),
    loading_actions: state.orders.loading_actions,
    loading_address_detail: state.address.loading_detail,
    loading_address: state.address.loading,
    new_item: state.address.new_item,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
