import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';
import NavigationService from '../../NavigationService';
import contragentsActions from '../../actions/Contragents';
import {getContragentsList} from '../../reducers/Contragents';
import ordersActions from '../../actions/Orders';
import styled from 'styled-components/native';
import {Loader, Input, LoaderPage, IconCheck} from '../../components/';
import ModalAddContragent from '../../components/helpers/ModalAddContragent';
import {Button, ButtonGroup, ListItem} from 'react-native-elements';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
    marginTop: 42;
`;

const Inner = styled.View`
    flex: 1;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 16;
`;

const Content = styled.View``;

const ButtonView = styled.View`
    position: absolute;
    bottom: 16;
    left: 28px;
    right: 28px;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    fontSize: 20;
    fontFamily: CoreRhino65Bold;
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    paddingTop: 8;
    paddingBottom: 16;
    height: 42;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            selectedType: 0,
            selectedContragent: null,
            recipient: {
                type: 1,
            },
            item: {
                name: '',
                email: '',
                phone: '',
            },
            errors: {
                name: false,
                email: false,
                phone: false,
            },
        };
    }

    getUpdatedProps = () => {
        return this.props;
    };

    componentWillReceiveProps(newProps) {
        if (newProps.new_item !== null && this.props.new_item === null) {
            this.setState({
                selectedContragent: newProps.new_item,
            });
        }
    }

    updateIndex = (index) => {
        this.setState(prevState => ({
            ...prevState,
            selectedType: index,
            recipient: {
                ...prevState.recipient,
                type: Number(index) + 1,
            },
        }));
        if (Number(index) === 1) {
            if (this.props.contragents.length <= 0) {
                this.props.listFetchContragents().then(() => {
                    if (this.getUpdatedProps().contragents.length === 1) {
                        this.setState({
                            selectedContragent: this.getUpdatedProps().contragents[0].id
                        })
                    }
                })
            }
        }
    };

    validateForm = () => {
        const {item, selectedType, selectedContragent} = this.state;
        if (selectedType === 0) {
            let phone = item.phone.replace(/[^0-9.]/g, '');
            let checkName = validator.isEmpty(item.name);
            let checkEmail = !validator.isEmail(item.email);
            let checkPhone = !validator.isMobilePhone(phone) && validator.isEmpty(item.phone);

            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    name: checkName,
                    email: checkEmail,
                    phone: checkPhone,
                },
            }));

            return !checkName && !checkEmail && !checkPhone;
        } else if (selectedType === 1) {
            return selectedContragent !== null;
        }

        return false;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        const {item, selectedType, selectedContragent} = this.state;

        if (selectedType === 0) {
            this.setState(prevState => ({
                ...prevState,
                recipient: {
                    ...prevState.recipient,
                    name: item.name,
                    email: item.email,
                    phone: item.phone,
                },
            }), function() {
                this.props.newOrder({recipient: this.state.recipient});
                NavigationService.navigate('OrdersStep2');
            });
        } else {
            this.setState(prevState => ({
                ...prevState,
                recipient: {
                    ...prevState.recipient,
                    contragent: selectedContragent,
                },
            }), function() {
                this.props.newOrder({recipient: this.state.recipient});
                NavigationService.navigate('OrdersStep2');
            });
        }

    };

    selectContragent = (id) => {
        this.setState({
            selectedContragent: id,
        });
    };

    render() {
        const {loading_actions, loading_contr, loading_contr_detail, contragents, contragents_obj} = this.props;
        const {selectedType, selectedContragent, item, errors, showModal} = this.state;
        const buttons = ['Физическое лицо', 'Юридическое лицо'];

        return (
            <Container>
                <FixedTitle><Title>Тип плательщика</Title></FixedTitle>
                <ScrollWrap>
                    {loading_actions === true && <Loader/>}
                    <Inner>
                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={selectedType}
                            buttons={buttons}
                            containerStyle={{
                                width: '100%',
                            }}
                        />
                        {selectedType === 0 ? <Content>
                            <Input
                                label="ФИО"
                                underlineColorAndroid='transparent'
                                onChangeText={(value) => this.setState(prevState => ({...prevState, item: {...prevState.item, name: value}}))}
                                onBlur={() => {this.setState(prevState => ({
                                    errors: {
                                        ...prevState.errors,
                                        name: validator.isEmpty(item.name),
                                    },
                                }));}}
                                error={errors.name}
                                value={item.name}/>
                            <Input
                                label="E-mail"
                                underlineColorAndroid='transparent'
                                onChangeText={(value) => this.setState(prevState => ({...prevState, item: {...prevState.item, email: value}}))}
                                onBlur={() => {this.setState(prevState => ({
                                    errors: {
                                        ...prevState.errors,
                                        email: !validator.isEmail(item.email),
                                    },
                                }));}}
                                error={errors.email}
                                value={item.email}/>
                            <Input
                                label="Мобильный телефон"
                                keyboardType={'phone-pad'}
                                type="tel"
                                underlineColorAndroid='transparent'
                                onChangeText={(value) => this.setState(prevState => ({...prevState, item: {...prevState.item, phone: value}}))}
                                onBlur={() => {this.setState(prevState => ({
                                    errors: {
                                        ...prevState.errors,
                                        phone: !validator.isMobilePhone(item.phone.replace(/[^0-9.]/g, '')) && validator.isEmpty(item.phone),
                                    },
                                }));}}
                                error={errors.phone}
                                value={item.phone}/>
                        </Content> : <Content>
                            {loading_contr === true && <LoaderPage/>}
                            {loading_contr === false && <Content>
                                {contragents.length > 0 && contragents.map(contragent => (<ListItem
                                    key={contragent.id}
                                    title={contragent.name}
                                    onPress={() => this.selectContragent(contragent.id)}
                                    bottomDivider
                                    chevron={selectedContragent === contragent.id ? <IconCheck/> : null}
                                />))}
                                <ListItem
                                    title="Новое юр. лицо"
                                    onPress={() => this.setState({showModal: true})}
                                    bottomDivider
                                    chevron={null}
                                />
                            </Content>}
                        </Content>}
                    </Inner>
                    <ModalAddContragent
                        visible={showModal}
                        hideModal={() => this.setState({showModal: false})}
                    />
                    {loading_contr_detail === true && <Loader/>}
                </ScrollWrap>
                <ButtonView>
                    <Button title="Продолжить оформление" onPress={() => this.submitForm()}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchContragents: () => dispatch(contragentsActions.listFetchContragents()),
    newOrder: (data) => dispatch(ordersActions.newOrder(data)),
});

const mapStateToProps = (state, props) => ({
    contragents: getContragentsList(state),
    contragents_obj: state.contragents.items,
    loading_contr: state.contragents.loading,
    loading_contr_detail: state.contragents.loading_detail,
    loading_actions: state.orders.loading_actions,
    new_contragent: state.contragents.new_item,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
