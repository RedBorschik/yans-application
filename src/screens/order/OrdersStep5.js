import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import styled from 'styled-components/native';
import {Loader, LoaderPage, IconCheck, Input} from '../../components/';
import {Button} from 'react-native-elements';
import {Alert} from 'react-native';

const Container = styled.View`
    flex: 10;
`;

const ScrollWrap = styled.ScrollView`
    flex: 10;
    marginTop: 42;
`;

const Inner = styled.View`
    flex: 10;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 16;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 16;
  left: 28px;
  right: 28px;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    fontSize: 20;
    fontFamily: CoreRhino65Bold;
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    paddingTop: 8;
    paddingBottom: 16;
    height: 42;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comment: '',
        };
    }


    getUpdatedProps = () => {
        return this.props;
    };

    submitForm = () => {
        const {comment} = this.state;

        this.props.newOrder({comment: comment});
        this.props.addFetchOrder(this.props.new).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                if (this.getUpdatedProps().last_order !== null) {
                    NavigationService.navigate('OrdersCreated');
                }
            }
        });
    };

    render() {
        const {loading_actions, loading, error, message, ordersHasError} = this.props;
        const {comment} = this.state;

        return (
            <Container>
                <FixedTitle><Title>Комментарий к заказу</Title></FixedTitle>
                <ScrollWrap>
                    {(loading_actions === true) && <Loader/>}
                    <Inner>
                        {loading === true && <LoaderPage/>}
                        <Input
                            label="Комментарий"
                            numberOfLines={5}
                            multiline={true}
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({...prevState, comment: value}))}
                            inputContainerStyle={{
                                height: 'auto',

                            }}
                            value={comment}/>
                    </Inner>
                </ScrollWrap>
                <ButtonView>
                    <Button title="Оформить заказ" onPress={() => this.submitForm()}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    ordersHasError: (bool) => dispatch(ordersActions.ordersHasError(bool)),
    addFetchOrder: (data) => dispatch(ordersActions.addFetchOrder(data)),
    newOrder: (data) => dispatch(ordersActions.newOrder(data)),
});

const mapStateToProps = (state, props) => ({
    loading: state.orders.loading,
    loading_actions: state.orders.loading_actions,
    new: state.orders.new,
    last_order: state.orders.last_order,
    error: state.orders.error,
    message: state.orders.message
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
