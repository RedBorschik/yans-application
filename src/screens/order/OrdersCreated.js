import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions, Linking} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import {getOrderById} from '../../reducers/Orders';
import styled from 'styled-components/native';
import Loader from '../../components/helpers/Loader';
import LoaderPage from '../../components/helpers/LoaderPage';
import {Alert} from 'react-native';
import {Button} from 'react-native-elements';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Inner = styled.View`
    flex: 1;
    paddingTop: 21;
    paddingBottom: 25;
    paddingHorizontal: 16;
`;

const Pay = styled.View`
    marginTop: 32;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingVertical: 16;
    paddingHorizontal: 16;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    fontSize: 17;
    lineHeight: 20;
`;

const SubTitle = styled(MainText)`
    paddingBottom: 16;
    fontFamily: CoreRhino45Regular;
`;

const TitleBold = styled(Title)`
    fontSize: 20;
    fontFamily: CoreRhino65Bold;
    lineHeight: 24;
`;

const Date = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontFamily: CoreRhino45Regular;
    fontSize: 15;
    lineHeight: 20;
`;

const Bold = styled(MainText)`
    fontFamily: CoreRhino65Bold;
`;

const Link = styled.TouchableOpacity``;

class Section extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {loading, loading_actions, item, error, message, ordersHasError} = this.props;
        return (
            <Container>
                <ScrollWrap>
                    {loading === true && <LoaderPage/>}
                    {loading_actions === true && <Loader/>}
                    {item !== null && !loading && <Inner>

                        <TitleBold>Заказ сформирован.</TitleBold>

                        <Title>Ваш заказ <TitleBold>№ {item.id}</TitleBold> <Date>от {item.date}</Date> успешно создан.</Title>

                        <MainText>Вы можете следить за выполнением своего заказа в Персональном разделе.</MainText>

                        <Button buttonStyle={{
                            marginTop: 16,
                        }} title="Перейти к заказу" onPress={() => NavigationService.navigate('OrdersDetail', {id: item.id})}/>

                        {item.payment ? <Pay>
                            <Bold style={{marginBottom: 8}}>Оплата заказа</Bold>

                            <MainText>{item.payment.name}</MainText>

                            {(item.payment.url && item.payment.url !== '') ? <Button buttonStyle={{
                                marginTop: 16,
                            }} title="Оплатить" onPress={() => { Linking.openURL(item.payment.url);}}/> : null}
                        </Pay> : null}
                    </Inner>}
                </ScrollWrap>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    detailFetchOrders: (id) => dispatch(ordersActions.detailFetchOrders(id)),
    ordersHasError: (bool) => dispatch(ordersActions.ordersHasError(bool)),
});

const mapStateToProps = (state, props) => ({
    id: state.orders.last_order,
    item: state.orders.last_order,
    loading: state.orders.loading,
    loading_actions: state.orders.loading_actions,
    error: state.orders.error,
    message: state.orders.message,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
