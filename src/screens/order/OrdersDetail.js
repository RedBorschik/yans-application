import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import NavigationService from '../../NavigationService';
import ordersActions from '../../actions/Orders';
import {getOrderById} from '../../reducers/Orders';
import styled from 'styled-components/native';
import Loader from '../../components/helpers/Loader';
import LoaderPage from '../../components/helpers/LoaderPage';
import IconCopy from '../../components/icons/Copy';
import DeleteButton from '../../components/icons/Delete';
import {Alert} from 'react-native';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Inner = styled.View`
    flex: 1;
    paddingTop: 21;
    paddingBottom: 25;
    paddingHorizontal: 16;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
`;

const SubTitle = styled(MainText)`
    paddingBottom: 16;
    fontFamily: CoreRhino65Bold;
`;

const ButtonsWrap = styled.View`
    position: absolute;
    top: 16;
    right: 8;
    zIndex: 1;
    flexDirection: row;
    alignItems: center;
`;

const PropWrap = styled.View`
    marginBottom: 16;
`;

const PropInner = styled.Text``;

const PropName = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    marginRight: 4;
`;

const PropValue = styled(MainText)`
    lineHeight: 18; 
`;

const Date = styled(PropName)`
    fontSize: 15;
    lineHeight: 20;
`;

const Status = styled.Text`
    fontSize: 15px;   
    color: #5bbb5e;
    fontFamily: CoreRhino65Bold;
`;

const Currency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 13;
`;

const PropPrice = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 20;
    paddingHorizontal: 4;
`;

const Block = styled.View`
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingHorizontal: 16;
    paddingVertical: 16;
`;

const Outer = styled.View``;

const Delimer = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef; 
    marginBottom: 16;
`;

const BasketItem = styled.TouchableOpacity`
    width: 100%;
    height: 114;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingVertical: 16;
    paddingHorizontal: 8;
    flexDirection: row;
    alignItems: stretch;
    marginBottom: 16;
`;

const BasketContentWrap = styled.View`
    flex: 1;
    paddingLeft: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    flexDirection: column;
    justifyContent: space-between;
`;

const BasketTitle = styled.Text`
    fontSize: 13;
    lineHeight: 17;
    fontFamily: CoreRhino45Regular;
`;

const BasketPrice = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 17;
    lineHeight: 20;
`;

const BasketCurrency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 13;
`;

const BasketImage = styled.Image`
    width: 82;
    height: 82;    
    borderRadius: 6;
    overflow: hidden;
`;

const BasketInfoRow = styled.View`
    flexDirection: row;
    alignItems: flex-end;
    justifyContent: space-between;
    width: 100%;
    paddingRight: 8;
`;

const BasketInfoCol = styled.View`
    alignItems: flex-start;
`;

const BasketInfoItem = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
    fontSize: 12;
    marginTop: 3;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view: true,
        };
    }

    getUpdatedProps = () => {
        return this.props;
    };

    cancelOrder = (id) => {
        Alert.alert(
            '',
            'Вы действительно хотите отменить этот заказ?',
            [
                {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Да', onPress: () => {this.props.cancelFetchOrder(id).then(() => {
                    if (this.getUpdatedProps().error) {
                        Alert.alert(
                            'Ошибка',
                            this.getUpdatedProps().message,
                            [
                                {text: 'OK'},
                            ],
                        );
                    }
                });}},
            ],
            {cancelable: true},
        );
    };

    repeatOrder = (id) => {
        this.props.repeatFetchOrders(id).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    '',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        })
    };

    render() {
        const {loading, loading_actions, item} = this.props;
        const {view} = this.state;
        if (item) {
            const {detail} = this.props.item;
            return (
                <Container>
                    <NavigationEvents onWillFocus={() => {
                        this.props.detailFetchOrders(this.props.id).then(() => {
                            if (this.getUpdatedProps().error) {
                                Alert.alert(
                                    'Ошибка',
                                    this.getUpdatedProps().message,
                                    [
                                        {text: 'OK'},
                                    ],
                                );
                            }
                        });
                        this.setState({view: true});
                    }} onWillBlur={() => { this.setState({view: false});}}/>
                    <ScrollWrap>
                        {loading === true && <LoaderPage/>}
                        {loading_actions === true && <Loader/>}
                        {detail && !loading && view && <Inner>
                            <ButtonsWrap>
                                <IconCopy onPress={() => this.repeatOrder(item.id)}/>
                                <DeleteButton onPress={() => this.cancelOrder(item.id)}/>
                            </ButtonsWrap>
                            <Title>Заказ № {detail.id} <Date>от {detail.date_insert}</Date></Title>
                            {detail.status && <PropWrap>
                                {detail.status === 'Отменен' ? <Status style={{color: '#ff0000'}}>{detail.status}</Status> : <Status>{detail.status}</Status>}
                            </PropWrap>}
                            {detail.recipient && <Delimer/>}
                            {detail.recipient && detail.recipient.length > 0 && <Outer>
                                <SubTitle>Информация о получателе:</SubTitle>
                                {detail.recipient.map((recipient, key) => (<PropWrap key={key}><PropInner>
                                    <PropName>{recipient.name}:&nbsp;</PropName>
                                    <PropValue>{typeof recipient.value === 'object' ? recipient.value.name : recipient.value}</PropValue>
                                </PropInner>
                                </PropWrap>))}
                            </Outer>}
                            {detail.pay && <Delimer/>}
                            {detail.pay && <Outer>
                                <SubTitle>Параметры оплаты:</SubTitle>
                                <PropWrap style={{marginBottom: 8}}>
                                    <PropInner>
                                        <PropValue>Счёт № {detail.pay.id} от {detail.pay.date}, {detail.pay.name}</PropValue>
                                    </PropInner>
                                </PropWrap>
                                <PropWrap>
                                    <PropInner>
                                        <PropName>Сумма к оплате по счёту:&nbsp;</PropName>
                                        <PropPrice>{detail.pay.price} <Currency>₸</Currency></PropPrice>
                                    </PropInner>
                                </PropWrap>
                            </Outer>}
                            {detail.delivery && <Delimer/>}
                            {detail.delivery && <Outer>
                                <SubTitle>Параметры отгрузки:</SubTitle>
                                <PropWrap>
                                    <PropInner>
                                        <PropValue>Отгрузка № {detail.delivery.id}</PropValue>
                                    </PropInner>
                                </PropWrap>
                                <PropWrap>
                                    <PropInner>
                                        <PropName>Стоимость доставки:&nbsp;</PropName>
                                        <PropPrice>{detail.delivery.price} <Currency>₸</Currency></PropPrice>
                                    </PropInner>
                                </PropWrap>
                                <PropWrap>
                                    <PropInner>
                                        <PropName>Служба доставки:&nbsp;</PropName>
                                        <PropValue>{detail.delivery.name}</PropValue>
                                    </PropInner>
                                </PropWrap>
                                <PropWrap>
                                    <PropInner>
                                        <PropName>Статус отгрузки:&nbsp;</PropName>
                                        <PropValue>{detail.delivery.status}</PropValue>
                                    </PropInner>
                                </PropWrap>
                            </Outer>}
                            {detail.basket && <Delimer/>}
                            {detail.basket && detail.basket.length && <Outer>
                                <SubTitle>Товары в заказе:</SubTitle>
                                {detail.basket && detail.basket.length > 0 && detail.basket.map(basket => (<BasketItem
                                    key={basket.id}
                                    onPress={() => NavigationService.navigate('Card', {id: basket.id})}>
                                    <BasketImage
                                        source={{uri: basket.image}}
                                        imageStyle={{resizeMode: 'cover'}}
                                    />
                                    <BasketContentWrap>
                                        <BasketTitle numberOfLines={2}>{basket.name}</BasketTitle>
                                        <BasketInfoRow>
                                            <BasketInfoCol>
                                                {basket.packing && <BasketInfoItem>{basket.packing}</BasketInfoItem>}
                                                {basket.quantity && <BasketInfoItem>{basket.quantity} шт</BasketInfoItem>}
                                            </BasketInfoCol>
                                            {basket.price && <BasketPrice>{Number(basket.price).toFixed(0)} <BasketCurrency>₸</BasketCurrency></BasketPrice>}
                                        </BasketInfoRow>
                                    </BasketContentWrap>
                                </BasketItem>))}
                            </Outer>}
                        </Inner>}
                    </ScrollWrap>
                </Container>
            );
        } else {
            return (<Container><LoaderPage/></Container>);
        }
    }
}

const mapDispatchToProps = (dispatch) => ({
    detailFetchOrders: (id) => dispatch(ordersActions.detailFetchOrders(id)),
    repeatFetchOrders: (id) => dispatch(ordersActions.repeatFetchOrders(id)),
    ordersHasError: (bool) => dispatch(ordersActions.ordersHasError(bool)),
    cancelFetchOrder: (id) => dispatch(ordersActions.cancelFetchOrder(id)),
});

const mapStateToProps = (state, props) => ({
    id: props.navigation.state.params && props.navigation.state.params.id ? props.navigation.state.params.id : null,
    item: props.navigation.state.params && props.navigation.state.params.id ? getOrderById(state, props.navigation.state.params.id) : null,
    loading: state.orders.loading,
    loading_actions: state.orders.loading_actions,
    error: state.orders.error,
    message: state.orders.message
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
