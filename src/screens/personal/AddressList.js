import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import addressActions from '../../actions/Address';
import {getAddressList} from '../../reducers/Address';
import styled from 'styled-components/native';
import {LoaderPage, EditButton, Loader} from '../../components/';
import DeleteButton from '../../components/icons/Delete';
import {Button} from 'react-native-elements';
import {Alert} from 'react-native';

const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const ItemsList = styled.View`
    flex: 1;
    overflow: visible;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 8;
`;

const Item = styled.View`
    width: 100%;
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingTop: 16;
    paddingHorizontal: 16;    
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
`;

const ButtonsWrap = styled.View`
    position: absolute;
    top: 8;
    right: 2;
    zIndex: 1;
    flexDirection: row;
    alignItems: center;
`;

const PropWrap = styled.View`
    marginBottom: 16;
`;

const PropInner = styled.Text``;

const PropName = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    marginRight: 4;
`;

const PropValue = styled(MainText)`
    lineHeight: 18; 
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

const Text = styled.Text`
    fontSize: 15px;   
    color: #245068;
    fontFamily: CoreRhino45Regular;
    paddingHorizontal: 8;
`;

class Section extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.listFetchAddress();
    }

    removeItem = (id) => {
        Alert.alert(
            '',
            'Вы действительно хотите удалить этот элемент?',
            [
                {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Да', onPress: () => {this.props.removeFetchAddress(id)}},
            ],
            {cancelable: true},
        );
    };

    render() {
        const {items, loading, loading_detail} = this.props;
        return (
            <Container>
                <ScrollWrap>
                    <ItemsList>
                        {items.length <= 0 && loading === false && <Text>Список адресов пуст</Text>}
                        {items.length > 0 ? items.map((item, key) => (<Item
                            key={item.id}>
                            <ButtonsWrap>
                                <EditButton onPress={() => NavigationService.navigate('AddressDetail', {id: item.id})}/>
                                <DeleteButton onPress={() => this.removeItem(item.id)}/>
                            </ButtonsWrap>
                            <Title>{new Entities().decode(item.name_shop).toUpperCase()}</Title>
                            {item.contact_person ? <PropWrap><PropInner>
                                <PropName>Контактное лицо:&nbsp;</PropName>
                                <PropValue>{item.contact_person}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {item.phone ? <PropWrap><PropInner>
                                <PropName>Телефон:&nbsp;</PropName>
                                <PropValue>{item.phone}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {(item.city && item.city.name) ? <PropWrap><PropInner>
                                <PropName>Город:&nbsp;</PropName>
                                <PropValue>{item.city.name}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            <PropWrap><PropInner>
                                <PropName>Название заведения:&nbsp;</PropName>
                                <PropValue>{new Entities().decode(item.name_shop)}</PropValue>
                            </PropInner>
                            </PropWrap>
                            {item.address ? <PropWrap><PropInner>
                                <PropName>Адрес:&nbsp;</PropName>
                                <PropValue>{item.address}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                        </Item>)) : null}
                    </ItemsList>
                    {loading === true && <LoaderPage/>}
                    {loading_detail === true && <Loader/>}
                </ScrollWrap>
                <ButtonView>
                    <Button title="Добавить новый адрес" onPress={() => NavigationService.navigate('AddressNew')}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchAddress: () => dispatch(addressActions.listFetchAddress()),
    removeFetchAddress: (id) => dispatch(addressActions.removeFetchAddress(id)),
});

const mapStateToProps = (state, props) => ({
    items: getAddressList(state),
    loading_detail: state.address.loading_detail,
    loading: state.address.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
