import React, {Component} from 'react';
import {connect} from 'react-redux';
import citiesActions from '../../actions/Cities';
import addressActions from '../../actions/Address';
import {getAddressById} from '../../reducers/Address';
import styled from 'styled-components/native';
import {Loader, Select, Input} from '../../components/';
import validator from 'validator';
import {Button} from 'react-native-elements';
import NavigationService from '../../NavigationService';
import {Alert} from 'react-native';

const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
    paddingHorizontal: 16;
`;

const Inner = styled.View`
    paddingTop: 21;
    paddingBottom: 80;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: {
                city: null,
                city_name: '',
                address: '',
                contact_person: '',
                name_shop: '',
                phone: '',
            },
            errors: {
                city: false,
                address: false,
                contact_person: false,
                name_shop: false,
                phone: false,
            },
        };
    }

    selectCity = (label, value) => {
        this.setState(prevState => ({
            item: {
                ...prevState.item,
                city: value,
                city_name: label,
            },
        }), () => {
            const {item} = this.state;
            let checkCity = item.city === null;
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    city: checkCity,
                },
            }));
        });
    };

    setStateFromProps = () => {
        const {item} = this.props;
        this.setState(prevState => ({
            item: {
                ...prevState.item,
                city: item.city.code,
                city_name: item.city.name,
                address: item.address,
                contact_person: item.contact_person,
                name_shop: item.name_shop,
                phone: item.phone.replace('8', '+7'),
            },
        }));
    };

    validateForm = () => {
        const {item} = this.state;
        let phone = item.phone.replace(/[^0-9.]/g, '');
        let checkNameShop = validator.isEmpty(item.name_shop);
        let checkCity = validator.isEmpty(item.city);
        let checkPhone = !validator.isMobilePhone(phone) || validator.isEmpty(item.phone);
        let checkPerson = validator.isEmpty(item.contact_person);
        let checkAddress = validator.isEmpty(item.address);

        this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                city: checkCity,
                address: checkAddress,
                contact_person: checkPerson,
                name_shop: checkNameShop,
                phone: checkPhone,
            },
        }));

        return !checkAddress && !checkCity && !checkPhone && !checkAddress && !checkPerson;
    };

    getUpdatedProps = () => {
        return this.props;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        const {item} = this.state;
        let data = {
            city: item.city,
            city_name: item.city_name,
            address: item.address,
            contact_person: item.contact_person,
            name_shop: item.name_shop,
            phone: item.phone,
            id: this.props.id,
        };

        this.props.updateFetchAddress(data).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                NavigationService.navigate('AddressList');
            }
        });
    };

    componentDidMount() {
        if (this.props.id && !this.props.item) {
            this.props.detailFetchAddress(this.props.id).then(() => {
                this.setStateFromProps();
            })
        } else if (this.props.item) {
            this.setStateFromProps();
        }
        if (this.props.cities.length <= 0) {
            this.props.fetchCities();
        }
    }

    render() {
        const {loading, loading_detail, cities} = this.props;
        const {item, errors} = this.state;
        return (
            <Container>
                <ScrollWrap>
                    {loading === true && <LoaderPage/>}
                    {loading_detail === true && <Loader/>}
                    <Inner>
                        <Input
                            label="Название организации"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, name_shop: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, name_shop: validator.isEmpty(item.name_shop)}}))}}
                            error={errors.name_shop}
                            value={item.name_shop}/>
                        <Input
                            label="Контакное лицо"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, contact_person: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, contact_person: validator.isEmpty(item.contact_person)}}))}}
                            error={errors.contact_person}
                            value={item.contact_person}/>
                        <Input
                            type="tel"
                            label="Телефон"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, phone: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, phone: !validator.isMobilePhone(item.phone.replace(/[^0-9.]/g, '')) || validator.isEmpty(item.phone)}}))}}
                            error={errors.phone}
                            value={item.phone}/>
                        <Select
                            title={'Выбор города'}
                            label={'Город:'}
                            text={item.city_name}
                            list={cities}
                            selected={item.city}
                            error={errors.city}
                            onSelect={(label, value) => this.selectCity(label, value)}/>
                        <Input
                            label="Адрес"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, address: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, address: validator.isEmpty(item.address)}}))}}
                            error={errors.address}
                            value={item.address}/>
                    </Inner>
                </ScrollWrap>
                <ButtonView>
                    <Button title="Сохранить" onPress={this.submitForm}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    detailFetchAddress: (id) => dispatch(addressActions.detailFetchAddress(id)),
    updateFetchAddress: (params) => dispatch(addressActions.updateFetchAddress(params)),
    fetchCities: () => dispatch(citiesActions.fetchCities()),
});

const mapStateToProps = (state, props) => ({
    id: props.navigation.state.params && props.navigation.state.params.id ? props.navigation.state.params.id : null,
    item: props.navigation.state.params && props.navigation.state.params.id ? getAddressById(state, props.navigation.state.params.id) : null,
    loading: state.address.loading,
    loading_detail: state.address.loading_detail,
    cities: state.cities.items,
    curState: state,
    error: state.address.error,
    message: state.address.message
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
