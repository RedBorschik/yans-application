import React, {Component} from 'react';
import {connect} from 'react-redux';
import contragentsActions from '../../actions/Contragents';
import legaltypesActions from '../../actions/LegalTypes';
import {getContragentsById} from '../../reducers/Contragents';
import styled from 'styled-components/native';
import {Loader, Select, Input} from '../../components/';
import validator from 'validator';
import {Button} from 'react-native-elements';
import NavigationService from '../../NavigationService';
import {Alert} from 'react-native';

const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Inner = styled.View`
    paddingTop: 21;
    paddingBottom: 90;
    paddingHorizontal: 16;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: {
                name: '',
                inn: '',
                bik: '',
                address: '',
                bank: '',
                rs: '',
                form: '',
                form_name: '',
            },
            errors: {
                name: false,
                inn: false,
                bik: false,
                address: false,
                bank: false,
                rs: false,
                form: false,
            },
        };
    }

    selectForm = (label, value) => {
        this.setState(prevState => ({
            item: {
                ...prevState.item,
                form_name: label,
                form: value,
            },
        }), () => {
            const {item} = this.state;
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    form: validator.isEmpty(item.form),
                },
            }));
        });
    };

    validateForm = () => {
        const {item} = this.state;
        let checkName = item.name === null || validator.isEmpty(item.name);
        let checkInn = item.inn === null || !validator.isNumeric(item.inn) || !validator.isLength(item.inn, {min: 12, max: 12});
        let checkBik = item.bik === null || !validator.isAlphanumeric(item.bik) || !validator.isLength(item.bik, {min: 8, max: 8});
        let checkRs = item.rs === null || !validator.isLength(item.rs, {min: 20, max: 20}) || !validator.isAlphanumeric(item.rs);
        let checkAddress = item.address === null || validator.isEmpty(item.address);
        let checkBank = item.bank === null || validator.isEmpty(item.bank);
        let checkForm = item.form === null || validator.isEmpty(item.form);


        this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                name: checkName,
                inn: checkInn,
                bik: checkBik,
                address: checkAddress,
                bank: checkBank,
                rs: checkRs,
                form: checkForm,
            },
        }));

        return !checkName && !checkInn && !checkBik && !checkAddress && !checkBank && !checkRs && !checkForm;
    };

    getUpdatedProps = () => {
        return this.props;
    };

    submitForm = () => {
        if (this.validateForm() === false) return false;

        this.props.updateFetchContragents({...this.state.item, id: this.props.id}).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                NavigationService.navigate('ContragentsList');
            }
        });
    };

    setStateFromProps = () => {
        const {item} = this.props;
         this.setState(prevState => ({
             item: {
                 ...prevState.item,
                 name: item.name,
                 inn: item.inn,
                 bik: item.bik,
                 address: item.address,
                 bank: item.bank,
                 rs: item.rs,
                 form: item.form.id,
                 form_name: item.form.name,
             },
         }));
    };

    componentDidMount() {
        if (this.props.id && !this.props.item) {
            this.props.detailFetchContragents(this.props.id).then(() => {
                this.setStateFromProps();
            })
        } else if (this.props.item) {
            this.setStateFromProps();
        }
        if (this.props.types.length <= 0) {
            this.props.listFetchLegalTypes();
        }
    }

    render() {
        const {loading, types} = this.props;
        const {item, errors} = this.state;
        return (
            <Container>
                <ScrollWrap>
                    {loading === true && <Loader/>}
                    <Inner>
                        <Select
                            title={'Выбор формы юр. лица'}
                            label={'Форма'}
                            text={item.form_name}
                            list={types}
                            selected={item.form}
                            error={errors.form}
                            onSelect={(label, value) => this.selectForm(label, value)}/>
                        <Input
                            label="Название организации"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, name: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, name: validator.isEmpty(item.name)}}))}}
                            error={errors.name}
                            value={item.name}/>
                        <Input
                            label="ИНН/БИН"
                            keyboardType="numeric"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, inn: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, inn: !validator.isNumeric(item.inn) || !validator.isLength(item.inn, {min: 12, max: 12})}}))}}
                            error={errors.inn}
                            value={item.inn}
                            infoText={item.inn.length > 0 ? 'Количество символов: ' + item.rs.length : ''}
                            errorText="Пожалуйста, заполните поле (12 цифр)"/>
                        <Input
                            label="Банк"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, bank: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, bank: validator.isEmpty(item.bank)}}))}}
                            error={errors.bank}
                            value={item.bank}/>
                        <Input
                            label="Расчетный счет"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, rs: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, rs: !validator.isLength(item.rs, {min: 20, max: 20}) || !validator.isAlphanumeric(item.rs)}}))}}
                            error={errors.rs}
                            value={item.rs}
                            infoText={item.rs.length > 0 ? 'Количество символов: ' + item.rs.length : ''}
                            errorText="Пожалуйста, заполните поле (20 символов)"/>
                        <Input
                            label="БИК"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, bik: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, bik: !validator.isAlphanumeric(item.bik) || !validator.isLength(item.bik, {min: 8, max: 8})}}))}}
                            error={errors.bik}
                            value={item.bik}
                            infoText={item.bik.length > 0 ? 'Количество символов: ' + item.rs.length : ''}
                            errorText="Пожалуйста, заполните поле (8 символов)"/>
                        <Input
                            label="Юридический адрес"
                            underlineColorAndroid='transparent'
                            onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, address: value}}))}
                            onBlur={() => {this.setState(prevState => ({errors: {...prevState.errors, address: validator.isEmpty(item.address)}}))}}
                            error={errors.address}
                            value={item.address}/>
                    </Inner>
                </ScrollWrap>
                <ButtonView>
                    <Button title="Сохранить" onPress={this.submitForm}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    detailFetchContragents: (id) => dispatch(contragentsActions.detailFetchContragents(id)),
    updateFetchContragents: (data) => dispatch(contragentsActions.updateFetchContragents(data)),
    listFetchLegalTypes: () => dispatch(legaltypesActions.listFetchLegalTypes()),
});

const mapStateToProps = (state, props) => ({
    id: props.navigation.state.params && props.navigation.state.params.id ? props.navigation.state.params.id : null,
    item: props.navigation.state.params && props.navigation.state.params.id ? getContragentsById(state, props.navigation.state.params.id) : null,
    loading: state.contragents.loading_detail,
    types: state.legaltypes.types,
    curState: state,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
