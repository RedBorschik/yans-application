import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {LoaderPage, Loader, Container, Input} from '../../components/';
import {Alert} from 'react-native';
import {Button} from 'react-native-elements';
import validator from 'validator';
import accountActions from '../../actions/Account';

const Wrap = styled.View`
    flex: 1;
`;

const FieldView = styled.View`
  paddingLeft: 9;
  paddingRight: 8;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: {
                name: '',
                email: '',
                phone: '',
                another_phone: '',
                password: '',
                confirm_password: '',
            },
            errors: {
                name: false,
                email: false,
                phone: false,
                another_phone: false,
                password: false,
                confirm_password: false,
            },
        };
    }

    componentWillMount() {
        this.props.getFetchProfile().then(() => {
            let profile = this.checkLoadedProfile();

            if (profile.phone) {
                profile.phone = profile.phone.replace('8', '+7');
            }
            if (profile.another_phone) {
                profile.another_phone = profile.another_phone.replace('8', '+7');
            }

            this.setState(prevState => ({
                ...prevState,
                item: {
                    ...prevState.item,
                    ...profile
                }
            }))
        });
    }

    checkLoadedProfile = () => {
      return this.props.profile;
    };

    validateForm = () => {
        const {item} = this.state;

        let valid = true;

        let phone = item.phone.replace(/[^0-9.]/g,'');
        let an_phone = item.another_phone.replace(/[^0-9.]/g,'');
        let checkName = validator.isEmpty(item.name);
        let checkEmail = !validator.isEmail(item.email);
        let checkPhone = !validator.isMobilePhone(phone) && !validator.isEmpty(item.phone);
        let checkAnotherPhone = !validator.isMobilePhone(an_phone) && !validator.isEmpty(item.another_phone);
        let checkPassword = !validator.isEmpty(item.password) && item.password.length < 6;
        let checkPasswordConfirm = item.password !== this.props.confirm_password && !validator.isEmpty(item.confirm_password);

        this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                name: checkName,
                email: checkEmail,
                phone: checkPhone,
                another_phone: checkAnotherPhone,
                password: checkPassword,
                confirm_password: checkPasswordConfirm,
            },
        }));

        if (checkName || checkAnotherPhone || checkPhone || checkPassword || checkPasswordConfirm || checkEmail) {
            valid = false;
        }

        return valid;
    };

    getUpdatedProps = () => {
        return this.props;
    };

    submitForm = () => {
        if (!this.validateForm()) return false;

        const {item} = this.state;

        let data = {
            name: item.name,
            email: item.email,
            phone: item.phone,
            another_phone: item.another_phone,
        };

        if (item.password && item.password !== '' && item.confirm_password && item.confirm_password !== '') {
            data.password = item.password;
            data.confirm_password = item.confirm_password;
        }

        this.props.updateFetchProfile(data).then(() => {
            if (!this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    '',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        });
    };

    footerElement = () => {
        return (
            <ButtonView>
                <Button title="Сохранить" onPress={this.submitForm}/>
            </ButtonView>
        );
    };

    render() {
        let {error, loading, loading_action, message, accountHasError} = this.props;
        let {item, errors} = this.state;

        return (
            <Container footer={this.footerElement()}>
                {loading === true && <LoaderPage/>}
                {loading_action === true && <Loader/>}

                {loading === false && <Wrap>
                    <Input
                        label="ФИО"
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, name: value}}))}
                        onBlur={() => {validator.isEmpty(item.name)}}
                        error={errors.name}
                        value={item.name}/>

                    <Input
                        label="E-mail"
                        keyboardType={'email-pad'}
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, email: value}}))}
                        onBlur={() => {!validator.isEmail(item.email)}}
                        error={errors.email}
                        value={item.email}/>

                    <Input
                        type="tel"
                        label="Мобильный телефон"
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, phone: value}}))}
                        onBlur={() => {!validator.isMobilePhone(item.phone.replace(/[^0-9.]/g,'')) && !validator.isEmpty(item.phone)}}
                        error={errors.phone}
                        errorText="Пожалуйста, введите корректный номер телефона"
                        value={item.phone}/>

                    <Input
                        type="tel"
                        label="Другой телефон"
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, another_phone: value}}))}
                        onBlur={() => {!validator.isMobilePhone(item.another_phone.replace(/[^0-9.]/g,'')) && !validator.isEmpty(item.another_phone)}}
                        error={errors.another_phone}
                        errorText="Пожалуйста, введите корректный номер телефона"
                        value={item.another_phone}/>

                    <Input
                        label="Новый пароль"
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, password: value}}))}
                        onBlur={() => !validator.isEmpty(item.password) && item.password.length < 6}
                        error={errors.password}
                        errorText="Пожалуйста, введите корректный пароль"
                        value={item.password}/>

                    <Input
                        label="Подтверждение пароля"
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState(prevState => ({item: {...prevState.item, confirm_password: value}}))}
                        onBlur={() => {item.password !== this.props.confirm_password && !validator.isEmpty(item.confirm_password)}}
                        error={errors.confirm_password}
                        errorText="Пожалуйста, введите подтверждение пароль"
                        value={item.confirm_password}/>
                </Wrap>}
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    accountHasError: (bool) => dispatch(accountActions.accountHasError(bool)),
    getFetchProfile: () => dispatch(accountActions.getFetchProfile()),
    updateFetchProfile: (data) => dispatch(accountActions.updateFetchProfile(data)),
});

const mapStateToProps = (state) => ({
    profile: state.account.profile,
    loading: state.account.loading,
    loading_action: state.account.loading_action,
    error: state.account.error,
    message: state.account.message
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
