import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {ListItem, Button} from 'react-native-elements';
import LoginActions from './../../actions/Login';
import NavigationService from '../../NavigationService';
import {colors} from '../../utils/theme';

const Container = styled.View`
    flex: 1;
    paddingVertical: 8;
    paddingLeft: 8;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

class AccountScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const list = [
            {
                name: 'Профиль',
                route: 'Profile',
            },
            {
                name: 'Контрагенты',
                route: 'ContragentsList',
            },
            {
                name: 'Адреса доставки',
                route: 'AddressList',
            },
            {
                name: 'Мои заказы',
                route: 'OrdersList',
            },
            {
                name: 'Настройки',
                route: 'Settings',
            },
        ];
        return (
            <Container>
                {
                    list.map((l, i) => (
                        <ListItem
                            key={i}
                            title={l.name}
                            onPress={() => {NavigationService.navigate(l.route);}}
                            bottomDivider
                            chevron={{size: 30, color: colors.GREY_3}}
                        />
                    ))
                }
                <ButtonView>
                    <Button title="Выход" onPress={this.props.logoutUser}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    logoutUser: () => dispatch(LoginActions.logoutUser()),
});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
