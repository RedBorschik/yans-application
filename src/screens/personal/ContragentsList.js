import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import contragentsActions from '../../actions/Contragents';
import {getContragentsList} from '../../reducers/Contragents';
import styled from 'styled-components/native';
import {LoaderPage, Loader, EditButton} from '../../components/';
import DeleteButton from '../../components/icons/Delete';
import {Button} from 'react-native-elements';
import {Alert} from 'react-native';

const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const ItemsList = styled.View`
    flex: 1;
    overflow: visible;
    paddingTop: 16;
    paddingBottom: 80;
    paddingHorizontal: 8;
`;

const Item = styled.View`
    width: 100%;
    marginBottom: 16;
    elevation: 8; 
    backgroundColor: #ffffff;
    shadowColor: rgba(55, 135, 185, 0.18);
    shadowOffset: {
        width: 0,
        height: 8
    };
    shadowRadius: 20; 
    shadowOpacity: 1.0;
    borderRadius: 6;
    paddingTop: 16;
    paddingHorizontal: 16;    
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
`;

const ButtonsWrap = styled.View`
    position: absolute;
    top: 8;
    right: 2;
    zIndex: 1;
    flexDirection: row;
    alignItems: center;
`;

const PropWrap = styled.View`
    marginBottom: 16;
`;

const PropInner = styled.Text``;

const PropName = styled.Text`
    color: rgba(35, 31, 32, 0.6);
    fontSize: 13;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
    marginRight: 4;
`;

const PropValue = styled(MainText)`
    lineHeight: 18; 
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

const Text = styled.Text`
    fontSize: 15px;   
    color: #245068;
    fontFamily: CoreRhino45Regular;
    paddingHorizontal: 8;
`;

class Section extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.listFetchContragents();
    }

    removeItem = (id) => {
        Alert.alert(
            '',
            'Вы действительно хотите удалить этот элемент?',
            [
                {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Да', onPress: () => {this.props.removeFetchContragents(id)}},
            ],
            {cancelable: true},
        );
    };

    render() {
        const {items, loading, loading_actions, itemsObj} = this.props;

        return (
            <Container>
                <ScrollWrap>
                    <ItemsList>
                        {items.length <= 0 && loading === false && <Text>Список юр. лиц пуст</Text>}
                        {items.length > 0 ? items.map((item, key) => (<Item
                            key={item.id}>
                            <ButtonsWrap>
                                <EditButton onPress={() => NavigationService.navigate('ContragentsDetail', {id: item.id})}/>
                                <DeleteButton onPress={() => this.removeItem(item.id)}/>
                            </ButtonsWrap>
                            <Title>{new Entities().decode(item.name).toUpperCase()}</Title>
                            <PropWrap><PropInner>
                                <PropName>Название организации:&nbsp;</PropName>
                                <PropValue>{new Entities().decode(item.name)}</PropValue>
                            </PropInner>
                            </PropWrap>
                            {(item.form && item.form.name) ? <PropWrap><PropInner>
                                <PropName>Форма:&nbsp;</PropName>
                                <PropValue>{item.form.name}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {item.inn ? <PropWrap><PropInner>
                                <PropName>ИНН:&nbsp;</PropName>
                                <PropValue>{item.inn}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {item.bik ? <PropWrap><PropInner>
                                <PropName>БИК:&nbsp;</PropName>
                                <PropValue>{item.bik}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {item.address && <PropWrap><PropInner>
                                <PropName>Юридический адрес:&nbsp;</PropName>
                                <PropValue>{item.address}</PropValue>
                            </PropInner>
                            </PropWrap>}
                            {item.bank ? <PropWrap><PropInner>
                                <PropName>Банк:&nbsp;</PropName>
                                <PropValue>{item.bank}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                            {item.rs ? <PropWrap><PropInner>
                                <PropName>Расчётный счёт:&nbsp;</PropName>
                                <PropValue>{item.rs}</PropValue>
                            </PropInner>
                            </PropWrap> : null}
                        </Item>)) : null}
                    </ItemsList>
                    {loading === true && <LoaderPage/>}
                    {loading_actions === true && <Loader/>}
                </ScrollWrap>
                <ButtonView>
                    <Button title="Добавить новое юр. лицо" onPress={() => NavigationService.navigate('ContragentsNew')}/>
                </ButtonView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchContragents: () => dispatch(contragentsActions.listFetchContragents()),
    removeFetchContragents: (id) => dispatch(contragentsActions.removeFetchContragents(id)),
});

const mapStateToProps = (state, props) => ({
    items: getContragentsList(state),
    itemsObj: state.contragents.items,
    loading: state.contragents.loading,
    loading_actions: state.contragents.loading_actions,
});

export default connect(mapStateToProps, mapDispatchToProps)(Section);
