import React, {Component} from 'react';
import {connect} from 'react-redux';
import basketActions from '../../actions/Basket';
import {Container, Loader, LoaderPage, LargeCard, ModalAddBasket} from '../../components/';
import NavigationService from '../../NavigationService';
import catalogActions from '../../actions/Catalog';
import {getPage, getPageCount, getItemsList} from '../../reducers/Catalog';
import {Alert} from 'react-native';
import styled from 'styled-components/native';

const ItemsList = styled.View``;

const Text = styled.Text`
    fontSize: 15px;   
    color: #245068;
    fontFamily: CoreRhino45Regular;
`;

class HomeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
        }
    }

    componentWillMount() {
        this.props.fetchFavoritesItems().then(() => {
            if (this.getUpdatedProps().error_catalog) {
                Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message_catalog,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        });
    }

    getUpdatedProps = () => {
        return this.props;
    };

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.page_count) || !this.props.new_items.length) return false;
        if (this.isCloseToBottom(nativeEvent)) {
            this.props.fetchCatalogItems().then(() => {
                if (this.getUpdatedProps().error_catalog) {
                    Alert.alert(
                        'Ошибка',
                        this.getUpdatedProps().message_catalog,
                        [
                            {text: 'OK'},
                        ],
                    );
                }
            });
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    addToBasket = (products) => {
        this.props.addFetchBasket(products).then(() => {
            if (this.getUpdatedProps().error) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            } else {
                this.setState({showModal: true});
                setTimeout(() => {
                    this.setState({showModal: false});
                }, 500);
            }
        });
    };

    removeFavorite = (id) => {
      this.props.fetchRemoveFavoritesItem(id).then(() => {
          if (this.getUpdatedProps().error) {
              if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                  'Ошибка',
                  this.getUpdatedProps().message,
                  [
                      {text: 'OK'},
                  ],
              );
          }
      })
    };

    render() {
        const {favorites, loading, loading_add, navigation} = this.props;
        const {showModal} = this.state;
        const {state} = navigation;
        return (
            <Container onScroll={this.onScrollPage}  nestedScrollEnabled={false}>
                <ItemsList>
                    {favorites.length > 0 ? favorites.map((item, key) => (<LargeCard
                        key={item.id}
                        item={item}
                        onPress={() => NavigationService.navigate('Card', {id: item.id, go_back_name: state.routeName}, 'Card' + item.id)}
                        addBasket={(products) => this.addToBasket(products)}
                        removeFavorite={() => this.removeFavorite(item.id)}
                    />)) : <Text>{loading === false && 'Список избранного пуст'}</Text>
                    }
                </ItemsList>
                {loading_add === true && <Loader/>}
                {loading === true && <LoaderPage/>}
                <ModalAddBasket
                    visible={showModal}
                    hidePopup={() => this.setState({showModal: false})}/>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchFavoritesItems: () => dispatch(catalogActions.fetchFavoritesItems()),
    fetchRemoveFavoritesItem: (id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id)),
    addFetchBasket: (products) => dispatch(basketActions.addFetchBasket(products)),
});

const mapStateToProps = (state) => ({
    favorites: getItemsList(state, 'favorites'),
    loading: state.catalog.loading.favorites,
    loading_add: state.basket.loading_add,
    page: getPage(state, 'favorites'),
    page_count: getPageCount(state, 'favorites'),
    catalog: state.catalog.pagination.favorites,
    error: state.basket.error_add,
    error_catalog: state.catalog.error,
    message: state.basket.message,
    message_catalog: state.basket.message,
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
