import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {LoaderPage, Loader, Container, Input} from '../../components/';
import {Alert, Switch} from 'react-native';
import accountActions from '../../actions/Account';
import notifyActions from '../../actions/Notifications';

const Wrap = styled.View`
    flex: 1;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

const SwitchWrap = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    marginBottom: 16;
`;

const SwitchLabel = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 22; 
    fontFamily: CoreRhino45Regular;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    paddingBottom: 16;
    paddingRight: 60;
    fontSize: 17;
    lineHeight: 20;
    fontFamily: CoreRhino65Bold;
`;

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            push: {
                order: false,
                items: false,
                news: false,
                chat: false,
            },
            hasLocked: !(!!props.fcm),
        };
    }

    getUpdatedProps = () => {
        return this.props;
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.fcm !== this.props.fcm && !!nextProps.fcm) {
            this.setState({hasLocked: false});
        }
    }

    componentWillMount() {
        this.props.fetchGetPush().then(() => {
            let push = this.getUpdatedProps().push;
            this.setState(prevState => ({
                ...prevState,
                push: {
                    ...prevState.push,
                    ...push
                },
            }));
        });
        this.props.checkPermission();
    }

    submitForm = (elem, value) => {
        this.setState(prevState => ({
            ...prevState,
            push: {
                ...prevState.push,
                [elem]: value,
            },
        }), () => {
            this.props.fetchUpdatePush(this.state.push).then(() => {
                if (this.getUpdatedProps().error) {
                    let push = this.getUpdatedProps().push;
                    this.setState(prevState => ({
                        ...prevState,
                        push: {
                            ...prevState.push,
                            ...push
                        },
                    }));
                    if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                        'Ошибка',
                        this.getUpdatedProps().message,
                        [
                            {text: 'OK'},
                        ],
                    );
                }
            });
        });
    };

    render() {
        const {loading, loading_action} = this.props;
        const {push, hasLocked} = this.state;
        const labels = {
            order: 'Информация о заказе',
            items: 'Поступление товаров',
            news: 'Новости и спец. предложения',
            chat: 'Сообщения из онлайн-чата',
        };

        return (
            <Container>
                {loading === true && <LoaderPage/>}
                {loading_action === true && <Loader/>}

                {loading === false && <Wrap pointerEvents={hasLocked ? 'none' : 'auto'} style={{
                    opacity: hasLocked ? 0.5 : 1,
                }}>
                    <Title>Получать Push-уведомления:</Title>
                    <SwitchWrap>
                        <SwitchLabel>Информация о заказе</SwitchLabel>
                        <Switch
                            trackColor={push.order ? '#5bbb5e61' : '#9e9e9e'}
                            thumbColor={push.order ? '#5bbb5e' : '#ffffff'}
                            onValueChange={(value) => this.submitForm('order', value)}
                            value={push.order}/>
                    </SwitchWrap>
                    <SwitchWrap>
                        <SwitchLabel>Поступление товаров</SwitchLabel>
                        <Switch
                            trackColor={push.items ? '#5bbb5e61' : '#9e9e9e'}
                            thumbColor={push.items ? '#5bbb5e' : '#ffffff'}
                            onValueChange={(value) => this.submitForm('items', value)}
                            value={push.items}/>
                    </SwitchWrap>
                    <SwitchWrap>
                        <SwitchLabel>Новости и спец. предложения</SwitchLabel>
                        <Switch
                            trackColor={push.news ? '#5bbb5e61' : '#9e9e9e'}
                            thumbColor={push.news ? '#5bbb5e' : '#ffffff'}
                            onValueChange={(value) => this.submitForm('news', value)}
                            value={push.news}/>
                    </SwitchWrap>
                    <SwitchWrap>
                        <SwitchLabel>Сообщения из онлайн-чата</SwitchLabel>
                        <Switch
                            trackColor={push.chat ? '#5bbb5e61' : '#9e9e9e'}
                            thumbColor={push.chat ? '#5bbb5e' : '#ffffff'}
                            onValueChange={(value) => this.submitForm('chat', value)}
                            value={push.chat}/>
                    </SwitchWrap>
                </Wrap>}
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchGetPush: () => dispatch(accountActions.fetchGetPush()),
    fetchUpdatePush: (data) => dispatch(accountActions.fetchUpdatePush(data)),
    checkPermission: () => dispatch(notifyActions.checkPermission()),
});

const mapStateToProps = (state) => ({
    push: state.account.push,
    loading: state.account.loading,
    loading_action: state.account.loading_action,
    error: state.account.error,
    message: state.account.message,
    fcm: state.notify.token,
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
