import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from '../../NavigationService';
import {NavigationEvents} from 'react-navigation';
import {Platform, Dimensions} from 'react-native';
import styled from 'styled-components/native';
import catalogActions from '../../actions/Catalog';
import basketActions from '../../actions/Basket';
import {getBasketList} from '../../reducers/Basket';

import {Button} from 'react-native-elements';
import LoaderPage from '../../components/helpers/LoaderPage';
import Loader from '../../components/helpers/Loader';
import CartItem from '../../components/cart/CartItem';


const Container = styled.View`
  flex: 1;
`;

const Title = styled.Text`
    color: #231f20;
    fontSize: 24;
    lineHeight: 29; 
    fontFamily: CoreRhino65Bold;
`;

const FixedTitle = styled.View`
    paddingHorizontal: 16;
    paddingBottom: 16;
    height: 47;
    width: 100%;
    backgroundColor: #fff;
    elevation: 5;
    position: absolute;
    left: 0;
    top: 0;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const Price = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 20;
    lineHeight: 24;
    paddingHorizontal: 4;
`;

const Currency = styled.Text`
    color: #1f4c65;
    fontFamily: CoreRhino65Bold;
    fontSize: 16;
`;

const Total = styled.View`
    flexDirection: row;
    alignItems: center;
`;

const TotalLabel = styled.Text`
    color: rgba(35, 31, 32, 0.5);
    fontFamily: CoreRhino45Regular;
    fontSize: 13;
`;

const ScrollView = styled.ScrollView`
    marginTop: 47;
    flex: 1;
`;

const ItemsList = styled.View`
    paddingTop: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    paddingBottom: 86;
`;

const ButtonView = styled.View`
  position: absolute;
  bottom: 24;
  left: 28px;
  right: 28px;
`;

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            view: true,
        };
    }

    render() {
        const {sum, items, loading, loading_add, fetchAddFavoritesItem, fetchRemoveFavoritesItem, removeFetchBasket, updateFetchBasket} = this.props;
        const {view} = this.state;

        return (
            <Container>
                <NavigationEvents onWillFocus={() => {
                    this.props.listFetchBasket();
                    this.setState({view: true});
                }} onWillBlur={() => { this.setState({view: false});}}/>
                <FixedTitle>
                    <Title>Корзина</Title>
                    {sum && sum > 0 ? <Total>
                        <TotalLabel>Итог:</TotalLabel>
                        <Price>{Number(sum).toFixed(0)}</Price>
                        <Currency>₸</Currency>
                    </Total> : null}
                </FixedTitle>
                <ScrollView>
                    <ItemsList>
                        {loading !== false ? <LoaderPage/> : null}
                        {loading_add !== false ? <Loader/> : null}
                        {items.length > 0 && view && !loading && items.map(item => {
                            return item.packing ? <CartItem
                                key={item.id}
                                item={item}
                                removeItem={() => removeFetchBasket([item.id])}
                                addFavorite={() => fetchAddFavoritesItem(item.product_id, item.id)}
                                removeFavorite={() => fetchRemoveFavoritesItem(item.product_id, item.id)}
                                updateItem={(item) => updateFetchBasket(item)}
                            /> : null
                        })}
                        {items.length <= 0 && !loading && <TotalLabel style={{paddingHorizontal: 16}}>Корзина пуста</TotalLabel>}
                    </ItemsList>
                </ScrollView>
                {items.length > 0 && <ButtonView>
                    <Button title="Оформить заказ" onPress={() => NavigationService.navigate('OrdersNew')}/>
                </ButtonView>}
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchBasket: () => dispatch(basketActions.listFetchBasket()),
    fetchAddFavoritesItem: (id, basket_id) => dispatch(catalogActions.fetchAddFavoritesItem(id, basket_id)),
    fetchRemoveFavoritesItem: (id, basket_id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id, basket_id)),
    removeFetchBasket: (ids) => dispatch(basketActions.removeFetchBasket(ids)),
    updateFetchBasket: (item) => dispatch(basketActions.updateFetchBasket(item)),
    basketIsLoading: (item) => dispatch(basketActions.basketIsLoading(bool)),
});

const mapStateToProps = (state) => ({
    items: getBasketList(state),
    sum: state.basket.sum,
    loading: state.basket.loading,
    loading_add: state.basket.loading_add,
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

