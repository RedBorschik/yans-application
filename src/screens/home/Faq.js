import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import {Container, IconChevron} from '../../components/';
import styled from 'styled-components/native';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
    paddingBottom: 16;
`;

const Title = styled(MainText)`
    width: 100%;
    paddingBottom: 30;
    fontSize: 19;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
`;

const CollapseTitle = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const CollapseName = styled(MainText)`
    fontFamily: CoreRhino65Bold;
    flex: 1;
    flexWrap: wrap;
    paddingBottom: 0;
`;

const IconChevronWrap = styled.View`
    width: 28;
    height: 28;
    marginLeft: 16;
    justifyContent: center;
    alignItems: center;
`;

const Delimer = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef; 
    marginVertical: 16;
`;

const Item = styled.View``;

class PageScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [
                {
                    show: false,
                    title: 'Насколько актуальна информация на сайте?',
                    text: 'Сайт и база данных взаимодействуют в режиме реального времени, то есть все изменения в прайс листах и остатках отражаются в полном объеме и своевременно. Тем не менее при наличии вопросов наши операторы будут рады проконсультировать вас.',
                },
                {
                    show: false,
                    title: 'Как насчет скидок?:)',
                    text: 'Мы за справедливую цену для всех, и у нас вы не найдете колонок и классификаций цен для разных партнеров. На сайте изначально указаны минимальные цены, и пересмотр цен на некоторые позиции возможен при единовременном заказе свыше одного миллиона тенге.',
                },
                {
                    show: false,
                    title: 'Почему некоторые товары представлены на сайте, но указанно, что их нет в наличии?',
                    text: 'Сайт взаимодействует с нашей складской базой данных, и таким образом показывает актуальную информацию о наличии товаров на данный конкретный момент. Складские запасы пополняются ежедневно, и даже если нужного вам товара не оказалось на складе, скорее всего он появится в ближайшие дни. Вы можете выяснить ожидаемый срок поступления у операторов call-центра.',
                },
                {
                    show: false,
                    title: 'Какое минимальное количество для заказа в регионы?',
                    text: 'Для заказа в регион минимальным количество - коробка. Вы можете заказать любой товар в количестве, кратной коробке.\n',
                },
                {
                    show: false,
                    title: 'Почему в некоторых товарах невозможно выбрать штучно, а только упаковками или коробками?',
                    text: 'Некоторые товары имеют неделимую упаковку (пример: стакан 200 мл. упакован по 50 шт.). В целях соблюдения санитарных норм мы не можем вскрывать подобную упаковку. В случае с такими товарами Вы можете выбирать количество, кратное минимальной упаковке (пример: стакан 200 мл. можете заказать 50 шт., 100 шт., 150 шт. и т.д.) В карточках таких товаров цена за шт. указана справочно, для Вашего удобства. ',
                },
                {
                    show: false,
                    title: 'Установлена ли минимальная сумма для заказа?',
                    text: 'Минимальной суммы для заказов нет, при самовывозе Вы сможете забрать произвольное количество товара. При доставке в черте г. Нур-Султан и г. Алматы минимальная сумма заказа 10000 тг., доставка бесплатна. Подробности в разделе "Доставка"',
                },
            ],
        };
    }

    changeCollapse = (index, collapsed) => {
        this.setState(prevState => ({
            ...prevState.list,
            items: prevState.list.map((item, i) => {
                if (i === index) {
                    item.show = collapsed;
                }
                return item;
            }),
        }));
    };

    render() {
        const {list} = this.state;
        return (
            <Container>
                <Title>Вопросы и ответы</Title>

                {list.map((item, key) => (<Item key={key}>
                    <Collapse
                        isCollapsed={item.show}
                        onToggle={(isCollapsed) => this.changeCollapse(key, isCollapsed)}>
                        <CollapseHeader>
                            <CollapseTitle>
                                <CollapseName>{item.title}</CollapseName>
                                <IconChevronWrap
                                    style={{
                                        transform: [{rotate: item.show ? '180deg' : '0deg'}],
                                    }}
                                ><IconChevron/></IconChevronWrap>
                            </CollapseTitle>
                        </CollapseHeader>
                        <CollapseBody>
                            <MainText style={{paddingTop: 16, paddingBottom: 0}}>{item.text}</MainText>
                        </CollapseBody>
                    </Collapse>
                    <Delimer/>
                </Item>))}
            </Container>
        );
    }
}

export default PageScreen;
