import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions, Linking} from 'react-native';
import {Button} from 'react-native-elements';
import NavigationService from '../../NavigationService';
import newsActions from '../../actions/News';
import {getNewsById} from '../../reducers/News';
import styled from 'styled-components/native';
import LoaderPage from '../../components/helpers/LoaderPage';
import ImageCard from '../../components/catalog/elements/ImageCard';
import {Slider} from '../../components/helpers';
import HTML from 'react-native-render-html';
const Entities = require('html-entities').XmlEntities;

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const Item = styled.View` 
    flex: 1;
    paddingTop: ${Platform.OS === 'android' ? 0 : 8};
    paddingBottom: 76;
`;

const SliderWrap = styled.View`
    height: ${Platform.OS === 'android' ? 224 : 189};
    paddingHorizontal: 1;
`;

const Content = styled.View`
    flex: 1;
    marginTop: 16;
    paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
    paddingBottom: 16;
    flexDirection: column;
    justifyContent: space-between;
`;

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 18; 
    fontFamily: CoreRhino45Regular;
`;

const Date = styled.View`
    height: 20;
    paddingHorizontal: 4;
    justifyContent: center;
    position: absolute;
    top: 14;
    left: -1;
    zIndex: 5;
    backgroundColor: #5BBB5E;
`;

const DateText = styled.Text`
    color: #ffffff;
    fontSize: 12;
    fontFamily: CoreRhino45Regular;
`;

const Name = styled.Text`
    fontSize: 17;
    lineHeight: 22;
    fontFamily: CoreRhino65Bold;
    color: #231f20;
`;

class NewsDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
        };
    }

    componentDidMount() {
        this.props.detailFetchNews(this.props.id).then(() => {
            this.setState({
                loaded: true,
            });
        });
    }

    getUpdatedProps = () => {
        return this.props;
    };

    render() {
        const {loaded} = this.state;
        const {item, loading, navigation} = this.props;
        const {state} = navigation;
        let images = item.images && item.images.length ? item.images.map(src => {return {img: src};}) : [];

        return (
            <Container>
                <ScrollWrap nestedScrollEnabled={false}>
                    {loaded ? <Item>
                        {<SliderWrap>
                            <Date><DateText>{item.date}</DateText></Date>
                            {images.length > 0 ? <Slider
                                items={images}
                                slideWidth={Dimensions.get('window').width - 2}
                                slideHeight={224}
                                resizeMode={'contain'}
                                bottomRadius={0}
                                slideInnerStyles={Platform.OS === 'android' ? {
                                    borderRadius: 0,
                                    elevation: 0,
                                } : {
                                    borderBottomLeftRadius: 0,
                                    borderBottomRightRadius: 0,
                                }}
                            /> : <ImageCard style={Platform.OS === 'android' ? {borderTopLeftRadius: 0, borderTopRightRadius: 0} : {}} image={item.image} resize={'contain'} is_large={true}/>}
                        </SliderWrap>}
                        <Content>
                            <Name numberOfLines={10}>{new Entities().decode(item.name.replace('&nbsp;', " "))}</Name>
                            <HTML
                                containerStyle={{
                                    paddingTop: 16,
                                }}
                                baseFontStyle={{
                                    fontFamily: 'CoreRhino45Regular',
                                    color: '#231f20',
                                    fontSize: 15,
                                    lineHeight: 19,
                                }}
                                onLinkPress={(evt, href) => {Linking.openURL(href);}}
                                html={item.description}/>
                            {item.filter ? <Button buttonStyle={{
                                marginTop: 24,
                            }} title="Перейти к товарам" onPress={() => NavigationService.navigate('Section', {filter: item.filter, go_back_name: state.routeName})}/> : null}
                        </Content>
                    </Item> : null}
                    {loading === true && <LoaderPage/>}
                </ScrollWrap>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    detailFetchNews: (id) => dispatch(newsActions.detailFetchNews(id)),
});

const mapStateToProps = (state, props) => ({
    item: props.navigation.state.params && props.navigation.state.params.id ? getNewsById(state, props.navigation.state.params.id) : null,
    loading: state.news.loading,
    error: state.news.error,
    message: state.news.message,
    id: props.navigation.state.params && props.navigation.state.params.id ? props.navigation.state.params.id : null,
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsDetail);
