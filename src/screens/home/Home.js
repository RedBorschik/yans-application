import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, ScrollView} from 'react-native';
import {LoaderPage, SmallCard, Banners} from '../../components/';
import NavigationService from '../../NavigationService';
import catalogActions from '../../actions/Catalog';
import {getPage, getPageCount, getItemsList} from '../../reducers/Catalog';
import bannersActions from '../../actions/Banners';
import {getBanners} from '../../reducers/Banners';
import styled from 'styled-components/native';

const ItemsList = styled.View`
    flex: 1;
    justifyContent: space-between;
    flexDirection: row;
    flexWrap: wrap;    
`;

const ContainerView = styled.View`
  flex: 1; 
  backgroundColor: #ffffff;  
`;

const InnerView = styled.View`
  paddingTop: ${Platform.OS === 'android' ? 8 : 0};
`;

const InnerViewPadding = styled.View`
  paddingHorizontal: ${Platform.OS === 'android' ? 16 : 8};
  paddingBottom: 16;
`;

const Top = styled.View`
  paddingTop: ${Platform.OS === 'android' ? 8 : 0};
`;

class HomeScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchBanners();
        this.props.fetchCatalogNewItems();
    }

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.page_count) || !this.props.new_items.length) return false;
        if (this.isCloseToBottom(nativeEvent)) {
            this.props.fetchCatalogItems();
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    render() {
        const {loading, banners, new_items, fetchAddFavoritesItem, fetchRemoveFavoritesItem} = this.props;
        const {state} = this.props.navigation;
        return (
            <ContainerView>
                <ScrollView onScroll={this.props.onScrollPage} nestedScrollEnabled={false}>
                    <InnerView>
                        {banners.length ? <Banners banners={banners}/> : <Top/>}
                    </InnerView>
                    <InnerViewPadding>
                        <ItemsList>
                            {new_items.length > 0 ? new_items.map((item, key) => (<SmallCard
                                key={item.id}
                                item={item}
                                onPress={() => NavigationService.navigate('Card', {id: item.id, go_back_name: state.routeName}, 'Card' + item.id)}
                                addFavorite={() => fetchAddFavoritesItem(item.id)}
                                removeFavorite={() => fetchRemoveFavoritesItem(item.id)}
                            />)) : null}
                            {loading !== false ? <LoaderPage/> : null}
                        </ItemsList>
                    </InnerViewPadding>
                </ScrollView>
            </ContainerView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchCatalogItems: () => dispatch(catalogActions.fetchCatalogItems()),
    fetchCatalogNewItems: () => dispatch(catalogActions.fetchCatalogNewItems()),
    fetchAddFavoritesItem: (id) => dispatch(catalogActions.fetchAddFavoritesItem(id)),
    fetchRemoveFavoritesItem: (id) => dispatch(catalogActions.fetchRemoveFavoritesItem(id)),
    fetchBanners: () => dispatch(bannersActions.fetchBanners()),
});

const mapStateToProps = (state) => ({
    new_items: getItemsList(state, 'new_items'),
    loading: state.catalog.loading.new_items,
    page: getPage(state, 'new_items'),
    page_count: getPageCount(state, 'new_items'),
    catalog: state.catalog,
    banners: getBanners(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
