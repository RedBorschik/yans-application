import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Dimensions} from 'react-native';
import NavigationService from '../../NavigationService';
import newsActions from '../../actions/News';
import {getNewsList} from '../../reducers/News';
import styled from 'styled-components/native';
import LoaderPage from '../../components/helpers/LoaderPage';
import NewsItem from '../../components/news/NewsItem';

const Container = styled.View`
    flex: 1;
`;

const ScrollWrap = styled.ScrollView`
    flex: 1;
`;

const ItemsList = styled.View`
    overflow: visible;
    paddingTop: 16;
    paddingBottom: 16;
    paddingHorizontal: ${Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 16 : 8};
    alignItems: stretch;
    flexWrap: wrap;
    flexDirection: row;
    justifyContent: space-between;
`;

const Text = styled.Text`
    fontSize: 15px;   
    color: #245068;
    fontFamily: CoreRhino45Regular;
    paddingHorizontal: 8;
`;

class NewsList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
        };
    }

    componentDidMount() {
        this.props.listFetchNews().then(() => {
            this.setState({
                loaded: true
            })
        })
    }

    getUpdatedProps = () => {
        return this.props;
    };

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
    };

    getNextPage = (nativeEvent) => {
        if (Number(this.props.page) > Number(this.props.count_pages)) return false;
        if (this.isCloseToBottom(nativeEvent) && this.state.view) {
            this.props.listFetchNews();
        }
    };

    onScrollPage = ({nativeEvent}) => {
        this.getNextPage(nativeEvent);
    };

    render() {
        const {loaded} = this.state;
        const {items, loading} = this.props;

        return (
            <Container>
                <ScrollWrap onScroll={this.onScrollPage} nestedScrollEnabled={false}>
                    <ItemsList>
                        {items.length <= 0 && loading === false && loaded === true && <Text>Новостей пока нет</Text>}
                        {items.length > 0 ? items.reverse().map(item => (<NewsItem
                            key={item.id}
                            item={item}
                            onPress={() => NavigationService.navigate('NewsDetail', {id: item.id})}
                        />)) : null}
                    </ItemsList>
                    {loading === true && <LoaderPage/>}
                </ScrollWrap>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchNews: () => dispatch(newsActions.listFetchNews()),
});

const mapStateToProps = (state, props) => ({
    items: getNewsList(state),
    loading: state.news.loading,
    page: state.news.page,
    count_pages: state.news.count_pages,
    error: state.news.error,
    message: state.news.message,
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
