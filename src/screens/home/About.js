import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import {Container} from '../../components/';
import styled from 'styled-components/native';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
    paddingBottom: 16;
`;

const Title = styled(MainText)`
    width: 100%;
    paddingBottom: 30;
    fontSize: 19;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
`;

const Subtitle = styled(Title)`
    fontSize: 15;
    lineHeight: 19; 
`;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginVertical: 0;
`;

const ListItemContainer = styled.View`
    width: 100%;
    minHeight: 44;
    paddingVertical: 8;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const ListInner = styled.View`
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 52) : (Dimensions.get('window').width - 36)};
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15; 
    lineHeight: 19;
    flex: 1;
    flexWrap: wrap;
`;

const Divider = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef;
`;

const IconWrap = styled.View`
    width: 28;
    height: 28;
    justifyContent: center;
    alignItems: center;
    marginRight: ${Platform.OS === 'android' ? 16 : 8};
    paddingRight: 0; 
`;

const Icon = styled.Image``;

class PageScreen extends Component {
    render() {
        const list = [
            {
                image: require('../../../assets/images/content/about/cart.png'),
                text: 'Большой ассортимент товаров.',
            },
            {
                image: require('../../../assets/images/content/about/money.png'),
                text: 'Выгодные цены. На некоторые позиции они ниже основных конкурентов на 35-40%.',
            },
            {
                image: require('../../../assets/images/content/about/delivery.png'),
                text: 'Быстрая доставка. Это не просто слова: свой заказ вы можете получить в тот же день. Мы привозим товары в течении шести рабочих часов после оформления, чем выгодно отличаемся от конкурентов. ',
            },
            {
                image: require('../../../assets/images/content/about/documents.png'),
                text: 'Предоставление сопроводительных документов и НДС. Наши клиенты могут рассчитывать, что их бумаги будут в порядке, но при этом не переплачивать за это, как происходит в большинстве компаний.',
            },
            {
                image: require('../../../assets/images/content/about/share.png'),
                text: 'Большое количество каналов связи для размещения заказа. В первую очередь это call-центр с операторами, форма на сайте, социальные сети (whatsapp, telegram, FB, VK, Instagram).',
            },
            {
                image: require('../../../assets/images/content/about/support.png'),
                text: 'Работа с каждым клиентом и учет индивидуальных пожеланий. Работа нашей компании строится на четких и отлаженных бизнес-процессах, которые позволяют специалистам оперативно принимать и обрабатывать заказы, избегая ошибок.',
            },
        ];
        return (
            <Container>
                <Title>О компании</Title>

                <Subtitle>Заказ и доставка товаров для HoReCa в Казахстане: быстро, удобно и качественно</Subtitle>

                <MainText>Что такое Yans? Мы современная и надежная компания, уже много лет занимающаяся продажей расходных материалов для HoReCa (hotel, restaurant and cafe) и действующая на территории Казахстана.</MainText>

                <Subtitle>Шесть причин обратиться к нам</Subtitle>

                {list.map((item, key) => (<ListItemWrap key={key}>
                    <ListItemContainer>
                        <IconWrap>
                            <Icon
                                style={{
                                    width: '100%',
                                    height: '100%',
                                }}
                                resizeMode="contain"
                                source={item.image}/>
                        </IconWrap>
                        <ListItem numberOfLines={10}>{item.text}</ListItem>
                    </ListItemContainer>
                </ListItemWrap>))}

                <Subtitle style={{marginTop: 16}}>Комфорт - в деталях!</Subtitle>

                <MainText>Товары для HoReCa - необходимые в хозяйстве вещи, начиная одноразовой посудой и моющими средствами, заканчивая  упаковкой и лентами для чековых принтеров. Нашими клиентами являются не только отели, рестораны и заведения общепита, но и компании самой разной направленности. Иными словами, мы поставляем товары, без которых невозможно комфортное функционирование большинства компаний. Мы искренне верим, что даем возможность компаниям развиваться, не отвлекаясь на закончившиеся чернила в принтере или заказ новой упаковки.</MainText>

                <MainText>Появившись на рынке в 2009 году, через пять лет Yans перешел от крупных оптовых продаж к розничной торговли, включив в клиентскую базу и небольших клиентов. Для удобства заказчиков была запущена служба доставки по Нур-Султану и Алматы, а также активно развивается доставка в регионы. Мы значительно расширили свой ассортимент, и сегодня у нас представлено более 1000 активный позиций.</MainText>

                <MainText>Главная задача Yans - предоставление компаниям удобного сервиса, который позволяет сотрудникам не тратить много времени на поиски удачной стоимости, надежного поставщика и оперативной доставки. Наши клиенты могут приобрести все в одном месте, поскольку именно у нас представлены самые выгодные предложения на рынке. Кроме того, мы организуем оперативную доставку товара на хороших условиях.</MainText>
            </Container>
        );
    }
}

export default PageScreen;
