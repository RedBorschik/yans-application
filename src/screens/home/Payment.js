import React, {Component} from 'react';
import {Platform, Dimensions} from 'react-native';
import {Container, IconChevron} from '../../components/';
import styled from 'styled-components/native';
import {Icon} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import HTML from 'react-native-render-html';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
    paddingBottom: 16;
`;

const Title = styled(MainText)`
    width: 100%;
    paddingBottom: 30;
    fontSize: 19;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
`;

const CollapseTitle = styled.View`
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const CollapseName = styled(MainText)`
    fontFamily: CoreRhino65Bold;
    flex: 1;
    flexWrap: wrap;
    paddingBottom: 0;
`;

const IconChevronWrap = styled.View`
    width: 28;
    height: 28;
    marginLeft: 16;
    justifyContent: center;
    alignItems: center;
`;

const Delimer = styled.View`
    width: 100%;
    height: 1;
    backgroundColor: #efefef; 
    marginVertical: 16;
`;

const Item = styled.View``;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginTop: 20;
`;

const ListItemContainer = styled.View`
    width: 100%;
    minHeight: 44;
    paddingVertical: 8;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const ListInner = styled.View`
    width: ${Platform.OS === 'android' ? (Dimensions.get('window').width - 52) : (Dimensions.get('window').width - 36)};
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15; 
    lineHeight: 19;
    flex: 1;
    flexWrap: wrap;
`;

const IconWrap = styled.View`
    width: 32;
    height: 32;
    justifyContent: center;
    alignItems: center;
    marginRight: ${Platform.OS === 'android' ? 16 : 8};
    paddingRight: 0; 
`;

class PageScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [
                {
                    show: false,
                    title: 'Оплата курьеру наличными',
                    text: 'Подойдет при заказах на небольшие суммы, когда клиент желает получить товары в короткий срок.\n' +
                    'Необходимая сумма оплачивается курьеру по факту доставки.',
                },
                {
                    show: false,
                    title: 'Оплата безналичными средствами с лицевого счета',
                    text: 'Если клиенту удобнее производить оплату по безналу, то после принятия заказа через оператора или в личном ' +
                    'кабинете интернет-магазина выставляется счет на оплату. Доставка осуществляется после его погашения, если не оговорены другие условия.',
                },
                {
                    show: false,
                    title: 'Служба доставки компании',
                    text: 'Компания предлагает воспользоваться услугами собственной службы доставки. Главное ее преимущество - оперативность. У клиента есть возможность получить товары день в день.\n' +
                    '\n' +
                    '- Заказ, который был оформлен с 09:00 до 13:00, будет доставлен с 15:00 до 21:00 этого дня\n' +
                    '- Заказ, поступивший после 13:00 до 18:00, привозят на следующий день с 9:00 до 15:00\n\n' +
                    'Доставка через нашу службу бесплатна, единственное ограничение это минимальная сумма заказа для доставки, которая составляет 10000 тг. Служба работает в пределах г. Нур-Султан и г. Алматы. \n' +
                    '\n' +
                    'Мы также отгружаем товары в регионы через транспортные компании. На данный момент мы сотрудничаем с компаниями JetLogistics и СДЭК. Также Вы можете самостоятельно выбрать курьерскую компанию.\n' +
                    '\n' +
                    '<span style="color: #ff0000">ВНИМАНИЕ: на данный момент действует ограничение по отправке заказов в регионы, минимальное количество единицы ассортимента должно быть кратно коробке. Это решение связано с частыми жалобами о повреждениях в процессе перевозки. ' +
                    'Также не осуществляется доставка некоторых особо легко деформируемых товаров, таких как бутылки. </span> ' +
                    '\n\n' +
                    '<span style="color: #ff0000">Подробности у наших операторов.</span>' +
                    '\n\n' +
                    'Мы надеемся в скором времени решить эту проблему и убрать данное ограничение.',
                },
            ],
        };
    }

    changeCollapse = (index, collapsed) => {
        this.setState(prevState => ({
            ...prevState.list,
            items: prevState.list.map((item, i) => {
                if (i === index) {
                    item.show = collapsed;
                }
                return item;
            }),
        }));
    };

    render() {
        const {list} = this.state;
        return (
            <Container>
                <Title>Оплата и доставка</Title>

                {list.map((item, key) => (<Item key={key}>
                    <Collapse
                        isCollapsed={item.show}
                        onToggle={(isCollapsed) => this.changeCollapse(key, isCollapsed)}>
                        <CollapseHeader>
                            <CollapseTitle>
                                <CollapseName>{item.title}</CollapseName>
                                <IconChevronWrap
                                    style={{
                                        transform: [{rotate: item.show ? '180deg' : '0deg'}],
                                    }}
                                ><IconChevron/></IconChevronWrap>
                            </CollapseTitle>
                        </CollapseHeader>
                        <CollapseBody>
                            <HTML
                                containerStyle={{
                                    paddingTop: 16,
                                }}
                                baseFontStyle={{
                                    fontFamily: 'CoreRhino45Regular',
                                    color: '#231f20',
                                    fontSize: 15,
                                    lineHeight: 19,
                                }}
                                html={item.text}/>
                        </CollapseBody>
                    </Collapse>
                    <Delimer/>
                </Item>))}

                <ListItemWrap>
                    <ListItemContainer>
                        <IconWrap>
                            <Icon
                                size={32}
                                name='phone-forwarded'
                                color='#929292' />
                        </IconWrap>
                        <ListItem numberOfLines={10}>На все вопросы может ответить наши специалисты call-центра. Звоните и заказывайте товары по самой выгодной цене на рынке!
                        </ListItem>
                    </ListItemContainer>
                </ListItemWrap>

            </Container>
        );
    }
}

export default PageScreen;