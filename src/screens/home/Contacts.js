import React, {Component} from 'react';
import {Linking, TouchableOpacity, Platform, Dimensions} from 'react-native';
import {Container, IconMap} from '../../components/';
import styled from 'styled-components/native';
import MapView, {Marker} from 'react-native-maps';
import {Icon} from 'react-native-elements';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    width: 100%;
    paddingBottom: 30;
    fontSize: 19;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
`;

const ContactItem = styled.View``;

const ListItemWrap = styled.View`
    paddingVertical: 0;
    marginBottom: 16;
`;

const ListItemContainer = styled.View`
    width: 100%;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const ListItem = styled.Text`
    color: #231f20;
    fontFamily: CoreRhino45Regular;
    fontSize: 15; 
    lineHeight: 19;
    flex: 1;
    flexWrap: wrap;
`;

const ListInner = styled.View`
    flex: 1;
    flexWrap: wrap;
`;

const IconWrap = styled.View`
    width: 28;
    height: 28;
    justifyContent: center;
    alignItems: center;
    marginRight: ${Platform.OS === 'android' ? 16 : 8};
    paddingRight: 0; 
`;

const Delimer = styled.View`
    width: 100%;
    height: 1;
    marginVertical: 16;
`;

class PageScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: [
                {
                    address: 'Республика Казахстан, г. Нур-Султан, 010000, ул. Жетiген, здание 17',
                    email: 'sales@yans.kz',
                    phones: ['+7 7172 783 733', '+7 701 0 733 733'],
                    lat: 51.18630308779139,
                    lng: 71.47136592321515,
                },
                {
                    address: 'Республика Казахстан, г. Алматы, А00А3А1, ул. Василия Бенберина 14 (20)',
                    email: 'sales@yans.kz',
                    phones: ['+7 727 317 77 33', '+7 701 0 733 733'],
                    lat: 43.27522736175756,
                    lng: 76.84262409325406,
                },
            ],
        };
    }

    render() {
        const {contacts} = this.state;
        return (
            <Container>
                {contacts.map((contact, index) => (<ContactItem key={index}>
                    {contact.address && <ListItemWrap>
                        <ListItemContainer>
                            <IconWrap>
                                <IconMap
                                    width={23}
                                    height={28}
                                    color="#929292"/>
                            </IconWrap>
                            <ListItem numberOfLines={10}>{contact.address}</ListItem>
                        </ListItemContainer>
                    </ListItemWrap>}
                    {contact.email && <ListItemWrap>
                        <ListItemContainer>
                            <IconWrap>
                                <Icon
                                    size={28}
                                    name='mail'
                                    color='#929292'/>
                            </IconWrap>
                            <ListInner>
                                <TouchableOpacity onPress={() => {Linking.openURL(contact.email);}}><MainText numberOfLines={10}>{contact.email}</MainText></TouchableOpacity>
                            </ListInner>
                        </ListItemContainer>
                    </ListItemWrap>}
                    {contact.phones && <ListItemWrap>
                        <ListItemContainer>
                            <IconWrap>
                                <Icon
                                    size={28}
                                    name='local-phone'
                                    color='#929292'/>
                            </IconWrap>
                            <ListInner>
                                {contact.phones.map((phone, i) => (
                                    <TouchableOpacity key={i} onPress={() => {Linking.openURL(phone);}}><ListItem style={{paddingBottom: 0}} numberOfLines={10}>{phone}</ListItem></TouchableOpacity>
                                ))}
                            </ListInner>
                        </ListItemContainer>
                    </ListItemWrap>}
                    <MapView
                        style={{
                            flex: 1,
                            height: 300,
                        }}
                        region={{
                            latitude: contact.lat,
                            longitude: contact.lng,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}>
                        <MapView.Marker
                            coordinate={{
                                latitude: contact.lat,
                                longitude: contact.lng,
                            }}
                            title={contact.address}
                        />
                    </MapView>
                    <Delimer/>
                </ContactItem>))}
            </Container>
        );
    }
}

export default PageScreen;
