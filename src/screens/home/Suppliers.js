import React, {Component} from 'react';
import {Platform, Dimensions, Linking} from 'react-native';
import {Container} from '../../components/';
import styled from 'styled-components/native';
import HTML from 'react-native-render-html';

const MainText = styled.Text`
    color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Title = styled(MainText)`
    width: 100%;
    paddingBottom: 30;
    fontSize: 19;
    lineHeight: 24;
    fontFamily: CoreRhino65Bold;
`;

class PageScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '&nbsp;У нас, как у компании с обширной сетью продаж по всей территории страны, широкий пул поставщиков, отношения с которыми мы ценим.&nbsp;<br>\n' +
            '\tНо, так как мы постоянно работаем над своим ассортиментом товаров, мы открыты для новых возможностей сотрудничества.<br>\n' +
            '\tПри выборе поставщиков мы уделяем большое внимание качеству товаров, ценовой политике партнеров и условиям поставки.<br>\n' +
            '\tСотрудничество же с нами дает наиболее полный охват регионов, качественную обратную связь и минимальные наценки на пути к конечному потребителю. Исповедуемая нами клиентоориентированность будет работать также и на ваш бренд.\n' +
            '\t<p>\n' +
            '\t\tПеречень документов, которые значительно ускорят рассмотрение вашей заявки:\n' +
            '\t</p>\n' +
            '\t<ul>\n' +
            '\t\t<li>коммерческое предложение;</li>\n' +
            '\t\t<li>каталог;</li>\n' +
            '\t\t<li>презентация (при наличии);</li>\n' +
            '\t\t<li>сертификаты.</li>\n' +
            '\t</ul>\n' +
            '\tКроме того, после первичного рассмотрения нам будут необходимы образцы вашей продукции.<br>\n' +
            '\t&nbsp;&nbsp;<br>\n' +
            '\tПредлагаем вам направить предложение по адресу <a href="mailto:a.tigran@yans.kz">a.tigran@yans.kz</a>, либо обратится к сотруднику call-центра.<br/>',
        };
    }

    render() {
        const {text} = this.state;
        return (
            <Container>
                <Title>Поставщикам</Title>

                <HTML
                    containerStyle={{
                        paddingTop: 16,
                    }}
                    baseFontStyle={{
                        fontFamily: 'CoreRhino45Regular',
                        color: '#231f20',
                        fontSize: 15,
                        lineHeight: 19,
                    }}
                    onLinkPress={(evt, href) => {Linking.openURL(href);}}
                    html={text}/>

            </Container>
        )
    }
}

export default PageScreen;
