import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';

import basketActions from '../../actions/Basket';
import ordersActions from '../../actions/Orders';
import loginActions from '../../actions/Login';
import NavigationService from '../../NavigationService';

import {TouchableOpacity} from 'react-native';

import {Loader, StartBackground, StartTitle, Input} from '../../components/';
import {Alert} from 'react-native';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

import {Button, ButtonGroup} from 'react-native-elements';

const Container = styled.View`
    flex: 1;
`;

const ScrollView = styled.ScrollView`
  flex: 1;
`;

const FormInnerView = styled.View`
  alignItems: stretch; 
  paddingLeft: 32;
  paddingRight: 32; 
`;

const FormValidationMessage = styled.Text`
  width: 100%;
  fontSize: 12;
  lineHeight: 16;
  color: ${colors.RED};
`;

const FieldView = styled.View`
  width: 100%;
  marginBottom: 4;
`;

const RowView = styled.View`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 50;
`;

const ButtonView = styled.View`
  marginBottom: 32;
`;

const Link = styled.Text`
  fontSize: 15;
  lineHeight: 15;
  color: ${colors.BLUE_8};
  fontFamily: 'CoreRhino65Bold';
`;

const SmallLink = styled.Text`
  fontSize: 12;
  lineHeight: 15;
  color: ${colors.GREY_2};
  fontFamily: 'CoreRhino45Regular';
`;

class Login extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            phone: '',
            email: '',
            password: '',
            phoneError: false,
            emailError: false,
            passwordError: false,
        };
    }

    renderInput = () => {
        if (this.state.selectedIndex === 0)
            return <FieldView>
                <Input
                    label="E-mail"
                    keyboardType={'email-pad'}
                    underlineColorAndroid='transparent'
                    onChangeText={(email) => this.setState({email: email.trim()})}
                    onBlur={() => {this.setState({emailError: !validator.isEmail(this.state.email)});}}
                    error={this.state.emailError}
                    value={this.state.email}
                    errorText="Пожалуйста, введите корректный E-mail"
                />
            </FieldView>;
        return <FieldView>
            <Input
                label="Телефон"
                type="tel"
                keyboardType={'phone-pad'}
                underlineColorAndroid='transparent'
                onChangeText={(phone) => this.setState({phone: phone.trim()})}
                onBlur={() => {this.setState({phoneError: !validator.isMobilePhone(this.state.phone.replace(/[^0-9.]/g, ''))});}}
                error={this.state.phoneError}
                value={this.state.phone}
                errorText="Пожалуйста, введите номер телефона"/>
        </FieldView>;
    };

    updateIndex = (selectedIndex) => {
        this.setState({selectedIndex});
    };

    validateForm = () => {
        let phone = this.state.phone.replace(/[^0-9.]/g, '');
        this.setState({
            emailError: !validator.isEmail(this.state.email),
            phoneError: !validator.isMobilePhone(phone),
            passwordError: validator.isEmpty(this.state.password),
        });

        if (!this.state.passwordError && (this.state.selectedIndex === 0 && !this.state.emailError || this.state.selectedIndex === 1 && !this.state.phoneError)) {
            this.submitForm();
        }
    };

    submitForm = () => {
        let {email, phone, password} = this.state;

        this.props.loginUser(email, phone, password).then(() => {
            if (!this.getUpdatedProps().error) {
                this.props.listFetchBasket();
                this.props.listFetchOrders();
                NavigationService.navigate('Main');
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        });
    };

    getUpdatedProps = () => {
        return this.props.login;
    };

    render() {
        const {loginHasError} = this.props;
        const {error, loading, message} = this.props.login;
        const buttons = ['По E-mail', 'По Номеру тел'];
        const {selectedIndex} = this.state;

        return (
            <Container>
                <StartBackground/>
                <ScrollView>
                    <Loader loading={loading}/>

                    <FormInnerView>

                        <StartTitle title="Авторизация"/>

                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={selectedIndex}
                            buttons={buttons}
                            containerStyle={{
                                width: '100%',
                            }}
                        />

                        {this.renderInput()}

                        <Input label="Пароль"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(password) => this.setState({password: password.trim()})}
                               onBlur={() => {this.setState({passwordError: validator.isEmpty(this.state.password)});}}
                               error={this.state.passwordError}
                               value={this.state.password}
                               errorText="Пожалуйста, введите пароль"/>

                        <ButtonView>
                            <Button title="Войти" onPress={this.validateForm}/>
                        </ButtonView>
                        <RowView>
                            <TouchableOpacity onPress={() => {NavigationService.navigate('Register');}}>
                                <Link>Регистрация</Link>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {NavigationService.navigate('PasswordForgot');}}>
                                <SmallLink>Забыли пароль?</SmallLink>
                            </TouchableOpacity>
                        </RowView>
                    </FormInnerView>
                </ScrollView>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    listFetchBasket: () => dispatch(basketActions.listFetchBasket()),
    listFetchOrders: () => dispatch(ordersActions.listFetchOrders()),
    loginUser: (email, phone, password) => dispatch(loginActions.loginUser(email, phone, password)),
    loginHasError: (bool, text) => dispatch(loginActions.loginHasError(bool, text)),
});

const mapStateToProps = (state) => ({
    login: state.login,
    token: state.token,
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
