import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';

import passwordActions from '../../actions/Password';
import NavigationService from '../../NavigationService';

import {TouchableOpacity} from 'react-native';

import {Loader, StartBackground, StartTitle, Input} from '../../components/';
import {Alert} from 'react-native';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

import {Button, ButtonGroup} from 'react-native-elements';

const ScrollView = styled.ScrollView`
  flex: 1;
`;

const FormInnerView = styled.View`
  alignItems: stretch; 
  paddingLeft: 32;
  paddingRight: 32; 
`;

const FormValidationMessage = styled.Text`
  width: 100%;
  fontSize: 12;
  lineHeight: 16;
  color: ${colors.RED};
`;

const FieldView = styled.View`
  width: 100%;
  marginBottom: 4;
`;

const RowView = styled.View`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 50;
`;

const ButtonView = styled.View`
  marginBottom: 32;
`;

const Link = styled.Text`
  fontSize: 15;
  lineHeight: 15;
  color: ${colors.BLUE_8};
  fontFamily: 'CoreRhino65Bold';
`;

class PasswordForgot extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            phone: '',
            email: '',
            phoneError: false,
            emailError: false,
        };
    }

    renderInput = () => {
        if (this.state.selectedIndex === 0)
            return <FieldView>
                <Input
                    label="E-mail"
                    keyboardType={'email-pad'}
                    underlineColorAndroid='transparent'
                    onChangeText={(email) => this.setState({email: email.trim()})}
                    onBlur={() => {this.setState({emailError: !validator.isEmail(this.state.email)});}}
                    error={this.state.emailError}
                    value={this.state.email}
                    errorText="Пожалуйста, введите корректный E-mail"
                />
            </FieldView>;
        return <FieldView>
            <Input
                label="Телефон"
                keyboardType={'phone-pad'}
                underlineColorAndroid='transparent'
                onChangeText={(phone) => this.setState({phone: phone.trim()})}
                onBlur={() => {this.setState({phoneError: !validator.isMobilePhone(this.state.phone)});}}
                error={this.state.phoneError}
                value={this.state.phone}
                errorText="Пожалуйста, введите номер телефона"/>
        </FieldView>;
    };

    updateIndex = (selectedIndex) => {
        this.setState({selectedIndex});
    };

    validateForm = () => {
        let phone = this.state.phone.replace(/[^0-9.]/g, '');
        this.setState({
            emailError: !validator.isEmail(this.state.email) || this.state.email === '',
            phoneError: !validator.isMobilePhone(phone) || phone === '',
        }, () => {
            if (this.state.selectedIndex === 0 && this.state.emailError === false || this.state.selectedIndex === 1 && this.state.phoneError === false) {
                this.submitForm();
            }
        });
    };

    getUpdatedProps = () => {
        return {error, message} = this.props.password;
    };

    submitForm = () => {
        let {email, phone} = this.state;

        this.props.fetchPasswordForgotSend(email, phone).then(() => {
            if (!this.getUpdatedProps().error) {
                NavigationService.navigate('PasswordCheck');
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string')  Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        });
    };

    render() {
        const {passwordHasError} = this.props;
        const {error, loading, message} = this.props.password;
        const buttons = ['По E-mail', 'По Номеру тел'];
        const {selectedIndex} = this.state;

        return (
            <ScrollView>
                <Loader loading={loading}/>

                <StartBackground/>

                <FormInnerView>
                    <StartTitle title="Восстановление пароля"/>

                    <ButtonGroup
                        onPress={this.updateIndex}
                        selectedIndex={selectedIndex}
                        buttons={buttons}
                        containerStyle={{
                            width: '100%',
                        }}
                    />

                    {this.renderInput()}

                    <ButtonView>
                        <Button title="Восстановить" onPress={this.validateForm}/>
                    </ButtonView>
                    <RowView>
                        <TouchableOpacity onPress={() => {NavigationService.navigate('Auth');}}>
                            <Link>Авторизация</Link>
                        </TouchableOpacity>
                    </RowView>
                </FormInnerView>
            </ScrollView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchPasswordForgotSend: (email, phone) => dispatch(passwordActions.fetchPasswordForgotSend(email, phone)),
    passwordHasError: (bool, text) => dispatch(passwordActions.passwordHasError(bool, text)),
});

const mapStateToProps = (state) => ({
    password: state.password,
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordForgot);
