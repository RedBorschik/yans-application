import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';
import registerActions from '../../actions/Register';
import NavigationService from '../../NavigationService';

import {TouchableOpacity} from 'react-native';
import {Loader, StartBackground, StartTitle, Input} from '../../components/';
import {Alert} from 'react-native';

import styled from 'styled-components/native';
import {colors, theme} from '../../utils/theme';

import {Button} from 'react-native-elements';

const ScrollView = styled.ScrollView`
  flex: 1;
`;

const FormInnerView = styled.View`
  alignItems: stretch; 
  paddingLeft: 32;
  paddingRight: 32; 
`;

const FieldView = styled.View`
  width: 100%;
  marginBottom: 4;
`;

const RowView = styled.View`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 50;
`;

const ButtonView = styled.View`
  marginBottom: 32;
`;

const Link = styled.Text`
  fontSize: 15;
  lineHeight: 15;
  color: ${colors.BLUE_8};
  fontFamily: 'CoreRhino65Bold';
`;

class Register extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirm_password: '',
            emailError: false,
            passwordError: false,
            passwordConfirmError: false,
        };
    }

    validateForm = () => {
        let checkEmail = !validator.isEmail(this.state.email);
        let checkPassword = validator.isEmpty(this.state.password) || this.state.password.length < 6;
        let checkPasswordConfirm = this.state.password !== this.state.confirm_password || this.state.confirm_password === '';

        this.setState({
            emailError: checkEmail,
            passwordError: checkPassword,
            passwordConfirmError: checkPasswordConfirm,
        });

        if (!checkEmail && !checkPassword && !checkPasswordConfirm) {
            this.submitForm();
        }
    };

    getUpdatedProps = () => {
        return {error, message} = this.props.register;
    };

    submitForm = () => {
        let {email, password, confirm_password} = this.state;

        this.props.registerUser(email, password, confirm_password).then(() => {
            if (!this.getUpdatedProps().error) {
                NavigationService.navigate('Main');
            } else {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string')  Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                );
            }
        });
    };

    render() {
        const {registerHasError} = this.props;
        const {error, loading, message} = this.props.register;

        return (
            <ScrollView>
                <Loader loading={loading}/>

                <StartBackground/>

                <FormInnerView style={{marginBottom: 20}}>
                    <StartTitle title="Регистрация"/>

                    <FieldView>
                        <Input
                            keyboardType={'email-pad'}
                            label="E-mail"
                            underlineColorAndroid='transparent'
                            onChangeText={(email) => this.setState({email: email.trim()})}
                            onBlur={() => {this.setState({emailError: !validator.isEmail(this.state.email)});}}
                            error={this.state.emailError}
                            value={this.state.email}
                            errorText="Пожалуйста, введите корректный E-mail"/>
                    </FieldView>

                    <FieldView>
                        <Input label="Пароль"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(password) => this.setState({password: password.trim()})}
                               onBlur={() => {this.setState({passwordError: validator.isEmpty(this.state.password)});}}
                               error={this.state.passwordError}
                               value={this.state.password}
                               errorText="'Пожалуйста, введите пароль'"/>
                    </FieldView>

                    <FieldView>
                        <Input label="Подтверждение пароля"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(confirm_password) => this.setState({confirm_password: confirm_password.trim()})}
                               onBlur={() => {this.setState({passwordConfirmError: validator.isEmpty(this.state.confirm_password)});}}
                               error={this.state.passwordConfirmError}
                               value={this.state.confirm_password}
                               errorText="Пожалуйста, подтвердите пароль"/>
                    </FieldView>

                    <ButtonView>
                        <Button title="Зарегистрироваться" onPress={this.validateForm}/>
                    </ButtonView>

                    <RowView>
                        <TouchableOpacity onPress={() => {NavigationService.navigate('Auth');}}>
                            <Link>Авторизация</Link>
                        </TouchableOpacity>
                    </RowView>
                </FormInnerView>
            </ScrollView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    registerUser: (email, password, confirm_password) => dispatch(registerActions.registerUser(email, password, confirm_password)),
    registerHasError: (bool, text) => dispatch(registerActions.registerHasError(bool, text)),
});

const mapStateToProps = (state) => ({
    register: state.register,
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
