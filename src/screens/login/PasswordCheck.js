import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';

import passwordActions from '../../actions/Password';
import NavigationService from '../../NavigationService';

import {TouchableOpacity} from 'react-native';

import {Loader, StartBackground, StartTitle, Input} from '../../components/';
import {Alert} from 'react-native';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

import {Button} from 'react-native-elements';

const ScrollView = styled.ScrollView`
  flex: 1;
`;

const FormInnerView = styled.View`
  alignItems: stretch; 
  paddingLeft: 32;
  paddingRight: 32; 
`;

const FormValidationMessage = styled.Text`
  width: 100%;
  fontSize: 12;
  lineHeight: 16;
  color: ${colors.RED};
`;

const FieldView = styled.View`
  width: 100%;
  marginBottom: 4;
`;

const RowView = styled.View`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 50;
`;

const ButtonView = styled.View`
  marginBottom: 32;
`;

const ErrorView = styled.View`
    marginVertical: 16;
`;

const MainText = styled.Text`
     color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Link = styled.Text`
  fontSize: 15;
  lineHeight: 19;
  color: ${colors.BLUE_8};
  fontFamily: 'CoreRhino65Bold';
  paddingTop: 4;
`;

class PasswordForgot extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            passwordError: false,
            send: false,
        };
    }

    validateForm = () => {
        this.setState({
            passwordError: this.state.password.length !== 6,
        });

        if (this.state.password.length === 6) {
            this.submitForm();
        }
    };

    getUpdatedProps = () => {
        return {error, message} = this.props.password;
    };

    submitForm = () => {
        let {password} = this.state;

        this.setState({send: true});

        this.props.fetchPasswordCheck(password).then(() => {
            if (!this.getUpdatedProps().error) {
                this.setState({send: false});
                NavigationService.navigate('PasswordChange');
            }
        });
    };

    render() {
        const {error, loading} = this.props.password;
        const {password, passwordError, send} = this.state;

        return (
            <ScrollView>
                <Loader loading={loading}/>

                <StartBackground/>

                <FormInnerView>
                    <StartTitle title="Смена пароля"/>

                    <Input
                        label="Одноразовый пароль"
                        keyboardType={'number-pad'}
                        underlineColorAndroid='transparent'
                        onChangeText={(value) => this.setState({password: value.trim()})}
                        onBlur={() => {this.setState({passwordError: password.length !== 6});}}
                        error={passwordError}
                        value={password}
                        errorText="Пожалуйста, введите одноразовый пароль"/>

                    {error === true && send === true && loading === false ? <ErrorView>
                        <MainText>Введен некорректный код. </MainText>
                        <TouchableOpacity onPress={() => {this.setState({send: false});NavigationService.navigate('PasswordForgot');}}>
                            <Link>Вернуться на восстановление пароля</Link>
                        </TouchableOpacity>
                    </ErrorView> : null}

                    <ButtonView>
                        <Button title="Изменить пароль" onPress={this.validateForm}/>
                    </ButtonView>
                </FormInnerView>
            </ScrollView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchPasswordCheck: (password) => dispatch(passwordActions.fetchPasswordCheck(password)),
});

const mapStateToProps = (state) => ({
    password: state.password,
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordForgot);
