import React, {Component} from 'react';
import {connect} from 'react-redux';
import validator from 'validator';

import passwordActions from '../../actions/Password';
import NavigationService from '../../NavigationService';

import {TouchableOpacity} from 'react-native';

import {Loader, StartBackground, StartTitle, Input} from '../../components/';
import {Alert} from 'react-native';

import styled from 'styled-components/native';
import {colors} from '../../utils/theme';

import {Button} from 'react-native-elements';

const ScrollView = styled.ScrollView`
  flex: 1;
`;

const FormInnerView = styled.View`
  alignItems: stretch; 
  paddingLeft: 32;
  paddingRight: 32; 
`;

const FormValidationMessage = styled.Text`
  width: 100%;
  fontSize: 12;
  lineHeight: 16;
  color: ${colors.RED};
`;

const FieldView = styled.View`
  width: 100%;
  marginBottom: 4;
`;

const RowView = styled.View`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 50;
`;

const ButtonView = styled.View`
  marginBottom: 32;
`;

const ErrorView = styled.View`
    marginVertical: 16;
`;

const MainText = styled.Text`
     color: #231f20;
    fontSize: 15;
    lineHeight: 19; 
    fontFamily: CoreRhino45Regular;
`;

const Link = styled.Text`
  fontSize: 15;
  lineHeight: 19;
  color: ${colors.BLUE_8};
  fontFamily: 'CoreRhino65Bold';
  paddingTop: 4;
`;

class PasswordForgot extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            confirm_password: '',
            passwordError: false,
            passwordConfirmError: false,
            send: false,
        };
    }

    validateForm = () => {
        let checkPassword = validator.isEmpty(this.state.password) || this.props.password.length < 6;
        let checkPasswordConfirm = this.state.password !== this.state.confirm_password || this.state.confirm_password === '';

        this.setState({
            passwordError: checkPassword,
            passwordConfirmError: checkPasswordConfirm,
        });

        if (!checkPassword && !checkPasswordConfirm) {
            this.submitForm();
        }
    };

    getUpdatedProps = () => {
        return {error, message} = this.props.password;
    };

    submitForm = () => {
        let {password, confirm_password} = this.state;

        this.setState({send: true});

        this.props.fetchPasswordChange(password, confirm_password).then(() => {
            if (this.getUpdatedProps().error !== true) {
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    '',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK', onPress: () => {NavigationService.navigate('Auth');}},
                    ],
                    {cancelable: false},
                );
                //NavigationService.navigate('PasswordChange');
            } else {
                this.setState({send: false});
                if (this.getUpdatedProps().message && typeof this.getUpdatedProps().message === 'string') Alert.alert(
                    'Ошибка',
                    this.getUpdatedProps().message,
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            }
        });
    };

    render() {
        const {error, loading} = this.props.password;
        const {password, passwordError, send} = this.state;

        return (
            <ScrollView>
                <Loader loading={loading}/>

                <StartBackground/>

                <FormInnerView>
                    <StartTitle title="Смена пароля"/>

                    <FieldView>
                        <Input label="Пароль"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(password) => this.setState({password: password.trim()})}
                               onBlur={() => {this.setState({passwordError: validator.isEmpty(this.state.password)});}}
                               error={this.state.passwordError}
                               value={this.state.password}
                               errorText="'Пожалуйста, введите пароль'"/>
                    </FieldView>

                    <FieldView>
                        <Input label="Подтверждение пароля"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(confirm_password) => this.setState({confirm_password: confirm_password.trim()})}
                               onBlur={() => {this.setState({passwordConfirmError: validator.isEmpty(this.state.confirm_password)});}}
                               error={this.state.passwordConfirmError}
                               value={this.state.confirm_password}
                               errorText="Пожалуйста, подтвердите пароль"/>
                    </FieldView>

                    <ButtonView>
                        <Button title="Изменить пароль" onPress={this.validateForm}/>
                    </ButtonView>
                </FormInnerView>
            </ScrollView>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchPasswordChange: (password, confirm_password) => dispatch(passwordActions.fetchPasswordChange(password, confirm_password)),
    passwordHasError: (bool, text) => dispatch(passwordActions.passwordHasError(bool, text)),
});

const mapStateToProps = (state) => ({
    password: state.password,
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordForgot);
