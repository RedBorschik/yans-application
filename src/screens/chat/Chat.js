import React from 'react';
import {connect} from 'react-redux';
import {Platform, View} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import ImagePicker from 'react-native-image-picker';
import CustomActions from '../../components/chat/CustomActions';
import CustomVoice from '../../components/chat/CustomVoice';

import {Alert} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import accountActions from '../../actions/Account';
import NavigationService from '../../NavigationService';
import {LoaderPage, Loader} from '../../components/';
import Preloader from '../../components/helpers/Preloader';

import {api} from '../../utils/api';

import {
    renderBubble,
    renderCustomActions,
    renderCustomView,
    renderLoadEarlier,
    renderMessageText,
    renderMessageImage,
    renderSystemMessage,
    renderTime,
    renderSend,
} from '../../components/chat/chatConfig';

import DocumentPicker from 'react-native-document-picker';
import axios from 'axios';
import moment from 'moment';
import 'moment';
import 'moment/locale/ru';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clientId: -1,
            messages: [],
            limit: 1,
            offset: 39124,
            step: 0,
            isFileLoading: false,
            isLoadingEarlier: false,
            isActionsVisible: false,
            isVoiceVisible: false,
            hasConnect: false,
            isRendered: false,
        };
    }

    clientId = 21927871;
    URL = api.ws_url;
    fileUploadURL = api.file_upload_url;
    token = this.props.hash;
    device_token = this.props.device_token;

    componentDidMount() {
        if (this.props.profile && this.props.profile.phone && this.props.profile.phone !== '') {
            this.checkDataProfile();
        } else {
            this.props.getFetchProfile().then(() => {
                this.checkDataProfile();
            });
        }
        this.setState({clientId: this.clientId, isRendered: true});
    };

    componentDidUpdate(prevProps) {
        if (this.props.connected !== prevProps.connected) {
            if (this.props.connected) {
                console.log('CONNECTED');
                this.connectSocket();
            } else {
                console.log('DISCONNECTED');
                this.setState({hasConnect: false, messages: []}, () => {
                    this.ws.close();
                });
            }
        }
    }

    componentWillUnmount() {
        console.log('ЗАКРЫТИЕ СОКЕТА');
        this.ws.close();
    }

    checkDataProfile = () => {
        if (!this.props.profile.phone || this.props.profile.phone === '') {
            Alert.alert(
                '',
                'Для использования онлайн-чата необходимо добавить номер телефона в вашем профиле',
                [
                    {text: 'Перейти в профиль', onPress: () => {NavigationService.navigate('Profile');}},
                ],
                {cancelable: false},
            );
        } else {
            this.connectSocket();
        }
    };

    connectSocket = () => {
        console.log(this.URL + '?token=' + this.token + '&client_id=' + this.clientId);

        this.ws = new WebSocket(this.URL + '?token=' + this.token + '&client_id=' + this.clientId);

        this.ws.onopen = () => {
            console.log('===WEBSOCKET CONNECTED===');
            this.setState({hasConnect: true});
        };

        this.ws.onerror = (error) => {
            console.log('===WEBSOCKET ERROR=== ' + error.message);
        };

        this.ws.onmessage = (evt) => {
            console.log('====== Msg ======');
            this.onMessage(evt);
        };

        this.ws.onclose = () => {
            console.log('===WEBSOCKET DISCONNECTED===');
            if (this.state.isRendered && this.props.connected) {
                this.setState({hasConnect: false, messages: []}, () => {
                    this.connectSocket()
                });
            }
        };
    };

    onMessage = (evt) => {
        const data = JSON.parse(evt.data);
        //console.log(data);

        if (data.type === 'message') {
            let messages = [];

            if (Array.isArray(data.message)) {
                for (let i = 0; i < data.message.length; i++) {
                    const {text, date, type_file, file, type_message} = data.message[i];

                    let os = new Date();
                    os = -os.getTimezoneOffset();
                    let d = new Date((moment(date, 'DD-MM-YYYY-HH-mm-ss') + (os * 60 * 1000)));
                    d.setSeconds(d.getSeconds() + 30);

                    messages.push({
                        _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
                        text: text,
                        createdAt: d,
                        image: type_file === 'image' ? file : undefined,
                        doc: type_file !== null && type_file.match(/^(doc|docx|xls|xlsx|ppt|pptx|pdf)$/) ? file : undefined,
                        video: type_file === 'video' ? file : undefined,
                        audio: type_file === 'audio' ? file : undefined,
                        user: {
                            _id: type_message === 'from_client' ? this.state.clientId : 0,
                        },
                    });
                }
            } else {
                if (data.message.type_message === 'to_client' || data.message.type_message === 'autoreply') {
                    const {text, date, type_file, file, type_message} = data.message;

                    let os = new Date();
                    os = -os.getTimezoneOffset();
                    let d = new Date((moment(date, 'DD-MM-YYYY-HH-mm-ss') + (os * 60 * 1000)));
                    d.setSeconds(d.getSeconds() + 30);

                    messages.push({
                        _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
                        text: text,
                        createdAt: d,
                        image: type_file === 'image' ? file : undefined,
                        doc: type_file !== null && type_file.match(/^(doc|docx|xls|xlsx|ppt|pptx|pdf)$/) ? file : undefined,
                        video: type_file === 'video' ? file : undefined,
                        audio: type_file === 'audio' ? file : undefined,
                        user: {
                            _id: type_message === 'from_client' ? this.state.clientId : 0,
                        },
                    });
                }
            }

            this.setState(prevState => ({messages: [...messages, ...prevState.messages]}));
        } else if (data.type === 'system') {
            // console.log(data);
        }
    };

    onSend = (messages = []) => {
        const step = this.state.step + 1;

        this.setState(previousState => {
            let mess = messages[0];
            const sentMessages = [{...mess}];
            console.log('cur mess', mess);
            return {
                messages: GiftedChat.append(previousState.messages, sentMessages),
                step,
            };
        });

        const m = {
            'method': 'sendMessage',
            'message': messages[0].text,
            'file': `${messages[0].file}`,
        };

        this.ws.send(JSON.stringify(m));
        console.log(JSON.stringify(m));
    };

    onFileSend = async (messages = [], src, type) => {
        const HEADER = {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        };

        let formData = new FormData();
        formData.append('hash', this.token);
        formData.append('device_token', this.device_token);
        formData.append('language', 'ru');
        formData.append('f', {
            uri: src,
            type: type,
            name: `testFile.${type.slice((type.lastIndexOf('/') - 1 >>> 0) + 2)}`,
        });

        return axios.post(this.fileUploadURL, formData, HEADER).then(res => {
            console.log(res.data.result);
            messages[0].file = res.data.result;
            this.onSend(messages);
            this.setState({isActionsVisible: false, isFileLoading: false, isVoiceVisible: false});
        }).catch(error => {
            console.log(error);
            this.setState({isActionsVisible: false, isFileLoading: false, isVoiceVisible: false});
        });

        // Вместо аксиоса можно использовать стандартный фетч
        // try {
        //     const res = await fetch(this.fileUploadURL, {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'multipart/form-data',
        //         },
        //         body: formData,
        //     });
        //     const json = await res.json();
        //     console.log(json);
        //     messages[0].file = json.result;
        //     this.onSend(messages);
        //     this.setState({isActionsVisible: false, isFileLoading: false, isVoiceVisible: false});
        // } catch (err) {
        //     console.log(err);
        //     this.setState({isActionsVisible: false, isFileLoading: false, isVoiceVisible: false});
        // }
    };

    onPhotoPress(onFileSend) {
        ImagePicker.launchCamera({storageOptions: {skipBackup: true}}, (response) => {
            this.setState({isFileLoading: true});

            if (!response.didCancel && !response.error) {
                const source = response.uri; //.replace("content://", "");
                console.log(source);

                const message = [{
                    text: 'Фотография',
                    user: {_id: this.state.clientId},
                    createdAt: moment(new Date(), 'DD-MM-YYYY-HH-mm-ss'),
                    _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
                    image: source,
                }];

                onFileSend(message, source, response.type);
            } else if (response.didCancel) {
                this.setState({isActionsVisible: false, isFileLoading: false});
            }
        });
    }

    onGalleryPress(onFileSend) {
        ImagePicker.launchImageLibrary({storageOptions: {skipBackup: true}}, (response) => {
            this.setState({isFileLoading: true});

            if (!response.didCancel && !response.error) {
                const source = response.uri; //.replace("content://", "");

                const message = [{
                    text: 'Картинка',
                    user: {_id: this.state.clientId},
                    createdAt: moment(new Date(), 'DD-MM-YYYY-HH-mm-ss'),
                    _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
                    image: source,
                }];

                onFileSend(message, source, response.type);
            } else if (response.didCancel) {
                this.setState({isActionsVisible: false, isFileLoading: false});
            }
        });
    }

    async onDocPress(onFileSend) {
        try {
            const res = await DocumentPicker.pick();
            this.setState({isFileLoading: true});
            console.log(res.uri);

            const message = [{
                text: 'Документ',
                user: {_id: this.state.clientId},
                createdAt: moment(new Date(), 'DD-MM-YYYY-HH-mm-ss'),
                _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
                doc1: res.uri //.replace("content://", ""),
            }];

            onFileSend(message, res.uri, res.type);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                this.setState({isActionsVisible: false, isFileLoading: false});
            } else {
                this.setState({isActionsVisible: false, isFileLoading: false});
                throw err;
            }
        }

    };

    onAudioPress(onFileSend) {
        this.setState({isFileLoading: true});
        console.log('ON AUDIO PRESS');

        const source = Platform.select({
            ios: 'file://testFile.m4a',
            android: 'file://sdcard/testFile.m4a',
        }).replace('file://', 'file:///');

        const type = Platform.select({
            ios: 'audio/m4a',
            android: 'audio/m4a',
        });

        const message = [{
            text: 'Голосовое сообщение',
            user: {_id: this.state.clientId},
            createdAt: moment(new Date(), 'DD-MM-YYYY-HH-mm-ss'),
            _id: Math.round(Math.random() * 1000000) + Math.round(Math.random() * 1000000),
            audio: source,
        }];

        onFileSend(message, source, type);
    }

    onLongPress(context, message) {
        console.log(context, message);

        // Событие для длительного нажатия на сообщение.
        // Требует доработки со стороны сервера.

        // const options = ['Удалить сообщение', 'Отмена'];
        // const cancelButtonIndex = options.length - 1;
        // context.actionSheet().showActionSheetWithOptions({
        //     options,
        //     cancelButtonIndex
        // }, (buttonIndex) => {
        //     switch (buttonIndex) {
        //         case 0:
        //             // Your delete logic
        //             break;
        //         case 1:
        //             break;
        //     }
        // });
    }

    render() {
        const {isRendered, hasConnect} = this.state;
        const {connected} = this.props;
        return (
            <View style={{flex: 1}}>
                <NavigationEvents onWillFocus={() => {if (isRendered === true && hasConnect === false) {this.checkDataProfile();}}}/>
                {
                    this.state.isActionsVisible &&
                    <CustomActions onClosePress={() => this.setState({isActionsVisible: false})}
                                   onPhotoPress={() => this.onPhotoPress(this.onFileSend)}
                                   onGalleryPress={() => this.onGalleryPress(this.onFileSend)}
                                   onDocPress={() => this.onDocPress(this.onFileSend)}
                                   isFileLoading={this.state.isFileLoading}
                    />

                }
                {
                    this.state.isVoiceVisible &&
                    <CustomVoice onClosePress={() => this.setState({isVoiceVisible: false})}
                                 onAudioPress={() => this.onAudioPress(this.onFileSend)}
                                 isFileLoading={this.state.isFileLoading}
                    />

                }
                {!hasConnect ? <LoaderPage/> : null}
                {hasConnect ? <GiftedChat
                    messages={this.state.messages}
                    onSend={this.onSend}
                    onLongPress={this.onLongPress}
                    renderAvatar={null}
                    renderLoadEarlier={renderLoadEarlier}
                    renderBubble={renderBubble}
                    renderMessageText={renderMessageText}
                    renderMessageImage={renderMessageImage}
                    renderTime={renderTime}
                    renderSend={renderSend}
                    renderSystemMessage={renderSystemMessage}
                    renderCustomView={renderCustomView}
                    renderActions={
                        (props) => renderCustomActions(props,
                            () => this.setState({isActionsVisible: true}),
                            () => this.setState({isVoiceVisible: true}),
                        )
                    }
                    placeholder='Сообщение'
                    locale='ru'
                    dateFormat='LL'
                    user={{
                        _id: this.state.clientId,
                    }}
                /> : null}
            </View>
        );
    }
};

const mapDispatchToProps = (dispatch) => ({
    getFetchProfile: () => dispatch(accountActions.getFetchProfile()),
});

const mapStateToProps = (state) => ({
    hash: state.token.hash,
    device_token: state.token.device_token,
    profile: state.account.profile,
    connected: state.device.connected,
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
