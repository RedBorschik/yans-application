import {AsyncStorage} from 'react-native';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';

import reducers from './../reducers';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['login', 'register', 'password', 'account', 'catalog', 'banners', 'navigator', 'address', 'contragents', 'basket', 'orders'],
    timeout: 0
};

const cfgStore = () => {
    const middlewares = [thunk];

    if (process.env.NODE_ENV !== 'production') {
        //middlewares.push(createLogger());
    }

    const enhancer = applyMiddleware(...middlewares);
    const persistedReducer = persistReducer(persistConfig, reducers);

    const store = createStore(persistedReducer, enhancer);
    const persistor = persistStore(store);
    return {store, persistor};
};

export default cfgStore;