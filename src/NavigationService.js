import 'react-native-gesture-handler';
import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function navigate(routeName, params, key) {
    let opt = {
        routeName: routeName,
        params: params
    };
    if (key) opt.key = key;
    _navigator.dispatch(
        NavigationActions.navigate(opt)
    );
}

// add other navigation functions that you need and export them

export default {
    navigate,
    setTopLevelNavigator,
};