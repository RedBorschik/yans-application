import React, {Component} from 'react';
import {StatusBar, AsyncStorage} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import cfgStore from './store/configureStore';
import Root from './containers';

const YellowBox = require('react-native').YellowBox;
YellowBox.ignoreWarnings(['Warning: componentWill']);
YellowBox.ignoreWarnings(['<Modal']);
YellowBox.ignoreWarnings(['Possible Unhandled Promise']);

const {store, persistor} = cfgStore();

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.disableYellowBox = true;
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Root/>
                </PersistGate>
            </Provider>
        );
    }
}