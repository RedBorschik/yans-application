import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Platform, View, AsyncStorage} from 'react-native';
import firebase from 'react-native-firebase';
import type {Notification, NotificationOpen} from 'react-native-firebase';
import notifyActions from './actions/Notifications';
import NavigationService from './NavigationService';

class Notify extends Component {
    constructor(props) {
        super(props);
    }

    isJson = (text) => {
        if (typeof text !== 'string') {
            return false;
        }
        try {
            JSON.parse(text);
            return true;
        } catch (error) {
            return false;
        }
    };

    navigateByNotificationData = (data) => {
        if (typeof data !== 'object') return false;
        if (!data.type) return false;

        switch (data.type) {
            case 'order':
                if (data.id) NavigationService.navigate('OrdersDetail', {id: data.id});
                break;
            case 'product':
                if (data.id) NavigationService.navigate('Cart', {id: data.id});
                break;
            case 'news':
                if (data.id) NavigationService.navigate('NewsDetail', {id: data.id});
                break;
            case 'chat':
                NavigationService.navigate('Chat');
                break;
        }
    };

    parseClickData = (action) => {
        let text = action.replace(/&quot;/g, '"');
        if (this.isJson(text)) {
            return JSON.parse(text);
        }
        return null;
    };

    createMessage = (message) => {
        const notification = new firebase.notifications.Notification().setNotificationId(message._notificationId).
            setTitle(message.title).
            setBody(message.body).
            setData({click_action_custom: message._data.click_action_custom}).
            android.
            setPriority(firebase.notifications.Android.Priority.High).
            android.
            setChannelId(this.props.channel_name);

        firebase.notifications().displayNotification(notification);
    };

    checkClickNotification = (notificationOpen) => {
        const action = notificationOpen.action;
        const notification: Notification = notificationOpen.notification;

        if (Platform.OS === 'android') {
            firebase.notifications().removeDeliveredNotification(notification._android._notification._notificationId);
        }
        if (notification._data && notification._data.click_action_custom) {
            this.navigateByNotificationData(this.parseClickData(notification._data.click_action_custom));
        }
    };

    addFirebaseListeners = () => {
        if (this.props.channel_name === '') return false;
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => this.props.fetchSaveFmcToken(fcmToken));
        //this.messageListener = firebase.messaging().onMessage((message) => this.createMessage(message));
        this.notificationListener = firebase.notifications().onNotification((message) => this.createMessage(message));
        this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => this.checkClickNotification(notificationOpen));
        this.removeNotificationGetInitialListener = firebase.notifications().getInitialNotification().then((notificationOpen: NotificationOpen) => {
            if (notificationOpen) {
                const notification: Notification = notificationOpen.notification;
                let id = null;
                if (Platform.OS === 'android') {
                    id = notification._android._notification._notificationId;
                }
                firebase.notifications().removeDeliveredNotification(id);
                if (notification._data && notification._data.click_action_custom) {
                    AsyncStorage.getItem('notification_id').then(value => {
                        if (id !== value) {
                            //console.log('Set click action:', notification._data.click_action_custom);
                            AsyncStorage.setItem('click_action', notification._data.click_action_custom);
                            AsyncStorage.setItem('notification_id', id);
                        }
                    });
                }
            }
        });
    };

    componentDidMount() {
        this.props.checkPermission();
        this.props.createChannel().then(() => {
            this.addFirebaseListeners();
        });
    }

    componentWillUnmount() {
        if (!!this.messageListener && typeof this.messageListener === 'function') {
            this.messageListener();
        }
        if (!!this.onTokenRefreshListener && typeof this.onTokenRefreshListener === 'function') {
            this.onTokenRefreshListener();
        }
        if (!!this.notificationListener && typeof this.notificationListener === 'function') {
            this.notificationListener();
        }
        if (!!this.removeNotificationOpenedListener && typeof this.removeNotificationOpenedListener === 'function') {
            this.removeNotificationOpenedListener();
        }
        if (!!this.removeNotificationGetInitialListener && typeof this.removeNotificationGetInitialListener === 'function') {
            this.removeNotificationGetInitialListener();
        }
    }

    render() {
        return (<View/>);
    }
}

const mapDispatchToProps = (dispatch) => ({
    checkPermission: () => dispatch(notifyActions.checkPermission()),
    createChannel: () => dispatch(notifyActions.createChannel()),
    fetchSaveFmcToken: () => dispatch(notifyActions.fetchSaveFmcToken()),
});

const mapStateToProps = (state) => ({
    channel_name: state.notify.channel_name,
});

export default connect(mapStateToProps, mapDispatchToProps)(Notify);

