import 'react-native-gesture-handler';
import React from 'react';
import {Platform, Dimensions, TouchableOpacity, View, StatusBar} from 'react-native';
import {createAppContainer, createSwitchNavigator, getActiveChildNavigationOptions} from 'react-navigation';
import {createStackNavigator, StackViewTransitionConfigs} from 'react-navigation-stack';
import {createDrawerNavigator, DrawerActions} from 'react-navigation-drawer';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import CheckToken from './containers/CheckToken';
import * as Screen from './screens/';

import {CustomDrawerContent, MainTabBar, IconHamburger, IconSearch, IconBack, IconLogo, IconEmpty} from './components';
import {colors} from './utils/theme';
import {messages} from './Messages';

const Chat = createStackNavigator({
    Chat: {
        screen: Screen.ChatScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'Chat',
});

const Order = createStackNavigator({
    OrdersNew: {
        screen: Screen.OrdersNewScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersStep1: {
        screen: Screen.OrdersStep1Screen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersStep2: {
        screen: Screen.OrdersStep2Screen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersStep3: {
        screen: Screen.OrdersStep3Screen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersStep4: {
        screen: Screen.OrdersStep4Screen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersStep5: {
        screen: Screen.OrdersStep5Screen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersCreated: {
        screen: Screen.OrdersCreatedScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'OrdersNew',
});

const Cart = createStackNavigator({
    Cart: {
        screen: Screen.CartScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'Cart',
});

const Catalog = createStackNavigator({
    MenuSections: {
        screen: Screen.MenuSectionsScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    MenuSubsections: {
        screen: Screen.MenuSubsectionsScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    MenuSubsectionsSecond: {
        screen: Screen.MenuSubsectionsSecondScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Section: {
        screen: Screen.SectionScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Card: {
        screen: Screen.CardScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Search: {
        screen: Screen.SearchScreen,
        headerMode: 'none',
        transitionConfig: () => StackViewTransitionConfigs.NoAnimation,
        navigationOptions: ({navigation, screenProps}) => ({
            header: null,
        }),
        defaultNavigationOptions: {
            gesturesEnabled: false,
        },
    },
}, {
    initialRouteName: 'MenuSections',
});

const Account = createStackNavigator({
    Account: {
        screen: Screen.AccountScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Profile: {
        screen: Screen.ProfileScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    AddressList: {
        screen: Screen.AddressListScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    AddressDetail: {
        screen: Screen.AddressDetailScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    AddressNew: {
        screen: Screen.AddressNewScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    ContragentsList: {
        screen: Screen.ContragentsListScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    ContragentsDetail: {
        screen: Screen.ContragentsDetailScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    ContragentsNew: {
        screen: Screen.ContragentsNewScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersList: {
        screen: Screen.OrdersListScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    OrdersDetail: {
        screen: Screen.OrdersDetailScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Settings: {
        screen: Screen.SettingsScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'Account',
});

const Home = createStackNavigator({
    Main: {
        screen: Screen.HomeScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    News: {
        screen: Screen.NewsListScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    NewsDetail: {
        screen: Screen.NewsDetailScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    About: {
        screen: Screen.AboutScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Faq: {
        screen: Screen.FaqScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Payment: {
        screen: Screen.PaymentScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Return: {
        screen: Screen.ReturnScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Suppliers: {
        screen: Screen.SuppliersScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
    Contacts: {
        screen: Screen.ContactsScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'Main',
});

const Favorites = createStackNavigator({
    Favorites: {
        screen: Screen.FavoritesScreen,
        headerMode: 'none',
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
    initialRouteName: 'Favorites',
});

const Tabs = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({navigation}) => {
            let focusedRouteName = navigation.state.routes[navigation.state.index].routeName;
            let opt = {
                headerTitle: messages.routes[focusedRouteName] || <IconLogo/>,
                headerStyle: {
                    paddingTop: 0,
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                },
                headerRight:  <IconSearch onPress={() => {navigation.navigate('Search', {go_back_name: focusedRouteName});}}/>,
                /*headerLeft: <IconBack onPress={() => {navigation.navigate('Home');}}/>,*/
            };
            if (focusedRouteName === 'NewsDetail') {
                opt.headerLeft = <IconBack onPress={() => {navigation.pop();}}/>;
            }
            return opt;
        },
    },
    Catalog: {
        screen: Catalog,
        navigationOptions: ({navigation}) => {
            let focusedRouteName = navigation.state.routes[navigation.state.index].routeName;
            let opt = {
                headerTitle: '',
                headerStyle: {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                },
                headerRight:  <IconSearch onPress={() => {navigation.navigate('Search', {go_back_name: focusedRouteName});}}/>,
            };
            if (focusedRouteName === 'MenuSections') {
                opt.headerStyle = {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                    elevation: 0,
                };
            }
            if (focusedRouteName !== 'MenuSections') {
                if (navigation.state.routes[navigation.state.index].params && navigation.state.routes[navigation.state.index].params.go_back_name) {
                    opt.headerLeft = <IconBack onPress={() => {navigation.navigate(navigation.state.routes[navigation.state.index].params.go_back_name);}}/>;
                } else {
                    opt.headerLeft = <IconBack onPress={() => {navigation.pop();}}/>;
                }
            }
            if (navigation.state.routes[navigation.state.index].params) {
                opt.headerTitle = navigation.state.routes[navigation.state.index].params.title;
            }
            if (focusedRouteName === 'Card') {
                opt.headerTitle = <IconLogo/>;
            }
            if (focusedRouteName === 'Search') {
                opt.header = null;
                opt.headerStyle = {
                    paddingTop: 0,
                    backgroundColor: 'transparent',
                    height: 0,
                    elevation: 0,
                };
            }
            return opt;
        },
    },
    Favorites: {
        screen: Favorites,
        navigationOptions: ({navigation}) => ({
            headerTitle: 'Избранное',
        }),
    },
    Cart: {
        screen: Cart,
        navigationOptions: ({navigation}) => {
            let focusedRouteName = navigation.state.routes[navigation.state.index].routeName;

            return {
                headerTitle: null,
                headerStyle: {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56: 44,
                    elevation: 0,
                },
                headerRight:  <IconSearch onPress={() => {navigation.navigate('Search', {go_back_name: focusedRouteName});}}/>,
            };
        },
    },
    Order: {
        screen: Order,
        navigationOptions: ({navigation}) => {
            let focusedRouteName = navigation.state.routes[navigation.state.index].routeName;
            let opt = {
                headerTitle: null,
                headerStyle: {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56: 44,
                    elevation: 0,
                },
                headerRight:  <IconSearch onPress={() => {navigation.navigate('Search', {go_back_name: focusedRouteName});}}/>,
            };

            if (focusedRouteName.indexOf('OrdersStep') >= 0) {
                opt.headerTitle = messages.routes[focusedRouteName] || null;
                opt.headerRight = null;
                opt.headerTitleStyle = {
                    fontFamily: 'CoreRhino65Bold',
                    fontWeight: 'normal',
                    fontSize: Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 18 : 17,
                    textAlign: Platform.OS === 'android' ? 'left' : 'center',
                    alignSelf: 'center',
                    paddingHorizontal: Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 8 : 0,
                    marginHorizontal: 'auto',
                    width: '90%',
                };
                opt.headerLeft = <IconBack onPress={() => {navigation.pop();}}/>;
                opt.headerStyle = {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                    elevation: 0,
                };
            }

            if (focusedRouteName === 'OrdersStep1') {
                opt.headerTitle = messages.routes[focusedRouteName] || null;
                opt.headerLeft = <IconBack onPress={() => {navigation.navigate('Cart');}}/>;
            }

            if (focusedRouteName === 'OrdersCreated') {
                opt.headerTitle = messages.routes[focusedRouteName] || null;
                opt.headerLeft = <IconBack onPress={() => {navigation.navigate('OrdersList');}}/>;
                opt.headerStyle = {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                };
            }

            if (focusedRouteName === 'OrdersNew') {
                opt.headerTitle = messages.routes[focusedRouteName] || null;
                opt.headerLeft = <IconBack onPress={() => {navigation.navigate('Cart');}}/>;
                opt.headerStyle = {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                };
            }

            return opt;
        },

    },
    Account: {
        screen: Account,
        navigationOptions: ({navigation}) => {
            let focusedRouteName = navigation.state.routes[navigation.state.index].routeName;
            let opt = {
                headerTitle: messages.routes[focusedRouteName] || <IconLogo/>,
                headerLeft: <IconBack onPress={() => {navigation.pop();}}/>,
                headerRight: <IconEmpty/>,
                headerStyle: {
                    backgroundColor: colors.WHITE,
                    height: Platform.OS === 'android' ? 56 : 44,
                },
            };
            if (focusedRouteName === 'Account') {
                opt.headerLeft = <IconHamburger onPress={() => {navigation.dispatch(DrawerActions.toggleDrawer());}}/>;
            }

            return opt;
        },
    },
    Chat: {
        screen: Chat,
        navigationOptions: ({navigation}) => ({
            headerTitle: 'Онлайн-чат',
        }),
    },
}, {
    tabBarComponent: MainTabBar,
    tabBarPosition: 'bottom',
    navigationOptions: ({navigation, screenProps}) => ({
        ...getActiveChildNavigationOptions(navigation, screenProps),
    }),
});

const MainStack = createStackNavigator({
    Tabs: {
        screen: Tabs,
    },
}, {
    cardStyle: {
        backgroundColor: colors.WHITE,
    },
    headerMode: 'float',
    headerBackTitleVisible: true,
    defaultNavigationOptions: ({navigation}) => ({
        headerStyle: {
            backgroundColor: colors.WHITE,
            height: Platform.OS === 'android' ? 56 : 44,
        },
        headerTitle: <IconLogo/>,
        headerTintColor: colors.TEXT,
        headerTitleStyle: {
            fontFamily: 'CoreRhino65Bold',
            fontWeight: 'normal',
            fontSize: Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 18 : 17,
            textAlign: Platform.OS === 'android' ? 'left' : 'center',
            alignSelf: 'center',
            overflow: 'hidden',
            paddingHorizontal: Platform.OS === 'android' && Dimensions.get('window').width >= 360 ? 8 : 0,
            marginHorizontal: 'auto',
            width: Platform.OS === 'android' ? (Dimensions.get('window').width - 96) : (Dimensions.get('window').width - 52),
        },
        headerLeft: <IconHamburger onPress={() => {navigation.dispatch(DrawerActions.toggleDrawer());}}/>,
        headerRight:  <IconSearch onPress={() => {navigation.navigate('Search', {go_back_name: navigation.state.routes[navigation.state.index].routeName});}}/>,
    }),
});

const DrawerNavigator = createDrawerNavigator({
    MainStack: {
        screen: MainStack,
    },
}, {
    contentComponent: CustomDrawerContent,
    drawerWidth: Platform.OS === 'android' ? 280 : 246,
    drawerPosition: 'left',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    hideStatusBar: false,
    overlayColor: 'rgba(0,0,0,.2)',
});

const AppStack = createStackNavigator({
    DrawerNavigator: {
        screen: DrawerNavigator,
    },
}, {
    headerMode: 'none',
    navigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
});

const AuthStack = createStackNavigator({
    Auth: {
        screen: Screen.LoginScreen,
    },
    Register: {
        screen: Screen.RegisterScreen,
    },
    PasswordForgot: {
        screen: Screen.PasswordForgotScreen
    },
    PasswordCheck: {
        screen: Screen.PasswordCheckScreen
    },
    PasswordChange: {
        screen: Screen.PasswordChangeScreen
    },
});

const Navigator = createSwitchNavigator({
    AuthLoading: {screen: CheckToken},
    Auth: {screen: AuthStack},
    App: {screen: AppStack},
}, {
    initialRouteName: 'AuthLoading',
    navigationOptions: {
        tabBarVisible: false,
    },
});

export default createAppContainer(Navigator);
